/* spelling mistake correction */

UPDATE `tms_status` SET `caption` = 'Transfer Requested' WHERE `tms_status`.`sid` = 6;

UPDATE `tms_status` SET `caption` = 'Full Transit' WHERE `tms_status`.`sid` = 27;

UPDATE `tms_status` SET `caption` = 'Partial Transit' WHERE `tms_status`.`sid` = 28; 