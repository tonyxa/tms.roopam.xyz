<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();


	public function SMTPMailer($subject, $body, $to_ids = '') {
        $mail = new JPhpMailer();

        $mail->IsSMTP();
        $mail->Host = SMTPHOST;
        $mail->SMTPSecure = SMTPSECURE;
        $mail->SMTPAuth = SMTPAUTH;
        $mail->Username = SMTPUSERNAME;
        $mail->Password = SMTPPASS;
		$mail->Port = SMTPPORT;
		$emailfrom =  json_decode(Mail_FROM);

        $mail->setFrom($emailfrom->email, $emailfrom->name);

        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;

        $to_ids = explode(',', $to_ids);
        foreach ($to_ids as $id) {
            $mail->addAddress($id);
        }


        if ($mail->send()) {
            return true;
        } else {
            return false;
        }
    }
}