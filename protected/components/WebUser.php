<?php
class WebUser extends CWebUser{
   
    protected $_mainuser_id = NULL;
  

    public function getMainuser_id(){
        $myVar = Yii::app()->user->getState('mainuser_id');
        return (null!==$myVar)?$myVar:$this->_mainuser_id;
    }

    public function setMainuser_id($value){
        Yii::app()->user->setState('mainuser_id', $value);
    }
   
}


