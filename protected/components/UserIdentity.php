<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
    private $_id;
    public function authenticate() {
       $record = Users::model()->findByAttributes(array('username' => $this->username, 'status' => 0));   

        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($record->password !== md5(base64_encode($this->password)))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {

        	if(isset($record->userid) && isset($record->user_type)){
			
	            $this->_id = $record->userid;
	            $this->setState('role', $record->user_type);
	            $this->setState('firstname', $record->first_name);
	            $this->setState('logged_time', date("Y-m-d H:i:s"));
				
				$usertype = UserRoles::model()->findByPk($record->user_type);
				$this->setState('rolename', $usertype->role);

				if($record->user_type == 1)
				{
	            $this->setState('mainuser_id', $record->userid);
	            $this->setState('mainuser_role', $record->user_type);
				$this->setState('mainuser_username', $record->first_name);            
	            
				}
				/* new update login time */
                $command = Yii::app()->db->createCommand
                ('UPDATE ' . Yii::app()->db->tablePrefix . 'users set last_modified = "'.date('Y-m-d H:i:s').'"  WHERE userid = '. $record->userid);
                $num = $command->execute(); 
                $this->errorCode = self::ERROR_NONE;
			}
			else{			
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;			
			}
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
}
