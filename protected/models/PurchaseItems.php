<?php

/**
 * This is the model class for table "{{purchase_items}}".
 *
 * The followings are the available columns in table '{{purchase_items}}':
 * @property integer $id
 * @property integer $purchase_id
 * @property string $item_code
 * @property string $item_name
 * @property integer $unit
 * @property string $model_no
 * @property string $batch_no
 * @property string $make
 * @property string $serial_no
 * @property integer $qty
 * @property integer $rate
 * @property integer $tax
 * @property integer $amount
 *
 * The followings are the available model relations:
 * @property Purchase $purchase
 * @property Unit $unit0
 */
class PurchaseItems extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PurchaseItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchase_id,item_name', 'required'),
            array('item_code,tax,prev_main,prev_hrs,prev_date','safe'),                        
			array('purchase_id, unit, qty', 'numerical', 'integerOnly'=>true),
                        array('rate,tax,amount', 'numerical'),                        
			array('item_code, item_name,warranty_date, model_no, batch_no, make, serial_no', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, purchase_id,warranty_date, item_code, item_name, unit, model_no, batch_no, make, serial_no, qty, rate, tax, amount,remaining_qty,tool_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'purchase' => array(self::BELONGS_TO, 'Purchase', 'purchase_id'),
			'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
                        'toolStatus' => array(self::BELONGS_TO, 'Status', 'tool_status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'purchase_id' => 'Purchase',
			'item_code' => 'Item Code',
			'item_name' => 'Item Name',
			'unit' => 'Unit',
			'model_no' => 'Model No',
			'batch_no' => 'Batch No',
			'make' => 'Make',
			'serial_no' => 'Serial No',
			'qty' => 'Qty',
			'rate' => 'Rate',
			'tax' => 'Tax',
			'amount' => 'Amount',
                        'warranty_date' => 'Warranty Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($page ='')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('purchase_id',$this->purchase_id);
		$criteria->compare('item_code',$this->item_code,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('model_no',$this->model_no,true);
		$criteria->compare('batch_no',$this->batch_no,true);
		$criteria->compare('make',$this->make,true);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('amount',$this->amount);
                $criteria->compare('warranty_date',$this->warranty_date);
                $criteria->compare('tool_status', $this->showPage($page));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function showPage($page) {
        if ($page == 'pending') {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Pending'))
                    ->queryRow();
        } else {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
                    ->queryRow();
        }
        return $qryres['sid'];
    }
}