<?php

/**
 * This is the model class for table "{{tool_category}}".
 *
 * The followings are the available columns in table '{{tool_category}}':
 * @property integer $cat_id
 * @property string $cat_name
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class ToolCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ToolCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tool_category}}';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_name,cat_code', 'required', 'message' => 'Please Enter {attribute}.'),
			array('created_by, updated_by,parent_id', 'numerical', 'integerOnly'=>true),
			array('cat_name', 'length', 'max'=>50),
			array('cat_name,cat_code', 'unique'),
			array('updated_date,parent_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cat_id, cat_name,cat_code, created_by, created_date, updated_by, updated_date,tool_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
      'parentId'  => array(self::BELONGS_TO, 'ToolCategory', 'parent_id'),
      'toolType' => array(self::BELONGS_TO, 'Status', 'tool_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cat_id' => 'Cat',
			'cat_name' => 'Category Name',
                        'cat_code' => 'Category Code',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
                        'parent_id' =>'Parent Category',
                        'tool_type' => 'Tool Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                if(isset($this->parent_id)){
                    if($this->parent_id!=""){
                     	$res = self::model()->findByPk($this->parent_id);
	                    if(!isset($res)){
	                        $criteria->addCondition('parent_id IS NULL');
	                    }else{
	                         $criteria->compare('parent_id',$this->parent_id,true);
	                    }
                    }else{
                    	 $criteria->compare('parent_id',$this->parent_id,true);
                    }
                }else{
                     $criteria->compare('parent_id',$this->parent_id,true);
                }

		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('cat_name',$this->cat_name,true);


                $criteria->compare('cat_code',$this->cat_code,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
                $criteria->compare('tool_type',$this->tool_type);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			//'sort' => array('defaultOrder' => 'parent_id ASC ,cat_id DESC',
                    'sort' => array('defaultOrder' => 'cat_id DESC',
            ),
		));
	}
        /* getting the all categories */
        public static function allcategories(){
                $models = ToolCategory::model()->findAll(
                        array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL',
                              'order'=>'cat_name'
                             )
                        );
                $data = array();
                foreach($models as $model) {
                        $row['id'] = $model->cat_id;
                        $row['text'] = $model->cat_name; //CHtml::link($model->cat_name, array('place/view', 'id' => $model->cat_id));
                        $row['children'] = ToolCategory::getChildcategories($model->cat_id);
                        $data[] = $row;
                }
                return $data;
        }

        public static function getChildcategories($id) {
                $data = array();
                //foreach(ToolCategory::model()->findAll('parent_id = ' . $id) as $model) {
                foreach( ToolCategory::model()->findAll(
                        array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id = ' . $id,
                              'order'=>'cat_name'
                             )
                        ) as $model) {
                        $row['id'] = $model->cat_id;
                        $row['text'] = $model->cat_name; //CHtml::link($model->cat_name, array('place/view', 'id' => $model->cat_id));
                        $row['children'] = ToolCategory::getChildcategories($model->cat_id);
                        $data[] = $row;
                }
                return $data;
        }

         public static function visualTree($catTree, $level) {
            $res = array();
            foreach ($catTree as $item) {
                $res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['text'];
                if (isset($item['children'])) {
                    $res_iter = self::visualTree($item['children'], $level + 1);
                    foreach ($res_iter as $key => $val) {
                        $res[$key] = $val;
                    }
                }
            }
            return $res;
        }


      public function getParentname($id){
			    $model       = ToolCategory::model()->findByPk($id);
				   if(!is_null($model)){
		           $parent      = $model->parent_id;
		           if(is_null($parent)){
								  return $model->cat_name;
		           }else{
							    $model1 = ToolCategory::model()->findByPk($parent);
			           return $model1->cat_name;
		           }
				   }else{
						return '<b style="color:red">NONE</b>';
				   }
		        //   echo "here";
		   }

			public static function getCategoryname($id) {

	        $model = ToolCategory::model()->findByPk($id);
         if(is_null($model)){
             return '<b style="color:red">NONE</b>';
         }else{
             return $model->cat_name;
         }
      }

			public static function getmainParentname($id){

							$model = ToolCategory::model()->findByPk($id);
							if(!is_null($model)){
								if(!is_null($model->parent_id)){
									$submodel = ToolCategory::model()->findByPk($model->parent_id);

									 if(!is_null($submodel->parent_id)){
										 $subchildmodel = ToolCategory::model()->findByPk($submodel->parent_id);
										 return $subchildmodel->cat_name;
									 }else{
										 	return $submodel->cat_name;
									 }
                 }else{
									 return $model->cat_name;
								 }
								}else{
										return '<b style="color:red">NONE</b>';
								}

			}

			public static function getsubCategoryname($id) {


			    $model = ToolCategory::model()->findByPk($id);
          if(!is_null($model)){
						if(!is_null($model->parent_id)){

              $submodel = ToolCategory::model()->findByPk($model->parent_id);
							if(!is_null($submodel->parent_id)){
								return $submodel->cat_name;
							}else{
								return $model->cat_name;
							}
            }else{
							return '<b style="color:red">NONE</b>';
						}
					}else{
						return '<b style="color:red">NONE</b>';
					}


			}

			 public static function getsubchildCategoryname($id){

			         $model = ToolCategory::model()->findByPk($id);
							 if(!is_null($model)){
								 if(!is_null($model->parent_id)){
									 $submodel = ToolCategory::model()->findByPk($model->parent_id);
                      if(!is_null($submodel->parent_id)){
											  $subchildmodel = ToolCategory::model()->findByPk($submodel->parent_id);
											}
                   }
                 }else{
										 return $model->cat_name;
								 }


							 if(!isset($subchildmodel)){
									 return '<b style="color:red">NONE</b>';
							 }else{
									 return $model->cat_name;
							 }
			 }

}
