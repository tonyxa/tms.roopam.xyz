<?php

/**
 * This is the model class for table "{{vendors}}".
 *
 * The followings are the available columns in table '{{vendors}}':
 * @property integer $id
 * @property string $vendor_code
 * @property string $vendor_name
 * @property string $vendor_address
 * @property string $vendor_phone
 * @property string $vendor_mob
 * @property string $vendor_email
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $updatedBy
 * @property Users $createdBy
 */
class Vendors extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Vendors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vendors}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendor_code, vendor_name, vendor_email, created_by, created_date', 'required'),
			array('vendor_code, vendor_name', 'unique'),
			array('created_by, updated_by,vendor_phone, vendor_mob', 'numerical', 'integerOnly'=>true),
			array('vendor_code, vendor_name', 'length', 'max'=>50),
			array('vendor_phone, vendor_mob', 'length', 'max'=>10),
			array('vendor_email', 'length', 'max'=>40),
			array('vendor_email', 'email'),
			array('vendor_address, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vendor_code, vendor_name, vendor_address, vendor_phone, vendor_mob, vendor_email, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vendor_code' => 'Vendor Code',
			'vendor_name' => 'Vendor Name',
			'vendor_address' => 'Vendor Address',
			'vendor_phone' => 'Phone',
			'vendor_mob' => 'Mobile',
			'vendor_email' => 'Email',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vendor_code',$this->vendor_code,true);
		$criteria->compare('vendor_name',$this->vendor_name,true);
		$criteria->compare('vendor_address',$this->vendor_address,true);
		$criteria->compare('vendor_phone',$this->vendor_phone,true);
		$criteria->compare('vendor_mob',$this->vendor_mob,true);
		$criteria->compare('vendor_email',$this->vendor_email,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'id DESC',
            ),
		));
	}
}
