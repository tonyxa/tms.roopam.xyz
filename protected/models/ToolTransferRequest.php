<?php

/**
 * This is the model class for table "{{tool_transfer_request}}".
 *
 * The followings are the available columns in table '{{tool_transfer_request}}':
 * @property integer $id
 * @property string $request_no
 * @property string $request_date
 * @property string $request_to
 * @property string $request_from
 * @property string $transfer_to
 * @property integer $paid_amount
 * @property integer $rating
 * @property string $remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property ToolTransferItems[] $toolTransferItems
 * @property Users $updatedBy
 * @property Users $createdBy
 */
class ToolTransferRequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ToolTransferRequest the static model class
	 */


	 public $tool_status;
	 public $item_reqdate;
	 public $tool_id;
	 public $duration_in_days;
	 public $duration_type;
	 public $req_date;
	 public $code;
	 public $item_name;
	 public $return_date;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tool_transfer_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('request_no, request_date, created_by, created_date', 'required'),
			array('request_no,paid_amount, rating, created_by, updated_by', 'numerical', 'integerOnly'=>true),
                  // array('request_no', 'unique'),
			array('request_to, request_from, transfer_to', 'length', 'max'=>50),
			array('remarks,updated_date,transfer_date,received_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, request_no, tool_status,req_date, item_reqdate, request_date, return_date, request_to, request_from, transfer_to, paid_amount, rating, remarks,service_report,request_owner_id,ack_user_id,request_status,staff_remarks,location, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'toolTransferItems' => array(self::HAS_MANY, 'ToolTransferItems', 'tool_transfer_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			 'Requestowner' => array(self::BELONGS_TO, 'Users', 'request_owner_id'),
			 'Transfer_from'=>array(self::BELONGS_TO, 'LocationType', 'request_from'),
			 'Request_from'=>array(self::BELONGS_TO, 'Users', 'transfer_to'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
                        'requestFrom' => array(self::BELONGS_TO, 'LocationType', 'request_from'),
                        'requestTo' => array(self::BELONGS_TO, 'LocationType', 'request_to'),
                        'requestStatus' => array(self::BELONGS_TO, 'Status', 'request_status'),
                        //'tool' => array(self::BELONGS_TO, 'Tools', 'tool_id'),
                    'location0' => array(self::BELONGS_TO, 'LocationType', 'location'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                       // 'tool_id' => 'Tool',
			'request_no' => 'Request No',
			'request_date' => 'Request Date',
			'request_to' => 'Request To',
			'request_from' => 'Request From',
			'transfer_to' => 'Transfer To',
			'paid_amount' => 'Paid Amount',
			'rating' => 'Rating',
			'remarks' => 'Remarks',
                        'service_report'=> 'Service Report',
                        'location' => 'Location',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
                        'request_owner_id' => 'Request Owner',
                        'ack_user_id' => 'Acknowledged User',
                        'request_status' => 'Request Status',
                        'staff_remarks' => 'Staff Remarks',
                        'tool_status' => 'Status',
                        'tool_id' => 'Status',
                        'item_reqdate' => 'Return Date',
                        'req_date' => 'Received Date',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($page)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.


		$criteria=new CDbCriteria;
		if($page == 'site')
		{

			$criteria->addCondition("transfer_to ='site'");


		}
		if($page == 'stock')
		{

			if(Yii::app()->user->role != 1 && Yii::app()->user->role != 9){
		      $criteria->addCondition("created_by = ". Yii::app()->user->id);
		    }else{
		    	$criteria->addCondition("transfer_to ='stock'");
		    }
				$criteria->addCondition("location IS NOT NULL");
		}
		if($page == 'vendor')
		{
			$criteria->addCondition("transfer_to ='vendor'");
		}

		$criteria->compare('id',$this->id);
		$criteria->compare('request_no',$this->request_no,true);
		$criteria->compare('request_date',$this->request_date,true);
		$criteria->compare('request_to',$this->request_to,true);
		$criteria->compare('request_from',$this->request_from,true);
		$criteria->compare('transfer_to',$this->transfer_to,true);
		$criteria->compare('paid_amount',$this->paid_amount);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('remarks',$this->remarks,true);
                $criteria->compare('service_report',$this->service_report,true);
                $criteria->compare('request_owner_id',$this->request_owner_id);
                $criteria->compare('ack_user_id',$this->ack_user_id);
                $criteria->compare('request_status',$this->request_status);
                $criteria->compare('staff_remarks',$this->staff_remarks,true);
                $criteria->compare('location',$this->location);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'id DESC',
            ),
		));
	}


	public function sitesearch()
	{

		$criteria=new CDbCriteria;
		$criteria->addCondition("t.transfer_to ='site'");

		if(Yii::app()->user->role != 1 && Yii::app()->user->role != 9){
		   $criteria->addCondition("created_by = ". Yii::app()->user->id." or request_owner_id = ".Yii::app()->user->id);
		}

		$criteria->select = 't.id, t.request_no, t.request_date, t.request_to, t.request_from,t.transfer_to,t.paid_amount,t.rating,t.remarks, t.service_report, t.request_owner_id, t.ack_user_id, t.request_status, t.staff_remarks, t.location, t.created_by, t.created_date, t.updated_by, t.updated_date';
        $criteria->join = ' INNER JOIN `tms_tool_transfer_info` AS `info` ON t.id = info.tool_transfer_id';
        $criteria->group='info.tool_transfer_id';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 't.id DESC',
            ),
		));
	}

	public function requestitems()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition("t.request_status =9 AND t.received_date !=''");
		$criteria->select = 't.id, t.request_no, t.request_date, t.request_to, t.request_from,t.transfer_to,t.paid_amount,t.rating,t.remarks, t.service_report, t.request_owner_id, t.ack_user_id, t.request_status, t.staff_remarks, t.location, t.created_by, t.created_date, t.updated_by, t.updated_date, items.req_date as req_date, items.tool_id as tool_id, items.duration_in_days as duration_in_days, items.duration_type as duration_type,items.code as code, items.item_name as item_name,items.req_date as item_reqdate, t.received_date, items.return_date as return_date';
        $criteria->join = ' INNER JOIN `tms_tool_transfer_items` AS `items` ON t.id = items.tool_transfer_id';
        //$criteria->group='items.tool_transfer_id';
        //$criteria->order = 'items.id ASC';
        $sort           = new CSort;

		$sort->attributes = array(

		'return_date'=>array(
			'asc'=>'items.return_date',
			'desc'=>'items.return_date desc',
		),

		'code'=>array(
			'asc'=>'items.code',
			'desc'=>'items.code desc',
		),

		'item_name'=>array(
			'asc'=>'items.item_name',
			'desc'=>'items.item_name desc',
		),

		'*',
			   );

	   $sort->defaultOrder = 'items.return_date ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,

		));
	}

	public static function getToolstatus($id) {
		$qryres = Yii::app()->db->createCommand("SELECT tms_status.caption FROM tms_tools INNER JOIN tms_status ON tms_tools.tool_condition=tms_status.sid WHERE tms_tools.id=".$id)->queryRow();
        return $qryres['caption'];
	}


	public function getReturndate($duration, $type, $req_date) {
		$prev_date = date('Y-m-d', strtotime("+3 days"));
		if($type!= '' && $duration != '') {
			$status = $this->getStatus($type);
			$date = '';

			if($status == 'Days') {
				$date = date("Y-m-d",strtotime($req_date."+".$duration." day"));
				if(date('Y-m-d') <$date){

					if($prev_date >=$date) {
						$date ='<p style="color: yellow;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					} else {
					$date = date("Y-m-d",strtotime($req_date."+".$duration." day"));
				     }
				} else if(date('Y-m-d') == $date){
				     $date ='<p style="color: yellow;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					} else {
					//if($prev_date <=$date) {
						//$date ='<p style="color: yellow;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					//} else {
						$date ='<p style="color: red;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					//}
				}

			} else {
				$date = date("Y-m-d",strtotime($req_date."+".$duration." month"));

				if(date('Y-m-d') <$date){
					//echo"greater";
				} else {
					if($prev_date <=$date) {
						$date ='<p style="color: yellow;">'.date("Y-m-d",strtotime($req_date."+".$duration." month")).'</p>';
					} else {
						$date ='<p style="color: red;">'.date("Y-m-d",strtotime($req_date."+".$duration." month")).'</p>';
					}
				}
			}

			} else {
				$date = $req_date;
				if(date('Y-m-d') <$date){
					//echo"greater";
				} else {
					if($prev_date <=$date) {
						$date ='<p style="color: yellow;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					} else {
						$date ='<p style="color: red;">'.date("Y-m-d",strtotime($req_date."+".$duration." day")).'</p>';
					}
				}
			}

			return $date;
	}

	public function getStatus($type){
		$qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$type)->queryRow();
        return $qryres['caption'];
	}

	public function getReturndatecolor($date) {
		$prev_date = date('Y-m-d', strtotime("+3 days"));
		if(date('Y-m-d') <$date){
			if($prev_date >=$date) {
				$return_date ='<p style="color: yellow;">'.$date.'</p>';
			} else {
			$return_date = $date;
			 }
		} else if(date('Y-m-d') == $date){
			 $return_date ='<p style="color: yellow;">'.$date.'</p>';
			} else {
				$return_date ='<p style="color: red;">'.$date.'</p>';
		}

		return $return_date;
	}

	public function stock($id){

		$requestitems =  Yii::app()->db->createCommand("SELECT * FROM tms_tool_transfer_items
            LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$id)->queryAll();
		$re_id = array();


		if(!empty($requestitems)){

	        foreach ($requestitems as $key => $value) {

	            $requestitems1 =  Yii::app()->db->createCommand("SELECT tms_tool_transfer_items.id FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=item_status  WHERE tms_tool_transfer_items.tool_transfer_id !=".$id." and tool_id = ".$value['tool_id']." and caption = 'Stock' and status_type = 'location_type'  and tms_tool_transfer_items.updated_date is not null")->queryAll();
	        }
	    }

	    if(!empty($requestitems1)){

		    foreach ($requestitems1  as $key => $value) {
		    	$re_id = $value['id'];
		    }
	    }

        if(!empty($re_id)){
           return 1;
		}else{
			return 2;
		}

	}
  public function addColor($st){
      if($st == 21){
          return "btn-info";
      }
      elseif($st == 7){
          return "btn-warning";
      }
      elseif ($st == 8) {
          return "btn-danger";
      }

  }




}




/*select t.id, t.request_no, t.request_date, t.request_to, t.request_from,t.transfer_to,t.paid_amount,t.rating,t.remarks, t.service_report, t.request_owner_id, t.ack_user_id, t.request_status, t.staff_remarks, t.location, t.created_by, t.created_date, t.updated_by, t.updated_date
from  `tms_tool_transfer_request` as t
INNER JOIN `tms_tool_transfer_info` AS info ON t.id = info.tool_transfer_id
where t.transfer_to ='site' and created_by =20
group by info.tool_transfer_id*/
