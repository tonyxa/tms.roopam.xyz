<?php

/**
 * This is the model class for table "{{clients}}".
 *
 * The followings are the available columns in table '{{clients}}':
 * @property integer $cid
 * @property string $name
 * @property string $nick_name
 * @property integer $project_type
 * @property string $description
 * @property integer $status
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property ProjectType $projectType
 * @property Status $status0
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Invoice[] $invoices
 * @property Projects[] $projects
 */

class Clients extends CActiveRecord
{	
	 public $status = 1;  //default radio button selection on create.
	 public $nick_name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Clients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{clients}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, project_type, status, created_date, created_by, updated_date, updated_by', 'required'),
			array('project_type, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name, nick_name', 'length', 'max'=>100),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cid, name, nick_name, project_type, description, status, created_date, created_by, updated_date, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'projectType' => array(self::BELONGS_TO, 'ProjectType', 'project_type'),
			'status0' => array(self::BELONGS_TO, 'Status', 'status'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'invoices' => array(self::HAS_MANY, 'Invoice', 'client_id'),
			'projects' => array(self::HAS_MANY, 'Projects', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cid' => 'Cid',
			'name' => 'Name',
			'nick_name' => 'Nick Name',
			'project_type' => 'Project Type',
			'description' => 'Description',
			'status' => 'Status',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cid',$this->cid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('nick_name',$this->nick_name,true);
		$criteria->compare('project_type',$this->project_type);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('updated_by',$this->updated_by);

		  return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
                    'criteria' => $criteria,'sort' => array(
                        'defaultOrder' => 'cid desc',),
                ));
	}

	 public function getViewname(){
       if(yii::app()->user->mainuser_role==1){
           return 1;
       }else{
           return 0;
       }
        
    }

}