<?php

/**
 * This is the model class for table "{{reqst_itemqty}}".
 *
 * The followings are the available columns in table '{{reqst_itemqty}}':
 * @property integer $id
 * @property integer $tool_transfer_id
 * @property integer $physical_condition
 * @property integer $qty
 */
class ReqstItemqty extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReqstItemqty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reqst_itemqty}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tool_transfer_id, physical_condition, qty', 'required'),
			array('tool_transfer_id, physical_condition, qty', 'numerical', 'integerOnly'=>true),
			array('transfer_item_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tool_transfer_id, physical_condition, qty, transfer_item_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tool_transfer_id' => 'Tool Transfer',
			'physical_condition' => 'Physical Condition',
			'qty' => 'Qty',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tool_transfer_id',$this->tool_transfer_id);
		$criteria->compare('physical_condition',$this->physical_condition);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('transfer_item_id',$this->transfer_item_id);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
