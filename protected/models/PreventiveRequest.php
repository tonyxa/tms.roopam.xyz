<?php

/**
 * This is the model class for table "{{preventive_request}}".
 *
 * The followings are the available columns in table '{{preventive_request}}':
 * @property integer $pr_id
 * @property integer $tool_id
 * @property string $date
 * @property string $report
 * @property string $remarks
 * @property double $cost
 * @property integer $created_by
 * @property string $created_date
 */
class PreventiveRequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PreventiveRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{preventive_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cost, date, report', 'required'),
			array('tool_id, created_by', 'numerical', 'integerOnly'=>true),
			array('cost', 'numerical'),
			array('report, remarks', 'length', 'max'=>100),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pr_id, tool_id, date, report, remarks, cost, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pr_id' => 'Pr',
			'tool_id' => 'Tool',
			'date' => 'Date',
			'report' => 'Report',
			'remarks' => 'Remarks',
			'cost' => 'Cost',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pr_id',$this->pr_id);
		$criteria->compare('tool_id',$this->tool_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}