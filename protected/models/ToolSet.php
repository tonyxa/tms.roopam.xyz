<?php

/**
 * This is the model class for table "{{tool_set}}".
 *
 * The followings are the available columns in table '{{tool_set}}':
 * @property integer $id
 * @property integer $toolset_id
 * @property integer $category
 * @property string $item_name
 * @property integer $qty
 *
 * The followings are the available model relations:
 * @property ToolCategory $category0
 */
class ToolSet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ToolSet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tool_set}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('toolset_id', 'required'),
			array('toolset_id, category, qty', 'numerical', 'integerOnly'=>true),
			array('item_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, toolset_id, category, item_name, qty', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category0' => array(self::BELONGS_TO, 'ToolCategory', 'category'),
                        'toolset' => array(self::BELONGS_TO, 'Tools', 'toolset_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'toolset_id' => 'Toolset',
			'category' => 'Category',
			'item_name' => 'Item Name',
			'qty' => 'Qty',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($page ='',$id ='')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
                if($page == 'toolsetview'){
                    $criteria->compare('toolset_id',$id);
                }else{
                    $criteria->compare('toolset_id',$this->toolset_id);
                }
		$criteria->compare('category',$this->category);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('qty',$this->qty);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}