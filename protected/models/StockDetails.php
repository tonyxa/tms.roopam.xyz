<?php

/**
 * This is the model class for table "{{stock_details}}".
 *
 * The followings are the available columns in table '{{stock_details}}':
 * @property integer $id
 * @property integer $tool_id
 * @property integer $work_site
 * @property string $serial_no
 * @property integer $cost
 * @property integer $stock
 *
 * The followings are the available model relations:
 * @property WorkSite $workSite
 * @property Tools $tool
 */
class StockDetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StockDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{stock_details}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tool_id, work_site', 'required'),
			array('tool_id, work_site, cost, stock', 'numerical', 'integerOnly'=>true),
			array('serial_no', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tool_id, work_site, serial_no, cost, stock', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workSite' => array(self::BELONGS_TO, 'WorkSite', 'work_site'),
			'tool' => array(self::BELONGS_TO, 'Tools', 'tool_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tool_id' => 'Tool',
			'work_site' => 'Work Site',
			'serial_no' => 'Serial No',
			'cost' => 'Cost',
			'stock' => 'Stock',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tool_id',$this->tool_id);
		$criteria->compare('work_site',$this->work_site);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('stock',$this->stock);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}