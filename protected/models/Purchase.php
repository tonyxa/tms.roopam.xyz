<?php

/**
 * This is the model class for table "{{purchase}}".
 *
 * The followings are the available columns in table '{{purchase}}':
 * @property integer $id
 * @property string $purchase_no
 * @property string $purchase_date
 * @property string $invoice_no
 * @property string $invoice_date
 * @property string $supplier_name
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $updatedBy
 * @property Users $createdBy
 * @property PurchaseItems[] $purchaseItems
 */
class Purchase extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Purchase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchase_no,purchase_date,invoice_no,invoice_date,supplier_name', 'required','message' => 'Please Enter {attribute}.'),
                        array('purchase_no','unique'),
			//array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('purchase_no, invoice_no, supplier_name', 'length', 'max'=>50),
			array('created_by, created_date, purchase_date, invoice_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, purchase_no, purchase_date, invoice_no, invoice_date, supplier_name, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'purchaseItems' => array(self::HAS_MANY, 'PurchaseItems', 'purchase_id'),
			'supplier' => array(self::BELONGS_TO, 'LocationType', 'supplier_name'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'purchase_no' => 'Purchase No',
			'purchase_date' => 'Purchase Date',
			'invoice_no' => 'Invoice No',
			'invoice_date' => 'Invoice Date',
			'supplier_name' => 'Supplier Name',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('purchase_no',$this->purchase_no,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('invoice_no',$this->invoice_no,true);
		$criteria->compare('invoice_date',$this->invoice_date,true);
		$criteria->compare('supplier_name',$this->supplier_name,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'id DESC',
            ),

		));
	}

        public function chkitems($id){

           $sql = "SELECT a.*,b.* FROM tms_purchase_items a INNER JOIN tms_tools b "
                    . "ON a.id = b.item_id"
                    . " WHERE a.purchase_id = $id AND b.ref_value <> '' AND b.ref_no <> ''";

             $result = Yii::app()->db->createCommand($sql)->queryAll();



             if(empty($result)){
             return true;
             }else{
             return false;
             }
//             echo "<pre>";
//             print_r($result);
//             die;
             }
}
