<?php

/**
 * This is the model class for table "{{payment_report}}".
 *
 * The followings are the available columns in table '{{payment_report}}':
 * @property integer $pmntid
 * @property integer $pjctid
 * @property integer $invid
 * @property integer $balance_amnt
 * @property string $paymentadded_date
 * @property string $remarks
 * @property integer $clntid
 * @property string $payment_source
 * @property integer $project_total_amnt
 */
class PaymentReport extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaymentReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payment_report}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		//	array('pjctid, invid, balance_amnt, paymentadded_date, remarks, clntid, payment_source, project_total_amnt', 'required'),
			array('pjctid, invid,  clntid', 'numerical', 'integerOnly'=>true),
			array('payment_source', 'length', 'max'=>250),
                    array('paid_amnt, project_total_amnt','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pmntid, pjctid, invid, balance_amnt,paid_amnt, paymentadded_date, remarks, clntid, payment_source, project_total_amnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pmntid' => 'Pmntid',
			'pjctid' => 'Pjctid',
			'invid' => 'Invid',
			'balance_amnt' => 'Balance Amnt',
			'paymentadded_date' => 'Paymentadded Date',
			'remarks' => 'Remarks',
			'clntid' => 'Clntid',
                    'paid_amnt'=>'Paid Amount',
			'payment_source' => 'Payment Source',
			'project_total_amnt' => 'Project Total Amnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pmntid',$this->pmntid);
		$criteria->compare('pjctid',$this->pjctid);
		$criteria->compare('invid',$this->invid);
		$criteria->compare('balance_amnt',$this->balance_amnt);
                $criteria->compare('paid_amnt',$this->paid_amnt);
		$criteria->compare('paymentadded_date',$this->paymentadded_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('clntid',$this->clntid);
		$criteria->compare('payment_source',$this->payment_source,true);
		$criteria->compare('project_total_amnt',$this->project_total_amnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}