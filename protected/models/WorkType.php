<?php

/**
 * This is the model class for table "{{work_type}}".
 *
 * The followings are the available columns in table '{{work_type}}':
 * @property integer $wtid
 * @property string $work_type
 * @property double $rate
 *
 * The followings are the available model relations:
 * @property Tasks[] $tasks
 */
class WorkType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WorkType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{work_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rate', 'numerical'),
			array('work_type', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('wtid, work_type, rate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tasks' => array(self::HAS_MANY, 'Tasks', 'work_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wtid' => 'Wtid',
			'work_type' => 'Work Type',
			'rate' => 'Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wtid',$this->wtid);
		$criteria->compare('work_type',$this->work_type,true);
		$criteria->compare('rate',$this->rate);

		return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
			'criteria'=>$criteria,
		));
	}
}