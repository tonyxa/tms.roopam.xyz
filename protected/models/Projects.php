<?php

/**
 * This is the model class for table "{{projects}}".
 *
 * The followings are the available columns in table '{{projects}}':
 * @property integer $pid
 * @property integer $client_id
 * @property string $name
 * @property integer $billable
 * @property integer $status
 * @property double $budget
 * @property string $start_date
 * @property integer $end_date
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Clients $client
 * @property Status $status0
 * @property Tasks[] $tasks
 */
class Projects extends CActiveRecord {

    public $billable = 3; //Default radio button selection 
    public $status = 1;  //default radio button selection on create.

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Projects the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{projects}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, status,  created_date, created_by, updated_date, updated_by', 'required'),
            array('client_id,billable, status, created_by, updated_by', 'numerical', 'integerOnly' => true),
            array('budget', 'numerical'),
            array('name', 'length', 'max' => 100),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('pid, name, billable, status, budget,  description, created_date, created_by, updated_date, updated_by,client_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'billable0' => array(self::BELONGS_TO, 'Status', 'billable'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
             'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
            'status0' => array(self::BELONGS_TO, 'Status', 'status'),
            'tasks' => array(self::HAS_MANY, 'Tasks', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pid' => 'Pid',
            'client_id' => 'Client',
            'name' => 'Project name',
            'billable' => 'Billable',
            'status' => 'Status',
            'budget' => 'Budget',
            'description' => 'Description',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('pid', $this->pid);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('billable', $this->billable);
        $criteria->compare('status', $this->status);
        $criteria->compare('budget', $this->budget);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25,),
                    'criteria' => $criteria,'sort' => array(
                        'defaultOrder' => 'pid desc',),
                ));
    }

}