<?php

/**
 * This is the model class for table "{{tool_transfer_items}}".
 *
 * The followings are the available columns in table '{{tool_transfer_items}}':
 * @property integer $id
 * @property integer $tool_transfer_id
 * @property string $code
 * @property string $item_name
 * @property integer $unit
 * @property string $serial_no
 * @property integer $duration_in_days
 * @property integer $qty
 *
 * The followings are the available model relations:
 * @property ToolTransferRequest $toolTransfer
 * @property Unit $unit0
 */
class ToolTransferItems extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ToolTransferItems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{tool_transfer_items}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tool_transfer_id, code, item_name ', 'required'),
            array('tool_transfer_id, unit, duration_in_days, qty, paid_amount, rating', 'numerical', 'integerOnly' => true),
            array('code, item_name, serial_no', 'length', 'max' => 50),
            array('remarks, service_report,tool_id,item_remarks,item_current_status,updated_by,updated_date,parent_info_id, return_qty, approved_qty', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, tool_transfer_id, code, item_name, unit, serial_no, duration_in_days, qty,physical_condition,item_status, paid_amount, rating, remarks, service_report,req_date,duration_type,location,item_remarks,item_current_status,updated_by,updated_date,parent_info_id, return_qty, approved_qty', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toolTransfer' => array(self::BELONGS_TO, 'ToolTransferRequest', 'tool_transfer_id'),
            'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
            'physicalCondition' => array(self::BELONGS_TO, 'Status', 'physical_condition'),
            'itemStatus' => array(self::BELONGS_TO, 'Status', 'item_status'),
            'tool' => array(self::BELONGS_TO, 'Tools', 'tool_id'),
            'durationType' => array(self::BELONGS_TO, 'Status', 'duration_type'),
            'location0' => array(self::BELONGS_TO, 'LocationType', 'location'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tool_transfer_id' => 'Tool Transfer',
            'tool_id' => 'Tool',
            'code' => 'Tool Name',
            'item_name' => 'Reference No',
            'unit' => 'Unit',
            'serial_no' => 'Serial No',
            'duration_in_days' => 'Duration',
            'duration_type' => 'Type',
            'qty' => 'Qty',
            'physical_condition' => 'Physical Condition',
            'item_status' => 'Status',
            'req_date' => 'Date',
            'paid_amount' => 'Paid Amount',
            'rating' => 'Rating',
            'remarks' => 'Remarks',
            'service_report' => 'Service Report',
            'location' => 'Location',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($id) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        if ($id) {

           $criteria->compare('tool_transfer_id', $id);
        } else {
           $criteria->compare('tool_transfer_id', $this->tool_transfer_id);
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('item_name', $this->item_name, true);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('serial_no', $this->serial_no, true);
        $criteria->compare('duration_in_days', $this->duration_in_days);
        $criteria->compare('duration_type', $this->duration_type);
        $criteria->compare('qty', $this->qty);
        //$criteria->compare('physical_condition', $this->physical_condition);
        $criteria->compare('item_status', $this->item_status);
        $criteria->compare('req_date', $this->req_date, true);
        $criteria->compare('paid_amount', $this->paid_amount);
        $criteria->compare('rating', $this->rating);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('service_report', $this->service_report, true);
        $criteria->compare('location',$this->location);
        $criteria->compare('return_qty',$this->return_qty);
        $criteria->compare('approved_qty',$this->approved_qty);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC',
            ),
        ));
    }
    public function search1($id) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->addCondition("paid_amount != ''");
        if ($id) {

           $criteria->compare('tool_transfer_id', $id);
        } else {
           $criteria->compare('tool_transfer_id', $this->tool_transfer_id);
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('item_name', $this->item_name, true);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('serial_no', $this->serial_no, true);
        $criteria->compare('duration_in_days', $this->duration_in_days);
        $criteria->compare('duration_type', $this->duration_type);
        $criteria->compare('qty', $this->qty);
        //$criteria->compare('physical_condition', $this->physical_condition);
        $criteria->compare('item_status', $this->item_status);
        $criteria->compare('req_date', $this->req_date, true);
        $criteria->compare('paid_amount', $this->paid_amount);
        $criteria->compare('rating', $this->rating);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('service_report', $this->service_report, true);
        $criteria->compare('location',$this->location);
        $criteria->compare('return_qty',$this->return_qty);
        $criteria->compare('approved_qty',$this->approved_qty);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC',
            ),
        ));
    }





    public function getDuration($id) {
        $transferitems = ToolTransferItems::model()->findByPk($id);
        if ($transferitems) {
            $duration=$transferitems->duration_in_days;
            $duration.='&nbsp';
            $duration.=isset($transferitems->durationType->caption)?$transferitems->durationType->caption:'';
            return $duration;

        }
        return null;
    }

    public function getCategory($id) {

        $category_id = Tools::model()->findByPk($id);
        $cat_id = $category_id->tool_category;
        if($cat_id)
        {

           $category_isd = ToolCategory::model()->findByPk($cat_id);
           $cat_name = $category_isd->cat_name;
           return $cat_name;


		}
        return null;
    }

     public function getCategoryparent($id,$parent=0) {

        $category_id = Tools::model()->findByPk($id);
        $cat_id = $category_id->tool_category;
        if($cat_id)
        {

           $category_isd = ToolCategory::model()->findByPk($cat_id);
           $parent_id = $category_isd->parent_id;
           if($parent_id != NULL)
           {
			 $parentids = ToolCategory::model()->findByPk($parent_id);
			 $p_name = $parentids->cat_name;
			 return $p_name;

		   }else{
			 return null;
			}


		}
        return null;
	}

	public function gettoolstatus($id){

       $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$id)->queryRow();
       return $qryres['caption'];
	}


	public function getitemsqty($id){


      $qryres1 = Yii::app()->db->createCommand("SELECT q.qty,caption FROM `tms_tool_transfer_items`  as t
       left join  tms_approve_itemqty as q on  q.tool_transfer_id = t.tool_transfer_id and t.id = q.transfer_item_id
       left join tms_status as sts on sts.sid = q.physical_condition
       WHERE t.id= ".$id )->queryAll();
       if(!empty($qryres1)){
         $qryres = $qryres1;
       }else{
         $qryres = Yii::app()->db->createCommand("SELECT q.qty,caption FROM `tms_tool_transfer_items`  as t
          left join  tms_reqst_itemqty as q on  q.tool_transfer_id = t.tool_transfer_id and t.id = q.transfer_item_id
          left join tms_status as sts on sts.sid = q.physical_condition
          WHERE t.id= ".$id )->queryAll();
       }
        $htm = '';
        foreach ($qryres as $key => $value) {
           $htm .= '<span style="width:100px;display:inline-block;">'.$value['caption'].'</span><span>'.$value['qty'].'</span><br/>';
        }
        return $htm;

    }

    public function missingreport(){
      $criteria=new CDbCriteria;
      $criteria->addCondition("t.transfer_to ='site'");
      if(Yii::app()->user->role != 1 && Yii::app()->user->role != 9){
         $criteria->addCondition("created_by = ". Yii::app()->user->id." or request_owner_id = ".Yii::app()->user->id);
      }
      $criteria->select = 't.id, t.request_no, t.request_date, t.request_to, t.request_from,t.transfer_to,t.paid_amount,t.rating,t.remarks, t.service_report, t.request_owner_id, t.ack_user_id, t.request_status, t.staff_remarks, t.location, t.created_by, t.created_date, t.updated_by, t.updated_date';
          $criteria->join = ' INNER JOIN `tms_tool_transfer_info` AS `info` ON t.id = info.tool_transfer_id';
          $criteria->group='info.tool_transfer_id';
      return new CActiveDataProvider($this, array(
        'criteria'=>$criteria,
        'sort' => array('defaultOrder' => 't.id DESC',
              ),
      ));

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('code', $this->code, true);
      $criteria->compare('item_name', $this->item_name, true);
      $criteria->compare('unit', $this->unit);
      $criteria->compare('serial_no', $this->serial_no, true);
      $criteria->compare('duration_in_days', $this->duration_in_days);
      $criteria->compare('duration_type', $this->duration_type);
      $criteria->compare('qty', $this->qty);
      //$criteria->compare('physical_condition', $this->physical_condition);
      $criteria->compare('item_status', $this->item_status);
      $criteria->compare('req_date', $this->req_date, true);
      $criteria->compare('paid_amount', $this->paid_amount);
      $criteria->compare('rating', $this->rating);
      $criteria->compare('remarks', $this->remarks, true);
      $criteria->compare('service_report', $this->service_report, true);
      $criteria->compare('location',$this->location);
      $criteria->compare('return_qty',$this->return_qty);
      $criteria->compare('approved_qty',$this->approved_qty);

      $criteria->select = 't.*,request.missing_qty';
          $criteria->join = ' LEFT JOIN `tms_requestitem_status` AS `request` ON t.id = request.item_id';

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          // 'sort' => array('defaultOrder' => 'id DESC',
          // ),
      ));






    }


}
