<?php

/**
 * This is the model class for table "{{tools}}".
 *
 * The followings are the available columns in table '{{tools}}':
 * @property integer $id
 * @property string $tool_code
 * @property string $tool_name
 * @property integer $tool_category
 * @property integer $unit
 * @property string $make
 * @property string $model_no
 * @property string $serial_no
 * @property string $ref_no
 * @property string $batch_no
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $pending_status
 * @property integer $location
 *
 * The followings are the available model relations:
 * @property StockDetails[] $stockDetails
 * @property WorkSite $location0
 * @property ToolCategory $toolCategory
 * @property Status $pendingStatus
 * @property Unit $unit0
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class Tools extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Tools the static model class
     */
    public $type = '';
    public $parent_category;
    public static $data = '';
    public $child;
    public $subchild;
    public $subchild2;
    public $count;
    public $statuscaption;
    public $status;
    public $prev_type;
    public $tool_type;
    public $tool_category;
    public $vendor_request_id;
    public $tool_status;
    public $item_current_status;
    public $item_status;
    public $return_qty;
    public $tool_transfer_id;
    public $item_id;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{tools}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tool_name, unit, created_by, created_date,item_id,serial_no', 'required'),
            array('tool_category, unit, created_by, updated_by, pending_status, location,qty, tool_condition', 'numerical', 'integerOnly' => true),
            array('tool_code, tool_name, model_no, serial_no, ref_no, batch_no', 'length', 'max' => 50),
            array('make', 'length', 'max' => 100),
            array('tool_code,updated_date,warranty_date,item_id,parent_category,tool_transfer_id,return_qty,item_status,item_current_status,prev_main,prev_hrs,prev_date,prev_type,tool_type,tool_category,serialno_status,stock_qty,vendor_request_id,tool_status', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, tool_name,statuscaption,warranty_date,item_id,item_current_status,tool_transfer_id,item_status,return_qty, tool_condition, tool_category,parent_category, unit, make, model_no, serial_no, ref_no, batch_no, created_by, created_date, updated_by, updated_date, pending_status, location,active_status,type,prev_hrs,prev_date,prev_main,prev_type,tool_type,serialno_status,stock_qty,vendor_request_id,tool_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'stockDetails' => array(self::HAS_MANY, 'StockDetails', 'tool_id'),
            'location0' => array(self::BELONGS_TO, 'LocationType', 'location'),
            'toolCategory' => array(self::BELONGS_TO, 'ToolCategory', 'tool_category'),
            'pendingStatus' => array(self::BELONGS_TO, 'Status', 'pending_status'),
            'item' => array(self::BELONGS_TO, 'PurchaseItems', 'item_id'),
            'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'tool_code' => 'Tool Code',
            'tool_name' => 'Tool Name',
            'tool_category' => 'Tool Category',
            'unit' => 'Unit',
            'make' => 'Make',
            'model_no' => 'Model No',
            'serial_no' => 'OEM Serial No',
            'ref_no' => 'Tool Name(Ref No)',
            'batch_no' => 'Batch No',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'pending_status' => 'Pending Status',
            'location' => 'Location',
            'qty' => 'Quantity',
            'parent_category' => 'Parent Category',
            'warranty_date' => 'Warranty Date',
            'child' => 'Child Category',
            'subchild' => 'Sub Child Category',
            'statuscaption' => 'Status',
            'tool_condition' => 'Status',
            'prev_main' => 'Preventive Maintenance',
            'prev_hrs' =>'Hours',
            'prev_date' => 'Preventive Maintenance Date',
            'prev_type' =>'Maintenance',
            'tool_type' =>'Tool type',
            'serialno_status' => 'Serial #',
            'stock_qty' => 'Stock Quantity',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($page, $loc = '',$type = '',$id ='') {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.



        $criteria = new CDbCriteria;



        $criteria->with = array('toolCategory');
        $criteria->together = true;

         //echo $this->tool_category;exit;

        $criteria->compare('id', $this->id);
        $criteria->compare('tool_code', $this->tool_code, true);
        $criteria->compare('tool_name', $this->tool_name, true);
        $criteria->compare('tool_category', $this->tool_category);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('make', $this->make, true);
        $criteria->compare('model_no', $this->model_no, true);
        $criteria->compare('serial_no', $this->serial_no, true);
        $criteria->compare('ref_no', $this->ref_no, true);
        $criteria->compare('batch_no', $this->batch_no, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('warranty_date',$this->warranty_date);
        $criteria->compare('location', $this->location);
        $criteria->compare('t.qty', $this->qty);
        $criteria->compare('tool_condition', $this->tool_condition);
        $criteria->compare('prev_hrs', $this->prev_hrs);
        $criteria->compare('serialno_status', $this->serialno_status);
        //$criteria->compare('prev_date', $this->prev_date);
        $criteria->compare('prev_main', $this->prev_main);
        $criteria->compare('stock_qty', $this->stock_qty);
        $criteria->compare('vendor_request_id', $this->vendor_request_id);
        $criteria->compare('t.active_status', $this->active_status);
        
        $criteria->addCondition('qty !=0');

        if(!empty($this->prev_date)){

            $date = explode('-', $this->prev_date);
            $date =array_reverse($date);
            $date=implode('-', $date);
            $criteria->addCondition("date(`prev_date`) = '".$date."' ");
        }

        if($page == "tools"){
          $criteria->addCondition("location IS NOT NULL");
        }

        if($page == "toolsview"){
			$criteria->compare('location', $loc);
            $criteria->addCondition('t.active_status = "1"');
            if($type == 0){
                $criteria->addCondition('pending_status ='.$this->getType($type));
            }else if($type == 1){
                 $criteria->addCondition('pending_status ='.$this->getType($type));
            }
        }else if($page == "categoryview"){
            $criteria->compare('tool_category', $id);
        }else if($page == "toolreport"){
            $criteria->compare('pending_status',$this->pending_status );
          //  $criteria->addCondition('stock_qty >0 or stock_qty is null');
        } else if($page == "toolsviewsite"){
            $criteria->group = 'tool_name';

			 $criteria->select = 't.tool_name, t.ref_no,   COUNT(*) AS count';
			 $criteria->compare('location', $loc);

            $criteria->addCondition('t.active_status = "1"');
            if($type == 0){
                $criteria->addCondition('pending_status ='.$this->getType($type));
            }else if($type == 1){
                 $criteria->addCondition('pending_status ='.$this->getType($type));
            }
        }else if($page == "toollog"){

             $date = date('Y-m-d');
             $future_date = date('Y-m-d', strtotime('+7 days'));

             $criteria->addCondition("prev_main = '1' ");
             $criteria->addCondition("date(`prev_date`) < '".$future_date."' " );

             /* site engineers */

             if(Yii::app()->user->role == 10){

                $loceType = $this->getLoceType();
                if( !empty($loceType)){
                    $criteria->addCondition('t.location = '.$loceType);
                }else{
                   $criteria->addCondition('t.location = NULL');
                }

            }



            if( $this->prev_type==1){

               $criteria->addCondition( "prev_main = '1' and date(`prev_date`) > '".$date."' ");
            }else if( $this->prev_type==2) {

               $criteria->addCondition( "prev_main = '1' and date(`prev_date`) < '".$date."' ");
            }

            if( $this->tool_type==3){

              $criteria->addCondition( "prev_main = '1' and  pending_status = 17 ");

            }else if($this->tool_type==4){
                $criteria->addCondition( "prev_main = '1' and  pending_status != 17 and  date(`prev_date`) > '".$date."' ");
            }


        }else{
            $criteria->compare('pending_status', $this->showPage($page));
        }


        if($page == "toolsview1"){

                $alltools = Yii::app()->db->createCommand("SELECT tl.id FROM `tms_location_type` as lo
                            inner join  `tms_tools` as  tl on tl.location = lo.id  WHERE lo.id= ".$loc)->queryAll();
                $data = array();
                $arr = 0;
                foreach ($alltools as $key => $value){
                    $info = Yii::app()->db->createCommand( "SELECT *  FROM `tms_tool_transfer_info` WHERE location = ".$loc." and tool_id = ".$value['id'] )->queryRow();

                    $items = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `tool_transfer_id`= ".$value['id']." and `duration_type` is not null ")->queryRow();

                    if( empty($info) and empty($items) ){
                        $data[] = $value['id'];
                    }
                }
                $arr = implode(' ,', $data);

               // print_r($arr);exit;

                if(!empty($data)){

                  $criteria->addCondition( "t.id in(".$arr.") ");
                }else{
                    $arr=0;
                  $criteria->addCondition( "t.id in(".$arr.") ");
                }
        }


        if($page=='toolstransferall'){

            $itemall = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` as itm left join tms_tools as tol on tol.id= itm.tool_id WHERE item_status=13 and itm.`location`= ".$loc )->queryAll();

            $data = array();
            foreach ( $itemall as $key => $value ) {
                 $data[] = $value['tool_id'];
            }
            $arr = implode(' ,', $data);
            if(!empty($data)){

                  $criteria->addCondition( "t.id in(".$arr.") ");
            }else{
                    $arr=0;
                  $criteria->addCondition( "t.id in(".$arr.") ");
            }


         }


        if(!empty($this->parent_category) && !empty($this->child) && !empty($this->subchild)){
        $criteria->compare('toolCategory.cat_id',$this->subchild);
        }elseif(!empty($this->parent_category) && !empty($this->child) && empty($this->subchild)){
           //$criteria->compare('toolCategory.parent_id',$this->child);
          $criteria->addCondition('toolCategory.parent_id IN  ('.$this->child.') or toolCategory.cat_id IN  ('.$this->child.')');
        }elseif(!empty($this->parent_category) && empty($this->child) && empty($this->subchild)){

        $list = $this->getParentcategory1();
        if(!empty($list)){
            $criteria->addCondition('toolCategory.parent_id IN  ('.implode(',',$list).') or toolCategory.parent_id IN  ('.$this->parent_category.')');
         }else{
           $criteria->addCondition('toolCategory.cat_id IN  ('.$this->parent_category.')');
         }

        }else{
        //$criteria->compare('toolCategory.cat_id', $this->tool_category);
        //$criteria->compare('toolCategory.parent_id', $this->parent_category);
        }



       /* if($this->status != ''){
       //echo $this->status;
		$res = Tools::model()->getcatids($this->status);

		$arr = array();
		foreach($res as $r){
		array_push($arr,$r['tool_id']);
		 }


		if(!empty($res)){
		$criteria->addCondition('id IN  ('.implode(',',$arr).')');
		}

		} */


        /*
        if ($loc) {
            $criteria->compare('location', $loc);
        } else {
            $criteria->compare('location', $this->location);
        }

         */
        $sort = new CSort();
        $sort->attributes = array(

            'parent_category'=>array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'child' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'subchild' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
                     '*',

                );
               $sort->defaultOrder = 'id DESC';

       /* return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' =>  $sort,
            'pagination' => array(
             'pageSize' => 100,
            ),

        ));*/


        $dp = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' =>  $sort,
			'pagination' => array(
             'pageSize' => 50,
            ),

        ));



            // $transferlocations = Yii::app()->db->createCommand("SELECT tms_tools_location.id as toollocid,tool_name,tool_category,tool_condition,tms_tools_location.qty,tms_tools_location.location,ref_no,tool_code,unit,make,model_no,serial_no,batch_no,tms_tools.created_by,tms_tools.created_date,tms_tools.updated_by,tms_tools.updated_date,pending_status,ref_value,item_id,tms_tools.active_status,warranty_date,prev_main,prev_date,prev_hrs,serialno_status FROM `tms_tools_location` join tms_tools on tms_tools.id = tms_tools_location.tool_id  left join tms_tool_category on tms_tool_category.cat_id = tms_tools.tool_category")->queryAll();


        $dp->setTotalItemCount(count($this->findAll($criteria)));
        return $dp;
    }



    public function getcatids($id){

		$catids = Yii::app()->db->createCommand("SELECT tool_id FROM tms_tool_transfer_items  WHERE physical_condition=".$id." ORDER BY id DESC ")->queryAll();

		return  $catids;

		}


    public function getParentcategory(){
        $list = array();
         $list[] = $this->parent_category;
         $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_tool_category')
                    ->where('parent_id=:pid', array(':pid' => $this->parent_category))
                    ->queryAll();
            foreach ($qryres as $key => $value) {
             $list[] = $value['cat_id'];
            }
            return $list;
    }

    public function getParentcategory1(){
        $list = array();
        // $list[] = $this->parent_category;
         $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_tool_category')
                    ->where('parent_id=:pid', array(':pid' => $this->parent_category))
                    ->queryAll();
            foreach ($qryres as $key => $value) {
             $list[] = $value['cat_id'];
            }
            return $list;
    }

    public function checkRefStatus($refno) {
        //(($model->ref_no == '') ? "0" : $model->ref_no),
        echo $refno;
        if (empty($refno)) {
            $status = "0";
        } else {
            $status = $refno;
        }
        return $status;
    }

    public function showPage($page) {
        if ($page == 'pending') {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Pending'))
                    ->queryRow();
        } else if ($page == 'toolset') {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool Set'))
                    ->queryRow();
        }
        else{
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
                    ->queryRow();
        }
        return $qryres['sid'];
    }

    public function  getType($type){
        if ($type == 0) {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
                    ->queryRow();
        } else {
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool Set'))
                    ->queryRow();
        }
        return $qryres['sid'];
    }

   public function showResults(){
      // $tblpx = Yii::app()->db->tablePrefix;

        $qry1 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'location_type', ':caption' => 'Vendor'))
                    ->queryRow();
       $id =  $qry1['sid'];

       $qryres = Yii::app()->db->createCommand()
                    ->select('id,name')
                    ->from('tms_location_type')
                    ->where('location_type != :type and active_status=:active_status', array(':type' => $id,':active_status'=>'1'))
                    ->queryAll();
       return $qryres;

   }


       public function checkStatus($itemid,$tcat){


         $qry1 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_purchase_items')
                    ->where('id=:id', array(':id' => $itemid))
                    ->queryRow();
                     $purchaseid =  $qry1['purchase_id'];

                    $qry2 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_purchase')
                    ->where('id=:id', array(':id' => $purchaseid))
                    ->queryRow();

                    if($qry2['active_status'] == 1 && $tcat == '' ){
                    return true;
                    }else{
                    return false;
                    }

    }

    public function getCategoryname($id) {

            $model = ToolCategory::model()->findByPk($id);
                if(is_null($model)){
                    return "None";
                }else{
                    return $model->cat_name;
                }
        }

    public static function getToolCategoryname($id,$parent=0) {

            $model = Tools::model()->findByPk($id);
                if(is_null($model)){
                    return "None";
                }else{
                     $modelcat = ToolCategory::model()->findByPk($model->tool_category);
                     if(is_null($modelcat)){
                    return "None";
                    }else{
                        if($parent==1){
                                 $modelparentcat = ToolCategory::model()->findByPk($modelcat->parent_id);
                                   if(is_null($modelparentcat)){
                                   return "None";
                                   }else{
                                      return $modelparentcat->cat_name;
                                   }
                        }else{
                        return $modelcat->cat_name;
                        }
                    }
                }
        }

        public function getparents($cid){
       $res = '';
       $res=$this->getparents1($cid);
      // print_r($res);
        Tools::$data = null;

        $res1 =  Tools::$data;
//        print_r($res1);
//        die;
       return $res;
       }

        public function getparents1($cid){
            // static $a = 0;

             /* aary declrtion needed*/
              $dat =array();


               //foreach(ToolCategory::model()->findAll('parent_id = ' . $id) as $model) {
               foreach( ToolCategory::model()->findAll(
                       array(
                             'select'=>'cat_id,parent_id,cat_name',
                             'condition'=>'cat_id = '. $cid,
                             //'condition'=>'parent_id = ' . $cid,
                           //active_status="1" AND parent_id = ' . $id
                             'order'=>'cat_name'
                            )
                       ) as $model) {
                       //$row['id'] = $model->cat_id;
                       $row['text'] = $model->cat_name; //CHtml::link($model->cat_name, array('place/view', 'id' => $model->cat_id));

                        $dat[] = $row['text'];
                       if(!empty($model->parent_id)){
                       $row['children'] = Tools::getparents1($model->parent_id);
                       //$data[] = $row['text'];
                       }
               }
               //$res = Booking::model()->getparents(41);

           $res1 = array_reverse($dat,true);
           return implode("-",$res1);
              // return $data;
       }
       public function getQty($cid){
           $sql = "SELECT SUM(qty) AS cnt FROM tms_tools WHERE tool_category ='$cid'";
           $query = Yii::app()->db->createCommand($sql)->queryRow();
           if(empty($query['cnt'])){
           return 0;
           }else{
           return $query['cnt'];
           }

       }

        public function allsearch($page, $loc = '',$type = '',$id ='' , $cate='') {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('toolCategory');
        $criteria->together = true;
        $criteria->compare('id', $this->id);
        $criteria->compare('tool_code', $this->tool_code, true);
        $criteria->compare('tool_name', $this->tool_name, true);
        // $criteria->compare('tool_category', $this->tool_category);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('make', $this->make, true);
        $criteria->compare('model_no', $this->model_no, true);
        $criteria->compare('serial_no', $this->serial_no, true);
        $criteria->compare('ref_no', $this->ref_no, true);
        $criteria->compare('batch_no', $this->batch_no, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('warranty_date',$this->warranty_date);
        $criteria->compare('location', $this->location);
        $criteria->compare('qty', $this->qty);
        $criteria->compare('stock_qty', $this->stock_qty);
        if($page == "alltoolsview"){


            if(!empty($cate)){
              $criteria->addCondition('t.tool_category in ('.$cate.') ');
            }else{
              $criteria->compare('location', $loc);
            }

            $criteria->addCondition('t.active_status = "1"');

            if($type == 0){
                $criteria->addCondition('pending_status ='.$this->getType($type));
            }else if($type == 1){
                 $criteria->addCondition('pending_status ='.$this->getType($type));
            }
        }else if($page == "categoryview"){
            $criteria->compare('tool_category', $id);
        }else if($page == "toolreport"){
            $criteria->compare('pending_status',$this->pending_status );
        }else{
            $criteria->compare('pending_status', $this->showPage($page));
        }

        if(!empty($this->parent_category) && !empty($this->child) && !empty($this->subchild)){
        $criteria->compare('toolCategory.cat_id',$this->subchild);
        }elseif(!empty($this->parent_category) && !empty($this->child) && empty($this->subchild)){
        $criteria->compare('toolCategory.parent_id',$this->child);
        }elseif(!empty($this->parent_category) && empty($this->child) && empty($this->subchild)){

        $list = $this->getParentcategory();
        $criteria->addCondition('toolCategory.parent_id IN  ('.implode(',',$list).')');
        }else{
        //$criteria->compare('toolCategory.cat_id', $this->tool_category);
        //$criteria->compare('toolCategory.parent_id', $this->parent_category);
        }

        /*
        if ($loc) {
            $criteria->compare('location', $loc);
        } else {
            $criteria->compare('location', $this->location);
        }

         */
        $sort = new CSort();
        $sort->attributes = array(

                       'parent_category'=>array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'child' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'subchild' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
                     '*',

                );
               $sort->defaultOrder = 'id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' =>  $sort,
			'pagination' => array(
            'pageSize' => 100,
        ),
        ));
    }
     public function alltoolsviewsearch($loc = '',$cate = '',$toolid = '',$type = 0, $requested_qty = '', $tool_type = '',$reqid='') {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.



        $check = Yii::app()->db->createCommand("SELECT tool_id FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$reqid." and tms_tools.serialno_status = 'Y' ")->queryAll();

        $toolarray = array();
        if(!empty($check)){

        foreach($check as $toolid){
          $toolarray[]  = $toolid['tool_id'];
        }
          $data = implode(",",$toolarray);

        }

        $criteria = new CDbCriteria;
        $criteria->with = array('toolCategory');
        $criteria->together = true;
        $criteria->compare('id', $this->id);
        $criteria->compare('tool_code', $this->tool_code, true);
        $criteria->compare('tool_name', $this->tool_name, true);
        // $criteria->compare('tool_category', $this->tool_category);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('make', $this->make, true);
        $criteria->compare('model_no', $this->model_no, true);
        $criteria->compare('serial_no', $this->serial_no, true);
        $criteria->compare('ref_no', $this->ref_no, true);
        $criteria->compare('batch_no', $this->batch_no, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('warranty_date',$this->warranty_date);
        $criteria->compare('location', $this->location);
        $criteria->compare('qty', $this->qty);     //echo $toolid;exit;
        $criteria->compare('stock_qty', $this->stock_qty);
        $criteria->compare('location', $this->location);
        $criteria->compare('serialno_status', $this->serialno_status);
        if(!empty($cate)){
            $criteria->addCondition('t.tool_category in ('.$cate.') ');
          }else{
            $criteria->compare('location', $loc);
          }

          $criteria->addCondition('t.active_status = "1"');

          if($type == 0){
              $criteria->addCondition('pending_status ='.$this->getType($type));
          }else if($type == 1){
               $criteria->addCondition('pending_status ='.$this->getType($type));
          }




        if(!empty($this->parent_category) && !empty($this->child) && !empty($this->subchild)){
        $criteria->compare('toolCategory.cat_id',$this->subchild);
        }elseif(!empty($this->parent_category) && !empty($this->child) && empty($this->subchild)){
        $criteria->compare('toolCategory.parent_id',$this->child);
        }elseif(!empty($this->parent_category) && empty($this->child) && empty($this->subchild)){

        $list = $this->getParentcategory();
        $criteria->addCondition('toolCategory.parent_id IN  ('.implode(',',$list).')');
        }else{
        //$criteria->compare('toolCategory.cat_id', $this->tool_category);
        //$criteria->compare('toolCategory.parent_id', $this->parent_category);
        }

        if($tool_type == 'Y'){
          $criteria->addCondition('t.serialno_status = "Y"');
        }
        if($tool_type == 'N'){
          $criteria->addCondition('t.serialno_status = "N"');
        }

        //new section avoid allocated tools in same req

        if(!empty($check)){
          $criteria->addCondition('t.id not in ('.$data.') ');
        }

        /*
        if ($loc) {
            $criteria->compare('location', $loc);
        } else {
            $criteria->compare('location', $this->location);
        }

         */ //echo "<pre>";print_r($criteria);exit;
        $sort = new CSort();
        $sort->attributes = array(

                       'parent_category'=>array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'child' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
						'subchild' => array(
                                'asc'=>'toolCategory.parent_id',
                                'desc'=>'toolCategory.parent_id desc',
                        ),
                     '*',

                );
               $sort->defaultOrder = 'id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' =>  $sort,
			'pagination' => array(
            'pageSize' => 100,
        ),
        ));
    }

    public function vendorsearch($page, $loc = '',$type = '',$id ='') {
        $criteria=new CDbCriteria;
		//$criteria->addCondition("t.transfer_to ='site'");
		$criteria->select = 't.id, t.ref_no, t.tool_name, tms_status.caption as statuscaption';
        $criteria->join = ' INNER JOIN `tms_tool_category` AS `cat` ON t.tool_category = cat.cat_id';
        $criteria->join = ' LEFT JOIN `tms_status` ON tms_status.sid = t.tool_condition';

        //$criteria->join = ' LEFT JOIN `tms_tool_transfer_items` AS `item` ON item.tool_id = t.id LEFT JOIN `tms_status` ON tms_status.sid = item.physical_condition';

        //$criteria->join = ' LEFT JOIN `tms_status`  ON tms_status.sid = item.physical_condition';
       // $criteria->addCondition("status.caption ='Damage'");
        //$criteria->group='info.tool_transfer_id';

         $criteria->addCondition("tms_status.caption ='Damage' OR tms_status.caption ='Breakdown'");
         $criteria->addCondition("t.location ='".$loc."'");

	   // $criteria->addCondition("item.physical_condition =1");
	   // $criteria->addCondition("item.physical_condition =2");
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 't.id DESC',
            ),
		));
    }

    public function toolsearch() {
		$criteria=new CDbCriteria;
		//$criteria->addCondition("t.transfer_to ='site'");
		$criteria->select = 't.ref_no, t.tool_name, tms_status.caption as statuscaption, t.tool_category, t.location';
        $criteria->join = ' INNER JOIN `tms_tool_category` AS `cat` ON t.tool_category = cat.cat_id';
        $criteria->join = ' LEFT JOIN `tms_tool_transfer_items` AS `item` ON item.tool_id = t.id LEFT JOIN `tms_status` ON tms_status.sid = item.physical_condition';
        //$criteria->join = ' LEFT JOIN `tms_status`  ON tms_status.sid = item.physical_condition';
       // $criteria->addCondition("status.caption ='Damage'");
        //$criteria->group='info.tool_transfer_id';

         $criteria->addCondition("tms_status.caption ='Damage' OR tms_status.caption ='Running'");
         //$criteria->addCondition("t.location ='".$loc."'");

	   // $criteria->addCondition("item.physical_condition =1");
	   // $criteria->addCondition("item.physical_condition =2");
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'item.id DESC',
            ),
		));

	}

	public function getStatus($id){

		/*$qryres = Yii::app()->db->createCommand("SELECT tms_status.caption FROM tms_tool_transfer_items
		left JOIN tms_status ON tms_status.sid=tms_tool_transfer_items.physical_condition WHERE tms_tool_transfer_items.tool_id=".$id." ORDER BY tms_tool_transfer_items.id DESC LIMIT 1")->queryRow();
        if(is_null($qryres)) {
			return "Running";
		} else {
			if(is_null($qryres['caption'])){
				return "Running";
			} else {
				return $qryres['caption'];
				}
		}*/


       $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$id)->queryRow();
        return $qryres['caption'];


	}


    public function getprev_type($id){

         $tools = Yii::app()->db->createCommand("SELECT prev_date FROM tms_tools WHERE id=".$id)->queryRow();

         if(date($tools['prev_date']) > date('Y-m-d') ){

            $res= "Upcoming";
         }else{
            $res = "Expired";
         }

        return $res;
    }


    public static function visualTree($catTree, $level) {
            $res = array();
            foreach ($catTree as $item) {

                $res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['text'];
                if (isset($item['children'])) {
                    $res_iter = self::visualTree($item['children'], $level + 1);
                    foreach ($res_iter as $key => $val) {
                        $res[$key] = $val;
                    }
                }
            }
            return $res;
        }


        public static function toolcategories(){

            $tblpx = Yii::app()->db->tablePrefix;

            $models = $qryres = Yii::app()->db->createCommand("SELECT cat_id,cat_name from {$tblpx}tool_category  join {$tblpx}status on {$tblpx}tool_category.tool_type = {$tblpx}status.sid where caption='TOOl' and parent_id IS NULL order by cat_name")->queryAll();


                $data = array();
                foreach($models as $model) {
                        $row['id'] = $model['cat_id'];
                        $row['text'] = $model['cat_name'];
                        $row['children'] = ToolCategory::getChildcategories($model['cat_id']);
                        $data[] = $row;
                }
                return $data;
        }


        public static function toolsetcategories(){

            $tblpx = Yii::app()->db->tablePrefix;

            $models = $qryres = Yii::app()->db->createCommand("SELECT cat_id,cat_name from {$tblpx}tool_category  join {$tblpx}status on {$tblpx}tool_category.tool_type = {$tblpx}status.sid where caption='Tool Set' and parent_id IS NULL order by cat_name")->queryAll();


                $data = array();
                foreach($models as $model) {
                        $row['id'] = $model['cat_id'];
                        $row['text'] = $model['cat_name'];
                        $row['children'] = ToolCategory::getChildcategories($model['cat_id']);
                        $data[] = $row;
                }
                return $data;
        }


        public static function allcategories(){
                $models = ToolCategory::model()->findAll(
                        array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL',
                              'order'=>'cat_name'
                             )
                        );
                $data = array();
                foreach($models as $model) {
                        $row['id'] = $model->cat_id;
                        $row['text'] = $model->cat_name; //CHtml::link($model->cat_name, array('place/view', 'id' => $model->cat_id));
                        $row['children'] = ToolCategory::getChildcategories($model->cat_id);
                        $data[] = $row;
                }
                return $data;
        }



        public function services($id){

            $tblpx = Yii::app()->db->tablePrefix;

            $models = $qryres = Yii::app()->db->createCommand("SELECT * from {$tblpx}preventive_request  where tool_id = " . $id ." and status = 'pending' ")->queryAll();

           if(!empty($models) and Yii::app()->user->role == 10){
              return "Request sent";
           }else if(!empty($models)){
              return "Request pending";
           }


        }


        public function getLoceType(){

            $tblpx = Yii::app()->db->tablePrefix;
            $models = $qryres = Yii::app()->db->createCommand("SELECT * from {$tblpx}location_assigned  where user_id = " . Yii::app()->user->id )->queryRow();
            if(!empty($models)){

             return $models['site_id'];
            }


        }

        public function getremqty($tool_id,$reqid){



           $check = Yii::app()->db->createCommand("SELECT tool_id,tms_tool_transfer_items.qty as reqqty,tms_tools.qty as qty FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$reqid." and tms_tools.serialno_status = 'N'  and tms_tool_transfer_items.tool_id = ".$tool_id)->queryRow();

            $remqty = $check['qty'] - $check['reqqty'];
            if($check['tool_id']!=''){
              return $remqty;
            }else{
               $toolqty=   Tools::model()->findByPk($tool_id);
               return $toolqty->qty;
            }

        }

        public function getlocationsts($tool_id,$item_status){
            

             $location =ToolTransferItems::model()->find('tool_id ='.$tool_id.' and item_current_status = "missing" ');

            
               $toollocid = Tools::model()->findByPk($tool_id);
                if($toollocid->serialno_status=='Y'){
                        if($toollocid['location']=="" ){
                                return " ";
                        }else{
                            
                        $loc       =   LocationType::model()->findByPk($toollocid['location']);
                        return $loc['name'];
               }
            }else{
               
                 $location=ToolTransferItems::model()->find('tool_id ='.$tool_id.' and item_status='.$item_status);
                if($location['location']==""){
                    
                    $return_qty=ToolTransferItems::model()->find('tool_id ='.$tool_id.' and return_qty IS NOT NULL and approved_qty IS NOT NULL');
                   
                    if(count($return_qty)>0){
                        $loc_tool=Tools::model()->findByPk($tool_id);
                        $loc       =   LocationType::model()->findByPk($loc_tool['location']);
                        return $loc['name'];                    }
                }else{
                 $loc       =   LocationType::model()->findByPk($location['location']);
                 return $loc['name'];
                }
                
            }
            

        }
        
        public function getReport($id,$tool_con,$item_id,$serialno_status,$item_current_status,$tool_transfer_id){
        $qryres1 = Yii::app()->db->createCommand("SELECT q.qty,caption,tool.location,tool.parent_id FROM `tms_tools` as tool
         left join tms_tool_transfer_items as t on t.tool_id = tool.id
         left join  tms_approve_itemqty as q on  q.tool_transfer_id = t.tool_transfer_id and t.id = q.transfer_item_id
         left join tms_status as sts on sts.sid = q.physical_condition
         WHERE tool.id = ".$id)->queryAll();
        
            if($serialno_status=='Y'){
               $tools_list=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=12');
               $tools_list_22=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=22');
               $tools_list_return_30=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=30 and return_qty IS NOT NULL');
               $tools_list_30=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=30 and return_qty IS  NULL');
               $tools_list_9=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=9 and item_current_status IS NOT NULL');
               $tools_list_12=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=12');

                if(!empty($tools_list)){
                    if(!empty($tools_list_22)){
                    $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools_list_22->physical_condition)->queryRow();
                
                    $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
    
                    }
                    elseif(!empty($tools_list_return_30)){
                        $request_from=ToolTransferRequest::model()->find('id='.$tools_list_return_30->tool_transfer_id);
                        if($request_from->transfer_to=='stock'){
                            $loc=1;
                        }
                        else{
                            $loc=$request_from->request_from; 
                        }
                        $location=LocationType::model()->findByPk($loc);

                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools_list_return_30->physical_condition)->queryRow();
                
                        $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption']."(".$location->name.")".'</span><br/>';
         
                    }
                    elseif(!empty($tools_list_9)){
                
                        $data = '<span style="width:100px;display:inline-block;">'.$tools_list_9->item_current_status.'</span><br/>';
         
                    }
                    elseif(!empty($tools_list_30)){
                        $request_to=ToolTransferRequest::model()->find('id='.$tools_list_30->tool_transfer_id);
                       
                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools_list_30->physical_condition)->queryRow();

                        $location=LocationType::model()->findByPk($request_to->request_to);
                        $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].' '."(".$location->name.")".'</span><br/>';
         
                    }
                    elseif(!empty($tools_list_12)){
                      if(isset($tools_list_12->physical_condition))
                      {
                          $condition=$tools_list_12->physical_condition;
                      }else{
                          $condition=3;
                      }
                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$condition)->queryRow();
               
                       $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
         
                    }
                    
                }
            }else{
             
               
             
                $tools=ToolTransferItems::model()->find('tool_transfer_id='.$tool_transfer_id.' and tool_id='.$id);
               
                if($tools->item_status==12){
                    $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools->physical_condition)->queryRow();
                
                    $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
    
                }
                else{
                    $tools_22=ToolTransferItems::model()->find('id='.$item_id.' and tool_id='.$id.' and item_status=22');
                    $tools_return_30=ToolTransferItems::model()->find('tool_id='.$id.' and physical_condition=30 and return_qty IS NOT NULL');
                    $tool_9=ToolTransferItems::model()->find('tool_id='.$id.' and item_status=9');
                    if(!empty($tool_9)){
                        $requestStatus=RequestitemStatus::model()->find('item_id='.$tool_9->id);
                    }
                    $tools_30=ToolTransferItems::model()->find('tool_id='.$id.' and physical_condition=30 and return_qty IS  NULL');

                    if(!empty($tools_22)){
                        
                        // $approved_qty=Yii::app()->db->createCommand("SELECT * FROM `tms_approve_itemqty` WHERE `tool_transfer_id`=2544 ")->queryRow();
                       
                        $approved_qty=tmsApproveItemqty::model()->findAll('tool_transfer_id='.$tools_22->tool_transfer_id);
                        
                        if(!empty($approved_qty)){
                            $data='';  
                            foreach($approved_qty as $row){
                             $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$row->physical_condition)->queryRow();
                        
                            $data .= '<span style="width:100px;display:inline-block;">'.$qryres['caption'].' '.$row->qty.'</span><br/>';
                   
                            }
                        }    
                    }elseif(!empty($tools_return_30)){
                        $request_from=ToolTransferRequest::model()->find('id='.$tools_return_30->tool_transfer_id);
                        if($request_from->transfer_to=='stock'){
                            $loc=1;
                        }
                        else{
                            $loc=$request_from->request_from; 
                        }
                        $location=LocationType::model()->findByPk($loc);
                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=30")->queryRow();
                        
                         $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption']."(".$location->name.")".'</span><br/>';
                
                    }elseif(!empty($requestStatus)){
                        $data=' ';
                        if(isset($requestStatus['accepted_qty']) && $requestStatus['accepted_qty']!=0){
                           
                            $data .= '<span style="width:100px;display:inline-block;">Accepted '.$requestStatus['accepted_qty'].'</span><br/>';
                           
                        }  if(isset($requestStatus['defective_qty']) && $requestStatus['defective_qty']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Defective '.$requestStatus['defective_qty'].'</span><br/>';

                        }
                         if(isset($requestStatus['not_requested_qty']) && $requestStatus['not_requested_qty']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Not Requested '.$requestStatus['not_requested_qty'].'</span><br/>';

                        }
                        if(isset($requestStatus['missing']) && $requestStatus['missing']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Missing '.$requestStatus['missing'].'</span><br/>';

                        }
                    }elseif(!empty($tools_30)){
                        $request_to=ToolTransferRequest::model()->find('id='.$tools_30->tool_transfer_id);
                        $location=LocationType::model()->findByPk($request_to->request_to);

                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=30")->queryRow();
                        
                        $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption']."(".$location->name.")".'</span><br/>';
               
                    }else{
                        if(isset($tools->physical_condition))
                        {
                            $condition=$tools->physical_condition;
                        }else{
                            $condition=3;
                        }
                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$condition)->queryRow();
                        
                        $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
               
                    }
                }
$data.=$item_id;
            }
            return $data;
        }

      public function getnewStatus($id,$tool_con,$serialno_status,$item_current_status,$tool_transfer_id){
          
        $tools = Tools::model()->findByPk($id);
        $qryres1 = Yii::app()->db->createCommand("SELECT q.qty,caption,tool.location,tool.parent_id FROM `tms_tools` as tool
         left join tms_tool_transfer_items as t on t.tool_id = tool.id
         left join  tms_approve_itemqty as q on  q.tool_transfer_id = t.tool_transfer_id and t.id = q.transfer_item_id
         left join tms_status as sts on sts.sid = q.physical_condition
         WHERE q.status ='1' AND tool.id = ".$id)->queryAll();
        $qryresult = Yii::app()->db->createCommand("SELECT tools.qty,status.caption FROM tms_status as status LEFT JOIN tms_tools as tools on tools.tool_condition=status.sid WHERE tools.id=".$id)->queryRow();
       
         
              $qryres = $qryresult;
       
              $htm = '';
                             $htm .= '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><span>'.$qryres['qty'].'</span><br/>';
            

              if(!empty($qryres1)){
                  $data =  $htm;
              }else{
                if($tools->location ==''){
                    if($tools->tool_condition==30){
                        $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools->tool_condition)->queryRow();
                    $data= $qryres['caption'];
                    }
                    else{
                  $data = 'Missing';
                  }
                }else{
                  if($tools->serialno_status == 'Y'){
                    $res =ToolTransferItems::model()->find('tool_id ='.$id.' and item_status=29');
                    $tools=Tools::model()->findByPk($id);
                      
                      if(isset($res->item_current_status)){
                          $data= $res->item_current_status;
                      }else{
                    $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tools->tool_condition)->queryRow();
                    $data= $qryres['caption'];
                    }
                  }else{
                    $res =ToolTransferItems::model()->find('tool_id ='.$id.' and tool_transfer_id='.$tool_transfer_id);
                    $return_status=Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE  return_qty is not null and tool_id=".$id)->queryRow();
                    $requestStatus=RequestitemStatus::model()->find('item_id='.$res->id);
                    if(count($qryres1)){
                        
                    if(count($requestStatus)>0){
                        if(count($return_status)>1){
                       
                            $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$return_status['physical_condition'])->queryRow();
                            // <span>'.$tools->qty.'</span>
                    
                        $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';

                        }else{
                        $data=' ';
                        if(isset($requestStatus['accepted_qty']) && $requestStatus['accepted_qty']!=0){
                           
                            $data .= '<span style="width:100px;display:inline-block;">Accepted'.$requestStatus['accepted_qty'].'</span><br/>';
                           
                        }  if(isset($requestStatus['defective_qty']) && $requestStatus['defective_qty']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Defective'.$requestStatus['defective_qty'].'</span><br/>';

                        }
                         if(isset($requestStatus['not_requested_qty']) && $requestStatus['not_requested_qty']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Not Requested'.$requestStatus['not_requested_qty'].'</span><br/>';

                        }
                        if(isset($requestStatus['missing']) && $requestStatus['missing']!=0){
                            $data .= '<span style="width:100px;display:inline-block;">Missing'.$requestStatus['missing'].'</span><br/>';

                        }
                        
                    }
                }else{
                    $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$res->physical_condition)->queryRow();
                    // <span>'.$tools->qty.'</span>
                    
                     $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
                    }
                } 
                else{ $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$qryres1['caption'])->queryRow();
                // <span>'.$tools->qty.'</span>
        
            $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
                }
             
                    
                  }

                }
              }



              // if($tools->qty!='' && $tools->location!=''){
              //
              //   $data =  $htm;
              // }else if($tools->location ==''){
              //   $data = 'Missing';
              // }else{
              //   echo "sdsds";
              //   if($tools->serialno_status == 'Y'){
              //     echo "dd";
              //     $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tool_con)->queryRow();
              //     $data= $qryres['caption'];
              //   }else{
              //     $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$tool_con)->queryRow();
              //     // <span>'.$tools->qty.'</span>
              //     $data = '<span style="width:100px;display:inline-block;">'.$qryres['caption'].'</span><br/>';
              //   }
              //
              // }
              return $data;
      }


      public function search1($page, $loc = '',$type = '',$id ='') {
          // Warning: Please modify the following code to remove attributes that
          // should not be searched.



          $criteria = new CDbCriteria;



          // $criteria->with = array('toolCategory');
          // $criteria->together = true;

           //echo $this->tool_category;exit;

          $criteria->compare('id', $this->id);
          $criteria->compare('tool_code', $this->tool_code, true);
          $criteria->compare('tool_name', $this->tool_name, true);
          $criteria->compare('tool_category', $this->tool_category);
          $criteria->compare('unit', $this->unit);
          $criteria->compare('make', $this->make, true);
          $criteria->compare('model_no', $this->model_no, true);
          $criteria->compare('serial_no', $this->serial_no, true);
          $criteria->compare('ref_no', $this->ref_no, true);
          $criteria->compare('batch_no', $this->batch_no, true);
          $criteria->compare('created_by', $this->created_by);
          $criteria->compare('created_date', $this->created_date, true);
          $criteria->compare('updated_by', $this->updated_by);
          $criteria->compare('updated_date', $this->updated_date, true);
          $criteria->compare('warranty_date',$this->warranty_date);
          $criteria->compare('t.location', $this->location);
          $criteria->compare('qty', $this->qty);
          //$criteria->compare('tool_condition', $this->tool_condition);
          $criteria->compare('prev_hrs', $this->prev_hrs);
          $criteria->compare('serialno_status', $this->serialno_status);
          //$criteria->compare('prev_date', $this->prev_date);
          $criteria->compare('prev_main', $this->prev_main);
          $criteria->compare('stock_qty', $this->stock_qty);
          $criteria->compare('vendor_request_id', $this->vendor_request_id);
          $criteria->compare('item.item_status', $this->item_status);
          $criteria->compare('item.return_qty', $this->return_qty);
          $criteria->compare('item.id', $this->item_id);

          $criteria->compare('t.active_status', $this->active_status);
          $criteria->select = 't.*,item.id as item_id,item.qty,item.location,item.item_current_status,item.item_status,item.return_qty,item.tool_transfer_id';

          $criteria->join = 'RIGHT JOIN tms_tool_transfer_items as item ON t.id=item.tool_id';
          $result =Tools::model()->findAll($criteria);
          foreach($result as $res){
        if($res['serialno_status']=='Y'){
          $criteria->addCondition('(t.qty >=0 ) and ( item.item_status=12)' );
        }
        else{
            $criteria->addCondition('(t.qty >=0 ) and ((item.item_status=9 || item.item_status=12 || (item.item_status=29) and item.return_qty IS NULL))' );
        
        }
    }


          if($this->tool_status == 'Missing'){
            
            $criteria->addCondition('t.location IS NULL');
          }else{
            if($this->tool_status == 'Breakdown'){
              $criteria->select = 't.*,aprov.physical_condition';
              $criteria->join = 'INNER JOIN tms_tool_transfer_items as item ON t.id=item.tool_id INNER JOIN tms_approve_itemqty as aprov ON aprov.transfer_item_id=item.id';
              $criteria->group='t.id';
              $criteria->addCondition("t.tool_condition=2");
            }else if($this->tool_status == 'Damage'){
               
              $criteria->select = 't.*,aprov.physical_condition';
              $criteria->join = 'INNER JOIN tms_tool_transfer_items as item ON t.id=item.tool_id INNER JOIN tms_approve_itemqty as aprov ON aprov.transfer_item_id=item.id';
              $criteria->group='t.id';
              $criteria->addCondition("t.tool_condition=1");
            } else if($this->tool_status == 'Running'){
                
              $tool_details =   Yii::app()->db->createCommand("SELECT tool_id,tms_approve_itemqty.physical_condition FROM `tms_approve_itemqty`
              left join tms_tool_transfer_items on tms_tool_transfer_items.tool_transfer_id = tms_approve_itemqty.tool_transfer_id and tms_tool_transfer_items.id = tms_approve_itemqty.transfer_item_id
              where tms_approve_itemqty.physical_condition = 2 OR tms_approve_itemqty.physical_condition = 1
              group by tool_id")->queryAll();

              $tool_id = array();
              if(!empty($tool_details)){
                foreach($tool_details as $key=>$value){
                  $tool = Tools::model()->findByPk($value['tool_id']);
                  $tool_ids = '';
                  if($tool->serialno_status == 'Y'){
                    array_push($tool_id,$value['tool_id']);
                  }
                }
                   $tool_ids = implode(",",$tool_id);
               }
               $criteria->select = 't.*';
               $criteria->join = 'LEFT JOIN tms_tool_transfer_items as item ON t.id=item.tool_id';
               $criteria->group='t.id';
               if(!empty($tool_details)){ 
                 if($tool_ids !=''){
                   $criteria->addCondition("t.id NOT IN (".$tool_ids.") AND t.location IS NOT NULL");
                  }
               }else{
                  $criteria->addCondition("t.location IS NOT NULL");
               }

            }
          }

          if(!empty($this->prev_date)){

            $date = explode('-', $this->prev_date);
            $date =array_reverse($date);
            $date=implode('-', $date);
            $criteria->addCondition("date(`prev_date`) = '".$date."' ");
          }




         /* if($this->status != ''){
         //echo $this->status;
      $res = Tools::model()->getcatids($this->status);

      $arr = array();
      foreach($res as $r){
      array_push($arr,$r['tool_id']);
       }


      if(!empty($res)){
      $criteria->addCondition('id IN  ('.implode(',',$arr).')');
      }

      } */


          /*
          if ($loc) {
              $criteria->compare('location', $loc);
          } else {
              $criteria->compare('location', $this->location);
          }

           */
          $sort = new CSort();
          $sort->attributes = array(

              'parent_category'=>array(
                                  'asc'=>'toolCategory.parent_id',
                                  'desc'=>'toolCategory.parent_id desc',
                          ),
              'child' => array(
                                  'asc'=>'toolCategory.parent_id',
                                  'desc'=>'toolCategory.parent_id desc',
                          ),
              'subchild' => array(
                                  'asc'=>'toolCategory.parent_id',
                                  'desc'=>'toolCategory.parent_id desc',
                          ),
                       '*',

                  );
                 $sort->defaultOrder = 'id DESC';

         /* return new CActiveDataProvider($this, array(
              'criteria' => $criteria,
              'sort' =>  $sort,
              'pagination' => array(
               'pageSize' => 100,
              ),

          ));*/


          $dp = new CActiveDataProvider($this, array(
              'criteria' => $criteria,
              'sort' =>  $sort,
        'pagination' => array(
               'pageSize' => 50,
              ),

          ));



              // $transferlocations = Yii::app()->db->createCommand("SELECT tms_tools_location.id as toollocid,tool_name,tool_category,tool_condition,tms_tools_location.qty,tms_tools_location.location,ref_no,tool_code,unit,make,model_no,serial_no,batch_no,tms_tools.created_by,tms_tools.created_date,tms_tools.updated_by,tms_tools.updated_date,pending_status,ref_value,item_id,tms_tools.active_status,warranty_date,prev_main,prev_date,prev_hrs,serialno_status FROM `tms_tools_location` join tms_tools on tms_tools.id = tms_tools_location.tool_id  left join tms_tool_category on tms_tool_category.cat_id = tms_tools.tool_category")->queryAll();


          $dp->setTotalItemCount(count($this->findAll($criteria)));
          return $dp;
      }

}