<?php

/**
 * This is the model class for table "{{dispatch_tool}}".
 *
 * The followings are the available columns in table '{{dispatch_tool}}':
 * @property integer $dt_id
 * @property integer $request_id_fk
 * @property string $date
 * @property integer $mode_of_transport
 * @property string $mode_name
 * @property string $person_name
 * @property string $comment
 * @property integer $created_by
 * @property string $created_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property ToolTransferRequest $requestIdFk
 */
class DispatchTool extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DispatchTool the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dispatch_tool}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment', 'required'),
			array('request_id_fk, mode_of_transport, created_by', 'numerical', 'integerOnly'=>true),
			array('mode_name, person_name', 'length', 'max'=>150),
			array('date, created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('dt_id, request_id_fk, date, mode_of_transport, mode_name, person_name, comment, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'requestIdFk' => array(self::BELONGS_TO, 'ToolTransferRequest', 'request_id_fk'),
			'modeOfTransport'=>array(self::BELONGS_TO, 'Status', 'mode_of_transport')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dt_id' => 'Dt',
			'request_id_fk' => 'Request Id Fk',
			'date' => 'Date',
			'mode_of_transport' => 'Mode Of Transport',
			'mode_name' => 'Mode Name',
			'person_name' => 'Person Name',
			'comment' => 'Comment',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dt_id',$this->dt_id);
		$criteria->compare('request_id_fk',$this->request_id_fk);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('mode_of_transport',$this->mode_of_transport);
		$criteria->compare('mode_name',$this->mode_name,true);
		$criteria->compare('person_name',$this->person_name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}