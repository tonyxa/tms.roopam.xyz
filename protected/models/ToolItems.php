<?php

/**
 * This is the model class for table "{{tool_items}}".
 *
 * The followings are the available columns in table '{{tool_items}}':
 * @property integer $id
 * @property integer $tool_allocated_id
 * @property string $code
 * @property string $item_name
 * @property integer $unit
 * @property string $serial_no
 * @property integer $duration_in_days
 * @property integer $qty
 * @property integer $late_return_fee
 *
 * The followings are the available model relations:
 * @property ToolAllocation $toolAllocated
 * @property Unit $unit0
 */
class ToolItems extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ToolItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tool_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, item_name,unit,qty,late_return_fee', 'required'),
			array('tool_allocated_id, unit, duration_in_days, qty, late_return_fee', 'numerical', 'integerOnly'=>true),
			array('code, item_name, serial_no', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tool_allocated_id, code, item_name, unit, serial_no, duration_in_days, qty, late_return_fee', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'toolAllocated' => array(self::BELONGS_TO, 'ToolAllocation', 'tool_allocated_id'),
			'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tool_allocated_id' => 'Tool Allocated',
			'code' => 'Code',
			'item_name' => 'Item Name',
			'unit' => 'Unit',
			'serial_no' => 'Serial No',
			'duration_in_days' => 'Duration In Days',
			'qty' => 'Qty',
			'late_return_fee' => 'Late Return Fee',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tool_allocated_id',$this->tool_allocated_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('duration_in_days',$this->duration_in_days);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('late_return_fee',$this->late_return_fee);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
