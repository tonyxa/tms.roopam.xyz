<?php

/**
 * This is the model class for table "{{view_toolreport}}".
 *
 * The followings are the available columns in table '{{view_toolreport}}':
 * @property integer $id
 * @property string $tool_code
 * @property string $tool_name
 * @property integer $tool_category
 * @property integer $unit
 * @property string $make
 * @property string $model_no
 * @property string $serial_no
 * @property string $ref_no
 * @property string $batch_no
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $pending_status
 * @property integer $location
 * @property integer $ref_value
 * @property double $qty
 * @property integer $stock_qty
 * @property integer $item_id
 * @property integer $tool_condition
 * @property string $active_status
 * @property string $warranty_date
 * @property string $prev_main
 * @property double $prev_hrs
 * @property string $prev_date
 * @property string $serialno_status
 */
class ViewToolreport extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewToolreport the static model class
	 */

	 public $type = '';
	 public $parent_category;
	 public static $data = '';
	 public $child;
	 public $subchild;
	 public $subchild2;
	 public $count;
	 public $statuscaption;
	 public $status;
	 public $prev_type;
	 public $tool_type;
	 public $tool_category;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{view_toolreport}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_date', 'required'),
			array(' tool_category, unit, created_by, updated_by, pending_status, location, ref_value, stock_qty, item_id, tool_condition', 'numerical', 'integerOnly'=>true),
			array('qty, prev_hrs', 'numerical'),
			array('tool_code, tool_name, model_no, serial_no, ref_no, batch_no', 'length', 'max'=>50),
			array('make', 'length', 'max'=>100),
			array('active_status, prev_main, serialno_status', 'length', 'max'=>1),
			array('updated_date, warranty_date, prev_date, parent_category', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tool_code, tool_name, tool_category, unit, make, model_no, serial_no, ref_no, batch_no, created_by, created_date, updated_by, updated_date, pending_status, location, ref_value, qty, stock_qty, item_id, tool_condition, active_status, warranty_date, prev_main, prev_hrs, prev_date, serialno_status, parent_category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

			return array(
					'stockDetails' => array(self::HAS_MANY, 'StockDetails', 'tool_id'),
					'location0' => array(self::BELONGS_TO, 'LocationType', 'location'),
					'toolCategory' => array(self::BELONGS_TO, 'ToolCategory', 'tool_category'),
					'pendingStatus' => array(self::BELONGS_TO, 'Status', 'pending_status'),
					'item' => array(self::BELONGS_TO, 'PurchaseItems', 'item_id'),
					'unit0' => array(self::BELONGS_TO, 'Unit', 'unit'),
					'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
					'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),

			);

	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tool_code' => 'Tool Code',
			'tool_name' => 'Tool Name',
			'tool_category' => 'Tool Category',
			'unit' => 'Unit',
			'make' => 'Make',
			'model_no' => 'Model No',
			'serial_no' => 'Serial No',
			'ref_no' => 'Ref No',
			'batch_no' => 'Batch No',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'pending_status' => 'Pending Status',
			'location' => 'Location',
			'ref_value' => 'Ref Value',
			'qty' => 'Qty',
			'stock_qty' => 'Stock Qty',
			'item_id' => 'Item',
			'tool_condition' => 'Tool Condition',
			'active_status' => 'Active Status',
			'warranty_date' => 'Warranty Date',
			'prev_main' => 'Prev Main',
			'prev_hrs' => 'Prev Hrs',
			'prev_date' => 'Prev Date',
			'serialno_status' => 'Serialno Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		// $criteria->with = array('toolCategory');
		// $criteria->together = true;
    $criteria->select = 't.id,tool_name,tool_category,ref_no,location,tool_condition,toolCategory.cat_id,sum(qty) as qty';
    $criteria->join = 'LEFT JOIN tms_tool_category toolCategory ON t.tool_category = toolCategory.cat_id';
		$criteria->compare('id',$this->id);
		$criteria->compare('tool_code',$this->tool_code,true);
		$criteria->compare('tool_name',trim($this->tool_name),true);
		$criteria->compare('tool_category',$this->tool_category);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('make',$this->make,true);
		$criteria->compare('model_no',$this->model_no,true);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('ref_no', trim($this->ref_no),true);
		$criteria->compare('batch_no',$this->batch_no,true);
		$criteria->compare('t.created_by',$this->created_by);
		$criteria->compare('t.created_date',$this->created_date,true);
		$criteria->compare('t.updated_by',$this->updated_by);
		$criteria->compare('t.updated_date',$this->updated_date,true);
		$criteria->compare('pending_status',$this->pending_status);
		$criteria->compare('location',$this->location);
		$criteria->compare('ref_value',$this->ref_value);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('stock_qty',$this->stock_qty);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('tool_condition',$this->tool_condition);
		$criteria->compare('active_status',$this->active_status,true);
		$criteria->compare('warranty_date',$this->warranty_date,true);
		$criteria->compare('prev_main',$this->prev_main,true);
		$criteria->compare('prev_hrs',$this->prev_hrs);
		$criteria->compare('prev_date',$this->prev_date,true);
		$criteria->compare('serialno_status',$this->serialno_status,true);



			if(!empty($this->parent_category) && !empty($this->child) && !empty($this->subchild)){
			$criteria->compare('toolCategory.cat_id',$this->subchild);
			}elseif(!empty($this->parent_category) && !empty($this->child) && empty($this->subchild)){

				$criteria->addCondition('toolCategory.parent_id IN  ('.$this->child.') or toolCategory.cat_id IN  ('.$this->child.')');
			}elseif(!empty($this->parent_category) && empty($this->child) && empty($this->subchild)){

					$list = $this->getParentcategory1();
					if(!empty($list)){

							$criteria->addCondition('toolCategory.parent_id IN  ('.implode(',',$list).') or toolCategory.parent_id IN  ('.$this->parent_category.')');
					 }else{
						 $criteria->addCondition('toolCategory.cat_id IN  ('.$this->parent_category.')');
					 }

			}else{

			}

			$criteria->group = "t.location,t.ref_no,t.tool_category,t.tool_name,t.tool_condition";

			$sort = new CSort();
			$sort->attributes = array(

					 'parent_category'=>array(
										'asc'=>'toolCategory.parent_id',
										'desc'=>'toolCategory.parent_id desc',
						),
					'child' => array(
										'asc'=>'toolCategory.parent_id',
										'desc'=>'toolCategory.parent_id desc',
						),
					'subchild' => array(
									 'asc'=>'toolCategory.parent_id',
								  	'desc'=>'toolCategory.parent_id desc',
					  ),
						'*',

					 );
					 $sort->defaultOrder = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' =>  $sort,
       'pagination' => array(
			 'pageSize' => 50,
			),
		));
	}

	public static function getmainParentname($id){

					$model = ToolCategory::model()->findByPk($id);
					if(!is_null($model)){
						if(!is_null($model->parent_id)){
							$submodel = ToolCategory::model()->findByPk($model->parent_id);

							 if(!is_null($submodel->parent_id)){
								 $subchildmodel = ToolCategory::model()->findByPk($submodel->parent_id);
								 return $subchildmodel->cat_name;
							 }else{
									return $submodel->cat_name;
							 }
						 }else{
							 return $model->cat_name;
						 }
						}else{
								return '<b style="color:red">NONE</b>';
						}

	}

	public static function getsubCategoryname($id) {


			$model = ToolCategory::model()->findByPk($id);
			if(!is_null($model)){
				if(!is_null($model->parent_id)){

					$submodel = ToolCategory::model()->findByPk($model->parent_id);
					if(!is_null($submodel->parent_id)){
						return $submodel->cat_name;
					}else{
						return $model->cat_name;
					}
				}else{
					return '<b style="color:red">NONE</b>';
				}
			}else{
				return '<b style="color:red">NONE</b>';
			}


	}

	 public static function getsubchildCategoryname($id){

					 $model = ToolCategory::model()->findByPk($id);
					 if(!is_null($model)){
						 if(!is_null($model->parent_id)){
							 $submodel = ToolCategory::model()->findByPk($model->parent_id);
									if(!is_null($submodel->parent_id)){
										$subchildmodel = ToolCategory::model()->findByPk($submodel->parent_id);
									}
							 }
						 }else{
								 return $model->cat_name;
						 }


					 if(!isset($subchildmodel)){
							 return '<b style="color:red">NONE</b>';
					 }else{
							 return $model->cat_name;
					 }
	 }

	 public function getlocationsts($tool_id,$locid){

				$location =ToolTransferItems::model()->find('tool_id ='.$tool_id.' and item_current_status = "missing" ');

				if($location['item_current_status']!=''){
					 return $location['item_current_status'];
				}else{

					$loc       =   LocationType::model()->findByPk($locid);
					return $loc['name'];
			 }

	 }

	 public function getStatus($id){

       $qryres = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$id)->queryRow();
         return $qryres['caption'];
  }

	public function getParentcategory1(){
			$list = array();
			// $list[] = $this->parent_category;
			 $qryres = Yii::app()->db->createCommand()
									->select('*')
									->from('tms_tool_category')
									->where('parent_id=:pid', array(':pid' => $this->parent_category))
									->queryAll();
					foreach ($qryres as $key => $value) {
					 $list[] = $value['cat_id'];
					}
					return $list;
	}


}
