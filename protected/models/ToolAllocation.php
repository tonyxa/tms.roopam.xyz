<?php


/**
 * This is the model class for table "{{tool_allocation}}".
 *
 * The followings are the available columns in table '{{tool_allocation}}':
 * @property integer $id
 * @property string $allocation_no
 * @property string $allocation_date
 * @property integer $allocated_site
 * @property string $allocation_stock
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property WorkSite $allocatedSite
 * @property Users $updatedBy
 * @property Users $createdBy
 * @property ToolItems[] $toolItems
 */
class ToolAllocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ToolAllocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tool_allocation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('allocation_no', 'required', 'message' => 'Please Enter {attribute}.'),
			array('allocated_site,allocation_date', 'required', 'message' => 'Please Choose {attribute}.'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('allocation_no', 'length', 'max'=>50),
			array('allocation_no', 'unique'),
			array('allocation_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, allocation_no, allocation_date, allocated_site,created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'allocatedSite' => array(self::BELONGS_TO, 'WorkSite', 'allocated_site'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'toolItems' => array(self::HAS_MANY, 'ToolItems', 'tool_allocated_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'allocation_no' => 'Allocation No',
			'allocation_date' => 'Allocation Date',
			'allocated_site' => 'Allocated Site',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('allocation_no',$this->allocation_no,true);
		$criteria->compare('allocation_date',$this->allocation_date,true);
		$criteria->compare('allocated_site',$this->allocated_site);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'id DESC',
            ),
		));
	}
}
