<?php

/**
 * This is the model class for table "{{accesslog}}".
 *
 * The followings are the available columns in table '{{accesslog}}':
 * @property integer $logid
 * @property integer $sqllogid
 * @property integer $empid
 * @property string $log_time
 * @property integer $status
 */
class Accesslog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Accesslog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{accesslog}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sqllogid, empid, log_time', 'required'),
			array('sqllogid, empid, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('logid, sqllogid, empid, log_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'logid' => 'Logid',
			'sqllogid' => 'Sqllogid',
			'empid' => 'Empid',
			'log_time' => 'Log Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('logid',$this->logid);
		$criteria->compare('sqllogid',$this->sqllogid);
		$criteria->compare('empid',$this->empid);
		$criteria->compare('log_time',$this->log_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}