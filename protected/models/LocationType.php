<?php

/**
 * This is the model class for table "{{location_type}}".
 *
 * The followings are the available columns in table '{{location_type}}':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $mob
 * @property string $email
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $location_type
 */
class LocationType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LocationType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{location_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('created_by, updated_by, location_type', 'numerical', 'integerOnly'=>true),
			array('code, name', 'length', 'max'=>50),
                        array('name', 'unique'),
			array('phone, mob','length', 'max'=>10),
                        array('phone, mob','numerical'),
			array('email','length', 'max'=>40),
                        array('email','email'),
			array('location_type,phone1,phone2,address, created_date, updated_date,active_status', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, code, name,phone1,phone2, address, phone, mob, email, created_by, created_date, updated_by, updated_date, location_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'locationType' => array(self::BELONGS_TO, 'Status', 'location_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'name' => 'Name',
			'address' => 'Address',
			'phone' => 'Phone',
			'mob' => 'Mobile',
			'email' => 'Email',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'location_type' => 'Location Type',
                        'phone1' => 'Phone 1',
                        'phone2' => 'Phone 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($page)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
            
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mob',$this->mob,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('location_type',$this->getLocation($page));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getLocation($page){
           if($page == 'Vendor'){
           $qryres = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption=:caption', array(':type'=>'location_type',':caption'=>'Vendor'))           
                        ->queryRow();    
           }else if($page == 'Site'){
           $qryres = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption=:caption', array(':type'=>'location_type',':caption'=>'Site'))           
                        ->queryRow();     
           }else{
               
           } 
           
           return $qryres['sid'];
            
        }
}