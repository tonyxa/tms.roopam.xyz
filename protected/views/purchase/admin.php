<?php
/* @var $this PurchaseController */
/* @var $model Purchase */

$this->breadcrumbs=array(
	'Purchases'=>array('index'),
	'Manage',
);

?>

<div class="clearfix">
    <div class="pull-right">
        <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2 || Yii::app()->user->role == $role) { ?>
            <?php
            echo CHtml::link('Add Purchase', $this->createAbsoluteUrl('purchase/create'), array('class' => 'btn blue'));
        }
        ?>
    </div>
    <h1>Manage Purchases</h1>
</div>
<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'purchase-grid',
	'dataProvider'=>$model->search(),
        'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
        'nextPageLabel'=>'Next ' ),
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
	'filter'=>$model,
	'columns'=>array(
		array(
			'value' => '$data->id',
			'headerHtmlOptions' => array('style' => 'display:none'),
			'htmlOptions' => array('style' => 'display:none', 'class'=>'rowId'),
			'filterHtmlOptions'=>array('style' => 'display:none'),

		),
		array(
			'value' => '$data->active_status',
			'headerHtmlOptions' => array('style' => 'display:none'),
			'htmlOptions' => array('style' => 'display:none', 'class'=>'status'),
			'filterHtmlOptions'=>array('style' => 'display:none'),

		),
		array('class' => 'IndexColumn', 'header' => 'Sl.No.'),
		//'id',
		'purchase_no',
		'purchase_date',
		'invoice_no',
		'invoice_date',
		array(
				'name' => 'supplier_name',
				'value' => 'isset($data->supplier->name)?$data->supplier->name:""',
				'type' => 'raw',
				'htmlOptions' => array('width' => '120px'),
				'filter' => CHtml::listData(LocationType::model()->findAll(
				array(
					'select' => array('id,name'),
					'condition'=>'location_type = 14 AND active_status = "1"',
					'distinct' => true
				)), "id", "name")
		),


		array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:50px;'),
            'template'=>'
                <ul name="tableaction" class="list-unstyled tableaction">
                    <li class="action_dropdown" onclick="actionList(this, event);"><a href="javascript:void(0)" class="glyphicon glyphicon-th-list" title="View Actions"></a>
                        <ul class="action_menu list-inline">
                            <li class="viewaction"><a href="javascript:void(0)" onclick="viewaction(this,event)">View</a></li>
                            <li class="updateaction"><a href="javascript:void(0)" onclick="updateaction(this,event)">Edit</a></li>
                            <li class="activeaction"><a href="javascript:void(0)" onclick="activeaction(this,event)" >Make Active</a></li>
                            <li class="inactiveaction"><a href="javascript:void(0)" onclick="inactiveaction(this,event)" >Make Inactive</a></li>
                            <li class="purchaseaction"><a href="javascript:void(0)" onclick="purchaseaction(this,event)">Purchase List</a></li>
                            <li class="toolaction"><a href="javascript:void(0)" onclick="toolaction(this,event)">Tool List</a></li>
                        </ul>
                    </li>
                </ul>'
		),















	),
)); ?>
</div>

       <div id="addPurchase" class="" role="dialog">
            <div class="">

            </div>
        </div>

<?php

Yii::app()->clientScript->registerScript('myjquery', '

		$(document).ready(function () {
		$(".createPurchase").click(function (event) {
			 event.preventDefault();
			// alert("hi");
			$.ajax({
				type: "GET",
				url:"'.Yii::app()->createUrl('purchase/create').'",
				success: function (response)
				{
					//alert(response);
					$("#addPurchase").html(response);
					$("#addPurchase").css({"display":"block"});

				}
			});
		});




		$(".editVendor").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
           // alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addPurchase").html(response);
                        $("#addPurchase").css({"display":"block"})
                    }

                });

        });




	});


//active status script starts from here
$(".changestatusicon").on("click", function (event) {

		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){

					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}

						   }
					   });

			}

       });
//active status script end here


   ');
?>


<script>
$(".action_menu").hide();

function actionList(curElem, event){
	event.stopPropagation();
	$(curElem).closest("tr").siblings().find(".action_menu").hide();
	$(curElem).children(".action_menu").toggle("slide", {direction:'right'});
	var status=$(curElem).closest("tr").find(".status").text();
	if(status == 1){
		$('.inactiveaction').show();
		$('.activeaction').hide();
	}else{
		$('.inactiveaction').hide();
		$('.activeaction').show();
	}
};
// View function

function viewaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/view&id=') ?>"+ rowid;
	window.location.href= url;
};

function updateaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/update&id=') ?>"+ rowid;
	window.location.href= url;
};

function activeaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/changestatus&id=') ?>"+ rowid;
	var answer = confirm ("Are you sure you want to change the status?");
	if (answer)
	{
		$.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function (response)
		{
			if(response.response == "success"){

				location.reload();
			}
			else
			{
				alert("Cannot change the status! ");
			}

						 }
					 });

		}
};
function inactiveaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/changestatus&id=') ?>"+ rowid;
	var answer = confirm ("Are you sure you want to change the status?");
	if (answer)
	{
		$.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function (response)
		{
			if(response.response == "success"){

				location.reload();
			}
			else
			{
				alert("Cannot change the status! ");
			}

						 }
					 });

		}
};
function purchaseaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/purchaselist&id=') ?>"+ rowid;
	window.location.href= url;
};
function toolaction(elem,event){
	event.stopPropagation();
	var rowid= $(elem).closest("tr").find(".rowId").text();
	var url ="<?php echo $this->createUrl('purchase/toollist&id=') ?>"+ rowid;
	window.location.href= url;
};

</script>
<style>
.action_dropdown{position:relative;text-align:center;}
    .action_menu{

        position: absolute;
        border-radius: 4px;
        right: 40px;
        top: -5px;
        border: 1px solid #ccc;
        background-color: #4d5965;
        white-space: nowrap;
        display:none;
    }
    .action_menu li{padding: 0px;}
    .action_menu li a{color: #fff;line-height: 18px;text-decoration: underline;  height: 30px; display: block; padding: 6px;}
    .action_menu li a:hover {text-decoration: none;}
    ul.action_menu.list-inline:after {
        content: '';
        position: absolute;
        display: block;
        border-top: 6px solid transparent;
        border-left: 6px solid #4d5965;
        border-bottom: 6px solid transparent;
        right: -6px;
        top: 8px;
      }
    /*.actioncount{min-width: 190px;}*/
    .tableaction{margin:0px;}

</style>
