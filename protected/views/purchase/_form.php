<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'purchase-form',
    'action' =>'#',
//	'enableAjaxValidation' => true,
//	'enableClientValidation' => true,
//	'clientOptions' => array(
//		'validateOnSubmit' => true,
//		'validateOnChange' => true,
//		'validateOnType' => false,),
        ));
?>



        <input type="hidden" name="check_uniqueallocation" id="check_uniqueallocation" value="<?php echo ($model->isNewRecord) ? 0 : $model->id; ?>">

        <div class="row">
            <div class="col-md-2">
            <!-- !$model->isNewRecord -->
                <?php echo $form->labelEx($model, 'purchase_no'); ?>
                <?php echo $form->textField($model, 'purchase_no', array('class' => 'form-control','readonly' => !$model->isNewRecord)); ?>
                <?php echo $form->error($model, 'purchase_no'); ?>
            </div>

            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'purchase_date'); ?>
       <?php
       if(!$model->isNewRecord){
       echo $form->textField($model,'purchase_date',array("id" => "purchasedate", "size" => "15",'class' => 'form-control','readonly'=>false));
       }else{
       echo CHtml::activeTextField($model, 'purchase_date', array("size" => "15", 'class' => 'form-control','readonly' => false));

                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Purchase_purchase_date',
                    'ifFormat' => '%Y-%m-%d',
                ));

       }
       ?>


                <?php echo $form->error($model, 'purchase_date'); ?>



            </div>




<div class="col-md-2">
<?php echo $form->labelEx($model, 'invoice_no'); ?>
<?php echo $form->textField($model, 'invoice_no', array('class' => 'form-control','readonly' => false)); ?>
<?php echo $form->error($model, 'invoice_no'); ?>
</div>

<div class="col-md-2">
<?php echo $form->labelEx($model, 'invoice_date'); ?>
<?php
if(!$model->isNewRecord){
echo $form->textField($model,'invoice_date',array('class' => 'form-control','readonly' => false));
}else{
 echo CHtml::activeTextField($model, 'invoice_date', array("size" => "15", 'class' => 'form-control'));

$this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Purchase_invoice_date',
                    'ifFormat' => '%Y-%m-%d',
                ));

}
 ?>
<?php echo $form->error($model, 'invoice_date'); ?>
     </div>



            <div class="col-md-2">
                <?php echo $form->labelEx($model, 'supplier_name'); ?>
                <?php // echo $form->textField($model, 'supplier_name', array('class' => 'form-control','readonly' => false,'value' =>$model->supplier->name)); ?>
                <?php
                echo CHtml::activeDropDownList($model, 'supplier_name', CHtml::listData(LocationType::model()->findAll(
                                        array(
                                            'select' => array('id,name'),
                                            'condition' => 'location_type = 14 AND active_status = "1"',
                )), "id", "name"), array('empty' => 'Select Supplier', 'class' => 'form-control'));
                ?>
                <?php echo $form->error($model, 'supplier_name'); ?>
            </div>
                <?php /* <div class="col-md-6">
                  <?php echo $form->labelEx($model,'created_by'); ?>
                  <?php echo $form->textField($model,'created_by',array('class' => 'form-control')); ?>
                  <?php echo $form->error($model,'created_by'); ?>
                  </div> */ ?>

                  <?php // if ($model->isNewRecord) {   ?>
                    <div class="col-md-2 text-right save-btnHold">
                    <?php 
                    
                    echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm blue purchase-save')); ?>
                          <!-- <button data-dismiss="modal" class="btn default btn-sm" onclick="javascript:window.location.reload()">Cancel</button> -->
                        <?php 
                        if($model->isNewRecord){
                            echo CHtml::Button($model->isNewRecord ? 'Cancel' : 'Cancel',array('class'=>'btn  btn-sm','onclick' => 'js:document.location.href="index.php?r=purchase/update&id="+'.$model->id));
                
                        }
                        else{
                            echo CHtml::Button($model->isNewRecord ? 'Cancel' : 'Cancel',array('class'=>'btn  btn-sm','onclick' => 'js:document.location.href="index.php?r=purchase/update&id="+'.$model->id)); 
                
                        }
                        
                        ?>
                        

                      </div>
                  <?php  //} ?>
        </div>

        <?php $this->endWidget(); ?>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'item-form',
            'action' =>'#',
                ));
        ?>

        <div class="table-responsive "><br>
            <table class="modaltable" id="items" width="500" border="1">
<?php
if (!$model->isNewRecord) {
?>
                    <thead>
                    <tr>
                        <td>No.</td>
                        <td style="display:none"></td>
                        <!--<td>Item Code</td>-->
    <!--                        <td>Item Name</td>-->
                        <td>Tool Name</td>
                        <td>Unit</td>
                        <td>Model No.</td>
                        <td>Batch No.</td>
                        <td>Make</td>
                        <td>Serial No.</td>
                        <td>Warranty date</td>
                        <td>Quantity</td>
                        <td>Rate</td>
                        <td>Tax</td>
                        <td>Amount</td>
                        <td>preventive maintenance</td>
                        <td  id="hours">Hours</td>
                        <td></td>
                    </tr>
                  </thead>
                  <tbody class="addrow">
    <?php
    if (!$model->isNewRecord && !empty($newmodel)) {
    $i = 1;
    foreach ($newmodel as $new) {


        //echo $new['id'];
        //echo count($arr);

        if (!empty($new['id']) && !empty($arr)) {
            for ($k = 0; $k < count($arr); $k++) {
                if ($new['id'] == $arr[$k]) {
                    $read = 'readonly';
                    break;
                } else {
                    $read = '';
                }
            }
        } else {
            $read = '';
        }
        ?>

                        <tr class="itemtr">
                            <td><?php echo $i; ?></td>
                            <td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="piid_<?php echo $i; ?>" value="<?php echo $new['id']; ?>" ></td>


                      <!--<td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_1" /></td>
                      <td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_1" /></td>
                      <td><input type="text" class="inputs form-control target" name="cost[]" id="cost_1" /></td> -->


        <?php /* <td><input type="text" name="item_code[]" class="inputs form-control target" id="item_code_1" value="<?php echo $new['item_code'];?>" /></td> */ ?>
                            <td><input type="text" name="item_name[]" class="inputs form-control target" id="item_name_<?php echo $i;?>" value="<?php echo $new['item_name']; ?>" <?php echo $read; ?> /></td>

                            <td> <select class="inputs form-control target" name="unit[]" id="unit_<?php echo $i;?>" <?php echo $read; ?>>
                                    <option value="">Please choose</option>
        <?php
        foreach ($unitRes as $uRes) {
            if ($uRes['id'] == $new['unit']) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            ?>
                                        <option value="<?php echo $uRes['id']; ?>" <?php echo $selected; ?>><?php echo $uRes['unitname']; ?></option>
                                    <?php } ?>
                                </select></td>

                            <td><input type="text" name="model_no[]" class="inputs form-control target" id="model_no_<?php echo $i;?>" value="<?php echo $new['model_no']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="batch_no[]" class="inputs form-control target" id="batch_no_<?php echo $i;?>" value="<?php echo $new['batch_no']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="make[]" class="inputs form-control target" id="make_<?php echo $i;?>" value="<?php echo $new['make']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_<?php echo $i;?>" value="<?php echo $new['serial_no']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="warranty_date[]" class="inputs form-control target datepicker" id="warranty_date_<?php echo $i;?>" value="<?php echo $new['warranty_date']; ?>" <?php echo $read; ?> readonly='readonly'/></td>
                            <td><input type="text" name="qty[]" onkeyup="chkinteger(this.id);"  class="inputs form-control target qty" id="qty_<?php echo $i;?>"  value="<?php echo $new['qty']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="rate[]" onkeyup="chknumber1(this.id);" class="inputs form-control target rate" id="rate_<?php echo $i;?>" value="<?php echo $new['rate']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="tax[]" onkeyup="chknumber1(this.id);" class="inputs form-control target tax" id="tax_<?php echo $i;?>" value="<?php echo $new['tax']; ?>" <?php echo $read; ?> /></td>
                            <td><input type="text" name="amount[]" readonly="true" class="inputs lst form-control amount" id="amount_<?php echo $i;?>" value="<?php echo $new['amount']; ?>" <?php echo $read; ?> /></td>

                            <td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_<?php echo $i;?>" value="" <?php if( $new['prev_main']==1){ echo "checked";} ?>></td>
                            <input type="hidden" id="prev_db_<?php echo $i;?>" value="<?= $new['prev_main']; ?>">
                            <td><input type="text" name="hrs[]" class="inputs form-control target hrs" value="<?php echo $new['prev_hrs']; ?>"  id="hrs_<?php echo $i;?>"></td>

                            <td></td>

        <?php if ($read != 'readonly') { ?>
                                <td id="delete_<?php echo $i; ?>"><input type="button" class="btn blue btn-sm savebtn" id="<?php echo $i; ?>" value="Save"></td>
        <?php  } ?>
                        </tr>
        <?php
        $i++; }
    }?>
    <?php
    $j= count($newmodel)+1;
    ?>
    <tr class="itemtr">
      <td><?php echo $j; ?></td><td style="display:none"><input class="inputs form-control" name="ids[]" id="ids<?php echo $j; ?>" type="hidden"></td>
      <td><input class="inputs form-control target1" name="item_name[]" id="item_name_<?php echo $j; ?>" type="text"></td>
      <td> <select class="inputs form-control target1" name="unit[]" id="unit_<?php echo $j;?>">
              <option value="">Please choose</option>
                  <?php
                  foreach ($unitRes as $uRes) {
                  ?>
                  <option value="<?php echo $uRes['id']; ?>"><?php echo $uRes['unitname']; ?></option>
              <?php } ?>
          </select></td>
      <td><input class="inputs form-control target1" name="model_no[]" id="model_no_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs form-control target1" name="batch_no[]" id="batch_no_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs form-control target1" name="make[]" id="make_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs form-control target1" name="serial_no[]" id="serial_no_<?php echo $j; ?>" type="text"></td>
      <td><input type="text" name="warranty_date[]" readonly='readonly' class="inputs form-control target datepicker" id="warranty_date_<?php echo $j;?>" value=""/></td>
      <td><input class="inputs form-control target1 qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs form-control target1 rate" onkeyup="chknumber1(this.id);" name="rate[]" id="rate_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs form-control target1 tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_<?php echo $j; ?>" type="text"></td>
      <td><input class="inputs lst form-control amount" name="amount[]" id="amount_<?php echo $j; ?>" readonly="true" type="text"></td>
      <td><input class="prev_main target1" name="prev_main[]" id="prev_main_<?php echo $j; ?>" value="" type="checkbox"></td>
      <td> <input name="hrs[]" class="inputs form-control hrs target1" id="hrs_<?php echo $j; ?>" style="display: none;" type="text"></td>
      <td></td>
      <?php //if ($read != 'readonly') { ?>
                                <td id="delete_<?php echo $j; ?>"><input type="button" class="btn blue btn-sm addbtn lst1" id="<?php echo $j; ?>" value="Create "></td>
        <?php // } ?>
      <!-- <td><input class="btn red btn-xs delete" data-id="3" value="Delete" type="button"></td> -->
    </tr>
    <?php

}
?>
  </tbody>
            </table>
        </div>



            <?php $this->endWidget(); ?>




<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
	});
    var i = $('#items tr').length;

//    $(document).on('keydown', '.target', function (e) {
//        var code = (e.keyCode ? e.keyCode : e.which);
//
//        if (code == 13) {
//             e.preventDefault();
//            return false;
//        }
//
//    });
// $('#hours').show();


$(document).ready(function () {
    // $('#hours').show();

     var prev_db = 0;
    for (var j = 1; j< i; j++) {

           prev_db = $('#prev_db_' + j).val();

        if(prev_db==1){
          //  $('#hours').show();
           $('#prev_main_' + j).closest('tr').find('.hrs').show();
        }else{
            // $('#hours').hide();
            $('#prev_main_' + j).closest('tr').find('.hrs').hide();
            //$('.prev_main').closest('tr').find('.hrs').hide();
        }

    }





$( function() {
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
  });

  $( document ).ajaxComplete(function() {
  $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
});

     /* new  prev maintain */

     $(document).on('change','.prev_main',function(){
              if(this.checked==true){
                 $(".prev_main").val(1);
                //  $('#hours').show();
                 $(this).closest('tr').find('.hrs').show();
                 $(this).closest('tr').find('.hrs').removeClass('check_class');
              }else{
                 $(".prev_main").val(0);
                // $('#hours').hide();

                 $(this).closest('tr').find('.hrs').hide();
                 $(this).closest('tr').find('.hrs').addClass('check_class');
              }
      });


});




    $(document).on('keypress', '.lst', function (e) {

        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {

            var oldcnt = i - 1;
            var piid = $('#piid_'+oldcnt).val();


           if(piid != undefined){
           if($('#qty_'+i).val() == ''){
           var status = 2;
           }else{
           var status =checkMandatory(i+1);
           i = i+1;
           }
           }else{
           var status = checkMandatory(i);
           }





            if(status != 1){
            return false;
            }

            html = '<tr class="itemtr">';
            html += '<td>' + i + '</td>';
            html += '<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="ids' + i + '" /></td>';
            // html += '<td><input type="text" class="inputs form-control target" name="item_code[]" id="item_code_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target" name="item_name[]" id="item_name_' + i + '" /></td>';


            html += '<td><select class="inputs form-control target" name="unit[]" id="unit_' + i + '"><option value="">Please choose</option>'
<?php foreach ($unitRes as $uRes) { ?>
                + '<option value="<?php echo $uRes['id']; ?>"><?php echo $uRes['unitname']; ?></option>'
<?php } ?>
            + '</select> </td>';
            /* <input type="text" class="inputs form-control" name="items_list_' + i + '" id="items_list_' + i + '" />*/
            html += '<td><input type="text" class="inputs form-control target" name="model_no[]" id="model_no_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target" name="batch_no[]" id="batch_no_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target" name="make[]" id="make_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target" name="serial_no[]" id="serial_no_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target datepicker" name="warranty_date[]" id="warranty_date_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target rate" onkeyup="chknumber1(this.id);"  name="rate[]" id="rate_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs lst form-control amount" name="amount[]" id="amount_' + i + '" readonly="true" /></td>';
            html += '<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_' + i + '"></td>';
            html +='<td> <input type="text" name="hrs[]" class="inputs form-control hrs target" id="hrs_' + i + '" style="display: none;"></td> ';

            html += '<td><input type="button" class="btn red btn-sm delete"  data-id="' + i + '" value="Delete"></td>';
            html += '</tr>';
            //$('#items').append(html);
            $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
            $(this).focus().select();
            i++;
            e.preventDefault();
            return false;
        }
    });
    $(document).on('click', '.lst1', function (e) {

// var code = (e.keyCode ? e.keyCode : e.which);
var code=13;
if (code == 13) {

    var oldcnt = i - 1;
    var piid = $('#piid_'+oldcnt).val();


   if(piid != undefined){
   if($('#qty_'+i).val() == ''){
   var status = 2;
   }else{
   var status =checkMandatory(i+1);
   i = i+1;
   }
   }else{
   var status = checkMandatory(i);
   }





    if(status != 1){
    return false;
    }

    html = '<tr class="itemtr">';
    html += '<td>' + i + '</td>';
    html += '<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="ids' + i + '" /></td>';
    // html += '<td><input type="text" class="inputs form-control target" name="item_code[]" id="item_code_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target" name="item_name[]" id="item_name_' + i + '" /></td>';


    html += '<td><select class="inputs form-control target" name="unit[]" id="unit_' + i + '"><option value="">Please choose</option>'
<?php foreach ($unitRes as $uRes) { ?>
        + '<option value="<?php echo $uRes['id']; ?>"><?php echo $uRes['unitname']; ?></option>'
<?php } ?>
    + '</select> </td>';
    /* <input type="text" class="inputs form-control" name="items_list_' + i + '" id="items_list_' + i + '" />*/
    html += '<td><input type="text" class="inputs form-control target" name="model_no[]" id="model_no_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target" name="batch_no[]" id="batch_no_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target" name="make[]" id="make_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target" name="serial_no[]" id="serial_no_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target datepicker" name="warranty_date[]" id="warranty_date_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target rate" onkeyup="chknumber1(this.id);"  name="rate[]" id="rate_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control target tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs lst form-control amount" name="amount[]" id="amount_' + i + '" readonly="true" /></td>';
    html += '<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_' + i + '"></td>';
    html +='<td> <input type="text" name="hrs[]" class="inputs form-control hrs target" id="hrs_' + i + '" style="display: none;"></td> ';

    html += '<td><input type="button" class="btn red btn-sm delete"  data-id="' + i + '" value="Delete"></td>';
    html += '</tr>';
    //$('#items').append(html);
    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
    $(this).focus().select();
    i++;
    e.preventDefault();
    return false;
}
});



    $(document).on('keypress', '.target', function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });



    $(document).on('keydown', '.inputs', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var index = $('.inputs').index(this) + 1;
            $('.inputs').eq(index).focus();
        }
    });
    $(document).on('click', '.delete', function () {
        var dataid = $(this).attr('data-id');
        $(this).parent().parent('tr').remove();
        AutoNumber();
        location.reload();
    });
    function AutoNumber() {
        var j = 1;
        $('.itemtr').each(function () {
            $(this).find("td:first").text(j);
            j++;
        });
    }




    $(document).on('click', '.delitem', function (event) {
        var pid = $('#check_uniqueallocation').val();
        var newthis = $(this);
        var id = $(this).attr("id");
        var answer = confirm("Are you sure you want to delete?");
        if (answer)
        {
            $.ajax({
                method: 'POST',
                url: '<?php echo Yii::app()->createUrl('purchase/deleteitem&id='); ?>' + id,
                data: {id: id,pid:pid},
                  dataType:'json',
                success: function (data) {
                    //newthis.parent().parent("tr").remove();
                    if(data.status == 1){
                        $(".addrow").html(data.html);
                        $().toastmessage('showSuccessToast', "Item  deleted successfully");
                    }else{
                        $().toastmessage('showErrorToast', "Some problem occured");
                    }
                    //location.reload();
                }
            });
        }
    });


    jQuery.validator.addMethod("unique",
            function (purchase_no, element) {
                var val = document.getElementById('check_uniqueallocation').value;               // alert(val);
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo Yii::app()->createUrl('Purchase/chkpnoexists'); ?>',
                    data: {pno: purchase_no, val: val},
                    success: function (data) {
                        result = (data == "true") ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            }
    );

//
//    $(document).ready(function () {
//        $("#workdetails").on("click", ".lst", function () {
//            var $selects = $(this).closest('tr').find('td select'),
//                    $cells = $(this).closest("tr").find("td input");
//            $("#workdetails").find("td input").removeClass("has-error");
//            $("#workdetails").find("td select").removeClass("has-error");
//            $cells.removeClass("has-error");
//            $selects.removeClass("has-error");
//            $cells.each(function () {
//                if ($(this).val().trim() === '') {
//                    flag++;
//                    $(this).addClass("has-error");
//                }
//            });
//            $selects.each(function () {
//                if ($(this).val() == '') {
//                    // flag++;
//                    $(this).addClass("has-error");
//                }
//            });
//        });
//    });


$(document).ready(function(){

$("#purchase-form").validate({
 rules: {
   'Purchase[purchase_no]':{
     required: true,
     /* remote: {
          data:{data:1},
          data: {purchase_id: $('#check_uniqueallocation').val()},
          url: "<?php echo Yii::app()->createAbsoluteUrl('purchase/uniqunes'); ?>",
          type: "post",
          complete: function(data){
              if( data.responseText == "false" ) {
                }
           }
      }, */
   },
   'Purchase[invoice_no]':{
     required: true,
   },
   'Purchase[supplier_name]':{
     required: true,
   },
   'Purchase[purchase_date]':{
     required: true,
   },
   'Purchase[invoice_date]':{
     required: true,
   },
 },
 messages: {
   'Purchase[purchase_no]':{
     required: "Purchase No. is required",
     remote: "Purchase No is unique",
   },
   'Purchase[invoice_no]':{
     required: "Invoice No. is required",
   },
   'Purchase[supplier_name]':{
     required: "Supplier Name is required",
   },
   'Purchase[purchase_date]':{
     required: "Purchase Date is required",
   },
   'Purchase[invoice_date]':{
     required: "Invoice Date is required",
   }

 }
});

$('.purchase-save').click(function() {
   if($("#purchase-form").valid()){
     var data = $("#purchase-form").serialize();
     $.ajax({
       url:"<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchase'); ?>",
       data:{data:data},
       method:'GET',
       dataType:'json',
       success: function (data) {
         console.log(data);
         if(data.status =='success'){
           $('#check_uniqueallocation').val(data.id);
           $(".table-responsive").removeClass("pointer");
           $().toastmessage('showSuccessToast', "Purchase  added successfully");
         }else{
           $().toastmessage('showErrorToast', "Some problem occured");
         }
       }

     })
   }
});

});


    $(document).ready(function () {
        $("#item-form").validate({
            rules: {
                'Purchase[purchase_no]': {
                    required: true,
                    unique: true,
                    //digits: true
                },
                'Purchase[invoice_no]': {
                    required: true

                },
                'Purchase[supplier_name]': {
                    required: true,
                },
                'Purchase[purchase_date]': {
                    required: true,
                },
                'Purchase[invoice_date]': {
                    required: true,
                },
//                'item_code[]': {
//                    required: true,
//                    //digits: true
//                },
                'item_name[]': {
                    required: true,
                    // digits: true
                },
                'unit[]': {
                    required: true,
                    digits: true
                },
//                'model_no[]': {
//                    required: true,
//                    //digits: true
//                },
                // 'batch_no[]': {
                //     required: true,
                //     //digits: true
                // },
//                'make[]': {
//                    required: true,
//                    //   digits: true
//                },
                // 'serial_no[]': {
                //     required: true,
                //     // digits: true
                // },
//                'warranty_date[]': {
//                    required: true,
//                    // digits: true
//                },
                'qty[]': {
                    required: true,
                    digits: true
                },
                'rate[]': {
                    required: true,
                    //digits: true
                },
                'tax[]': {
                    required: true,
                    //digits: true
                },
                'amount[]': {
                    required: true,
                    //digits: true
                },
              /*  'hrs[]': {
                    required: true,

                },*/

            },
            messages: {
                'Purchase[purchase_no]': {
                    required: "Purchase No. is required",
                    unique: "Request No is unique",
                },
                'Purchase[invoice_no]': {
                    required: "Invoice No. is required",
                },
                'Purchase[supplier_name]': {
                    required: "Supplier Name is required",
                },
                'Purchase[purchase_date]': {
                    required: "Purchase Date is required",
                },
                'Purchase[invoice_date]': {
                    required: "Invoice Date is required",
                },
//                'item_code[]': {
//                    required: "Required",
//                },
                'item_name[]': {
                    required: "Required",
                },
                'unit[]': {
                    required: "Required",
                },
//                'model_no[]': {
//                    required: "Required",
//                },
                // 'batch_no[]': {
                //     required: "Required",
                // },
//                'make[]': {
//                    required: "Required",
//                },
                // 'serial_no[]': {
                //     required: "Required",
                // },
//                'warranty_date[]': {
//                    required: "Required",
//                },
                'qty[]': {
                    required: "Required",
                },
                'rate[]': {
                    required: "Required",
                },
                'tax[]': {
                    required: "Required",
                },
                'amount[]': {
                    required: "Required",
                },
                /*'hrs[]': {
                    required: "Required",
                },*/


            },
            submitHandler: function () {
                var formData = JSON.stringify($("#item-form").serializeArray());


                $.ajax({
                    method: "POST",
                    data: {formdata: formdata},
                    url: '<?php echo Yii::app()->createUrl('purchase/create'); ?>',
                    success: function (data) {
                    }
                });
            }
        });


        $(document).on('change', '.rate,.tax,.qty', function (e) {
            var amount = '';var rate = '';var tax = '';var qty = '';
            rate = $(this).closest('tr').find('.rate').val();
            tax = $(this).closest('tr').find('.tax').val();
            qty = $(this).closest('tr').find('.qty').val();
            amount = (parseFloat(rate) * parseInt(qty)) + parseFloat(tax);
            if(!isNaN(amount)){
                 $(this).closest('tr').find('.amount').val(parseFloat(Math.round(amount * 100) / 100).toFixed(2));
            }else{
                $(this).closest('tr').find('.amount').val(0);
            }
        });
    });


function checkMandatory(i){

  if(i == 1){
     var j = i;
   }else{
     var j = i - 1;
   }
var item_name = $('#item_name_'+j).val();
var unit = $('#unit_'+j).val();
var model_no = $('#model_no_'+j).val();
var batch_no = $('#batch_no_'+j).val();
var make = $('#make_'+j).val();
var serial_no = $('#serial_no_'+j).val();
var warranty_date = $('#warranty_date_'+j).val();
var qty = $('#qty_'+j).val();
var rate = $('#rate_'+j).val();
var tax = $('#tax_'+j).val();
var amount = $('#amount_'+j).val();
var prev_main = $('#prev_main_'+j).val();
var prev_hrs =$('#hrs_'+j).val();
//chknumber('');

var id = $('#check_uniqueallocation').val();
var result = 2;




if(item_name != '' && unit != '' && qty != '' && rate != '' && tax != '' && amount != ''  ){
var stuff ={'id': id,'item_name':item_name,'unit':unit,'model_no':model_no,'batch_no':batch_no,'make':make,'serial_no':serial_no,'warranty_date':warranty_date,'qty':qty,'rate':rate,'tax':tax,'amount':amount,'prev_main':prev_main,'prev_hrs':prev_hrs};
 // alert(stuff);

      $.ajax({
      type: "POST",
      async: false,
      data    : {result:JSON.stringify(stuff)},
      url: '<?php echo Yii::app()->createUrl('purchase/create1'); ?>',
      dataType:'json',
      success: function (data) {
        if(data.status == 1){
          $().toastmessage('showSuccessToast', "Item  added successfully");
          $(".addrow").html(data.html).find(".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
        }else{
          $().toastmessage('showErrorToast', "Some problem occured");
        }
      result = (data == "1") ? "1" : "2";
      }
      });
      // return true if username is exist in database

      return result;
}else{
return 2;
}

}


  function chknumber1(id) {
            var val = $('#' + id).val();

            if (val != '' && val != '.') {
                if (isNaN(Number(val))) {
                    $('#' + id).val('');

                }
            }
        }



function chkinteger(id) {
            var value1 = $('#' + id).val();

            if(value1>0){
                value1 = Math.trunc(value1);
                $('#' + id).val(value1);
            }
            if (value1 != '' && value1 != '.') {
                if (isNaN(Number(value1))) {
                   $('#' + id).val();
                }
            }
        }

        $(document).on('click','.target',function() {
        resetprevious();
        var str = $(this).prop('id');
        var id = str.replace ( /[^\d.]/g, '' );
        var x = Number(""+id+"")
        if(x != '' && x != undefined){
            
        $('#delete_'+x).html('');
        $('#delete_'+x).html('<input type="button" class="btn blue btn-sm savebtn"  id="' + x + '" value="Save">');
        $('#'+str).addClass('current');
        }
});

function resetprevious(){
  var val = $('.current').attr('id');

        if(val != undefined){
        var id = val.replace ( /[^\d.]/g, '' );
        if(id != '' && id != undefined){
        var recid = $('#piid_'+id).val();
        $('#delete_'+id).html('');
        $('#delete_'+id).html('<input type="button" class="btn red btn-sm delitem" id="'+recid+'" value="Delete">');
        $('#'+val).removeClass('current');
        }
        }

}

$(document).on("click",".savebtn",function() {
    
var id = $(this).prop('id');
var recid = $('#piid_'+id).val();
var j = id;

var item_name = $('#item_name_'+j).val();
var unit = $('#unit_'+j).val();
var model_no = $('#model_no_'+j).val();
var batch_no = $('#batch_no_'+j).val();
var make = $('#make_'+j).val();
var serial_no = $('#serial_no_'+j).val();
var warranty_date = $('#warranty_date_'+j).val();
var qty = $('#qty_'+j).val();
var rate = $('#rate_'+j).val();
var tax = $('#tax_'+j).val();
var amount = $('#amount_'+j).val();
var prev_main = $('#prev_main_'+j).prop('checked'); 
// alert(prev_main);
// if(prev_main==true){
//   $('#hrs_'+j).show();
// }
// else{
//   $('#hrs_'+j).hide();
// }
var prev_hrs =$('#hrs_'+j).val();

//chknumber('');
var pid = $('#check_uniqueallocation').val();
var result = 2;

if(item_name != '' && unit != '' && qty != '' && rate != '' && tax != '' && amount != ''  ){
var stuff ={'id': recid,'pid':pid,'item_name':item_name,'unit':unit,'model_no':model_no,'batch_no':batch_no,'make':make,'serial_no':serial_no,'warranty_date':warranty_date,'qty':qty,'rate':rate,'tax':tax,'amount':amount,'prev_main':prev_main,'prev_hrs':prev_hrs};

            $.ajax({
                method: 'POST',
                async: false,
                data    : {result:JSON.stringify(stuff)},
                url: '<?php echo Yii::app()->createUrl('purchase/update1'); ?>',
                dataType:'json',
                success: function (data) {
                if(data.status == 1){
                  $(".addrow").html(data.html);
                  $().toastmessage('showSuccessToast', "Item  updated successfully");
                }else{
                  $().toastmessage('showErrorToast', "Some problem occured");
                }
                }
            });
            }

});
// $(document).on("click",".addbtn",function() {
    
//     var id = $(this).prop('id');
//     var recid = $('#piid_'+id).val();
//     var j = id;
    
//     var item_name = $('#item_name_'+j).val();
//     var unit = $('#unit_'+j).val();
//     var model_no = $('#model_no_'+j).val();
//     var batch_no = $('#batch_no_'+j).val();
//     var make = $('#make_'+j).val();
//     var serial_no = $('#serial_no_'+j).val();
//     var warranty_date = $('#warranty_date_'+j).val();
//     var qty = $('#qty_'+j).val();
//     var rate = $('#rate_'+j).val();
//     var tax = $('#tax_'+j).val();
//     var amount = $('#amount_'+j).val();
//     var prev_main = $('#prev_main_'+j).val();
//     var prev_hrs =$('#hrs_'+j).val();
    
//     //chknumber('');
//     var pid = $('#check_uniqueallocation').val();
//     var result = 2;
    
//     if(item_name != '' && unit != '' && qty != '' && rate != '' && tax != '' && amount != ''  ){
//     var stuff ={'id': recid,'pid':pid,'item_name':item_name,'unit':unit,'model_no':model_no,'batch_no':batch_no,'make':make,'serial_no':serial_no,'warranty_date':warranty_date,'qty':qty,'rate':rate,'tax':tax,'amount':amount,'prev_main':prev_main,'prev_hrs':prev_hrs};
    
//                 $.ajax({
//                     method: 'POST',
//                     async: false,
//                     data    : {result:JSON.stringify(stuff)},
//                     url: '<?php // echo Yii::app()->createUrl('purchase/create1'); ?>',
//                     dataType:'json',
//                     success: function (data) {
//                     if(data.status == 1){
//                       $(".addrow").html(data.html);
//                       $().toastmessage('showSuccessToast', "Item  updated successfully");
//                     }else{
//                       $().toastmessage('showErrorToast', "Some problem occured");
//                     }
//                     }
//                 });
//                 }
    
//     });


</script>

<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    .check_class{
      display : none;
    }
    .toast-container {
	  width:350px;
	}
	.toast-position-top-right {
	  top: 57px;
	  right: 6px;
	}
	.toast-item-close {
	  background-image: none;
	  cursor: pointer;
	  width: 12px;
	  height: 12px;
	  text-align: center;
	  border-radius: 2px;
	}
	.toast-item-image {
	  font-size: 24px;
	}
	.toast-item-close:hover {
	  color: red;
	}
	.toast-item {
	  border: transparent;
	  border-radius: 3px;
	  font-size: 10px;
	  opacity: 1;
	  background-color: rgba(34,45,50,0.8);
	}
	.toast-item-wrapper p {
	  margin: 0px 5px 0px 42px;
	  font-size: 14px;
	  text-align: justify;
	}

	.toast-type-success {
	  background-color: #00A65A;
	  border-color: #00A65A;
	}
	.toast-type-error {
	  background-color: #DD4B39;
	  border-color: #DD4B39;
	}
	.toast-type-notice {
	  background-color: #00C0EF;
	  border-color: #00C0EF;
	}
	.toast-type-warning {
	  background-color: #F39C12;
	  border-color: #F39C12;
	}
  .save-btnHold button, .save-btnHold input {
    margin-top: 15px;
}
</style>
