
<h1>View Purchase</h1>
        <div class="row">
            <div class="col-md-2">
                <label>Purchase No</label> : <b><?php echo $model->purchase_no; ?></b>
						</div>

            <div class="col-md-2">
                <label>Invoice No</label> : <b><?php echo $model->invoice_no; ?></b>
            </div>
						<div class="col-md-2">
							<label>Purchase Date</label> : <b><?php echo $model->purchase_date; ?></b>
						</div>
						<div class="col-md-2">
							<label>Invoice Date</label> : <b><?php echo $model->invoice_date; ?></b>
						</div>
						<div class="col-md-2">
						<label>Supplier Name </label> : <b><?php echo $model->supplier->name; ?></b>
            </div>

        </div>

        <div class="table-responsive "><br>
					<table border="1" class="table">
							<thead>
                    <tr>
                        <th class="text_align">No.</th>
                        <th class="text_align">Tool Name</th>
                        <th class="text_align">Unit</th>
                        <th class="text_align">Model No.</th>
                        <th class="text_align">Batch No.</th>
                        <th class="text_align">Make</th>
                        <th class="text_align">Serial No.</th>
                        <th class="text_align">warranty date</th>
                        <th class="text_align">Quantity</th>
                        <th class="text_align">Rate</th>
                        <th class="text_align">Tax</th>
                        <th class="text_align">Amount</th>
                        <th class="text_align">preventive maintenance</th>
                        <th class="text_align">Hours</th>
                        <th></th>
                    </tr>
								</thead>
									<tbody class="addrow">
												<?php
												$i = 1;
										    foreach ($newmodel as $new) {
													$unit = Unit::model()->findByPk($new['unit']);
												?>
                        <tr class="tr_class">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $new['item_name']; ?></td>
                            <td><?php echo $unit->unitname; ?></td>
                            <td><?php echo $new['model_no'];?></td>
                            <td><?php echo $new['batch_no'];?></td>
                            <td><?php echo $new['make'];?></td>
                            <td><?php echo $new['serial_no'] ; ?></td>
                            <td><?php echo $new['warranty_date'] ; ?></td>
                            <td><?php echo $new['qty'] ;?></td>
                            <td><?php echo $new['rate'] ; ?></td>
                            <td><?php echo $new['tax'] ; ?></td>
                            <td><?php echo $new['amount'] ; ?></td>
                            <td>
															<?php
															if($new['prev_main'] == 1){
																echo "Yes";
															} else{
																echo "No";
															}
															 ?>
														</td>
                            <td><?php echo ($new['prev_hrs'] !='')?$new['prev_hrs']:""; ?></td>
                            <td></td>
                            </tr>
													<?php $i++; } ?>
												</tbody>
            </table>
        </div>
