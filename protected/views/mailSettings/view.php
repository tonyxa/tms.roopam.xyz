<?php
/* @var $this MailSettingsController */
/* @var $model MailSettings */

$this->breadcrumbs=array(
	'Mail Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MailSettings', 'url'=>array('index')),
	array('label'=>'Create MailSettings', 'url'=>array('create')),
	array('label'=>'Update MailSettings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MailSettings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MailSettings', 'url'=>array('admin')),
);
?>

<h1>View MailSettings #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'role_id',
		'status',
	),
)); ?>
