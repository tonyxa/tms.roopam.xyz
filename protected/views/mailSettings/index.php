<?php
/* @var $this MailSettingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Settings',
);

$this->menu=array(
	array('label'=>'Create MailSettings', 'url'=>array('create')),
	array('label'=>'Manage MailSettings', 'url'=>array('admin')),
);
?>

<h1>Mail Settings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
