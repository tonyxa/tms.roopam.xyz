<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */

$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List UserRoles', 'url'=>array('index')),
	//array('label'=>'Create UserRoles', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-roles-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage User Roles</h1>
<div class="half-table">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-roles-grid',
	'dataProvider'=>$model->search(),
	 'filter'=>$model,
        'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ','nextPageLabel'=>'Next ' ),
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'columns'=>array(
		 array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
		'role',
//		array(
//			'class'=>'CButtonColumn',
//                        'template'=>'{update}',
//                        'buttons'=>array(
//                            'update' => array(
//                                'label'=>'',
//                                'imageUrl' =>false,
//                                'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit'),
//                           ),
//                        ),
                        
		/*
     'buttons'=>array
    (
      'update' => array
        (
        'url' => '$this->grid->controller->createUrl("update", array("id"=>$data->id,"asDialog"=>1,"gridId"=>$this->grid->id))',
        'click' => 'function(){$("#cru-frame1").attr("src",$(this).attr("href")); $("#cru-dialog1").dialog("open");  return false;}',
        ),
     ),  */
		//),
	),
)); ?>
</div>

<div id="id_view"></div>
 <?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog1',
    'options' => array(
        'title' => 'Update User Role',
        'autoOpen' => false,
        'modal' => false,
        'width' => "450",
        'height' =>"auto",
    ),
));
?>
<iframe id="cru-frame1" width="450" height="350" scrolling="auto" style="min-height:350px;"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>



<style type="text/css">
    #user-roles-grid_c0, #user-roles-grid_c1
    {
        width:0px;;
    }
    
    td.button-column{
        /*text-align: left !important;*/
    }
    
    
</style>
