<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */

$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserRoles', 'url'=>array('index')),
);
?>

<h2>Create new Role</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>