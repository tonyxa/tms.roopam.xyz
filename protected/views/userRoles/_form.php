<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-roles-form',
	'enableAjaxValidation'=>false,
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'role'); ?>
		<?php echo $form->textField($model,'role',array('class'=>'form-control input-medium','size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="form-group">
		 <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->