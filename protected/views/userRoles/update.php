<?php
/* @var $this UserRolesController */
/* @var $model UserRoles */

$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List UserRoles', 'url'=>array('index')),
//	array('label'=>'Create UserRoles', 'url'=>array('create')),
);
?>

<h2>Update <?php echo $model->role; ?> Role</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>