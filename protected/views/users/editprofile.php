<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'My profile'=>array('users/myprofile'),
	'Update my profile',
);
?>

<h1>Update my profile</h1>

<?php echo $this->renderPartial('_edit_profile_form', array('model'=>$model)); ?>