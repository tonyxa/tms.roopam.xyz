<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->userid,
);

$this->menu = array(
    array('label' => 'List Users', 'url' => array('index')),
    array('label' => 'Create Users', 'url' => array('create')),
    array('label' => 'Update Users', 'url' => array('update', 'id' => $model->userid)),
    array('label' => 'Delete Users', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->userid), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Users', 'url' => array('admin')),
);
?>

<h1>View Users #<?php echo $model->userid; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'userid',
        array(
            'name' => 'user_type',
            'type' => 'raw',
            'value' => $model->userType->role,
        ),
        'first_name',
        'last_name',
        'username',
        'password',
        'email',
         array(
            'name' => 'reporting_person',
            'type' => 'raw',
            'value' => ($model->reporting_person===NULL?"---":$model->reportingPerson->first_name." ".$model->reportingPerson->last_name),
        ),
        'last_modified',
        'reg_date',
        'reg_ip',
        'activation_key',
        'email_activation',
        'status',
    ),
));
?>
