<?php
$this->breadcrumbs = array("Registration",);
?>

<h1>Registration</h1>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-register-form',
        'enableClientValidation' => false,
        'clientOptions' => array(
        'validateOnSubmit' => false,
        ),
            ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model); ?>
    <div id="customformlook">
        <div class="row"><div class="subrow">    
                <?php echo $form->labelEx($model, 'user_type'); ?>

                <?php echo $form->dropDownList($model, 'user_type', array('5' => 'Tourist', '6' => 'Tour Agency'), array('empty' => '--', 'style' => 'width:275px;')); ?>

                <?php echo $form->error($model, 'user_type'); ?>
            </div></div>

        <div class="row">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'first_name'); ?>
                <?php echo $form->textField($model, 'first_name'); ?>
                <?php echo $form->error($model, 'first_name'); ?>
            </div>

            <div class="subrow">
                <?php echo $form->labelEx($model, 'last_name'); ?>
                <?php echo $form->textField($model, 'last_name'); ?>
                <?php echo $form->error($model, 'last_name'); ?>
            </div>
        </div>

        <div class="row">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username',array('maxlength'=>32)); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>
            <div class="subrow">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email'); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
        </div>

        <div class="row">
            <div class="subrow">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password',array('maxlength'=>32)); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
            <div class="subrow">
                <?php echo $form->labelEx($model, 'retype_pwd'); ?>
                <?php echo $form->passwordField($model, 'retype_pwd',array('maxlength'=>32)); ?>
                <?php echo $form->error($model, 'retype_pwd'); ?>
            </div>
        </div>
        <?php if (CCaptcha::checkRequirements()): ?>
            <div class="row">
                <?php echo $form->labelEx($model, 'verifyCode'); ?>
                <div>
                    <?php $this->widget('CCaptcha'); ?>
                    <br />
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                </div>
                <div class="hint">Please enter the letters as they are shown in the image above.</div>
                <div class="subrow"><?php echo $form->error($model, 'verifyCode'); ?></div>
            </div>
        <?php endif; ?>        
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>



    <?php $this->endWidget(); ?>

</div><!-- form -->