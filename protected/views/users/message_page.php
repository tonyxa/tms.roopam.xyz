<?php
$flash_message = Yii::app()->user->getFlashes();
if(count($flash_message)==0)
    $this->redirect(array('/'));
?>

<div class="form">
    <?php

    foreach ($flash_message as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>
</div>
