<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->userid=>array('view','id'=>$model->userid),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Users', 'url'=>array('index')),
//	array('label'=>'Create Users', 'url'=>array('create')),
);
?>

<h2>Update User: <span class="username"><?php echo $model->first_name; ?> <?php echo $model->last_name; ?></span></h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>