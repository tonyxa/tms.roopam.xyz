<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */

?>


<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
//        'htmlOptions' => array(
//            'class' => ' table table-bordered'),
    ));
    ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->  

    <div class="row">
        <div  class="subrow">
            <?php echo $form->labelEx($model, 'first_name'); ?>
            <?php echo $form->textField($model, 'first_name', array('class' => 'form-control input-medium', 'size' => 30, 'maxlength' => 30)); ?>
            <?php echo $form->error($model, 'first_name'); ?>
        </div>


        <div  class="subrow">
            <?php echo $form->labelEx($model, 'last_name'); ?>
            <?php echo $form->textField($model, 'last_name', array('class' => 'form-control input-medium', 'size' => 30, 'maxlength' => 30)); ?>
            <?php echo $form->error($model, 'last_name'); ?>
        </div>
    </div>


    <div class="row">   
        <div class="subrow">
<?php echo $form->labelEx($model, 'employee_id'); ?>
            <?php echo $form->textField($model, 'employee_id', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'employee_id'); ?>
          </div>
	<div class="subrow">
            <?php echo $form->labelEx($model, 'designation'); ?>
             <?php echo $form->textField($model, 'designation', array('class' => 'form-control input-medium', 'size' => 30, 'maxlength' => 70)); ?>
            <?php echo $form->error($model, 'designation'); ?>
        </div>
    </div>
    
     <div class="row">   
        <div class="subrow">
<?php echo $form->labelEx($model, 'username'); ?>
            <?php echo $form->textField($model, 'username', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'username'); ?>
          </div>
    
      
         <div class="subrow">
<?php
$model->password = '';
?>
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password', array('class'=>'form-control input-medium','autocomplete' => 'off', 'size' => 30, 'maxlength' => 30)); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>
    </div>
 
    <div class="row"> 
    <div class="subrow">
<?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('class'=>'form-control input-medium','size' => 30, 'maxlength' => 70)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>

      
            <div class="subrow">
<?php echo $form->labelEx($model, 'reporting_person'); ?>

            <?php
            echo $form->dropDownList($model, 'reporting_person', CHtml::listData(Users::model()->findAll(array(
                                'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                                'condition' => 'user_type in (1,2,8)',
                                'order' => 'first_name',
                                'distinct' => true
                            )), 'userid', 'first_name'), array('class'=>'form-control','empty' => '-----------', 'style' => 'width:250px'));
            ?>

            <?php echo $form->error($model, 'reporting_person'); ?>
        </div>
        </div>
        <div class="row">
                <div class="subrow">
         <?php echo $form->labelEx($model, 'user_type'); ?>
            <?php echo $form->dropDownList($model, 'user_type', CHtml::listData(UserRoles::model()->findAll(array('order' => 'role ASC')), 'id', 'role'), array('class'=>'form-control','empty' => '-Choose a role-', 'style' => 'width:250px;')); ?>
<?php echo $form->error($model, 'user_type'); ?>
    </div>  

   
        <div class="subrow">
<?php echo $form->labelEx($model, 'status'); ?>
            <div class="radio_btn">
<?php
echo $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array(
    'labelOptions'=>array('style'=>'display:inline'),
    'separator' => ''));
?>
            </div>

            <?php echo $form->error($model, 'status'); ?>
        </div>
   
    </div>



    <div class="row">
        <div class="subrow">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
            <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    div.form .row{ 
        width: 500px;
    }
    div.form .subrow{
        width: 50%;
        float:left;
    }
    div.form .subrow {
        padding: 0 12px !important;
    }
    div.form input, div.form textarea, div.form select {
        margin: 0.2em 0 0.5em 0;
    }
</style>
