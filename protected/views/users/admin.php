<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
                array('name'=>'userid', 'htmlOptions' => array('width' => '30px','style'=>'font-weight: bold')),
		'first_name',
		'last_name',
		'username',
            array(
                'name' => 'user_type',
                'value' => '$data->userType->role',
                'type' => 'raw',
                'filter' => CHtml::listData(UserRoles::model()->findAll(
                                array(
                                    'select' => array('id,role'),
                                    'order' => 'role',
                                    'distinct' => true
                        )), "id", "role")
            ),
		'email',
		/*
		'password',
		'last_modified',
		'reg_date',
		'reg_ip',
		'activation_key',
		'email_activation',
		'status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
