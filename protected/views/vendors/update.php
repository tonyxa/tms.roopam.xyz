<?php
/* @var $this VendorsController */
/* @var $model Vendors */

$this->breadcrumbs=array(
	'Vendors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List Vendors', 'url'=>array('index')),
	array('label'=>'Create Vendors', 'url'=>array('create')),
	array('label'=>'View Vendors', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Vendors', 'url'=>array('admin')),
);*/
?>


<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Update Vendor</h4>
        </div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>

</div>

