<?php
/* @var $this VendorsController */
/* @var $model Vendors */

$this->breadcrumbs=array(
	'Vendors'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List Vendors', 'url'=>array('index')),
	array('label'=>'Create Vendors', 'url'=>array('create')),
);
*/
/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('vendors-grid', {
		data: $(this).serialize()
	});
	return false;
});
");*/
?>

<h1>Manage Vendors</h1>

<?php /*?><p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><?php */?><!-- search-form -->
<div class="add-btn">
        <?php if (Yii::app()->user->role == 1 ||  Yii::app()->user->role == 2) { ?>
            <button data-toggle="modal" data-target="#addVendors"  class="btn blue createVendor">Add Vendors</button>
        <?php } ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vendors-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-bordered',
	'filter'=>$model,
	'columns'=>array(
		/*'id',*/
		'vendor_code',
		'vendor_name',
		'vendor_address',
		'vendor_phone',
		'vendor_mob',
		/*
		'vendor_email',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
		array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:60px;'),
			'template' => '{update}{delete}',
			'buttons' => array(
               
                'update' => array(
                    'label'=>'edit',
                    'options' => array('class' => 'editVendor'),
                    'url' => 'Yii::app()->createUrl("vendors/update",array("id"=>$data->id,))',
                ),
                
              
            ),
		),
	),
)); ?>


 <!-- Add Work site Popup -->
        <div id="addVendors" class="modal" role="dialog">
            <div class="modal-dialog modal-lg">
		
            </div>
        </div>
        



<?php

Yii::app()->clientScript->registerScript('myjquery', ' 
		
		$(document).ready(function () {
		$(".createVendor").click(function (event) {
			 event.preventDefault();
			// alert("hi");
			$.ajax({
				type: "GET",
				url:"'.Yii::app()->createUrl('vendors/create').'",
				success: function (response)
				{
					//alert(response);
					$("#addVendors").html(response);
					$("#addVendors").css({"display":"block"});

				}
			});
		});
		
		 
                
            
		$(".editVendor").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
           // alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addVendors").html(response);
                        $("#addVendors").css({"display":"block"})
                    }

                });
            
        });
		

		

	});

 
           
   ');
?>