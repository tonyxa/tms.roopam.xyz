<?php
/* @var $this VendorsController */
/* @var $model Vendors */
/* @var $form CActiveForm */
?>

<div class="modal-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vendors-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => false,),
)); ?>
	
    
    <div class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_code'); ?>
                    <?php echo $form->textField($model, 'vendor_code', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'vendor_code'); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_name'); ?>
                    <?php echo $form->textField($model, 'vendor_name', array('maxlength' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'vendor_name'); ?>
                </div>
            </div>

            <div class="row">
            	<div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_phone'); ?>
                    <?php echo $form->textField($model,'vendor_phone',array('class' => 'form-control','size'=>20,'maxlength'=>20)); ?>                    
					<?php echo $form->error($model, 'vendor_phone'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_mob'); ?>
                    <?php echo $form->textField($model, 'vendor_mob', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'vendor_mob'); ?>
                </div>
                

                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_email'); ?>
                    <?php echo $form->textField($model, 'vendor_email', array('size' => 40, 'maxlength' => 40, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'vendor_email'); ?>
                </div>
				<div class="col-md-6">
                    <?php echo $form->labelEx($model, 'vendor_address'); ?>
                    <?php echo $form->textArea($model,'vendor_address',array('class' => 'form-control','rows'=>6, 'cols'=>50)); ?>                    
                    <?php echo $form->error($model, 'vendor_address'); ?>
                </div>
                
            </div>
           
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn green')); ?>
                <button data-dismiss="modal" class="btn default" onclick="javascript:window.location.reload()">Close</button>
            </div>

            
        </div>
    </div>
    
<?php $this->endWidget(); ?>

</div><!-- model body -->