<?php
/* @var $this VendorsController */
/* @var $data Vendors */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_code')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_name')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_address')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_phone')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_mob')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_mob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendor_email')); ?>:</b>
	<?php echo CHtml::encode($data->vendor_email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>