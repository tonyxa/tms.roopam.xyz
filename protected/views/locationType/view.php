<?php
/* @var $this LocationTypeController */
/* @var $model LocationType */

$this->breadcrumbs=array(
	'Location Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List LocationType', 'url'=>array('index')),
	array('label'=>'Create LocationType', 'url'=>array('create')),
	array('label'=>'Update LocationType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LocationType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LocationType', 'url'=>array('admin')),
);
?>

<h1>View LocationType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
		'address',
		'phone',
		'mob',
		'email',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		'location_type',
	),
)); ?>
