<?php
/* @var $this LocationTypeController */
/* @var $model LocationType */

$this->breadcrumbs=array(
	'Location Types'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List LocationType', 'url'=>array('index')),
	array('label'=>'Manage LocationType', 'url'=>array('admin')),
);*/
?>

<h1>Create Vendor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
