  
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'location-type-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => false,),
)); ?>

            <div class="row">               
                  <div class="col-md-6">
                    <?php echo $form->labelEx($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'name'); ?>
                  </div>
                 <div class="col-md-6">
                    <?php echo $form->labelEx($model,'address'); ?>
					<?php echo $form->textArea($model,'address',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'address'); ?>    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <?php echo $form->labelEx($model,'phone'); ?>
				<?php echo $form->textField($model,'phone',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'phone'); ?>
                </div>
                <div class="col-md-6">
				<?php echo $form->labelEx($model,'mob'); ?>
				<?php echo $form->textField($model,'mob',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'mob'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <?php echo $form->labelEx($model,'phone1'); ?>
				<?php echo $form->textField($model,'phone1',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'phone1'); ?>
                </div>
                <div class="col-md-6">
				<?php echo $form->labelEx($model,'phone2'); ?>
				<?php echo $form->textField($model,'phone2',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'phone2'); ?>
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
               <?php echo $form->labelEx($model,'email'); ?>
				<?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'email'); ?>
                </div>
                <div class="col-md-6">
                <?php echo $form->labelEx($model,'code'); ?>
				<?php echo $form->textField($model,'code',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'code'); ?>
                </div>
            </div>
            <div class="row save-btnHold">
				<div class="col-md-12 text-center">
                                    <br>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn blue')); ?>
                <button data-dismiss="modal" class="btn default" onclick="javascript:window.location.reload()">Cancel</button>
            </div>
            </div>
<?php $this->endWidget(); ?>
<style>
    @media(min-width: 767px){
        #content{
        width: 60%;
    }
    }
    
</style>
