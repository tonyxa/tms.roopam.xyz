<?php
/* @var $this LocationTypeController */
/* @var $model LocationType */

$this->breadcrumbs=array(
	'Location Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>


<h1>Update Vendor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
