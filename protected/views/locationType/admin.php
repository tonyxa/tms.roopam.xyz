<?php
/* @var $this LocationTypeController */
/* @var $model LocationType */

$this->breadcrumbs=array(
	'Location Types'=>array('index'),
	'Manage',
);

/*
$this->menu=array(
	array('label'=>'List LocationType', 'url'=>array('index')),
	array('label'=>'Create LocationType', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('location-type-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
 * 
 */
?>


<?php /*
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/
?>

<?php /*<div class="add-btn">
        <?php if (Yii::app()->user->role == 1 ||  Yii::app()->user->role == 2) { ?>
            <button data-toggle="modal" data-target="#addLocationtype"  class="btn blue createLocationtype">Add Vendor</button>
        <?php } ?>
</div>
 
 */ ?>
<div class="clearfix">
    <div class="pull-right">
        <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2 || Yii::app()->user->role == $role) { ?>
            <?php
            echo CHtml::link('Add Vendor', $this->createAbsoluteUrl('locationType/create'), array('class' => 'btn blue'));
        }
        ?>
    </div>
    <h1>Manage Vendor</h1>
</div>
<div class="table-responsive">
<?php 
$page = 'Vendor';
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'location-type-grid',
	'dataProvider'=>$model->search($page),
        'itemsCssClass' => 'table table-bordered',
	'filter'=>$model,
	'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'Sl.No.'),
		//'id',
		'code',
		'name',
		'address',
		'phone',
		'mob',
                'phone1',
                'phone2',
		/*
		'email',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
                 * 
                 */
//		array( //
//                'name' => 'location_type',    
//                ),
		array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:160px;'),
			//'template' => '{update}{delete}',
                        'template' => '{Make Inactive}{Make Active}{edit}',
			'buttons' => array(
               
                'edit' => array(
                    'label'=>'',
                    //'options' => array('class' => 'editLocationtype'),
                    'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit'),
                    'imageUrl' =>false,
                    'url' => 'Yii::app()->createAbsoluteUrl("locationType/update",array("id"=>$data->id,))',
                    'visible' => '($data->active_status == 1)',
                ),
                
                 /* Active section starts from here */    
                        'Make Active' => array(			    
                        'url'=>'Yii::app()->createUrl("locationType/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-success'), 
                        'visible' => '($data->active_status == 0)',    
			),
			/* Active section end here */
                            
                        /* Inactive section starts from here */    
                        'Make Inactive' => array(			    
                        'url'=>'Yii::app()->createUrl("locationType/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-warning'),  
                        'visible' => '($data->active_status == 1)',     
			),             
              
            ),
		),
	),
)); ?>
</div>
 <!-- Add Work site Popup -->
        <div id="addLocationtype" class="modal" role="dialog">
            <div class="modal-dialog modal-lg">
		
            </div>
        </div>
        
 
 
<?php

Yii::app()->clientScript->registerScript('myjquery', ' 
		
		$(document).ready(function () {
		$(".createLocationtype").click(function (event) {
			 event.preventDefault();
			// alert("hi");
			$.ajax({
				type: "GET",
				url:"'.Yii::app()->createUrl('locationType/create').'",
				success: function (response)
				{
					//alert(response);
					$("#addLocationtype").html(response);
					$("#addLocationtype").css({"display":"block"});

				}
			});
		});
		
		 
                
            
		$(".editLocationtype").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
           // alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addLocationtype").html(response);
                        $("#addLocationtype").css({"display":"block"})
                    }

                });
            
        });
		

		

	});

 
		
//active status script starts from here
$(".changestatusicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){					
					
					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
//active status script end here
 
           
           
   ');
?>
