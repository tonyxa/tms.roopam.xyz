<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error <?php  echo $code; ?></h2>

<div class="error">
<?php //echo CHtml::encode($message); ?>
<?php switch($code)
	{
		case 400:
			$this->pageTitle = 'Bad Request';
			echo ('The data you submitted is invalid');
			break;
		case 404:
			$this->pageTitle = 'Page Not Found';
			echo 'The page you requested was not found.';
			break;
		case 403:
			$this->pageTitle = 'Authorization Not Found';
			echo 'You are not authorized to perform this action.';
			break; 
		default:
			$this->pageTitle = 'Error';
			echo 'Some problem has occured. Please Contact the technical team.';
	}

?>
</div>