<?php
$this->pageTitle = 'Customer - Login form';
?>

<ul class="breadcrumb">
    <li><?php echo CHtml::link('Home', array('home/index')); ?></li>
    <li class="active">Customer login</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
        <h1>Login</h1>
        <div class="content-form-page">
            <div class="row">
                <div class="col-md-7 col-sm-7">

                    <div id="statusMsg">
                        <?php
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            //$key would be info, success, warning,error 
                            echo '<div class="message ' . $key . ' alert alert-success alert-dismissable">' . $message . "</div>\n";
                        }
                        ?>
                    </div>

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'site-login-form',
                        'enableClientValidation' => false,
                        'htmlOptions' => array('class' => 'form-horizontal form-without-legend', 'role' => 'form')
                    ));
                    ?>
                    <div class="form-group">
                        <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                        <div class="col-lg-8">
                            <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'username') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
                        <div class="col-lg-8">
                            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'password'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-offset-4 padding-left-0">
                            <?php echo CHtml::link('Forget Password?', array('site/forgotpassword')); ?>
                        </div>
                        <div class="col-md-offset-4 padding-left-0">
                            <?php echo CHtml::link('Create new account', array('site/register')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
               
            </div>
        </div>

    </div>
    <!-- END CONTENT -->
</div>
