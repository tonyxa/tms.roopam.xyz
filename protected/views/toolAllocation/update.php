<?php
/* @var $this ToolAllocationController */
/* @var $model ToolAllocation */


$this->breadcrumbs=array(
	'Tool Allocations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
/*
$this->menu=array(
	array('label'=>'List ToolAllocation', 'url'=>array('index')),
	array('label'=>'Create ToolAllocation', 'url'=>array('create')),
	array('label'=>'View ToolAllocation', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ToolAllocation', 'url'=>array('admin')),
); */
?>


<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Update Tool Allocation</h4>
        </div>
<?php echo $this->renderPartial('_form', array('model'=>$model,'newmodel'=>$newmodel,'units'=>$units)); ?>

</div>

</div>
