


<?php
/* @var $this ToolsController */
/* @var $model Tools */
/* @var $form CActiveForm */
?>
<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/jquery.validate.js'); ?>
<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'tool-allocation-form',
       /* 'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,), */
    ));
    ?>
    <div class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'allocation_no'); ?>
                    <?php echo $form->textField($model, 'allocation_no', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'allocation_no'); ?>
                </div>

                <div class="col-md-6">
                    
                    <?php echo $form->labelEx($model, 'allocation_date'); ?>
					<?php echo CHtml::activeTextField($model, 'allocation_date', array('class'=>'form-control')); ?>
					<?php
					$this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'ToolAllocation_allocation_date',
						'ifFormat' => '%Y-%m-%d',
					));
					?>
					<?php echo $form->error($model, 'allocation_date'); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'allocated_site'); ?>
                    <?php
                    echo $form->dropDownList($model, 'allocated_site', CHtml::listData(WorkSite::model()->findAll(
                                            array(
                                                'select' => array('id,site_name'),
                                                'order' => 'site_name',
                                                'distinct' => true
                                    )), "id", "site_name"), array('class' => 'form-control', 'empty' => 'Please choose'));
                    ?>
                    <?php echo $form->error($model, 'allocated_site'); ?>
                </div>
                   
            </div>
           
            <div class="row">
            <table width="500" border="1" id="tool_allocation">
                <tr>
                    <td>No.</td>
                    <td>Code</td>
                    <td>Name</td>
                    <td>Unit</td>
                    <td>Serial No</td>
                    <td>Duration</td>
                    <td>Quantity</td>
                    <td>Late Return Fee</td>
                </tr>
                <?php
						
				$i=1;
				foreach($newmodel as $new) 
				{
				?>
                <tr class="worktr">
                    <td>1</td>
                    <td><input type="text" class="inputs form-control" name="code[]" id="code_1" value="<?php echo $new['code'];?>"/></td>
                    <td><input type="text" class="inputs form-control" name="name[]" id="name_1" value="<?php echo $new['item_name'];?>"/></td>
                    <td>
                            <select class="inputs form-control target" name="unit[]" id="work_site_1">
                                <option value="">Please choose</option>
                                <?php foreach ($units as $unit) { ?>
                                    <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>
                                <?php } ?> 
                            </select>
                        </td>
                    <td><input type="text" name="no[]" class="inputs  form-control" id="no_1" value="<?php echo $new['serial_no'];?>" /></td>
                    <td><input type="text" name="duration[]" class="inputs  form-control" id="duration_1" value="<?php echo $new['duration_in_days'];?>"/></td>
                    <td><input type="text" name="quantity[]" class="inputs  form-control" id="quantity_1" value="<?php echo $new['qty'];?>"/></td>
                    <td><input type="text" name="fee[]" class="inputs lst form-control" id="fee_1" value="<?php echo $new['late_return_fee'];?>"/></td>
                    <td></td>
                </tr>
                <?php
				$i++;
				}
				
				?>
            </table>
            </div>
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn green')); ?>
                <button data-dismiss="modal" class="btn default" onclick="javascript:window.location.reload()">Close</button>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div><!-- model body -->

<script type="text/javascript">
var i = $('#tool_allocation tr').length;
var flag=0;
$(document).on('keypress', '.lst', function(e) {
  var code = (e.keyCode ? e.keyCode : e.which);
 // alert(flag);
  if(flag==0){
  if (code == 13) {
    html = '<tr class="worktr">';
    html += '<td>' + i + '</td>';
    html += '<td><input type="text" class="inputs form-control" name="code[]" id="code_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs form-control" name="name[]" id="name_' + i + '" /></td>';
   
    html += '<td><select class="inputs form-control target" name="unit[]" id="unit_' + i + '"><option value="">Please choose</option>'
<?php foreach ($units as $unit) { ?>
                + '<option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>'
<?php } ?>
            + '</select> </td>';
    html += '<td><input type="text" class="inputs  form-control" name="no[]" id="no_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs  form-control" name="duration[]" id="duration_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs  form-control" name="quantity[]" id="quantity_' + i + '" /></td>';
    html += '<td><input type="text" class="inputs lst form-control" name="fee[]" id="fee_' + i + '" /></td>';
    html += '<td><input type="button" class="btn green btn-xs delete"  data-id="' + i + '" value="Delete"></td>';
    html += '</tr>';
    $('#tool_allocation').append(html);
    $(this).focus().select();
    i++;
    e.preventDefault();
    return false;
  }
}
});

$(document).on('keydown', '.inputs', function(e) {
	  var code = (e.keyCode ? e.keyCode : e.which);
	  if (code == 13) {
		var index = $('.inputs').index(this) + 1;
		$('.inputs').eq(index).focus();
	  }
	});

 $(document).on('click', '.delete', function () {
        var dataid = $(this).attr('data-id');
        $(this).parent().parent('tr').remove();
        AutoNumber();
    });
    function AutoNumber() {
        var j = 1;
        $('.worktr').each(function () {
            $(this).find("td:first").text(j);
            j++;
        });
    }
	

/*
 $(document).ready(function () {
        $("#tool_allocation").on("click", ".lst", function () {
            var $selects = $(this).closest('tr').find('td select'),
            $cells = $(this).closest("tr").find("td input");
            $("#tool_allocation").find("td input").removeClass("has-error");
            $("#tool_allocation").find("td select").removeClass("has-error");
            $cells.removeClass("has-error");
            $selects.removeClass("has-error");
            $cells.each(function () {
                if ($(this).val().trim() === '') {
                    flag++;
                    $(this).addClass("has-error");
                }else{
                    flag=0;
                }
            });
            $selects.each(function () {
                if ($(this).val() == '') {
                    flag++;
                    $(this).addClass("has-error");
                }else{
                    flag=0;
                }
            });
        });
    });
   */
   
    $(document).ready(function () {
        $("#tool-allocation-form").validate({
		
            rules: {
               'ToolAllocation[allocation_no]': {
                    required: true,
                },
                'ToolAllocation[allocation_date]': {
                    required: true

                },
                'ToolAllocation[allocated_site]': {
                    required: true,
                },
                
                'code[]': {
                    required: true,
                },
                'name[]': {
                    required: true,
                },
                'unit[]': {
                    required: true,
                   // digits: true,
                },
                'no[]': {
                   // required: true,
                },
                'duration[]': {
                   // required: true,
                    digits: true,
                },
                'quantity[]': {
                    //required: true,
                    digits: true,
                },
                'fee[]': {
                    //required: true,
                    digits: true,
                },
            },
            messages: {
                'ToolAllocation[allocation_no]': {
                    required: "Allocation No is required",
                },
                 'ToolAllocation[allocation_date]': {
                    required: "Allocation Date is required",
                },
               'ToolAllocation[allocated_site]': {
                    required: "Allocated Site is required",
                },
               
                'Tools[ref_no]': {
                    required: "Tool unit is required",
                },
                'code[]': {
                    required: "Required",
                },
                'name[]': {
                    required: "Required",
                },
                'unit[]': {
                    required: "Required",
                },
                'no[]': {
                    required: "Required",
                },
                'duration[]': {
                    required: "Required",
                },
                'quantity[]': {
                    required: "Required",
                },
                'fee[]': {
                    required: "Required",
                },
            },
            submitHandler: function () {
                var formData = JSON.stringify($("#tool-allocation-form").serializeArray());
                $.ajax({
                    method: "POST",
                    data: {formdata: formdata},
                    url: '<?php echo Yii::app()->createUrl('toolAllocation/create'); ?>',
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
		});
        
    });
</script>


<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }

</style>
