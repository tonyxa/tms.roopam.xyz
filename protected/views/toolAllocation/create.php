<?php
/* @var $this ToolAllocationController */
/* @var $model ToolAllocation */

$this->breadcrumbs=array(
	'Tool Allocations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ToolAllocation', 'url'=>array('index')),
	array('label'=>'Manage ToolAllocation', 'url'=>array('admin')),
);
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">New Tool Allocation</h4>
        </div>

<?php echo $this->renderPartial('_form', array('model'=>$model,'units'=>$units)); ?>
</div>

</div>
