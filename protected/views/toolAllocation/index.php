<?php
/* @var $this ToolAllocationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tool Allocations',
);

$this->menu=array(
	array('label'=>'Create ToolAllocation', 'url'=>array('create')),
	array('label'=>'Manage ToolAllocation', 'url'=>array('admin')),
);
?>

<h1>Tool Allocations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
