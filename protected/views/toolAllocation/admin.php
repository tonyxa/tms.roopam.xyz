<?php
/* @var $this ToolAllocationController */
/* @var $model ToolAllocation */

$this->breadcrumbs=array(
	'Tool Allocations'=>array('index'),
	'Manage',
);
/*
$this->menu=array(
	array('label'=>'List ToolAllocation', 'url'=>array('index')),
	array('label'=>'Create ToolAllocation', 'url'=>array('create')),
);*/


?>

<h1>Manage Tool Allocations</h1>

<div class="">
    <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) { ?>
        <button data-toggle="modal" data-target="#addtoolalloc"  class="btn blue createtoolallocation">Add Tool Allocation</button>
    <?php } ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tool-allocation-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-bordered',
	'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),    
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'filter'=>$model,
	'columns'=>array(
		 array('class' => 'IndexColumn', 'header' => 'Sl.No.','htmlOptions'=>array('style'=>'width:20px;'),),
		'allocation_no',
		'allocation_date',
		//'allocated_site',
		array(
            'name' => 'allocated_site',
            'value' => '$data->allocatedSite->site_name',
        ),
        
		//'created_by',
		//'created_date',
		/*
		'updated_by',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:60px;'),
			'template' => '{update}{delete}',
			 'buttons' => array(
               
                'update' => array(
                    'label'=>'edit',
                    'options' => array('class' => 'editAllocation'),
                    'url' => 'Yii::app()->createUrl("toolAllocation/update",array("id"=>$data->id,))',
                ),
                
              
            ),
		),
	),
)); ?>


<!-- Add tools Popup -->
<div id="addtoolalloc" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery','

$(document).ready(function(){
		$(".createtoolallocation").click(function (event) {
			 event.preventDefault();			
			$.ajax({
				type: "GET",
				url:"'.Yii::app()->createUrl('toolAllocation/create').'",
				success: function (response)
				{					
					$("#addtoolalloc").html(response);
					$("#addtoolalloc").css({"display":"block"});
				}
			});
		});
		
		 
                
            
		$(".editAllocation").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
           // alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addtoolalloc").html(response);
                        $("#addtoolalloc").css({"display":"block"})
                    }

                });
            
        });
		

		

	});
');
?>
