<?php
/* @var $this ToolAllocationController */
/* @var $data ToolAllocation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('allocation_no')); ?>:</b>
	<?php echo CHtml::encode($data->allocation_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('allocation_date')); ?>:</b>
	<?php echo CHtml::encode($data->allocation_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('allocated_site')); ?>:</b>
	<?php echo CHtml::encode($data->allocated_site); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>