<?php
/* @var $this ToolAllocationController */
/* @var $model ToolAllocation */

$this->breadcrumbs=array(
	'Tool Allocations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ToolAllocation', 'url'=>array('index')),
	array('label'=>'Create ToolAllocation', 'url'=>array('create')),
	array('label'=>'Update ToolAllocation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ToolAllocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ToolAllocation', 'url'=>array('admin')),
);
?>

<h1>View ToolAllocation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'allocation_no',
		'allocation_date',
		'allocated_site',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
