
<style>

.btn.default, .btn-primary{
    margin-top: 20px;
}
div label {
    font-weight: bold;
    font-size: 0.9em;
    display: block;
}
.thead th{
    background-color: #44709C;
    color: #fff;
    border-top: 1px solid transparent !important;
    border-bottom: 1px solid transparent !important;
}
.tooldetails{
    padding: 10px 20px;
    background-color: #eee;
    margin-bottom: 10px;
    color: black;
}
.tooldetails div{padding: 3px;display:inline-block;margin-right: 10px;}

</style>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');
?>
<?php
Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#toolform').submit();

    });



");
?>
<div class="container" id="project">



    <h1>Tool Life History</h1>





    <form id="toolform"   action="<?php $url = Yii::app()->createAbsoluteUrl("tools/toolhistory"); ?>" method="POST">

        <div class="search-form" >
            <?php $sid = Status::model()->find(array('condition' => 'caption = "Tool" and status_type = "pending_status"'))->sid; ?>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Name : ', ''); ?>
                    <?php
                    //echo CHtml::textField('tool_name','',array('class'=>''));
                    $options = CHtml::listData(Tools::model()->findAll(array('select'=>'id,concat_ws(" ",tool_name,concat("(",ref_no,")")) as tool_name','condition' => 'active_status = "1" AND serialno_status ="Y"')), 'id', 'tool_name');
                    echo CHtml::dropDownList('tool_name', $tool_name, $options, array('class' => 'tool_name valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Code : ', ''); ?>
                    <?php
                    //echo CHtml::textField('tool_code',$tool_code,array('class'=>''));
                    $options = CHtml::listData(Tools::model()->findAll(array('condition' => 'active_status = "1" AND serialno_status ="Y"')), 'ref_no', 'ref_no');
                    echo CHtml::dropDownList('tool_code', $tool_code, $options, array('class' => 'tool_code valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <label></label>
                    <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-primary btn-sm')); ?>
                    <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('toolhistory') . '"', 'class' => 'btn btn-sm default')); ?>

                </div>
            </div>
        </div>
    </form>

    <br/><br/>
    <?php if (!empty($sql)) { ?>
        <div colspan="4" class="tooldetails">
            <div>Tool Name : <b><?php echo strtoupper($sql[0]['item_name']); ?></b></div>
            <div>Tool Code : <b><?php echo $sql[0]['code']; ?></b></div>
            <div>Tool Category : <b><?php echo Tools::getToolCategoryname($sql[0]['tool_id']); ?></b></div>
            <div>Parent Category : <b><?php echo Tools::getToolCategoryname($sql[0]['tool_id'], 0); ?></b></div>
        </div>
    <?php }   ?>

    <div class="table-responsive">
        <table class='table table-bordered'>
        <?php if (!empty($sql)) {    ?>     
        
    <!-- <th colspan="4" class="tooldetails">
        <div>Tool Name : <?php echo strtoupper($sql[0]['item_name']); ?></div>
        <div>Tool Code : <?php echo $sql[0]['code']; ?></div>
        <div>Tool Category : <?php echo Tools::getToolCategoryname($sql[0]['tool_id']); ?></div>
        <div>Parent Category : <?php echo Tools::getToolCategoryname($sql[0]['tool_id'], 0); ?></div>
        </th>
        </tr> -->

        <tr style="background-color: #5d5d5d;color: #fff;">
                    
            <th>Sl No</th><th>Date From</th><th>Date To</th><th>Location</th></tr>

            <?php
            $k = 1;
            foreach ($sql as $k => $row) {
                $k++; ?>
            <tr>
                <td><?= $k; ?></td>
                <td> <?php 
                if(isset($row['req_date'])){
                    echo date('d-m-Y', strtotime($row['req_date'])); 
                }else{
                    echo date('d-m-Y', strtotime($row['return_date']));
                }
                 ?></td>
                       
                <td><?php
                    if ((count($sql) - 1) < $k) {
                        // echo date('d-m-Y');
                        if(isset($row['return_date'])){
                            echo date('d-m-Y', strtotime($row['return_date']));
                        }else{
                             echo date('d-m-Y', strtotime($row['req_date']));
                        }
                        //echo date('d-m-Y', strtotime($row['req_date']));
                    } else {
                        
                        if(isset($row['return_date'])){
                            echo date('d-m-Y', strtotime($row['return_date']));
                        }else{
                             echo date('d-m-Y', strtotime($row['req_date']));
                        }
                    }
                    ?>
                </td>
                <td><?php
                        $name = $row['name'];
                        if ($row['caption'] == "Stock") {
                            $id = $row['tool_transfer_id'];
                            if ($id != NULL) {
                                $stat = $row['request_status'];
                                $caption = Status::model()->findByPk($stat)['caption'];
                                // if ($caption == "Received back to stock") {
                                //     $location = $row['request_from'];
                                //     $name = LocationType::model()->findByPk($location)['name'];
                                // }
                            }
                        }
                        if($row['type']!="Vendor")
                        {
                            $row['type'] ="";
                        }
                        else
                        {
                            $row['caption'] ="";
                        }
                        if($row['item_current_status']!="missing"){
                            if($name==NULL)
                            {
                                $name= LocationType::model()->findByPk(1)['name'];
                            }
                          
                            $request_to=ToolTransferRequest::model()->findByPk($row['tool_transfer_id']);
                            if($row['caption']=='In Transit'){
                              if(isset($request_to->request_to)){
                                 $name= LocationType::model()->findByPk($request_to->request_to);

                                echo  $row['caption'] ."".$row['type']. "<b> (" . $name->name.")</b>";
                              }
                              if(isset($request_to->request_from)){
                                // $name= LocationType::model()->findByPk($request_to->request_from);
                                if($request_to->transfer_to=='stock'){
                                    $request_to->transfer_to= LocationType::model()->findByPk(1);
                                }
                               echo  $row['caption'] ."".$row['type']. "<b> (" . $request_to->transfer_to->name.")</b>";
                             }

                            }else{
                               // echo  $row['caption'] ."".$row['type']. "<b> (" . $name.")</b>";

                            echo '<b>'.$name.'</b>';
                            }
                        }
                        else
                        {
                            echo "Missing</b>";
                        }?>
                </td>
            </tr>  
            <tr>  
                    <?php
                    
                                
            }

        
        } else {

                 echo "<p> No results found.</p>";
        
        }//end if($sql)
                ?>
        </table>
    </div>


    <script>
        $(document).ready(function () {
            $(".ddn").select2();
            $(document).on('change', '.tool_name', function (event) {
                var id = $(this).val();
                //alert(id);
                var thiss = $(this);
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    data: {id: id},
                    url: '<?php echo Yii::app()->createUrl('Tools/gettoolcode'); ?>',
                    success: function (data) {
                        // alert(data.tools);
                        $(".tool_code").html(data.tools);
                        //$(".item_name").val(data.name);
                        //thiss.closest('tr').find('.newcode').val(code);
                        //thiss.closest('tr').find('.item_name').val(data.name);

                        console.log(data);
                    }
                });
            });

        });
    </script>


    
