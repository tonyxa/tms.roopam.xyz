<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');
?>
<?php
Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#toolform').submit();

    });



");
?>
<div class="container" id="project">



    <h1>Service Report</h1>

    <form id="toolform"   action="<?php $url = Yii::app()->createAbsoluteUrl("tools/servicehistory"); ?>" method="POST">

        <div class="search-form" >
            <?php $sid = Status::model()->find(array('condition' => 'caption = "Tool" and status_type = "pending_status"'))->sid; ?>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Name : ', ''); ?>
                    <?php
                    //echo CHtml::textField('tool_name','',array('class'=>''));
                    $options = CHtml::listData(Tools::model()->findAll(array('select'=>'id,concat_ws(" ",tool_name,concat("(",ref_no,")")) as tool_name','condition' => 'active_status = "1" AND serialno_status ="Y"')), 'id', 'tool_name');
                    echo CHtml::dropDownList('tool_name', $tool_name, $options, array('class' => 'tool_name valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Code : ', ''); ?>
                    <?php
                    //echo CHtml::textField('tool_code',$tool_code,array('class'=>''));
                    $options = CHtml::listData(Tools::model()->findAll(array('condition' => 'active_status = "1" AND serialno_status ="Y"')), 'ref_no', 'ref_no');
                    echo CHtml::dropDownList('tool_code', $tool_code, $options, array('class' => 'tool_code valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <label></label>
                    <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-primary btn-sm')); ?>
                    <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('Servicehistory') . '"', 'class' => 'btn btn-sm default')); ?>

                </div>
            </div>
        </div>
    </form>

    <br/><br/>

    <?php if (!empty($tool)) {  ?>

<div class="table-responsive">
        <table class='table table-bordered'>
<?php if (!empty($tool)) {    ?>
    <tr>
        <th colspan="2" style="border-right: none;" class="tooldetails">
            <div>Tool Name : <?php echo strtoupper($tool['item_name']); ?></div>
            <div>Tool Code : <?php echo $tool['code']; ?></div>
            <div>Tool Category : <?php echo Tools::getToolCategoryname($tool['tool_id']); ?></div>
            <div>Parent Category : <?php echo Tools::getToolCategoryname($tool['tool_id'], 0); ?></div>
            <?php
                $name =  $tool['name'];
                if ($tool['caption'] == "Stock") {
                                $id = ($tool['tool_transfer_id'] != NULL)?$tool['tool_transfer_id']:NULL;
                                if ($id != NULL) {

                                    $tools = Yii::app()->db->createCommand("SELECT request_status FROM `tms_tool_transfer_items` left join tms_tool_transfer_request on tms_tool_transfer_request.id = `tms_tool_transfer_items`.id
                                     WHERE `tool_transfer_id` = ".$id)->queryRow();

                                    $stat = $tools['request_status'];
                                    $caption = Status::model()->findByPk($stat)['caption'];
                                    if ($caption == "Received back to stock") {
                                        $location = $tool['request_from'];
                                        $name = LocationType::model()->findByPk($location)['name'];
                                    }
                                }
                 }

                ?>
            <div>Present Status : <?= $name . "<b>(" . $tool['caption'] . ")</b>"; ?></div>


        </th>
        <th colspan="4" style="border-right: none;border-left: none;" class="tooldetails">
        <div>Company Sl No  : <?php echo $tool['serial_no']; ?></div>
        <div>Supplier Name  : <?php echo $tool['supplier_name']; ?></div>
        <div>Purchase Cost  : <?php echo round($tool['rate'], 2); ?></div>
        <div>Make  : <?php echo $tool['make']; ?></div>



        </th>
    </tr>
     <?php } else {
          echo "<p> No results found </p>";
        }
     ?>
    <tr style="background-color: #5d5d5d;color: #fff;font-weight: bold;font-size: 13px;">

        <th>Sl No</th>
        <th>Service date</th>
        <th>Service vendor</th>
        <th>Service report</th>
        <th>Service remarks</th>
        <th>Service cost</th>
    </tr>

    <?php

    $total_service_cost=0;
    if(!empty($service )){

        $k = 1;
        foreach ($service as  $data) {
            $total_service_cost+=$data['paid_amount'];

    ?>
    <tr>

        <td><?= $k; ?></td>
        <td><?= ($data['req_date'])?$data['req_date']:'--' ?></td>
        <td><?= ($data['name'])?$data['name']:'--' ?></td>
        <td><?= ($data['service_report'])?$data['service_report']:'--' ?></td>
        <td><?= ($data['remarks'])?$data['remarks']:'--' ?></td>
        <td><?= ($data['paid_amount'])?round($data['paid_amount'] ,2):'--'?></td>

    </tr>

    <?php  $k++; } ?>

     <tr>
         <td colspan="5"><span style="float: right;font-weight: bold;color: black">Total cost</span></td>
          <td class="disp"><?= $total_service_cost; ?> </td>

    </tr>
    <?php } else{ ?>

    <tr><td colspan="6"> No results found</td></tr>

   <?php } ?>


        </table>
    </div>

    <!--  preventive request  -->


  <div>

    <table class='table table-bordered' style="width:100%;margin: 0 auto;">

            <tr>
                <th colspan="6" style="text-align: center;background-color:#5d5d5d;color:#fff;font-weight: bold;font-size: 13px;">
                   Preventive Maintenance Services
                </th>
            </tr>
            <tr style="background-color: #5d5d5d;color: #fff;font-weight: bold;font-size: 13px;">

                <th>Sl No</th>
                <th>Service date</th>
                <th>Service vendor</th>
                <th>Service report</th>
                <th>Service remarks</th>
                <th style="width: 134px">Service cost</th>

            </tr>

            <?php

            $total_premain_cost = 0;

            if(!empty( $pre_request)){
                $k=1;
                foreach($pre_request as $key => $data) {
                   $total_premain_cost+= $data['cost'];
                ?>

            <tr>
              <td><?= $k; ?></td>
              <td><?= date('d-m-Y', strtotime($data['date'])) ?></td>
              <td><?= $data['name']; ?></td>
              <td><?= $data['report'] ?></td>
              <td><?= $data['remarks'] ?></td>
              <td><?= round($data['cost']) ?></td>

            </tr>

        <?php } $k++; ?>

        <tr>
            <td colspan="5"><span style="float: right;font-weight: bold;color: black">Total cost</span>
            </td>
            <td class="disp"><?= $total_premain_cost; ?> </td>

        </tr>

        <tr>
          <td colspan="5"><span style="float: right;font-weight: bold;color: black">Vendor service cost + Preventive maintenance cost</span></td>
          <td class="disp"><?= $total_premain_cost + $total_service_cost ; ?> </td>

        </tr>

        <?php  } else {  ?>
          <tr><td colspan="6">No result found</td></tr>
        <?php } ?>



    </table>


    </div><br/><br/>


    <!--  maintainance cost  -->

    <div class="table-responsive">

        <table  class='table table-bordered' style="width:100%;margin: 0 auto;">


            <tr>
                <td colspan="4" style="text-align: center;background-color:#5d5d5d;color: #fff;font-weight: bold;font-size: 13px;">Maintenance Cost</td>
            </tr>
             <?php if(!empty($service_details)) {
                    $wa_amount = 0;
                    $wo_amount = 0;

                   foreach($service_details as  $data) {

                        if(!empty($data['warranty_date']) && strtotime($data['warranty_date']) > strtotime($data['request_date']) ) {
                                     $wa_amount +=  $data['paid_amount'];

                            }
                          if($data['warranty_date']== NULL || strtotime($data['warranty_date']) < strtotime($data['request_date']))  {

                                   $wo_amount +=  $data['paid_amount'];
                            }

                     } ?>
            <tr>

              <!-- <td>Under Warranty  <?php //if($tool['warranty_date']!= NULL){ echo '( '.date('d-m-Y', strtotime($tool['warranty_date']) ). ' )' ; } else{ echo  ''; } ?></td>  -->
              <td>Under Warranty </td>
              <td class="disp" style="width: 129px;"><?= round($wa_amount) ?></td>
            </tr>
            <tr>
               <td>W/O Warranty</td>
               <td class="disp"><?= round($wo_amount) ?></td>
            </tr>

        <tr>

         <td><span style="float: right;font-weight: bold;color: black">Total maintenance cost</span></td>
          <td class="disp"><?= round($wa_amount + $wo_amount) ; ?> </td>

        </tr>

        <tr>
         <td><span style="float: right;font-weight: bold;color: black">Purchase Cost</span></td>
          <td class="disp"><?= round($tool['rate']); ?> </td>

        </tr>

        <tr>
            <?php
              $percentage = 90;
              $percnt = ($percentage / 100) * $tool['rate'];
              $totservicecost = $total_premain_cost + $total_service_cost;

            ?>
            <?php if( $totservicecost > $tool['rate'] ) { ?>
              <td colspan="2" style="text-align: center;background-color:#a93f3f;font-weight: bold;color: white; ">Service cost exceeds</td>
            <?php } else { ?>
             <td colspan="2" style="text-align: center;background-color:#80c595;font-weight: bold;color: white; ">Cost Effective</td>
            <?php } ?>

        </tr>

        <?php   } else{ ?>
        <tr><td>No results found</td></tr>

        <?php } ?>

    </table>


    </div><br/><br/>




    <!--  Project wise   -->

<div class="table-responsive" >
        <table class='table table-bordered'>

    <tr>
        <th colspan="4" style="text-align: center;background-color:#5d5d5d;color:#fff;font-weight: bold;font-size: 13px;">
        Project wise use
        </th>
    </tr>
    <tr style="background-color: #5d5d5d;color: #fff;">

        <th>Project</th>
        <th>No of days</th>
        <th>Project wise expense</th>

    </tr>

    <?php
    $j=1;
    $total_days=0;
    $project_cost=0;
    $tot_project_cost=0;
    $total_cost=0;
    $total_cost= $total_service_cost+$total_premain_cost;

    if (!empty($projects)) {
       foreach($projects as $k => $row){ ?>

    <tr>

        <td><?= $row['name'] ?></td>
        <td>
        <?php

        if ((count($projects ) - 1) < $k) {

            $date1 = date('Y-m-d');

        } else {

            $date1=  date('Y-m-d', strtotime($projects[$k]['req_date']));

        }

        $date2      = $row['req_date'];

        if($date2 != $date1){

            $datediff  = $date2 - $date1;
            $days =  floor($datediff / (60 * 60 * 24));
        }else{

             $days = 1;
        }
        echo  $days;

        ?>
        </td>

        <td>
         <?php

            $total_days = count($projects);
            $project_cost = (($total_cost + $tool['rate'] )/$total_days)*$days;
            $tot_project_cost += $project_cost;
            echo round($project_cost);
        ?>

        </td>


    </tr>

    <?php
       $j++; } ?>

       <tr >
         <td><span style="float:right;font-weight: bold;">Total</span></td>
         <td class="disp"><?= $total_days ?></td>
         <td class="disp"><?= round($tot_project_cost); ?></td>
       </tr>

       <?php  } else { ?>
         <tr><td colspan="3">No results found</td></tr>
      <?php  } ?>
       </table>

    </div>

    <?php  }else{ ?>

      <tr><td>No results found</td></tr>
   <?php  } ?>







    <script>
        $(document).ready(function () {
            $(".ddn").select2();
            $(document).on('change', '.tool_name', function (event) {
                var id = $(this).val();
               /* alert(id);*/
                var thiss = $(this);
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    data: {id: id},
                    url: '<?php echo Yii::app()->createUrl('Tools/gettoolcode'); ?>',
                    success: function (data) {
                        // alert(data.tools);
                        $(".tool_code").html(data.tools);
                        //$(".item_name").val(data.name);
                        //thiss.closest('tr').find('.newcode').val(code);
                        //thiss.closest('tr').find('.item_name').val(data.name);

                        console.log(data);
                    }
                });
            });

        });
    </script>


    <style>

        .btn.default, .btn-primary{
            margin-top: 20px;
        }
        div label {
            font-weight: bold;
            font-size: 0.9em;
            display: block;
        }
        .thead th{
            background-color: #5d5d5d;
            color: #fff;
            border-top: 1px solid transparent !important;
            border-bottom: 1px solid transparent !important;
        }
        .table>tbody>tr>th, .table-bordered>tbody>tr>td{

            border: 1px solid #ccc;
        }
        .disp{
           font-weight: bold;
            font-size: 13px;
        }
        .tooldetails div{padding: 3px;}


    </style>
