<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'preventive-request-form1',
	 'enableAjaxValidation' => true,
       'clientOptions' => array(
           'validateOnSubmit' => true,
           'validateOnChange' => true,
           'validateOnType' => false,
           ),
)); ?>



	

	<div class="row">
        <div class="col-md-4">
           <label style="font-size: 50%; ">Tool name : &nbsp; <?= $newmodel->tool_name; ?></label>
           <label>Ref no : &nbsp; <?= $newmodel->ref_no; ?></label>
        </div>

       

	</div><br>

    <div class="row">
       <div class="col-md-4">
            <?php echo $form->labelEx($model,'cost'); ?>
            <?php echo $form->textField($model,'cost' , array('class'=>'form-control','readonly'=> true)); ?>
            <?php echo $form->error($model,'cost'); ?>
        </div>
    </div>

    <div class="row">
      <div class="col-md-4">

    		<?php echo $form->labelEx($model,'report'); ?>
    		<?php echo $form->textarea($model,'report',array('class'=>'form-control','size'=>60,'maxlength'=>100,'readonly'=> true)); ?>
    		<?php echo $form->error($model,'report'); ?>
      </div>
	</div>



  <div class="row">
        <div class="col-md-4">
            <?php echo $form->labelEx($model,'date'); ?>
             <?php  
             $model->date = date('d-m-Y' ,strtotime($model->date));

                echo $form->textField($model,'date', array('class'=>'form-control datepicker','disabled'=>'disabled')); ?> 
          
            <?php echo $form->error($model,'date'); ?>
        </div>
  </div>



	<div class="row">
      <div class="col-md-4">
    		<?php echo $form->labelEx($model,'remarks'); ?>
    		<?php echo $form->textarea($model,'remarks',array('class'=>'form-control remarks','size'=>60,'maxlength'=>100)); ?>
    		<?php echo $form->error($model,'remarks'); ?>
      </div>
	</div> 

	

	


	<div class="row buttons ">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Approve' : 'Approve' , array('class'=>'btn blue submit' , 'style'=>'margin-left: 24px; margin-top: 20px;' )); ?>

    <?php //echo CHtml::submitButton($model->isNewRecord ? 'Reject' : 'Reject' , array('class'=>'btn red reject' , 'style'=>'margin-left: 24px; margin-top: 20px;' )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">
$(document).ready(function () {



    $( function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
      });


     setTimeout(function() {
           $('#message').fadeOut('slow');
       }, 3000);


     /* submit function */

          $("#preventive-request-form1").validate({
            rules: {
              
                'PreventiveRequest[remarks]': {
                    required: true

                },
                
            },
            messages: {

                'PreventiveRequest[remarks]': {
                    required: "Please add Remarks",
                },
                
            },
            submitHandler: function () {
      /*  alert('hi'); */
                var formData = JSON.stringify($("#preventive-request-form1").serializeArray());
                $.ajax({
                    method: "POST",
                    data: {formdata: formdata},
                    url: '<?php echo Yii::app()->createUrl('tools/PrevApprove'); ?>',
                    success: function (data) {
                    }
                });
            }
        });
  


    });
    
</script>

<style type="text/css">

  .error{
    color: red;
  }
</style>


