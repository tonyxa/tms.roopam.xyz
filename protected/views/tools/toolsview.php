<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick='$("#addtools").css({"display":"none"})'>&times;</button>
            <h2 class="modal-title">Tools</h2>
        </div>

        <div class="modal-body">
                   
            <h3> <?php   echo $page_type;?></h3>
                   
            <div class="row modalrowdiv ">
                <?php
               if($page_type == 'vendor')
                {
					$this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'tools-grid',
                    'dataProvider' => $model->vendorsearch($page, $loc,$type,''),
                    'itemsCssClass' => 'table table-bordered',
                    'selectableRows' => 1,
                    'selectionChanged' => 'function(id){if($.fn.yiiGridView.getSelection(id)!=""){ var code = $(".codeval").val();'
                    . 'var id = $.fn.yiiGridView.getSelection(id);'
                    . '$.ajax({
                        type : "POST",
                        dataType : "JSON",
                        data: {id: id},
                        url: "'. $this->createUrl("Tools/gettooldetails").'",
                        success: function (data) {
                        $("#addtools").css({"display":"none"})
                        $("#"+code+"").closest("tr").find(".newcode").val(id);
                        $("#"+code+"").closest("tr").find(".code").val(data.refcode);
                        $("#"+code+"").closest("tr").find(".item_name").val(data.name);
                        $("#"+code+"").closest("tr").find(".unit").html(data.units);
                        $("#"+code+"").closest("tr").find(".unit").val(data.unit);
                        $("#"+code+"").closest("tr").find(".quantity").val(data.qty);
                        }
                        });'
                    . '}}',
                     'filter' => $model,
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('width' => '50px')),
                        //'tool_code',
                        array(
                            'name' => 'ref_no',
                            'type' => 'raw',
                            'htmlOptions' => array('width' => '80px'),
                        ),
                        'tool_name',
                        
                        'statuscaption',
                    ),
                ));
                
			    } else { 
                
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'tools-grid',
                    'dataProvider' => $model->search($page, $loc,$type,''),
                    'itemsCssClass' => 'table table-bordered',
                    'selectableRows' => 1,
                    'selectionChanged' => 'function(id){if($.fn.yiiGridView.getSelection(id)!=""){ var code = $(".codeval").val();'
                    . 'var id = $.fn.yiiGridView.getSelection(id);'
                    . '$.ajax({
                        type : "POST",
                        dataType : "JSON",
                        data: {id: id},
                        url: "'. $this->createUrl("Tools/gettooldetails").'",
                        success: function (data) {
                        $("#addtools").css({"display":"none"})
                        $("#"+code+"").closest("tr").find(".newcode").val(id);
                        $("#"+code+"").closest("tr").find(".code").val(data.refcode);
                        $("#"+code+"").closest("tr").find(".item_name").val(data.name);
                        $("#"+code+"").closest("tr").find(".unit").html(data.units);
                        $("#"+code+"").closest("tr").find(".unit").val(data.unit);
                        $("#"+code+"").closest("tr").find(".quantity").val(data.qty);
                        }
                        });'
                    . '}}',
                     'filter' => $model,
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('width' => '50px')),
                        //'tool_code',
                        array(
                            'name' => 'ref_no',
                            'type' => 'raw',
                            'htmlOptions' => array('style'=>'width: 80px'),
                        ),
                        'tool_name',
                    ),
                ));
                
			}
                ?>
            </div>
        </div>
    </div>

</div>
<input type="text" style="display:none" value="<?php echo $code; ?>" name="codeval" class="codeval"/>
