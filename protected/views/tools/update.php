<?php
/* @var $this ToolsController */
/* @var $model Tools */

$this->breadcrumbs=array(
	'Tools'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<h1>Update Tools</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model,'work_sites'=>$work_sites,'newmodel' => $newmodel)); ?>

