<style type="text/css">
.dest{
    color: #777777d1;
    font-size: 100%;
}
.checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]{position:static;margin-left:0px;}
.tool-details div{padding: 3px;}

.has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
/*    body{
        margin-top: 100px;
        font-family: 'Trebuchet MS', serif;
        line-height: 1.6
    }
    */

    .tooset_qty, .qty{
    border: 1px solid #AAAAAA !important;
    /* height: 24px; */
    }

    ul.tabs{
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    ul.tabs li{
        background: none;
        color: #222;
        display: inline-block;
        padding: 10px 15px;
        cursor: pointer;
    }

    ul.tabs li.current{
        background: #ededed;
        color: #222;
    }

    .tab-content{
        display: none;
        background: #ededed;
        padding: 15px;
    }

    .tab-content.current{
        display: inherit;
    }
    .blue, .default{
        margin-top: 20px;
    }

</style>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);

Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');


?>

<h1>Create Tool/Tool Set</h1>

<?php //echo $this->renderPartial('_form', array('model'=>$model,'unitRes'=>$unitRes,'newmodel'=>$newmodel));  ?>


        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Tool</li>
            <li class="tab-link" data-tab="tab-2">Tool Set</li>
        </ul>
        <div id="tab-1" class="tab-content current">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'tools-form',
                   // 'enableAjaxValidation'=>true,
                   // 'enableClientValidation' => true,
                   //  'clientOptions' => array(
                   // 'validateOnSubmit' => true,
                   // 'validateOnChange' => true,
                   // 'validateOnType' => false,),


                ));
            ?>


             <div class="row">
                   <h4 style="padding: 10px;">Tool Details</h4>

                <div class="table-responsive">
                    <table class='table table-bordered'>
                       <tr>
                          <th class="tool-details" style="border-right: none;width:25%;font-weight: bold;">
                              <div>Tool Name :  <span class="dest"><?= $item_model['item_name'] ?></span></div>
                              <div>Make :  <span class="dest"><?= $item_model['make'] ?></span></div>
                              <div>Model No :  <span class="dest"><?= $item_model['model_no'] ?></span></div>
                              <div>Quantity :  <span class="dest"><?= $item_model['qty'] ?></span></div>
                              <div>Remaining Qty :  <span class="dest"><?= $item_model['remaining_qty'] ?></span></div>

                          </th>
                           <th class="tool-details" style="border-right: none;border-left: none;">
                           <div> Company Sl No  :  <span class="dest"><?= $item_model['serial_no']; ?></span></div>
                           <div>Batch NO    :  <span class="dest"><?= $item_model['batch_no']; ?></span></div>
                           <div>Purchase Cost  :  <span class="dest"><?= round($item_model['rate'], 2); ?></span></div>
                           <div>Warranty Date :  <span class="dest"><?=  ($item_model['warranty_date']!='')?date('d-m-Y', strtotime($item_model['warranty_date'])):" -- "; ?></span></div>
                           </th>

                       </tr>
                    </table>
               </div>

             </div>
             <br>

             <h3>Create Tool</h3>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->hiddenField($model,'type',array('value'=>'tool')); ?>
                    <?php

                    $model->type ='tool';

                    ?>
                </div>
            </div>

            <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'parent_category'); ?>
            <?php
            echo $form->dropDownList($model, 'parent_category', CHtml::listData(
                    ToolCategory::model()->findAll( array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL AND active_status ="1"',
                              'order'=>'cat_name'
                             )),"cat_id", "cat_name"
                    ), array('class' => 'valid ddn form-control', 'empty' => 'Please choose',
                    'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                           $("#childCategory").html(data);

                        }'
                    )));
            ?>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">

            <?php echo "<label>Child Category</label>";?>
            <?php
             $parent = (isset($_GET['Tools']['parent_category'])? $_GET['Tools']['parent_category'] : '' );

             $sel = (isset($_GET['childCategory'])? $_GET['childCategory'] : '' );
            ?>

            <?php

            if(empty($sel)){
            $options[''] = 'Select Child';
            }else{
            $options[''] = '';
            }

            echo CHtml::dropDownList('childCategory',array(),$options,array('class' => 'valid ddn form-control',
            'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                        $("#subchildCategory").html(data);
                        }'
                )));


            ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo "<label>Sub Child Category</label>";?>
            <?php


             $selsub1 = (isset($_GET['subchildCategory'])? $_GET['subchildCategory'] : '' );

             $options1[''] = 'Select Sub Child';
             echo CHtml::dropDownList('subchildCategory',array(),$options1,array('class' => 'valid ddn form-control'));
            ?>
        </div> <br>

        <div class="col-md-3" style="margin-top: -18px; ">
                    <?php echo $form->labelEx($model, 'Location'); ?>
                    <?php
                    echo $form->dropDownList($model, 'location', CHtml::listData($model->showResults(), "id", "name"), array('class' => 'ddn form-control'));
                    ?>
                    <?php echo $form->error($model, 'location'); ?>
        </div>
<!--            </div>
            <br>

            <div class="row">-->
                <div class="col-md-3" style="margin-top: 17px; ">
                    <?php echo $form->labelEx($model, 'qty'); ?>
                    <?php echo $form->textField($model, 'qty', array('class' => 'form-control qty')); ?>
                    <?php echo $form->error($model, 'qty'); ?>
                </div>
                <div class="col-md-3" style="margin-top: 17px; ">
                    <?php echo $form->labelEx($model, 'serialno_status',array('style'=>'display:block;')); ?>
                    <?php
                    $model->serialno_status = 'N';
                    echo $form->radioButtonList($model, 'serialno_status', array('Y' => 'Yes', 'N' => 'No'), array('class' => 'form-control serialno_status', 'separator' => '', 'labelOptions' => array('style' => 'display:inline'))); ?>
                    <?php echo $form->error($model, 'serialno_status'); ?>
                </div>

                <div class="col-md-2 prev_main_section" style="margin-top: 34px; ">

                    <?php echo $form->labelEx($model, 'prev_main'); ?>
                       <input type="checkbox" name="prev_main" id="pr_main" <?php if(isset($item_model->prev_main) && $item_model->prev_main==1){ echo "checked" ;}else{ echo NULL ; } ?> >

                </div>
                <input type="hidden"  id="prev_main_db" value="<?php echo (isset($item_model->prev_main))?$item_model->prev_main:"" ?>">

                <div class="col-md-2" style="margin-top: 17px; ">

                    <label class="hrs">Hours</label>
                    <input type="text" name="prev_hrs" class="hrs form-control" id="prev_hrs" value="<?php echo (isset($item_model->prev_hrs) )?$item_model->prev_hrs:NULL ?>">

                </div>



<!--            </div>  <br>
            <div class="row save-btnHold">-->
		<div class="col-md-3 save-btnHold">
                    <?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue btn-sm subconfirm')); ?>
                    <button data-dismiss="modal" class="btn btn-sm default btn-default" onclick="javascript:window.location.reload()">Cancel</button>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div id="tab-2" class="tab-content">
            <button class="toolsetlink btn btn-sm blue" style="float:right;cursor:pointer;margin-top:0px;">Create Tool Set</button>
            <div class ="toolset" style="display:none">
            <?php

            $id = isset($_GET['id'])?$_GET['id']:0;

                $this->renderPartial('toolsetcreate',array(
            			'model'=>$model,'id' =>$id,
            		));
                ?>
            </div>


             <div class="row">
                   <h4 style="padding: 10px;">Tool Set Details</h4>

                <div class="table-responsive">
                    <table class='table table-bordered'>
                       <tr>
                          <th class="tool-details" style="border-right: none;width:25%;font-weight: bold;">
                            <div>  Tool Name :  <span class="dest"><?= $item_model['item_name'] ?></span></div>
                            <div>Make :  <span class="dest"><?= $item_model['make'] ?></span></div>
                            <div>Model No :  <span class="dest"><?= $item_model['model_no'] ?></span></div>
                            <div>Quantity :  <span class="dest"><?= $item_model['qty'] ?></span></div>
                            <div>Remaining Qty :  <span class="dest"><?= $item_model['remaining_qty'] ?></span></div>
                          </th>
                           <th class="tool-details" style="border-right: none;border-left: none;">
                           <div> Company Sl No  :  <span class="dest"><?= $item_model['serial_no']; ?></span></div>
                           <div>Batch NO    :  <span class="dest"><?= $item_model['batch_no']; ?></span></div>
                           <div>Purchase Cost  :  <span class="dest"><?= round($item_model['rate'], 2); ?></span></div>
                           <div>Warranty Date :  <span class="dest">
                                  <?= ($item_model['warranty_date']!='')?date('d-m-Y', strtotime($item_model['warranty_date'])):""; ?>
                                </span></div> 
                            </th>

                       </tr>
                    </table>
               </div>

             </div>
             <br>
             <h4>Add to Tool Set</h4>
            <div class="row">

                <?php
                    $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'tools-set-form',

                        ));
                  ?>
                <div class="col-md-3">

                    <?php echo $form->hiddenField($model,'type',array('value'=>'toolset')); ?>
                    <?php echo $form->labelEx($model, 'Tool Set'); ?>
                    <?php //echo $form->textField($model, 'request_to', array('maxlength' => 50, 'class' => 'form-control'));  ?>
                    <?php
                    echo $form->dropDownList($model, 'tool_name', CHtml::listData(Tools::model()->findAll(
                                            array(
                                                'select' => array('id,ref_no,tool_category'),
                                                'condition'=>'pending_status = 17',
                                    )), "id", "ref_no"), array('class' => 'ddn form-control', 'empty' => 'Please choose','style'=>'width:100% !important;border: 1px solid #AAAAAA !important;height: 28px;'));
                    ?>
                    <?php echo $form->error($model, 'tool_name'); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $form->labelEx($model, 'qty'); ?>
                    <?php echo $form->textField($model, 'qty', array('class' => 'form-control toolset_qty','style'=>'width:100% !important;border: 1px solid #AAAAAA !important;')); ?>
                    <?php echo $form->error($model, 'qty'); ?>
                </div>






<!--            <div class="row save-btnHold">-->
		<div class="col-md-6 save-btnHold ">
                    <?php echo CHtml::Button($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue btn-sm subtoolset')); ?>
                    <button data-dismiss="modal" class="btn default btn-sm btn-default" onclick="javascript:window.location.reload()">Cancel</button>
<!--				</div> -->
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>





<style>
    
</style>

<script type="text/javascript">

    $(document).ready(function () {
        $(".prev_main_section").hide();

              $(".subconfirm").click(function(){

                  var serial_no =   $("input[name='Tools[serialno_status]']:checked").val();
                  var msg ='';
                  if( serial_no == 'Y'){
                    msg = ' with Serial Number';
                  }else{
                    msg = ' without Serial Number';
                  }
                   if($("#tools-form").valid()) {
                        if(confirm('Are you sure to create a tool ' +msg)) {

                            document.getElementById("tools-form").submit();
                        }
                    }

              });

              $(".subtoolset").click(function(){

                  //  document.getElementById("tools-set-form").submit();

                   if($("#tools-set-form").valid()) {

                          document.getElementById("tools-set-form").submit();

                    }

              });





        $("#tools-form").validate({


            rules: {
                'Tools[parent_category]': {
                    required: true,
                    //digits: true
                },

                // 'childCategory': {
                //     required: true,
                //     //digits: true
                // },

                'Tools[location]': {
                    required: true,

                },
                'Tools[qty]': {
                    required: true,
                    digits: true,
                    valid: true,

                },

            },
            messages: {
                'Tools[tool_category]': {
                    required: "Tool Category is required",
                    valid: '',
                },
                'Tools[location]': {
                    required: "Location is required",
                     valid: '',
                },
                'Tools[qty]': {
                    required: 'Required',
                    digits: 'Quantity must be an integer',
                    valid: 'Requested quantity is not available',
                },

            },
            submitHandler: function () {

                var formData = JSON.stringify($("#tools-form").serializeArray());

                $.ajax({
                    method: "POST",
                    data: {formdata: formData},
                    url: '<?php echo Yii::app()->createUrl('tools/toolcreate'); ?>',
                    success: function (data) {
                        alert(data)
                        return false;
                    }
                });
            }
        });


         $("#tools-set-form").validate({
            rules: {
                'Tools[tool_name]': {
                    required: true,
                    //digits: true
                },
                'Tools[qty]': {
                    required: true,
                    digits: true,
                    validqty: true,

                },

            },
            messages: {
                'Tools[tool_name]': {
                    required: "Tool Set is required",
                },
                'Tools[qty]': {
                    required: 'Required',
                    digits: 'Quantity must be an integer',
                    validqty: 'Requested quantity is not available',
                },


            },
            submitHandler: function () {

                var formData = JSON.stringify($("#tools-set-form").serializeArray());
                $.ajax({
                    method: "POST",
                    data: {formdata: formData},
                    url: '<?php echo Yii::app()->createUrl('tools/toolcreate'); ?>',
                    success: function (data) {
                        alert(data)
                        return false;
                    }
                });
            }
        });

    $('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

        $('.toolsetlink').click(function(){
            $('.toolset').toggle();


        });


        // new prev main

        var prmain= $('#prev_main_db').val();

        if(prmain==1){
           $('.hrs').show();

        }else{
           $('.hrs').hide();

        }

    $('#pr_main').change(function() {

        if(this.checked==true){
          $('.hrs').show();
        }else{
          $('.hrs').hide();
        }

    });






    });
    jQuery.validator.addMethod("valid",
            function (qty, element) {
                var qty = $('#Tools_qty').val();

                var id = <?php echo isset($_GET['id'])?$_GET['id']:0; ?>;

             //alert(qty);
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo Yii::app()->createUrl('Tools/checkvalidation'); ?>',
                    data: {id: id, qty: qty},
                    success: function (data) {
                        result = (data == "true") ? true : false;
                    }
                });

                // return true if username is exist in database
                return result;
            }
    );
    jQuery.validator.addMethod("validqty",
            function (qty, element) {
                var qty = $('.toolset_qty').val();
                var id = <?php echo isset($_GET['id'])?$_GET['id']:0; ?>;

             //alert(qty);
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo Yii::app()->createUrl('Tools/checkvalidation'); ?>',
                    data: {id: id, qty: qty},
                    success: function (data) {
                        result = (data == "true") ? true : false;
                    }
                });

                // return true if username is exist in database
                return result;
            }
    );

    $(document).ready(function () {
//var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
        $(".ddn").select2();
        $(".ddn1").select2();
    });


$('#Tools_tool_category').change(function(){
    $('#toolcat_text').html($("#Tools_tool_category option:selected").text());
});

$(document).on('click', '.serialno_status', function () {
    var val = $(this).val();
    if (val == 'Y') {
        $(".prev_main_section").show();
    } else {
        $(".prev_main_section").hide();
    }
});

</script>

</div>

</div>

