<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<?php if(Yii::app()->user->hasFlash("error")): ?>
      <div id="message" style="color: red;font-weight: bold;font-size: 100%;">
          <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
<?php endif; ?>

<h2 style="margin-left: 17px;">Request</h2>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'preventive-request-form',
	 'enableAjaxValidation' => true,
       'clientOptions' => array(
           'validateOnSubmit' => true,
           'validateOnChange' => true,
           'validateOnType' => false,
           ),
)); ?>





	

	<div class="row">
        <div class="col-md-4">
           <label style="font-size: 50%; ">Tool name : &nbsp; <?= $newmodel->tool_name; ?></label>
           <label>Ref no : &nbsp; <?= $newmodel->ref_no; ?></label>
        </div>

       

	</div><br>

    <div class="row">
       <div class="col-md-4">
            <?php echo $form->labelEx($model,'cost'); ?>
            <?php echo $form->textField($model,'cost' , array('class'=>'form-control',)); ?>
            <?php echo $form->error($model,'cost'); ?>
        </div>
    </div>

    <div class="row">
      <div class="col-md-4">

		<?php echo $form->labelEx($model,'report'); ?>
		<?php echo $form->textarea($model,'report',array('class'=>'form-control','size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'report'); ?>
      </div>
	</div>

<!-- 	<div class="row">
       <div class="col-md-4">
		<?php //echo $form->labelEx($model,'remarks'); ?>
		<?php //echo $form->textField($model,'remarks',array('class'=>'form-control','size'=>60,'maxlength'=>100)); ?>
		<?php //echo $form->error($model,'remarks'); ?>
       </div>
	</div> -->

	

	
    <div class="row">
        <div class="col-md-4">
            <?php echo $form->labelEx($model,'date'); ?>
             <?php  echo $form->textField($model,'date', array('class'=>'form-control datepicker',)); ?> 
          
            <?php echo $form->error($model,'date'); ?>
        </div>
    </div>


	<div class="row buttons ">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Request' : 'Request' , array('class'=>'btn blue' , 'style'=>'margin-left: 24px; margin-top: 20px;')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">
$(document).ready(function () {

    $( function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
      });


     setTimeout(function() {
           $('#message').fadeOut('slow');
       }, 3000);
  


    });
    
</script>


