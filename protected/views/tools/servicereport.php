<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css'); 
?>
<div class="container" id="project">
    <div class="row">
        
        
    <form id="toolform"   action="<?php $url = Yii::app()->createAbsoluteUrl("tools/toolhistory"); ?>" method="POST">

        <div class="search-form" >
            <?php $sid = Status::model()->find(array('condition' => 'caption = "Tool" and status_type = "pending_status"'))->sid; ?>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Name : ', ''); ?>
                    <?php
                    //echo CHtml::textField('tool_name','',array('class'=>'')); 
                    $options = CHtml::listData(Tools::model()->findAll(array('select'=>'id,concat_ws(" ",tool_name,concat("(",ref_no,")")) as tool_name','condition' => 'active_status = "1"')), 'id', 'tool_name');
                    echo CHtml::dropDownList('tool_name', $tool_name, $options, array('class' => 'tool_name valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div> 
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?php echo CHtml::label('Tool Code : ', ''); ?> 
                    <?php
                    //echo CHtml::textField('tool_code',$tool_code,array('class'=>''));
                    $options = CHtml::listData(Tools::model()->findAll(array('condition' => 'active_status = "1"')), 'ref_no', 'ref_no');
                    echo CHtml::dropDownList('tool_code', $tool_code, $options, array('class' => 'tool_code valid ddn form-control', 'empty' => 'Please Select'));
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <label></label><br>
<?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn default')); ?> 
<?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('servicereport') . '"', 'class' => 'btn default')); ?>

                </div>
            </div> 
        </div>
    </form>

        <br><br>
       <h1>Service Report</h1>


       <table class="table table-bordered">
           
           <tr>
               <th style="text-align: center;" colspan="6">SERVICE REPORT-DATE</th>
           </tr>
           
           <tr>
               <td colspan="6">                                      
                   <div class="col-md-12">                       
                       <div class="row">
                       <div class="col-md-6">TOOL NAME : <?php if(isset($tooldet[0]['tname'])){
                       echo '<b>'.$tooldet[0]['tname'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?> </div>
                      
                        <div class="col-md-6">PURCHASE COST : 
                        <?php if(isset($tooldet[0]['amount'])){
                       echo '<b>'.$tooldet[0]['amount'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                        </div>   
                       </div><br>
                                                
                       <div class="row">
                       <div class="col-md-6">TOOL CATEGORY : 
                       <?php if(isset($tooldet[0]['tcname'])){
                       echo '<b>'.$tooldet[0]['tcname'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                       </div>
                        <div class="col-md-6">SUPPLER NAME : 
                        <?php if(isset($tooldet[0]['supplier_name'])){
                       echo '<b>'.$tooldet[0]['supplier_name'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                        </div>   
                       </div><br>
                       <div class="row">
                       <div class="col-md-6">PARENT CATEGORY :                        
                       <?php if(isset($tooldet[0]['parent_category'])){
                       echo '<b>'.$tooldet[0]['parent_category'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                       </div>
                       <div class="col-md-6">MAKE : 
                       <?php if(isset($tooldet[0]['make'])){
                       echo '<b>'.$tooldet[0]['make'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                        </div>   
                       </div><br>
                        <div class="row">
                       <div class="col-md-6">PRESENT STATUS : 
                       <?php if(isset($tooldet[0]['present_status'])){
                       echo '<b>'.$tooldet[0]['present_status'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                       </div>
                       <div class="col-md-6">COMPANY SL NO : 
                       <?php if(isset($tooldet[0]['company_slno'])){
                       echo '<b>'.$tooldet[0]['company_slno'].'</b>';
                       }else{
                       echo "";    
                       }
                          ?>
                        </div>   
                            
                        
                       </div><br>
                   </div>
               </td>                           
           </tr>
           <tr >
               <th>SL NO</th>
               <th>SERVICE DATE </th>
               <th>SERVICE VENDOR</th>
               <th>SERVICE REPORT</th>
               <th>SERVICE COST</th>
               <th>REMARKS</th>
           </tr>
           
          
           <tr>
                <?php for($i=0;$i<count($vendorrec);$i++){ ?>
               <td><?php echo $i+1;?></th>
               <td><?php echo $vendorrec[$i]['req_date']; ?> </td>
               <td><?php echo $vendorrec[$i]['req_date']; ?> </td>
               <td><?php echo $vendorrec[$i]['service_report']; ?> </td>
               <td><?php echo $vendorrec[$i]['paid_amount']; ?> </td>
               <td><?php echo $vendorrec[$i]['remarks']; ?> </td>
                <?php } ?>               
           </tr>
           
           <tr>
               <td colspan="4" style="text-align: right;">Total</td>
               <td><?php if(isset($service_total)){ echo $service_total; }else{ echo ""; } ?></td>
               <td></td>
           </tr>
           <tr>
               <td colspan="1"></td>
               <td colspan="4"><br>
           <div class="row">
               <div class="col-md-12">
                   <div class="col-md-2"></div>
                   <div class="col-md-8">
                       <table class="table table-bordered">
                           <tr>
                               <th colspan="3" style="text-align:center;" >TOTAL USE PROJECT WISE</th>
                           </tr>
                           <tr>
                               <td>PROJECT</td>
                               <td>NO OF DAYS</td>
                               <td>PROJECT WISE EXPENSE</td>
                           </tr>
                           <tr>
                               <td>TOTAL NO OF DAYS</td>
                               <td></td>
                               <td></td>
                           </tr>
                       </table>  
                   </div>
                   <div class="col-md-2"></div>
               </div>
           </div>
                   
                   <div class="row">
               <div class="col-md-12">
                   <div class="col-md-2"></div>
                   <div class="col-md-8">
                       <table class="table table-bordered">
                           <tr>
                               <th colspan="3" style="text-align:center;" >MAINTENANCE  COST</th>
                           </tr>
                           <tr>
                               <td>UNDER WRNTY</td>
                               <td>W/O WRNTY</td>
                               <td>PURCHASE COST</td>
                           </tr>
                            <tr>
                               <td>TOTAL</td>
                               <td></td>
                               <td></td>
                           </tr>
                       </table>  
                   </div>
                   <div class="col-md-2"></div>
               </div>
           </div>
               </td>
               <td colspan="1"></td>
           </tr>

       </table>
       
</div>
    </div>
    <script>
        $(document).ready(function () {
            $(".ddn").select2();
            $(document).on('change', '.tool_name', function (event) {
                var id = $(this).val();
                //alert(id);
                var thiss = $(this);
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    data: {id: id},
                    url: '<?php echo Yii::app()->createUrl('Tools/gettoolcode'); ?>',
                    success: function (data) {
                        // alert(data.tools);
                        $(".tool_code").html(data.tools);
                        //$(".item_name").val(data.name);
                        //thiss.closest('tr').find('.newcode').val(code);
                        //thiss.closest('tr').find('.item_name').val(data.name);

                        console.log(data);
                    }
                });
            });

        });
    </script>