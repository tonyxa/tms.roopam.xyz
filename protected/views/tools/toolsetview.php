

<h1>Tool Set </h1>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'toolset-form',

    ));
    ?>
    
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($newmodel, 'ref_no'); ?>
                    <?php echo $form->textField($newmodel, 'ref_no', array('class' => 'form-control','readonly'=> true)); ?>
                    <?php echo $form->error($newmodel, 'ref_no'); ?>
                </div>
                <div class="col-md-6">
                      <?php $newmodel->tool_category = $newmodel->toolCategory->cat_name; ?>
                    <?php echo $form->labelEx($newmodel, 'tool_category'); ?>
                    <?php echo $form->textField($newmodel, 'tool_category', array('maxlength' => 50, 'class' => 'form-control' ,'readonly'=> true)); ?>
                    <?php echo $form->error($newmodel, 'tool_category'); ?>
                </div>

                <div class="col-md-6">
                    <?php $newmodel->tool_category = $newmodel->location0->name; ?>
                    <?php echo $form->labelEx($newmodel, 'location'); ?>
                    <?php echo $form->textField($newmodel, 'location', array('maxlength' => 50, 'class' => 'form-control','readonly'=> true)); ?>
                    <?php echo $form->error($newmodel, 'location'); ?>
                </div><br/>
              
           
            
                <div class="col-md-12"> <br/>
                  <input type="hidden"  id="prev_main_db" value="<?= $newmodel->prev_main ?>">
 
                <div class="col-md-3">
                    <?php echo $form->labelEx($newmodel, 'prev_main'); ?>
                    <input type="checkbox" name="prev_main" id="pr_main" <?php if($newmodel->prev_main==1){ echo "checked" ;}else{ echo NULL ; } ?> >
                 
                </div>

                <div class="col-md-3" >
                
                    <label class=hrs >Hours</label>
                    <input type="text" name="prev_hrs" class="hrs"  value="<?= ($newmodel->prev_hrs)?$newmodel->prev_hrs:NULL ?>">
                 
                </div>

                

                <div class="col-md-3" class=dat>
                    <label class=dat >Date</label>
                    <?php 
                    if($newmodel->prev_date != NULL ){
                        $newmodel->prev_date= date('Y-m-d', strtotime($newmodel->prev_date ));
                    }else{
                        $newmodel->prev_date = NULL;
                    }
                    

                 echo CHtml::activeTextField($newmodel, 'prev_date' , array( 'class' => 'inputs form-control target dat')); 
               
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Tools_prev_date',
                    'ifFormat' => '%Y-%m-%d',
                )); 

                ?>

                    
                   
                </div>

                </div>

                 </div>

        


            
            <div class="row save-btnHold">
               <div class="col-md-12 text-center">
                        <br>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue' ,'style'=>"margin-bottom: -36px;")); ?>
                        
                        

                       </div>
                </div>
             <?php $this->endWidget(); ?>


<h1>Tool Set Items</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'tool-set-grid',
    'itemsCssClass' => 'table table-bordered',
    'dataProvider'=>$model->search($page,$id),
    'filter'=>$model,
    'columns'=>array(
       // 'id',
         array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('style' => 'width:20px;'),),
//         array(
//                        'name' => 'toolset_id',
//                       // 'value' => '(isset($data->toolset["tool_name"]) && isset($data->toolset))?$data->toolset["tool_name"]:""',
//             ),
        //'toolset_id',
        //'category',
//        array(
//                        'name' => 'category',
//                        'value' => 'isset($data->category0["cat_name"])?$data->category0["cat_name"]:""',
//             ),
        'item_name',
        'qty',
    ),
)); ?>


<script type="text/javascript">

$(document).ready(function () {

   // new prev main

        var prmain= $('#prev_main_db').val();

       

        if(prmain==1){
           $('.hrs').show();
            $('.dat').show();
          
        }else{
           $('.hrs').hide();
           $('.dat').hide();
          
        }

    $('#pr_main').change(function() {
        
        if(this.checked==true){
          $('.hrs').show();
           $('.dat').show();
        }else{
          $('.hrs').hide();
           $('.dat').hide();
        }

    });


});

    

</script>