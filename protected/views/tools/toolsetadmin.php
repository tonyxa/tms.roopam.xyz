<?php
/* @var $this ToolsController */
/* @var $model Tools */
/*
  $this->breadcrumbs=array(
  'Tools'=>array('index'),
  'Manage',
  );

  $this->menu=array(
  array('label'=>'List Tools', 'url'=>array('index')),
  array('label'=>'Create Tools', 'url'=>array('create')),
  );

  Yii::app()->clientScript->registerScript('search', "
  $('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
  });
  $('.search-form form').submit(function(){
  $.fn.yiiGridView.update('tools-grid', {
  data: $(this).serialize()
  });
  return false;
  });
  ");
 * 
 */
?>

<h1>Manage Tool Set</h1>

<?php /*

  <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
  <div class="search-form" style="display:none">
  <?php $this->renderPartial('_search',array(
  'model'=>$model,
  )); ?>
  </div><!-- search-form -->
 */ ?>
<div class="">
    <?php /* if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) { ?>
        <button data-toggle="modal" data-target="#addtools"  class="btn blue createtools">Add Tools</button>
    <?php } */?>
</div>
<div class="table-responsive">
<?php
    $form = $this->beginWidget('CActiveForm', array(
           'action'=>Yii::app()->createUrl('Tools/changestatus'),  
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tools-grid',
    'dataProvider' => $model->search('toolset'),
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),    
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    'filter' => $model,
    'columns' => array(
      
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        /*'ref_no',*/
        'tool_name',
       /* array(
            'name' => 'tool_category',
            'value' => '$data->toolCategory->cat_name',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
            'filter' => CHtml::listData(ToolCategory::model()->findAll(
                            array(
                                'select' => array('cat_id,cat_name'),
                                'order' => 'cat_name',
                                'distinct' => true
                    )), "cat_id", "cat_name")
        ),
        array(
            'name' =>'unit',
            'type' =>'raw',
            'value' => '"Set"',
        ),*/
       array(
            'name' => 'unit',
            'value' => '$data->unit0->unitname',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
            'filter' => CHtml::listData(Unit::model()->findAll(
                            array(
                                'select' => array('id,unitname'),
                                'order' => 'unitname',
                                'condition'=>'active_status = "1"',
                                'distinct' => true
                    )), "id", "unitname")
        ),
        //'unit',
        'make',
        'model_no',
//        array(
//        'name'  => 'ref_no',    
//        //'value' => ($model->ref_no == '' ? "0" : $model->ref_no),    
//        'value' => $model->checkRefStatus($model->ref_no),
//        ), 
        'ref_no',
        array(
            'name'=> 'location',
             'value' => '$data->location != "" ? $data->location0->name : 0',    
             'filter' => CHtml::listData($model->showResults(), "id", "name")
//              'filter' => CHtml::listData(LocationType::model()->findAll(
//                            array(
//                                'select' => array('id,name'),                              
//                                'order' => 'id',
//                                'distinct' => true
//                    )), "id", "name")
        ), 
        'qty',

        'warranty_date',
         array(
             'name'=> 'tool_category',
             'value'=> '$data->toolCategory->cat_name',

             'filter' => CHtml::dropDownList( 'Tools[tool_category]', $model->tool_category ,  $testmodel, array( 'empty' => 'All' )), 
             /* 'filter'=>false,
             'filter' => CHtml::listData(ToolCategory::model()->findAll(
                            array(
                                'select' => array('cat_id,cat_name'),                              
                                'order' => 'cat_id',
                                'distinct' => true
                    )), "cat_id", "cat_name")*/
             
             
         ),

        array(
          
             'name'=>  'serial_no',
             'header'=> 'Serial No',
             'value'=> '$data->serial_no',
        ),

        array(
          
             'name'=> 'prev_main',
             'value'=> '($data->prev_main==1)?"YES":"NO"',
             'filter' =>array( 1=>'YES',0=>'NO'),
             
        ),

        'prev_hrs',
        array(
            'name'=>'prev_date',
            //'value'=>'$data->prev_date',
            'value'=>'($data->prev_date!=NULL)?Yii::app()->dateFormatter->format("y-MM-d",strtotime($data->prev_date)):NULL',
            'htmlOptions'=>array('width' => '100px'),
        ),
       
         array(
            'name' => 'active_status',
            'value' => '(isset($data->active_status) && $data->active_status) ? "Yes": "No"',
            'type' => 'raw',
            'filter' => array(0 => 'No', 1 => 'Yes'),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Action',
            'htmlOptions' => array('style' => 'width:160px;'),
            //'template' => '{update}{delete}',
            'template' => '{Make Inactive}{Make Active}{update}{view}',
            'buttons' => array(
                
                
                /* Active section starts from here */    
                        'Make Active' => array(			    
                        'url'=>'Yii::app()->createUrl("tools/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-success'), 
                        'visible' => '($data->active_status == 0)',    
			),
			/* Active section end here */
                            
                        /* Inactive section starts from here */    
                        'Make Inactive' => array(			    
                        'url'=>'Yii::app()->createUrl("tools/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-warning'),  
                        'visible' => '($data->active_status == 1)',                          
			),
                
                'update' => array(
                    'label' => '',
                    //'url' => 'Yii::app()->createUrl("tools/update", array("id"=>$data->id))',
                    'url' => 'Yii::app()->createAbsoluteUrl("tools/update", array("id"=>$data->id, "type"=>2))',
                    //'options' => array('class' => 'actionitem updatelink'),
                    'options' => array('class' => 'actionitem icon-pencil icon-comn', 'title'=>'Edit'),
                    'visible' => '($data->active_status == 1)',
                    'imageUrl' =>false
                ),
                'view' => array(
                    'label' => '',
                    'url' => 'Yii::app()->createAbsoluteUrl("tools/toolsetview", array("id"=>$data->id))',
                    //'options' => array('class' => 'actionitem updatelink'),
                    'options' => array('class' => 'icon-eye icon-comn', 'title'=>'View'),
                    'visible' => '($data->active_status == 1)',
                    'imageUrl' =>false
                ),
                
			/* Inactive section end here */  	
               /*  'create' => array(
                    'label' => 'create',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/add.png',
                    'url' => 'Yii::app()->createUrl("tools/toolcreate", array("id"=>$data->id))',
                    'options' => array('class' => 'createlink'),
                    'visible'=>'($data->tool_category == "")',
                ),
                 */       
               
            ),
        ),
    ),
));
?>
</div>
 <div id="addPurchase" class="modal" role="dialog">
            <div class="modal-dialog modal-lg">
		
            </div>
        </div>


<?php /*echo CHtml::ajaxLink("Change Status",
        $this->createUrl('Tools/changestatus'),
      array(
      "type" => "post",
      "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("tools-grid","selectedIds").toString()}',
      "success" => 'js:function(data){     
          
       }' ),array(
      'class' => 'btn btn-primary button'
      )
      ); 
*/


?>

<?php //echo CHtml::submitButton('Change Status', array('class' => 'btn green')); ?>


<?php $this->endWidget(); ?>


<!-- Add tools Popup -->
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function(){
		$(".createtools").click(function (event) {
			 event.preventDefault();			
			$.ajax({
				type: "GET",
				url:"' . Yii::app()->createUrl('tools/create') . '",
				success: function (response)
				{					
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
		});
		
                $("#selectedIds_all").click(function(){
                if(this.checked){
                $("div.checker span").addClass("checked");
                }else{
                $("div.checker span").removeClass("checked");
                }
                return true;    
                });
                

	$(".createlink").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
           
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						
                        $("#addPurchase").html(response);
                        $("#addPurchase").css({"display":"block"})
                    }

                });
            
        });
        
            
		$(document).on("click", ".updatelink", function (event) {
                    event.preventDefault();
                    var url= $(this).attr("href");
                    $.ajax({
				type: "GET",
				url:url,
				success: function (response)
				{					
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
                });
		
                
		

	});
        
		
//active status script starts from here
$(".changestatusicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){					
					
					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
//active status script end here
');
?>
