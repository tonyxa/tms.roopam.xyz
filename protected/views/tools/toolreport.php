<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h1>Location of Tools</h1>
 <div class="table-responsive">
 <?php

 $this->renderPartial('_search', array('model' => $model, 'categories_list'=> $categories_list)) ?><br>

<?php
 
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tools-grid',
    'dataProvider' => $model->search1($page),
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    //'filter' => $model,
    'columns' => array(

        array('class' => 'IndexColumn', 'header' => 'S.No.','htmlOptions' => array('width' => '50px')),


        array(
            'name' => 'parent_category',
            // 'value' => 'isset($data->tool_category) ? $data->toolCategory->getParentname($data->toolCategory->parent_id) : "---"',
            'value' => 'isset($data->tool_category) ? $data->toolCategory->getmainParentname($data->tool_category) : "---"',
            'type' => 'raw',
           // 'htmlOptions' => array('width' => '200px'),
        ),


         array(
            'name' => 'child',
          //  'value' => 'isset($data->tool_category) ? $data->toolCategory->getCategoryname($data->toolCategory->parent_id) : "---"',
            'value' => 'isset($data->tool_category) ? $data->toolCategory->getsubCategoryname($data->tool_category) : "---"',
            'type' => 'raw',
           // 'htmlOptions' => array('width' => '200px'),
        ),

        array(
            'name' => 'subchild',
            //'value' => 'isset($data->tool_category) ? $data->toolCategory->cat_name : "---"',
            'value' => 'isset($data->tool_category) ? $data->toolCategory->getsubchildCategoryname($data->tool_category) : "---"',
            'type' => 'raw',
           // 'htmlOptions' => array('width' => '200px'),
        ),

         'tool_name',
        array(
            'name' => 'ref_no',
            'type' => 'raw',
           // 'htmlOptions' => array('width' => '80px'),
        ),
        array(
            'name' => 'qty',
            // 'type' => 'raw',
           // 'htmlOptions' => array('width' => '80px'),
        ),
        //'tool_name',
        //'serial_no',
        //'location'
        array(
            'name' => 'location',
            // 'value' => ' isset($data->location) ? $data->location0->name." (". $data->location0->locationType->caption . ")" : "---"',
            'value' => '$data->getlocationsts($data->id,$data->item_status)',
            'type' => 'raw',
            //'htmlOptions' => array('width' => '120px'),
        ),

        // array(
        //     'name' => 'tool_condition',
        //     'type' => 'raw',
        //     'value' => '$data->getStatus("$data->tool_condition")',
        //    // 'htmlOptions' => array('width' => '80px'),
        // ),
        array(
            'name' => 'tool_condition',
            'type' => 'raw',
            // 'value' => '$data->getnewStatus($data->id,"$data->tool_condition",$data->serialno_status,$data->item_current_status,$data->tool_transfer_id,$data->item_status)',
           // 'htmlOptions' => array('width' => '80px'),
           'value' => '$data->getReport($data->id,"$data->tool_condition",$data->item_id,$data->serialno_status,$data->item_current_status,$data->tool_transfer_id,$data->item_status)',

        ),


    ),
));
?>
 </div>