

            <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'tools-grid',
                    'dataProvider' => $model->alltoolsviewsearch($loc,$cate,$toolid,$type,$requested_qty,$tool_type,$reqid),
                    'itemsCssClass' => 'table table-bordered',
                    'selectableRows' => 1,
                    'selectionChanged' => 'function(id){if($.fn.yiiGridView.getSelection(id)!=""){ var code = $(".codeval").val();'
                    . 'var id = $.fn.yiiGridView.getSelection(id);'
                    //. 'var loc = '.$loc.';'
                    . '$.ajax({
                        type : "POST",
                        dataType : "JSON",
                        data: {id: id},
                        url: "'. $this->createUrl("Tools/gettooldetails").'",
                        success: function (data) {
                      // $("#addtools").css({"display":"none"})
                        $("#addtools").css({"display":"block"})
                        $("#"+code+"").closest("tr").find(".newcode").val(id);
                        $("#"+code+"").closest("tr").find(".code").val(data.refcode);
                        $("#"+code+"").closest("tr").find(".item_name").val(data.name);
                        $("#"+code+"").closest("tr").find(".unit").html(data.units);
                        $("#"+code+"").closest("tr").find(".unit").val(data.unit);
                        $("#"+code+"").closest("tr").find(".quantity").val(data.qty);
                        }
                        });'
                    . '}}',
                   //  'filter' => $model,
                    'columns' => array(
                        array(
                'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => '100',
            ),
                        array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('width' => '50px')),
                        //'tool_code',
                        array(
                            'name' => 'ref_no',
                            'type' => 'raw',
                            //'htmlOptions' => array('width' => '80px'),
                        ),
                        'tool_name',
                        array(
                            'name' => 'stock_qty',
                            'type' => 'raw',
                            //'value' => '($data->qty)?$data->qty:""',
                            //'value' => '($data->id)?$data->getremqty($data->id,'.$reqid.'):""',
                            'htmlOptions' => array('class' => 'allc_qty'),
                        ),
                        array(
                            'name' => 'location',
                            'type' => 'raw',
                            'value' => '($data->location)?$data->location0->name:""',
                            //'htmlOptions' => array('width' => '80px'),
                        ),
                        array(
                            'header'=>'Request Quantity',
                            'type' => 'raw',
                            'value' => '"<input type=\"text\" name=\"item_quantity[]\" id=\"item_quantity\" class=\"item_quantity\">"',
                            'htmlOptions' => array('width' => '80px'),
                          //  'visible'=> ('$data->serialno_status == N'),
                        ),

                        array(
                            'type' => 'raw',
                            'value' => '"<input type=\"hidden\" name=\"serialno_status[]\" id=\"serialno_status\" class=\"serialno_status\" value=\"$data->serialno_status\">"',
                            'htmlOptions' => array('width' => '80px','style' =>'display:none;'),
                            'headerHtmlOptions'=>array('style' =>'display:none;'),
                            'filter'=>false
                            //'visible'=> false,
                        ),
//                        array(
//                            'name' => 'duration_in_days',
//                            'type' => 'raw',
//                            'value' => function($model){
//                                return '<input type="text" name="duration[]" id="duration_'.$model->id.'">';
//                            },
//                        ),
                    ),
                ));
                ?>

                <script>
                jQuery(function($) {

                        grid = $('#tools-grid');

                        $('tr', grid).each(function() {
                          var tool_type ='<?php echo $tool_type;?>'
                          if(tool_type =="Y"){
                            $('td:eq(6), th:eq(6)',this).hide();
                          }
                          //  $('td:eq(0), th:eq(0)',this).hide();

                        });


                    });

                </script>

<style>
.topspace{margin-top:15px;}
.btn.focus, .btn:focus, .btn:hover {
    color: #fff;
    text-decoration: none;
}
label{color: #333;}
.modal .modal-header .close {
    margin-top: -2px !important;
    vertical-align: middle;
    float: none;
    margin-left:10px;
}
.table-bordered>tbody>tr>td{border-bottom:none;border-left:none;}
</style>
