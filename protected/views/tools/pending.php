<?php
/* @var $this ToolsController */
/* @var $model Tools */
/*
  $this->breadcrumbs=array(
  'Tools'=>array('index'),
  'Manage',
  );

  $this->menu=array(
  array('label'=>'List Tools', 'url'=>array('index')),
  array('label'=>'Create Tools', 'url'=>array('create')),
  );

  Yii::app()->clientScript->registerScript('search', "
  $('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
  });
  $('.search-form form').submit(function(){
  $.fn.yiiGridView.update('tools-grid', {
  data: $(this).serialize()
  });
  return false;
  });
  ");
 * 
 */
?>

<h1>Manage Pending Tools</h1>

<?php /*

  <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
  <div class="search-form" style="display:none">
  <?php $this->renderPartial('_search',array(
  'model'=>$model,
  )); ?>
  </div><!-- search-form -->
 */ ?>
<div class="">
    <?php /* if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) { ?>
        <button data-toggle="modal" data-target="#addtools"  class="btn blue createtools">Add Tools</button>
    <?php } */?>
</div>
<div class="table-responsive">
<?php
    $form = $this->beginWidget('CActiveForm', array(
           'action'=>Yii::app()->createUrl('Tools/changestatus'),  
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tools-grid',
    'dataProvider' => $model->search('pending'),
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'columns' => array(
       /*  array(
               'name' => 'check',
               'id' => 'selectedIds',
               'value' => '$data->id',
               'class' => 'CCheckBoxColumn',
               'selectableRows' => '100',

           ),*/
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        /*'tool_code',*/
        'item_name',
       /* array(
            'name' => 'tool_category',
            'value' => '$data->toolCategory->cat_name',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
            'filter' => CHtml::listData(ToolCategory::model()->findAll(
                            array(
                                'select' => array('cat_id,cat_name'),
                                'order' => 'cat_name',
                                'distinct' => true
                    )), "cat_id", "cat_name")
        ),*/
        array(
            'name' => 'unit',
            'value' => '$data->unit0->unitname',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
            'filter' => CHtml::listData(Unit::model()->findAll(
                            array(
                                'select' => array('id,unitname'),
                                'order' => 'unitname',
                                'condition'=>'active_status = "1"',
                                'distinct' => true
                    )), "id", "unitname")
        ),
        //'unit',
        'make',
        'model_no',
//        array(
//        'name'  => 'ref_no',    
//        //'value' => ($model->ref_no == '' ? "0" : $model->ref_no),    
//        'value' => $model->checkRefStatus($model->ref_no),
//        ), 
        
        'qty',
        'remaining_qty',
        'batch_no',
        'warranty_date',
        /* 'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'name' => 'tool_status',
            'value' => '$data->toolStatus->caption',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
           /* 'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                "condition"=> 'status_type = "pending_status"',
                                'order' => 'sid',
                                'distinct' => true
                    )), "sid", "caption")
            * 
            */
        ),
        
        array(
            'class' => 'CButtonColumn',
            'header' => 'Action',
            'htmlOptions' => array('style' => 'width:90px;'),
            //'template' => '{update}{delete}{create}',
            'template' => '{create}',
            'buttons' => array(
                /*'update' => array(
                    'label' => 'update',
                    'url' => 'Yii::app()->createAbsoluteUrl("tools/update", array("id"=>$data->id))',
                    'options' => array('class' => 'actionitem'),
                    //'options' => array('class' => 'actionitem updatelink'),
                ),*/
                 'create' => array(
                    'label' => '',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/add.png',
                    'imageUrl'=>false,
                    //'url' => 'Yii::app()->createUrl("tools/toolcreate", array("id"=>$data->id))',
                     'url' => 'Yii::app()->createAbsoluteUrl("tools/toolcreate", array("id"=>$data->id))',
                    'options' => array('class' => 'createlink icon-plus icon-comn', 'title'=>'Create'),
                    //'visible'=>'($data->tool_category == "")',
                     //'visible'=>'$data->checkStatus($data->item_id,$data->tool_category)',
                ),

               
            ),
        ),
    ),
));
?>
</div>
 <div id="addPurchase" class="modal" role="dialog">
            <div class="modal-dialog modal-lg">
		
            </div>
        </div>


<?php /*echo CHtml::ajaxLink("Change Status",
        $this->createUrl('Tools/changestatus'),
      array(
      "type" => "post",
      "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("tools-grid","selectedIds").toString()}',
      "success" => 'js:function(data){     
          
       }' ),array(
      'class' => 'btn btn-primary button'
      )
      ); 
*/


?>

<?php //echo CHtml::submitButton('Change Status', array('class' => 'btn green')); ?>


<?php $this->endWidget(); ?>


<!-- Add tools Popup -->
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function(){
		$(".createtools").click(function (event) {
			 event.preventDefault();			
			$.ajax({
				type: "GET",
				url:"' . Yii::app()->createUrl('tools/create') . '",
				success: function (response)
				{					
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
		});
		
                $("#selectedIds_all").click(function(){
                if(this.checked){
                $("div.checker span").addClass("checked");
                }else{
                $("div.checker span").removeClass("checked");
                }
                return true;    
                });
                

//	$(".createlink").click(function (event) {
//            event.preventDefault();
//            var url = $(this).attr("href");
//           
//                $.ajax({
//                    type: "GET",
//                    url: url,
//                    success: function (response)
//                    {
//						
//                        $("#addPurchase").html(response);
//                        $("#addPurchase").css({"display":"block"})
//                    }
//
//                });
//            
//        });
        
            
		$(document).on("click", ".updatelink", function (event) {
                    event.preventDefault();
                    var url= $(this).attr("href");
                    $.ajax({
				type: "GET",
				url:url,
				success: function (response)
				{					
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
                });
		
                
		

	});
');
?>
