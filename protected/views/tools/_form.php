<?php
/* @var $this ToolsController */
/* @var $model Tools */
/* @var $form CActiveForm */
?>


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tools-form',
    ));
    ?>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'ref_no'); ?>
                    <?php echo $form->textField($model, 'ref_no', array('class' => 'form-control','readonly'=> true)); ?>
                    <?php echo $form->error($model, 'ref_no'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'tool_name'); ?>
                    <?php echo $form->textField($model, 'tool_name', array('maxlength' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'tool_name'); ?>
                </div>
            </div>

            <div class="row">
                 <div class="col-md-6">
                   <?php echo $form->labelEx($model, 'Location'); ?>
                   <?php
						echo $form->dropDownList($model, 'location', CHtml::listData(LocationType::model()->findAll(
                                            array(
                                                'select' => array('id,name'),
                                                'order' => 'name',
                                                'condition' =>'active_status ="1"',
                                                'distinct' => true
                                    )), "id", "name"), array('class' => 'form-control', 'empty' => 'Please choose'));
                    ?>
                    <?php echo $form->error($model, 'location'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'batch_no'); ?>
                    <?php echo $form->textField($model, 'batch_no', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'batch_no'); ?>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'tool_category'); ?>
                    <?php
						echo $form->dropDownList($model, 'tool_category', CHtml::listData(ToolCategory::model()->findAll(
                                            array(
                                                'select' => array('cat_id,cat_name'),
                                                'order' => 'cat_name',
                                                'distinct' => true
                                    )), "cat_id", "cat_name"), array('class' => 'form-control', 'empty' => 'Please choose','disabled'=>'disabled'));
                    ?>
                    <?php echo $form->error($model, 'tool_category'); ?>
                </div>
                <?php if($model->pending_status!=17){?>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'unit'); ?>
                    <?php
                    echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(
                                            array(
                                                'select' => array('id,unitname'),
                                                'order' => 'unitname',
                                                'condition' =>'id ! ="1"',
                                                'distinct' => true
                                    )), "id", "unitname"), array('class' => 'form-control', 'empty' => 'Please choose'));
                    ?>

                    <?php echo $form->error($model, 'unit'); ?>
                </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'make'); ?>
                    <?php echo $form->textField($model, 'make', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'make'); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'model_no'); ?>
                    <?php echo $form->textField($model, 'model_no', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'model_no'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <?php echo $form->labelEx($model, 'serial_no'); ?>
                    <?php echo $form->textField($model, 'serial_no', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'serial_no'); ?>

                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'warranty_date'); ?>
                    <?php echo CHtml::activeTextField($model, 'warranty_date', array("size" => "15", 'class' => 'form-control datepicker'));

                // $this->widget('application.extensions.calendar.SCalendar', array(
                //     'inputField' => 'Tools_warranty_date',
                //     'ifFormat' => '%Y-%m-%d',
                // )); 
                ?>
                    <?php echo $form->error($model, 'warranty_date'); ?>
                </div>


            </div><br/>



            <div class="row">
                <div class="col-md-3">
                  <?php if($model->serialno_status == 'Y') { ?>
                    <?php echo $form->labelEx($model, 'prev_main'); ?>
                    <input type="checkbox" name="prev_main" id="pr_main" <?php if($model->prev_main==1){ echo "checked" ;}else{ echo NULL ; } ?> >
                   <?php //echo $form->error($model, 'prev_main'); ?>
                   <?php } ?>
                </div>

                <div class="col-md-3" >

                    <label class=hrs >Hours</label>
                    <input type="text" name="prev_hrs" class="hrs form-control"  value="<?= ($model->prev_hrs)?$model->prev_hrs:NULL ?>">

                </div>

                <input type="hidden" name="date" id="date" value="<?php echo  $model->prev_main; ?>">

                <div class="col-md-3" >
                    <label class=dat >Date</label>
                    <?php $model->prev_date= ($model->prev_date)?date('Y-m-d',strtotime($model->prev_date)):NULL; ?>

                 <?php
                 echo CHtml::activeTextField($model, 'prev_date' , array( 'class' => 'inputs form-control target dat datepicker'));

                // $this->widget('application.extensions.calendar.SCalendar', array(
                //     'inputField' => 'Tools_prev_date',
                //     'ifFormat' => '%Y-%m-%d',
                // ));

                ?>

                    <!-- <input type="text" name="prev_date" class="inputs form-control target dat datepicker" value="<?= ($model->prev_date)?date('Y-m-d',strtotime($model->prev_date)):NULL ?>"> -->

                </div>

            </div>



            <div class="row save-btnHold">
    		   <div class="col-md-12 text-center">
                        <br>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue' ,'style'=>"margin-bottom: -36px;")); ?>



                       </div>
                </div>
             <?php $this->endWidget(); ?>

             <div class="text-center" style="padding-right: 142px;">

             <button  class="btn-default " data-dismiss="modal" onclick="javascript:window.location.reload()" style="margin-top: -55px;  text-align: center;">Cancel</button>


              </div>







<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    var i = $('#workdetails tr').length;


    $(document).ready(function() {


        $('#cancel1').click(function(){

              //e.preventDefault();

           window.location.reload();
        })

        if($('#date').val()==1){

         $('.hrs').show();
         $('.dat').show();
        }else{

         $('.hrs').hide();
         $('.dat').hide();
        }



     $( function() {
        $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd',changeYear:true});
        
      });


    $('#pr_main').change(function() {

        var dat = $('#date').val();


        if(this.checked==true){
            $('.hrs').show();
            $('.dat').show();


        }else{
             $('.hrs').hide();
             $('.dat').hide();
        }


    });

});


    $(document).on('keypress', '.lst', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            html = '<tr class="worktr">';
            html += '<td>' + i + '</td>';
            html += '<td style="display:none"><input type="hidden" class="inputs form-control target" name="ids[]" id="ids' + i + '" /></td>';
            html += '<td><select class="inputs form-control target" name="work_site[]" id="work_site_' + i + '"><option value="">Please choose</option>'
<?php foreach ($work_sites as $worksite) { ?>
                + '<option value="<?php echo $worksite['id']; ?>"><?php echo $worksite['site_name']; ?></option>'
<?php } ?>
            + '</select> </td>';
            /* <input type="text" class="inputs form-control" name="work_site_' + i + '" id="work_site_' + i + '" />*/
            html += '<td><input type="text" class="inputs form-control target" name="serial_no[]" id="serial_no_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs form-control target" name="cost[]" id="cost_' + i + '" /></td>';
            html += '<td><input type="text" class="inputs lst form-control" name="stock[]" id="stock_' + i + '" /></td>';
            html += '<td><input type="button" class="btn green btn-xs delete"  data-id="' + i + '" value="Delete"></td>';
            html += '</tr>';
            $('#workdetails').append(html);
            $(this).focus().select();
            i++;
            e.preventDefault();
            return false;
        }
    });

    $(document).on('keypress', '.target', function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $(document).on('keydown', '.inputs', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var index = $('.inputs').index(this) + 1;
            $('.inputs').eq(index).focus();
        }
    });
    $(document).on('click', '.delete', function () {
        var dataid = $(this).attr('data-id');
        $(this).parent().parent('tr').remove();
        AutoNumber();
    });
    function AutoNumber() {
        var j = 1;
        $('.worktr').each(function () {
            $(this).find("td:first").text(j);
            j++;
        });
    }

    $(document).on('click', '.deleteitem', function (event) {
	    var newthis = $(this);
		var id = $(this).attr("id");
		//alert(id);
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
		$.ajax({
                    method: "POST",
                    url: '<?php echo Yii::app()->createUrl('tools/deleteitem&id='); ?>'+id,
                    data: {id: id},
                    success: function (data) {
							newthis.parent().parent("tr").remove();
							//console.log(data);

                    }
         });
	 }
	});


    $(document).ready(function () {
        $("#tools-form").validate({
            rules: {
//                'Tools[tool_code]': {
//                    required: true,
//                    /*digits: true*/
//                },
                'Tools[tool_name]': {
                    required: true

                },
                'Tools[tool_category]': {
                    required: true,
                },
                'Tools[unit]': {
                    required: true,
                },
                /*'Tools[ref_no]': {
                    required: false,
                },*/
                'work_site[]': {
                    required: true,
                    digits: true
                },
                'serial_no[]': {
                    required: true,
                    digits: true
                },
                'cost[]': {
                    required: true,
                    digits: true
                },
                'stock[]': {
                    required: true,
                    digits: true
                },
            },
            messages: {
//                'Tools[tool_code]': {
//                    required: "Tool code is required",
//                },
                'Tools[tool_name]': {
                    required: "Tool name is required",
                },
                'Tools[tool_category]': {
                    required: "Tool category is required",
                },
                'Tools[unit]': {
                    required: "Tool unit is required",
                },
                /*'Tools[ref_no]': {
                    required: "Ref no is required",
                },*/
                'work_site[]': {
                    required: "Required",
                },
                'serial_no[]': {
                    required: "Required",
                },
                'cost[]': {
                    required: "Required",
                },
                'stock[]': {
                    required: "Required",
                },
            },
            submitHandler: function () {
				//alert('hi');
                var formData = JSON.stringify($("#tools-form").serializeArray());
                $.ajax({
                    method: "POST",
                    data: {formdata: formdata},
                    url: '<?php echo Yii::app()->createUrl('tools/create'); ?>',
                    success: function (data) {
                    }
                });
            }
        });
    });

</script>

<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    @media(min-width:767px){
        #content{
        width: 60%;
        }
    }

</style>
