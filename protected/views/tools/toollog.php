

<h1>Preventive Maintenance Log </h1>
<h2> Upcoming/Expired</h2>

<?php if(Yii::app()->user->hasFlash("success")): ?>
      <div id="message" style="color: green;font-weight: bold;font-size: 150%;">
          <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>
<?php endif; ?>


<!-- <span><?php //echo CHtml::link('Request',array('Tools/Request'),array('class'=>'btn btn blue send')); ?></span> -->





<div class="table-responsive">
<?php
    $form = $this->beginWidget('CActiveForm', array(
           'action'=>Yii::app()->createUrl('Tools/changestatus'),  
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tools-grid',
    'dataProvider' => $model->search('toollog'),
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),    
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    'columns' => array(
   
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        'tool_name',
       array(
            'name' => 'tool_type',
            'value' => '($data->pending_status==17)?"Tool Set": "Tool" ',
            'cssClassExpression' => '($data->pending_status==17)? "set" : "tool"',
             'filter' =>array(3=>'Tool Set',4=>'Tool'),
          ),

        'model_no',
        'ref_no',
        array(
            'name'=> 'location',
             'value' => '$data->location != "" ? $data->location0->name : 0',    
             'filter' => CHtml::listData($model->showResults(), "id", "name")
             
        ), 
        //'qty',
       // 'warranty_date',
         array(
             'name'=> 'tool_category',
             'value'=> '$data->toolCategory->cat_name',

             'filter' => CHtml::dropDownList( 'Tools[tool_category]', $model->tool_category ,  $testmodel, array( 'empty' => 'All' )), 
             
             /* 'filter' => CHtml::listData(ToolCategory::model()->findAll(
                            array(
                                'select' => array('cat_id,cat_name'),                              
                                'order' => 'cat_id',
                                'distinct' => true
                    )), "cat_id", "cat_name")*/
             
             
         ),

          array(
          
             'name'=>  'serial_no',
             'header'=> 'Serial No',
             'value'=> '$data->serial_no',
             
             
           ),

      
          array(
            'name'=>'prev_date',
          
            'value'=>'($data->prev_date!=NULL)?Yii::app()->dateFormatter->format("d-MM-y",strtotime($data->prev_date)):NULL',
            'htmlOptions'=>array('width' => '100px'),
          ),

          array(
            'name'=>'prev_type',
            'type'=>'raw',
           // 'value'=>'(date($data->prev_date)>date("Y-m-d"))?"Upcoming":"Expired"',
           

            'value'=>'((date($data->prev_date)>date("Y-m-d"))?"Upcoming":"Expired").(!empty($data->services($data->id))?"<div class=\"req\">".$data->services($data->id)."</div>":"<div></div>") ',

            'cssClassExpression' => '(date($data->prev_date)>date("Y-m-d"))? "up" : "ex"',
            'filter' =>array(1=>'Upcoming',2=>'Expired'),

          ),
         

        

          array(

            'class'  => 'CButtonColumn',
            'header' => 'Action',
            'htmlOptions' => array('style' => 'width:70px;'),
            'template' => ' {view}{update} {update1} {add} {approve} ' ,

            'buttons' => array(

              'view' => array(          
                        'url'=>'Yii::app()->createUrl("tools/View", array("id"=>$data->id))',
                          
                ),
                  
              'update' => array(   
                        
                        'url'=>'Yii::app()->createUrl("tools/update", array("id"=>$data->id ,"type"=>1 ))',
                        'visible' => '( strtotime(date($data->prev_date)) < strtotime(date("Y-m-d")) and $data->pending_status!=17 and Yii::app()->user->role == 1)',    
                 ),

              'update1' => array(  
                       'label'=>'',
                       'options' => array('class' =>'fa fa-pencil ','title'=>'Update','style'=>"color:#8FBC8F"), 
                       'url'=>'Yii::app()->createUrl("tools/toolsetview", array("id"=>$data->id ,"type"=>1 ))',
                       'visible' => '($data->pending_status== 17 and Yii::app()->user->role == 1)',    
                 ),

              'add' => array(  
                       'label'=>'', 
                       'options' => array('class' =>'icon-cursor icon-comn','title'=>'Request','style'=>"color:#8FBC8F"), 
                       'url'=>'Yii::app()->createUrl("tools/Prev_request", array("id"=>$data->id ))',
                       'visible' => '( Yii::app()->user->role == 10 or Yii::app()->user->role == 1 )', 
                       
                 ),

              'approve' => array(  
                       'label'=>'<i class="icon-check" aria-hidden="true"  style="color:#8FBC8F"></i>', 
                       'url'=>'Yii::app()->createUrl("tools/PrevApprove", array("id"=>$data->id ))',
                       'visible' =>  '( ((!empty($data->services($data->id)))?true:false))',
                       //'visible' => '( Yii::app()->user->role == 1 )', 

                       
                 ),

             ),
        ),




      
    ),
));




?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">

/*setTimeout(function() {
           $('#message').fadeOut('slow');
       }, 3000);*/
  
</script>

<!-- <div id="id_view" style="display:none;"></div> -->


<style type="text/css">
  
  .up{
    color:green;
    font-weight: bold;
  }
  .ex{
    color:red;
    font-weight: bold;
  }


  .set{
    color:blue;
    font-weight: bold;
  }
  .tool{
    color:black;
    font-weight: bold;
  }
  .req{
    background-color:#3b434c;
    color:white;
    font-size:10px !important;
    line-height:12px;
    padding:2px;
    float:right
  }
  .send{

    float: right;
    float: right; 
    margin-top: -39px; 
    margin-left: -130px;
  }
</style>





