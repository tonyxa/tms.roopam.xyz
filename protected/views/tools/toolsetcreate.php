

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'createtoolset-form',
    'action' => Yii::app()->createUrl('tools/createtoolset'),
        ));
?>
<h4>Create Tool Set</h4>

<div class="row">

<!--    <div class="col-md-3">
         <?php echo $form->labelEx($model, 'Tool Set Name'); ?>
         <?php echo $form->textField($model, 'tool_name',array('class'=>'form-control tool_set_name')); ?>    <span class="error errorname"></span>
         <?php echo $form->error($model, 'tool_name'); ?>
    </div>-->

<!--    <div class="col-md-2">
    <?php //echo $form->labelEx($model, 'unit'); ?>

        <?php //echo $form->error($model, 'unit'); ?>
    </div>  -->

    <div class="col-md-2">
        <?php echo $form->labelEx($model, 'Location'); ?>
        <?php
         //echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(array('condition'=>'unitname = "Set"')), "id", "unitname"), array('class' => 'form-control','style'=>'display:none'));
        ?>
         <?php
        echo $form->dropDownList($model, 'location', CHtml::listData($model->showResults(), "id", "name"), array('class' => 'form-control'));
        ?>
        <?php echo $form->error($model, 'location'); ?>
    </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'parent_category'); ?>
             <span class="error errorname"></span>
            <?php
            echo $form->dropDownList($model, 'parent_category', CHtml::listData(
                    ToolCategory::model()->findAll( array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL ',
                              'order'=>'cat_name'
                             )),"cat_id", "cat_name"
                    ), array('class' => 'valid ddn1 form-control parent_cat','id'=>'parent_category1', 'style'=>'width:100% !important;border: 1px solid #AAAAAA !important;height: 28px;','empty' => 'Please choose',
                    'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                         $("#childCategory1").html(data);
                        }'
                    )));
            ?>
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo "<label>Child Category</label>";?>
            <span class="error errorname1"></span>
            <?php
             $parent = (isset($_GET['Tools']['parent_category'])? $_GET['Tools']['parent_category'] : '' );
             $sel = (isset($_GET['childCategory'])? $_GET['childCategory'] : '' );
            ?>

            <?php

            if(empty($sel)){
            $options[''] = 'Select Child';
            }else{
            $options[''] = '';
            }

            echo CHtml::dropDownList('childCategory1',array(),$options,array('class' => 'valid ddn1 form-control child_cat','style'=>'width:100% !important;border: 1px solid #AAAAAA !important;',
            'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                        $("#subchildCategory1").html(data);
                        }'
                )));


            ?>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <?php echo "<label>Sub Child Category</label>";?>
            <?php


             $selsub1 = (isset($_GET['subchildCategory'])? $_GET['subchildCategory'] : '' );

             $options1[''] = 'Select Sub Child';
             echo CHtml::dropDownList('subchildCategory1',array(),$options1,array('class' => 'valid ddn1 form-control','style'=>'width:100% !important;border: 1px solid #AAAAAA !important;',));
            ?>
        </div>
         <!--  preventive maintenance  -->
         
        <div class="col-md-2" style="margin-top: 19px; ">
            <?php  echo $form->labelEx($model, 'prev_main'); ?>
            <input type="checkbox" name="prev_main1" id="pr_main1" value="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 hrs1">
            <label class="hrs1">Hours</label>
            <input type="text" name="Tools_prev_hrs1" class="hrs1 form-control" id="prev_hrs" value="">
        </div>

        <!--  pre end  -->


        <div class="col-md-7">
            <div class=" save-btnHold">
                <?php echo CHtml::Button('Add', array('class' => 'btn btn-sm blue createtool')); ?>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>


<script type="text/javascript">

    $(document).ready(function () {



        $('.hrs1').hide();
        $('#pr_main1').change(function() {

        if(this.checked==true){
          $('.hrs1').show();
        }else{
          $('.hrs1').hide();
        }

    });
        /*$("#createtoolset-form").validate({
            rules: {
                'Tools[tool_name]': {
                    required: true,
                    //digits: true
                },


            },
            messages: {
                'Tools[tool_category]': {
                    required: "Tool Name is required",
                },


            },
            submitHandler: function () {
                var formData = JSON.stringify($("#createtoolset-form").serializeArray());
                var id = <?php  //echo $id;?>;
                $.ajax({
                    method: "POST",
                    data: {formdata: formdata,id:id},
                    url: '<?php //echo Yii::app()->createUrl('tools/createtoolset'); ?>',
                    success: function (data) {
                        alert(data)
                        return false;
                    }
                });
            }
        });
        */

        $('.createtool').click(function(){
           var Tools = $("#createtoolset-form").serializeArray();
           var name = $('.parent_cat').val();
           var name2= $('.child_cat').val();
           if(name !="" && name2!=""){
               $('.errorname').html('');
               $('.errorname1').html('');
           var id = <?php  echo $id;?>;
                $.ajax({
                    method: "POST",
                    data: {Tools: Tools,id:id},
                    url: '<?php echo Yii::app()->createUrl('tools/createtoolset'); ?>',
                    success: function (data) {
                       // alert(data)
                        //return false;
                        $('.tool_set_name').val('');
                         $('.toolset').toggle();
                         $("select#Tools_tool_name").append(''+data+'');
                    }
                });
                }else{
                    $('.errorname').html('This field is Required');

                    if(name =="" && name2==""){
                         $('.errorname').html('This field is Required');
                         $('.errorname1').html('This field is Required');
                    }
                    if(name !="" && name2==""){
                         $('.errorname').html('');
                         $('.errorname1').html('This field is Required');
                    }


                }
        });
    });
    </script>
