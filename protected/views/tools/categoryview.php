<h1>Tools</h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tools-grid',
    'dataProvider' => $model->search($page,'','',$id),
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'columns' => array(
       
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
       
        'tool_name',
       
        array(
            'name' => 'unit',
            'value' => '$data->unit0->unitname',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
            'filter' => CHtml::listData(Unit::model()->findAll(
                            array(
                                'select' => array('id,unitname'),
                                'order' => 'unitname',
                                'condition'=>'active_status = "1"',
                                'distinct' => true
                    )), "id", "unitname")
        ),
       
        'make',
        'model_no',
        'ref_no',
        array(
            'name'=> 'location',
             'value' => '$data->location != "" ? $data->location0->name : 0',    
             'filter' => CHtml::listData($model->showResults(), "id", "name")
        ), 
        'qty',
         array(
             'name'=> 'tool_category',
             'value'=> '$data->toolCategory->cat_name',
              'filter'=>false,
             'filter' => CHtml::listData(ToolCategory::model()->findAll(
                            array(
                                'select' => array('cat_id,cat_name'),                              
                                'order' => 'cat_id',
                                'distinct' => true
                    )), "cat_id", "cat_name")
              
         ),
        array(
            'name' => 'pending_status',
            'value' => '$data->pendingStatus->caption',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),          
        ),
    ),
));
?>



