<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick='$("#addtools").css({"display":"none"})'>&times;</button>
            <h4 class="modal-title">Tools</h4>
        </div>

        <div class="modal-body">
                   
            <div class="row modalrowdiv ">
               <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'tools-grid',
                    'dataProvider' => $model->search($page, $loc,$type,''),
                    'itemsCssClass' => 'table table-bordered',
                    'selectableRows' => 1,
                    'selectionChanged' => 'function(id){if($.fn.yiiGridView.getSelection(id)!=""){ var code = $(".codeval").val();'
                    . 'var id = $.fn.yiiGridView.getSelection(id);'
                    . 'var loc = '.$loc.';'
                    . '$.ajax({
                        type : "POST",
                        dataType : "JSON",
                        data: {id: id, loc: loc},
                        url: "'. $this->createUrl("Tools/gettallooldetails").'",
                        success: function (data) {
                        $("#addtools").css({"display":"none"})
                        $("#"+code+"").closest("tr").find(".newcode").val(id);
                        $("#"+code+"").closest("tr").find(".total_quantity").val(data.total_qty);
                        $("#"+code+"").closest("tr").find(".item_name").val(data.name);
                        $("#"+code+"").closest("tr").find(".unit").html(data.units);
                        $("#"+code+"").closest("tr").find(".unit").val(data.unit);
                        $("#"+code+"").closest("tr").find(".quantity").val("");
                        }
                        });'
                    . '}}',
                     'filter' => $model,
                    'columns' => array(
                        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('width' => '50px')),
                        //'tool_code',
                        /*array(
                            'name' => 'ref_no',
                            'type' => 'raw',
                            'htmlOptions' => array('width' => '80px'),
                        ),  $("#"+code+"").closest("tr").find(".quantity").val(data.qty);  */   
                        'tool_name',
                        'count',
                    ),
                ));
                ?>
            </div>
        </div>
    </div>

</div>
<input type="text" style="display:none" value="<?php echo $code; ?>" name="codeval" class="codeval"/>
