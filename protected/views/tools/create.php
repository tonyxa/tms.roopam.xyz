<?php
/* @var $this ToolsController */
/* @var $model Tools */

$this->breadcrumbs=array(
	'Tools'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tools', 'url'=>array('index')),
	array('label'=>'Manage Tools', 'url'=>array('admin')),
);
?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">New Tools</h4>
        </div>
<?php echo $this->renderPartial('_form', array('model'=>$model,'work_sites'=>$work_sites)); ?>
</div>

</div>
