<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/select2.min.css');

?>

<div class="wide form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">

         <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'parent_category'); ?>
            <?php
            echo $form->dropDownList($model, 'parent_category', CHtml::listData(
                    ToolCategory::model()->findAll( array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL ',
                              'order'=>'cat_name'
                             )),"cat_id", "cat_name"
                    ), array('class' => 'valid ddn form-control', 'empty' => 'Please choose',
                    'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                        $("#childCategory").html(data);
                        }'
                    )));
            ?>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo "<label>CHILD CATEGORY</label>";//echo $form->label($model, 'parent_category'); ?>
            <?php
             $parent = (isset($_GET['Tools']['parent_category'])? $_GET['Tools']['parent_category'] : '' );
             $sel = (isset($_GET['childCategory'])? $_GET['childCategory'] : '' );
            ?>

            <?php
            //echo $form->dropDownList($model, 'parent_category', $categories_list, array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));
            //echo $form->dropDownList($model,'parent_category',array(), array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));
            if(empty($sel)){
            $options[''] = 'Select Child';
            }else{
            $options[''] = '';
            }

            echo CHtml::dropDownList('childCategory',array(),$options,array('class' => 'valid ddn form-control',
            'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('tools/getchildcategories1'),
                        'data' => array('cid' => 'js:this.value'),
                        'success' => 'function(data) {
                        $("#subchildCategory").html(data);
                        }'
                )));


            ?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo "<label>SUB CHILD CATEGORY</label>"; //echo $form->label($model, 'tool_category'); ?>
            <?php
            /*
              echo $form->dropDownList($model, 'tool_category', CHtml::listData(ToolCategory::model()->findAll(
              array(
              'select' => array('cat_id,cat_name'),
              'order' => 'cat_name',
              'condition' =>'active_status = "1"',
              'distinct' => true
              )), "cat_id", "cat_name"), array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));
             *
             */
            //echo $form->dropDownList($model, 'tool_category', $categories_list, array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));

             $selsub1 = (isset($_GET['subchildCategory'])? $_GET['subchildCategory'] : '' );

             $options1[''] = 'Select Sub Child';
             echo CHtml::dropDownList('subchildCategory',array(),$options1,array('class' => 'valid ddn form-control'));
            ?>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'tool_name'); ?>
            <?php
            $model->tool_name = trim($model->tool_name);
            echo $form->textField($model, 'tool_name', array('class' => 'form-control', ));
            ?>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'ref_no'); ?>
            <?php
            $model->ref_no = trim($model->ref_no);
            echo $form->textField($model, 'ref_no', array('class' => 'form-control', ));
            ?>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <?php echo $form->label($model, 'location'); ?>
            <?php
            $tblpx = yii::app()->db->tablePrefix;

            $sql = "SELECT id, name,caption,sid,status_type FROM {$tblpx}location_type JOIN {$tblpx}status ON sid =location_type WHERE status_type='location_type' and active_status ='1'";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $listdata = CHtml::listData($result, 'id', 'name', 'caption');

            echo $form->dropDownList($model, 'location', $listdata, array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));
            ?>
        </div>

        <div class="col-md-2 col-sm-6 col-xs-12">
           <?php echo $form->label($model, 'tool_condition'); ?>
            <?php
            $tblpx = yii::app()->db->tablePrefix;

            $sql = "SELECT sid, caption FROM {$tblpx}status WHERE status_type = 'physical_condition'";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $listdata = CHtml::listData($result, 'sid', 'caption');

			      // echo $form->dropDownList($model, 'tool_condition', $listdata, array('class' => 'valid ddn form-control', 'empty' => 'Please choose'));
			?>
      <select class="valid ddn form-control" name="Tools[tool_status]" id="Tools_tool_status">
      <option value="">Please choose</option>
      <option value="Damage" <?php echo ($model->tool_status =='Damage')?'selected':"";?>>Damage</option>
      <option value="Breakdown" <?php echo ($model->tool_status =='Breakdown')?'selected':"";?>>Breakdown</option>
      <option value="Running" <?php echo ($model->tool_status =='Running')?'selected':"";?>>Running</option>
      <option value="Missing" <?php echo ($model->tool_status =='Missing')?'selected':"";?>>Missing</option>
      </select>
        </div>

        <div class="col-md-2 col-sm-6 col-xs-12">
        <?php echo CHtml::submitButton('GO', array('class' => 'btn btn-sm btn-primary', 'style'=>'margin-top: 20px;')); ?>&nbsp;
        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('toolreport') . '"', 'class' => 'btn default btn-sm')); ?>
        </div>
    </div>



<?php $this->endWidget(); ?>

</div>

<style>
    div.form input{
        margin: 0 0 0.5em 0;
    }
    .btn.default{
        margin-top: 20px !important;
    }

</style>

<script>
$(document).ready(function() {
//var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
$(".ddn").select2();

var parent = '<?php echo $parent; ?>';
var sel = '<?php echo $sel; ?>';

if(parent != '' || sel != ''){
$.ajax({
type: "GET",
data:{cid:parent,sel:sel},
url: '<?php echo Yii::app()->createUrl('tools/getchildcategories'); ?>',
success: function (response)
{
$("#childCategory").html(response);
$('#childCategory').val(sel).trigger('change');
}
});
}

var subchild = '<?php echo $selsub1;  ?>';
if(parent != '' && sel != '' && subchild != ''){
$.ajax({
type: "GET",
data:{cid:sel,sel:subchild},
url: '<?php echo Yii::app()->createUrl('tools/getchildcategories'); ?>',
success: function (response)
{
$("#subchildCategory").html(response);
$('#subchildCategory').val(subchild).trigger('change');
}
});
}

});
</script>
