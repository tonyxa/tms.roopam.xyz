

<h1><?php echo $model->tool_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'tool_code',
		'tool_name',
		array(
			'name' =>'tool_category',
			'value'=>$model->toolCategory->cat_name,
			),
		array(
			 'name' =>'unit',
			 'value' => $model->unit0->unitname,
			),
		//'make',
		'model_no',
		'ref_no',
		array(
			'name'=>'prev_date',
			'value' =>date('d-m-Y',strtotime($model->prev_date)),
			),
		//'batch_no',
		//'created_by',
		//'created_date',
		//'updated_by',
		//'updated_date',
	),
)); ?>
