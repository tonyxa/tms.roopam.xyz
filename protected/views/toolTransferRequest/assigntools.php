<style type="text/css">
  
  .head{
    color: #000;
    font-size: 13px;
    font-weight: bold;
  }
</style>

<?php
Yii::app()->clientScript->registerScript('myScript', "
    $('#tool-transfer-request-form').submit(function() {
        $('#loading_icon').show();
    });
");
?>

      <h1>Change Supervisor

          <span id="loading_icon" style="display:none;">
              <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style="height: 32px; width: 32px;"/>
          </span>
      </h1>
      <div id="msg"></div>

          <!--  Assign to another engineer  -->
          
            <div class="row">
                    <div class="col-md-6 form-group">

                                <?php 
                                  $Locations =  Yii::app()->db->createCommand("SELECT id,name FROM `tms_location_type`
                                  WHERE active_status = '1' and location_type= 13 ")->queryAll(); 

                                ?>
                                <label class="head">Select site</label>
                                <select id="chsite" name="site" class="form-control" style="width:96%;display: inline-block;">                      
                                  <option value="">Please select site</option>
                                  <?php  foreach($Locations as $data) { ?>
                                  <option value="<?= $data['id'] ?>"><?=  $data['name'] ?></option>
                                  <?php } ?>
                                 
                                </select>
                                <span class="icon-eye icon-comn" id="tool_dis" title="view"></span> 

                      </div>

                    <div class="col-md-6 form-group">
                    <label class="head">Change from</label>
                    <select id ="siteengfrm" name="siteengfrm" class="form-control">
                       <option value="">Please choose</option>
                    </select>

                  </div>
              </div>


            <div class="row">

                <div class="col-md-6 form-group">
                  <label class="head">Change to</label>
                  <select id ="siteengto" name="siteengto" class="form-control">
                    <option value="">Please choose</option>
                  </select>

                </div>

                 <div class="col-md-1 form-group">
                 <label>&nbsp;</label>
                    <input type="number" id="duration" name="duration" class="form-control" placeholder="Duration" min="1">

                </div>

                <div class="col-md-3 form-group" >
                 <label>&nbsp;</label>

                    <select id="dur_type" name="dur_type" class="form-control">
                      <option value="">Duration type</option>
                      <option value="15">Days</option>
                      <option value="16">Months</option>
                    </select>

                </div>
            
             
            </div>

          

            <div class="row text-center">
               <button class="btn btn-sm btn-primary" id="submit">Submit</button>
                
              
            </div>
           
        <!--  end  -->


    <div id="addtools" class="modal" role="dialog">
        <div class="modal-dialog modal-md">

        </div>
    </div>




    <script type="text/javascript">

       /* change user */

       $(document).on('change', '#chsite', function (event) {

         $('#msg').html('');

            var id = $(this).val();
            
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/getengineer'); ?>',
                success: function (data) {

                  $('#siteengfrm').html(data);
                  $('#siteengto').html(data);
                    
                }
            });
           
      });


    $("#submit").click(function(){

          $('#msg').html('');
            
            var locid = $('#chsite option:selected').val();
            var engfrm = $('#siteengfrm option:selected').val();
            var engto = $('#siteengto option:selected').val();
            var duration = $('#duration').val();
            var dur_type = $('#dur_type option:selected').val();

           
              if ( locid && engfrm &&  engto && duration && dur_type) {

                  if(engfrm != engto){

                    if (confirm('Are you sure to submit?')) { 

                      $('#loading_icon').show();

                        $.ajax({
                        type: "POST",
                        data: {locid: locid, engfrm:engfrm, engto:engto , duration:duration, dur_type:dur_type},
                        url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/changeeng'); ?>',
                        success: function (response)
                        {   
                           
                          $('#loading_icon').hide();
                          if(response==1){
                            $('#msg').html('<div class="alert alert-success" style="width: 302px;">Changed Successfully</div>');
                          }else{
                            $('#msg').html('<div class="alert alert-danger" style="width: 302px;">No item to transfer</div>');
                          }

                          
                            
                        }
                    });

                  }

                  }else{
                     
                    $('#msg').html('<div class="alert alert-danger" style="width: 302px;">Select Different Engineers</div>');

                  }
               
               } else {

                 $('#msg').html('<div class="alert alert-danger" style="width: 302px;">Please Select All fields</div>');

                  
              }  


              

    });


    $("#tool_dis").click(function(){

            var id = $('#chsite option:selected').val();
            var page_type = 'site';
            var code = 'find_1';
            var type = 0;
            
            if (id) {
                $.ajax({
                    type: "GET",
                    data: {id: id, code: code,type:type, page_type:page_type},
                    url: '<?php echo Yii::app()->createUrl('Tools/transferalltools'); ?>',
                    success: function (response)
                    {
                        $("#addtools").html(response);
                        $("#addtools").css({"display": "block"});
                    }
                });
            } else {
                alert("Please Select Site");
            }

    });



          


    </script>


        
       
        

     

