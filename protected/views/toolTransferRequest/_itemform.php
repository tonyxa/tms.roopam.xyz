<?php
Yii::app()->clientScript->registerScript('myScript', "
    $('#tool-transfer-request-form').submit(function() {
        $('#loading_icon').show();
    });
");
?>
 


<?php
if ($model->isNewRecord) {
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/siteview&id='.$id.''),
            /* 'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
} else {
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/update&id=' . $id . ''),
            /* 'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
}
?>

<?php
$page = 'site';
Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedItems(){
var itemids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            itemids[i]= $(this).val();
          });
       return itemids;
   }

 $('#selected_tasks_all').click(function(){
                if(this.checked){
                $('div.checker span').addClass('checked');
                }else{
                $('div.checker span').removeClass('checked');
                }
                return true;    
                });
                

function getUnSelectedItems(){
var uncheckeditemids = [];
 $(\"input[name='selected_tasks[]']:not(:checked)\").each(function(i) {
            uncheckeditemids[i]= $(this).val();
          });
       return uncheckeditemids;
   }
$(document).ready(function(){
$(\".approve\").click(function (event) { 
$('#loading_icon').show();
var selected_tasks = 0;
    
    $(\"input[name='selected_tasks[]']:checked\").each(function(i) {       
        selected_tasks = 1;
        
    });
    if(selected_tasks==0)
        {
            alert(\"Please select task(s)\"); 
            return false;
        }
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $id . "';
		$.ajax({
			type: \"POST\",
                        data :{ids:getSelectedItems(),unchecked:getUnSelectedItems(),remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/Appovestatus') . "',
			success: function (response)
			{					
                            location.reload();
			}
		});
	});
        

$(\".acknowledge\").click(function (event) { 
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/acknowldgestatus') . "',
			success: function (response)
			{					
                            location.reload();
			}
		});
	});


$(\".reject\").click(function (event) { 
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/rejectstatus') . "',
			success: function (response)
			{					
                            location.reload();
			}
		});
	});




$(\".finalapprove\").click(function (event) { 

            $('#loading_icon').show();
            var id = '" . $id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/Finalapproval') . "',
			success: function (response)
			{					
                           location.reload();
			}
		});
});

$(\".finalreject\").click(function (event) { 

            $('#loading_icon').show();
            var id = '" . $id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/finalreject') . "',
			success: function (response)
			{					
                           location.reload();
			}
		});
});
});
");
?>

<?php
if(empty($newmodel)) {
	$table_show = '';
} else {
	$table_show = 'display: none;';
	
}
?>


        <input type="hidden" id="check_uniqueallocation" value="<?php echo ($model->isNewRecord) ? 0 : $model->id; ?>">
        <div class="row">
			<input type="hidden" name="request_id" value="<?php echo $id; ?>" >
        </div>


        <?php /* if ($page == 'vendor') { ?>
          <div class="row">

          <div class="col-md-6">
          <?php echo $form->labelEx($model, 'paid_amount'); ?>
          <?php echo $form->textField($model, 'paid_amount', array('maxlength' => 50, 'class' => 'form-control')); ?>
          <?php echo $form->error($model, 'paid_amount'); ?>
          </div>
          <div class="col-md-6">
          <?php echo $form->labelEx($model, 'rating'); ?>
          <?php echo $form->textField($model, 'rating', array('maxlength' => 50, 'class' => 'form-control')); ?>
          <?php echo $form->error($model, 'rating'); ?>
          </div>
          </div>

          <div class="row">


          <div class="col-md-6">
          <?php echo $form->labelEx($model, 'remarks'); ?>
          <?php echo $form->textArea($model, 'remarks', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
          <?php echo $form->error($model, 'remarks'); ?>
          </div>
          <div class="col-md-6">
          <?php echo $form->labelEx($model, 'service_report'); ?>
          <?php echo $form->textArea($model, 'service_report', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
          <?php echo $form->error($model, 'service_report'); ?>
          </div>
          </div>
          <?php } */ ?>
        <div class="table-responsive "><br>
            <table id="workdetails" width="500" border="1" class="modaltable" style="<?php echo $table_show; ?>">
                <?php if ($model->isNewRecord) { ?>
                    <tr>
                        <td>S.No.</td>
                        <td>Type</td>
                        <td>Code</td><td></td>
                        <td>Item Name</td>
                        <td>Units</td>
                        <td>Duration</td>
                        <td>Duration Type</td>
                        <td>Quantity</td>
                        <td></td>
                    </tr>

                    <tr class="worktr">
                        <td>1</td>
                        <td><?php
                            $options = array('0' => 'Tool', '1' => 'Tool Set');
                            echo CHtml::dropDownList('type', '', $options, array('class' => 'form-control type'));
                            ?>
                        </td>
                        <td><input type="text" name="code[]" class="inputs form-control target code" id="code_1" readonly=true />
                        </td>
                        <td><span class="icon-magnifier icon-comn find" id = "find_1"></span>
                            <?php //echo CHtml::image(Yii::app()->request->baseUrl . '/images/find.png', 'find', array('class' => 'find', 'id' => 'find_1')); ?>
                            <input type="text" name="newcode[]" class="inputs form-control target newcode" id="newcode_1" style="display:none"/> </td>


                        <?php /*
                          $tools = array();
                          echo CHtml::dropDownList('newcode[]', '',$tools,array('class'=>'inputs form-control target code','empty'=>'Please choose')); */ ?>

                        <td><input type="text" name="item_name[]" class="inputs form-control target item_name" id="item_name_1" readonly=true/></td>
                        <td>

                            <select class="inputs form-control target unit" name="unit[]" id="unit_1">
                                <option value="">Please choose</option>
                                <?php foreach ($units as $unit) { ?>
                                    <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>
                                <?php } ?> 
                            </select>
                        </td>

                        <td><input type="text" name="duration[]" class="inputs form-control target duration" id="duration_1" /></td>
                        <td><?php
                            $durationtype = CHtml::listData($duration_type, "sid", "caption");
                            echo CHtml::dropDownList('duration_type[]', '', $durationtype, array('class' => 'inputs form-control target duration_type', 'id' => 'duration_type_1', 'empty' => 'Please choose'));
                            ?>
                        </td>
                        <td><input type="text" name="quantity[]" class="inputs lst form-control target quantity" id="quantity_1" readonly="true" /></td>                        
                        <td></td>
                    </tr>
                <?php } else {
                    ?>

                    <tr>
                        <td>S.No.</td>
                        <td style="display:none"></td>
                        <td>Type</td>
                        <td>Code</td><td></td>
                        <td>Item Name</td>
                        <td>Units</td>
                        <td>Duration</td>
                        <td>Duration Type</td>
                        <td>Quantity</td>
                        <td></td>
                    </tr>
                    <?php
                    $i = 1;
                    foreach ($newmodel as $new) {
                        ?>
                        <tr class="worktr">
                            <td><?php echo $i; ?></td>
                            <td><?php
                                $options = array('0' => 'Tool', '1' => 'Tool Set');
                                echo CHtml::dropDownList('type', '', $options, array('class' => 'form-control type'));
                                ?>
                            </td>
                            <td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" value="<?php echo $new['id']; ?>" ></td>
                            <td><input type="text" name="code[]" class="inputs form-control target code" id="code_1" value="<?php echo $new['code']; ?>" readonly=true/></td>
                            <td><?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/find.png', 'find', array('class' => 'find', 'id' => 'find_1')); ?><input type="text" name="newcode[]" class="inputs form-control target newcode" id="newcode_1" style="display:none"/>   </td>
                              <!--<td><input type="text" name="item_name[]" class="inputs form-control target" id="item_name_1" value="<?php echo $new['item_name']; ?>" /></td>-->


                <!--<td><input type="text" name="code[]" class="inputs form-control target" id="code_1" /></td>-->
                <!--                            <td>
                <input type="text" name="code[]" class="inputs form-control target newcode" id="code_1" style="display:none"/>                               
                            <?php
                            $tools = array();
                            echo CHtml::dropDownList('newcode[]', '', $tools, array('class' => 'inputs form-control target code', 'empty' => 'Please choose'));
                            ?>
                </td>-->
                            <td><input type="text" name="item_name[]" class="inputs form-control target item_name" id="item_name_1" value="<?php echo $new['item_name']; ?>" readonly=true/></td>
                            <td>
                                <select class="inputs form-control target unit" name="unit[]" id="unit_1">
                                    <option value="">Please choose</option>
                                    <?php
                                    foreach ($units as $unit) {
                                        if ($unit['id'] == $new['unit']) {
                                            //echo "hi";
                                            ?>
                                            <option value="<?php echo $unit['id']; ?>" selected><?php echo $unit['unitname']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?> 
                                </select>
                            </td>

                            <td><input type="text" name="duration[]" class="inputs form-control target duration" id="duration_1" value="<?php echo $new['duration_in_days']; ?>"/></td>
                            <td><?php /*
                              $durationtype = CHtml::listData($duration_type,"sid","caption");
                              echo CHtml::dropDownList('duration_type[]', '',$durationtype,array('class'=>'inputs form-control target duration_type','id'=>'duration_type_1','empty'=>'Please choose')); */ ?>
                                <select class="inputs form-control target duration_type" name="duration_type[]" id="duration_type_1">
                                    <option value="">Please choose</option>
                                    <?php
                                    foreach ($duration_type as $duration) {
                                        if ($duration['sid'] == $new['duration_type']) {
                                            //echo "hi";
                                            ?>
                                            <option value="<?php echo $duration['sid']; ?>" selected><?php echo $duration['caption']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $duration['sid']; ?>"><?php echo $duration['caption']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?> 
                                </select>            
                            </td>
                            <td><input type="text" name="quantity[]" class="inputs form-control target quantity" id="quantity_1" value="<?php echo $new['qty']; ?>" readonly="true"/></td>                        
                            <td >
                                <?php if ($i !== 1) { ?>
                                    <input type="button" class="btn green btn-xs  deleteitem"  id="<?php echo $new['id']; ?>" value="Delete">
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
            </table>
        </div>

        <div class="row save-btnHold">
			<div class="col-md-12 text-center">
				
				
    <h4><span><i></i></span></h4>    
    <?php
   if ((($tmodel->request_status == $treq) || ($tmodel->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {
        echo '<textarea cols="50" rows="3" name="remark" class="remarks"></textarea><br>';
    }
    if (($tmodel->request_status == $treq) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {
    
        ?>
       
         <input type="button" value="ACKNOWLEDGE" name="Submit" class="btn btn-info acknowledge"/> 
        <?php
    }
    if ((($tmodel->request_status == $treq) || ($tmodel->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {
		
        ?>
       <!-- <input type="submit" value="APPROVE" name="Submit" class="btn btn-warning approve"/> -->
       
       <?php echo CHtml::submitButton($model->isNewRecord ? 'APPROVE' : 'Save', array('class' => 'btn btn-warning')); ?>
       
        <input type="button" value="REJECT" name="Submit" class="btn btn-danger reject"/> 
        <?php
    } else {
        echo $tmodel->staff_remarks;
    }


    $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Approved'))
            ->queryRow();

    if ((Yii::app()->user->role == 1 || Yii::app()->user->role == 10) && ($tmodel->request_status == $qryres2['sid'])) {
        ?>
        <br><br> <input type="button" value="ACCEPTED TOOLS IN GOOD CONDITION" name="Submit" class="btn btn-info finalapprove"/>
        <input type="button" value="TRANSFER REJECT" name="Submit" class="btn btn-danger finalreject"/>
    <?php } 
    ?>
				
				
				
				
            <?php // echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn green')); ?>
            <!--<button data-dismiss="modal" class="btn default" onclick="javascript:window.location.reload()">Close</button>-->
            <div id="loading_icon" style="display:none">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>



<script type="text/javascript">
	var leng = '<?php echo $total_qty;?>';
	var t_count = '<?php echo count($newmodel); ?>';
	var count = '';
	if(t_count == 0)
	{
		count = leng;
	} else {
		count = leng-t_count;
	}
	//alert(t_count);
	//alert(leng);
    var i = $('#workdetails tr').length;
//    var flag=0;

		//alert(i);

	var j =1;
	
	if(t_count != (leng-1)) {
		
		
		var kt= ($('#workdetails tr').length-1);
		
		var tcount = ($('#workdetails tr').length-1);
		
	
    $(document).on('keypress', '.lst', function (e) {
		j++;
		
		tcount = ($('#workdetails tr').length);
		
        var code = (e.keyCode ? e.keyCode : e.which);  
        
         var item_name =   $(this).closest('tr').find('.item_name').val();
		  var duration =   $(this).closest('tr').find('.duration').val();
		  var duration_type =   $(this).closest('tr').find('.duration_type').val();
		  var quantity =   $(this).closest('tr').find('.quantity').val();
        if(item_name != '' && duration !='' && duration_type !='' && quantity !='') {
			$(this).closest('tr').find('.quantity').removeClass('lst');
//        if(flag==0){
        //if (code == 13) {
        if (tcount <=count) {
            html = '<tr class="worktr">';
            html += '<td>' + tcount + '</td>';
            html += '<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="ids' + tcount + '" /></td>';
            html += '<td><select class="inputs form-control target type" >'
                    + '<option value="0" selected>Tools</option>'
                    + '<option value="1">Tool Set</option>'
                    + '</select> </td>';

            html += '<td><input type="text" name="code[]" class="inputs form-control target code" id="code_' + tcount + '" readonly=true/>'
                    + '</td>';
            html += '<td><img src ="<?php echo Yii::app()->request->baseUrl ?>/images/find.png" alt="find" class="find" id="find_' + tcount + '"/><input type="text" name="newcode[]" class="inputs form-control target newcode" id="newcode_' + i + '" style="display:none"/>    </td>'
            html += '<td><input type="text" name="item_name[]" class="inputs form-control target item_name" id="item_name_' + tcount + '" readonly=true/></td>';
            html += '<td><select class="inputs form-control target unit" name="unit[]" id="unit_' + tcount + '"><option value="">Please choose</option>'
<?php foreach ($units as $unit) { ?>
                + '<option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>'
<?php } ?>
            + '</select> </td>';
            /* <input type="text" class="inputs form-control" name="work_site_' + i + '" id="work_site_' + i + '" />*/
//            html += '<td><input type="text" class="inputs form-control target" name="serial_no[]" id="serial_no_1" /></td>';
            html += '<td><input type="text" name="duration[]" class="inputs form-control target duration" id="duration_' + tcount + '" /></td>';
            html += '<td><select class="inputs form-control target duration_type" name="duration_type[]" id="duration_type_' + tcount + '"><option value="">Please choose</option>'
<?php foreach ($duration_type as $duration) { ?>
                + '<option value="<?php echo $duration['sid']; ?>"><?php echo $duration['caption']; ?></option>'
<?php } ?>
            + '</select> </td>';

            html += '<td><input type="text" name="quantity[]" class="inputs lst form-control target quantity" id="quantity_' + tcount + '"  readonly="true"/></td>';
            html += '<td><input type="button" class="btn green btn-xs delete"  data-id="' + tcount + '" value="Delete"></td>';
            html += '</tr>';
            $('#workdetails').append(html);
            $(this).focus().select();
            i++;
            e.preventDefault();
            return false;
        }
        
	}
//        }
    });
    
    
}
    
    
    $(document).on('keypress', '.target', function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $(document).on('keydown', '.inputs', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var index = $('.inputs').index(this) + 1;
            $('.inputs').eq(index).focus();
        }
    });
    $(document).on('click', '.delete', function () {
        var dataid = $(this).attr('data-id');
        $(this).parent().parent('tr').remove();
        AutoNumber();
    });
    function AutoNumber() {
        var j = 1;
        $('.worktr').each(function () {
            $(this).find("td:first").text(j);
            j++;
        });
    }
    jQuery.validator.addMethod("unique",
            function (request_no, element) {
                var val = document.getElementById('check_uniqueallocation').value;               // alert(val);
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/checkvalidation'); ?>',
                    data: {request_no: request_no, val: val},
                    success: function (data) {
                        result = (data == "true") ? true : false;
                    }
                });

                // return true if username is exist in database
                return result;
            }
    );

    $(document).on('click', '.deleteitem', function (event) {
        var newthis = $(this);
        var id = $(this).attr("id");
        //alert(id);
        var answer = confirm("Are you sure you want to delete?");
        if (answer)
        {
            $.ajax({
                method: "POST",
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/deleteitem&id='); ?>' + id,
                data: {id: id},
                success: function (data) {
                    newthis.parent().parent("tr").remove();
                    //console.log(data);

                }
            });
        }
    });



//    $(document).ready(function () {
//        $("#workdetails").on("click", ".lst", function () {
//            var $selects = $(this).closest('tr').find('td select'),
//                    $cells = $(this).closest("tr").find("td input");
//            $("#workdetails").find("td input").removeClass("has-error");
//            $("#workdetails").find("td select").removeClass("has-error");
//            $cells.removeClass("has-error");
//            $selects.removeClass("has-error");
//            $cells.each(function () {
//                if ($(this).val().trim() === '') {
//                    flag++;
//                    $(this).addClass("has-error");
//                }
//            });
//            $selects.each(function () {
//                if ($(this).val() == '') {
//                    // flag++;
//                    $(this).addClass("has-error");
//                }
//            });
//        });
//    });

    $(document).ready(function () {
        $("#tool-transfer-request-form").validate({
            rules: {
                'ToolTransferRequest[request_no]': {
                    required: true,
                    unique: true,
                },
                'ToolTransferRequest[request_date]': {
                    required: true

                },
                'ToolTransferRequest[request_from]': {
                    required: true,
                },
                'ToolTransferRequest[request_to]': {
                    required: true

                },
                'ToolTransferRequest[request_owner_id]': {
                    required: true

                },
                'ToolTransferRequest[paid_amount]': {
                    number: true
                },
                'ToolTransferRequest[rating]': {
                    number: true
                },

                'code[]': {
                    required: true,
                    unique: true,
                },
                'item_name[]': {
                    required: true,
                },
                'unit[]': {
                    required: true,
                },
                'serial_no[]': {
                    required: true,
                },
                'duration[]': {
                    required: true,
                    number: true,
                },
                'duration_type[]': {
                    required: true,
                },
                'quantity[]': {
                    required: true,
                    number: true,
                },

            },
            messages: {
                'ToolTransferRequest[request_no]': {
                    required: "Request No is required",
                    unique: "Request No is unique",
                },
                'ToolTransferRequest[request_date]': {
                    required: "Request Date is required",
                },
                'ToolTransferRequest[request_from]': {
                    required: "Request From is required",
                },
                'ToolTransferRequest[request_to]': {
                    required: "Request To is required",
                },
                'ToolTransferRequest[request_owner_id]': {
                    required: "Required",
                },
                'ToolTransferRequest[paid_amount]': {
                    number: "Paid Amount must be an integer",
                },
                'ToolTransferRequest[rating]': {
                    number: "Rating must be an integer",
                },
                'code[]': {
                    required: "Required",
                },
                'item_name[]': {
                    required: "Required",
                },
                'unit[]': {
                    required: "Required",
                },
                'serial_no[]': {
                    required: "Required",
                },
                'duration[]': {
                    required: "Required",
                    number: "Duration must be an integer",
                },
                'duration_type[]': {
                    required: "Required",
                },
                'quantity[]': {
                    required: "Required",
                    number: "Quantity must be an integer",
                },

            },
            /* submitHandler: function () {
             var formData = JSON.stringify($("#tool-transfer-request-form").serializeArray());
             $.ajax({
             method: "POST",
             data: {formdata: formdata},
             url: '/index.php?r=ToolTransferRequest/create',
             success: function (data) {
             console.log(data);
             }
             });
             }, */

        });
        
                $.validator.addMethod("unique", function(value, element) {
    var parentForm = $(element).closest('form');
    var timeRepeated = 0;
    if (value != '') {
        $(parentForm.find(':text')).each(function () {
            if ($(this).val() === value) {
                timeRepeated++;
            }
        });
    }
    return timeRepeated === 1 || timeRepeated === 0;

}, "* Duplicate");
        
        
        $(document).on('change', '#ToolTransferRequest_request_from', function (event) {
            $('.code').val("");
            $('.item_name').val("");
            $('.unit').val("");
            $('.duration').val("");
            $('.duration_type').val("");
            $('.quantity').val("");
        });
        $(document).on('change', '.type', function (event) { 
            $(this).closest('tr').find('.code').val("");
            $(this).closest('tr').find('.item_name').val("");
            $(this).closest('tr').find('.unit').val("");
            $(this).closest('tr').find('.duration').val("");
            $(this).closest('tr').find('.duration_type').val("");
            $(this).closest('tr').find('.quantity').val("");
        });
        $(document).on('click', '.code', function (event) {
            var code = $(this).closest('tr').find('.find').attr('id');
            var type = $(this).closest('tr').find('.type').val();
            getdetails(code,type);
        });
        $(document).on('click', '.find', function (event) {
            var code = $(this).attr('id');
            var type = $(this).closest('tr').find('.type').val();
            getdetails(code,type);
        });
        function getdetails(code,type) { 
            var id = '<?php echo $request_from; ?>';
            var item_id = '<?php echo $id; ?>';
            var code = code;
            var type = type;

           

           // if (id) {
                $.ajax({
                    type: "GET",
                    data: {id: id, code: code,type:type , item_id:item_id },
                    url: '<?php echo Yii::app()->createUrl('Tools/alltoolsview'); ?>',
                    success: function (response)
                    {
                        $("#addtools").html(response);
                        $("#addtools").css({"display": "block"});
                    }
                });
           /* } else {
                alert("Please Select Request from");
            } */
        }
        ;

        $(document).on('change', '.type', function (event) {
            var id = $(this).val();
            var thiss = $(this);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/gettool'); ?>',
                success: function (data) {
                    //alert(data.tools);
                    // $(".code").html(data.tools);
                    thiss.closest('tr').find('select.code').html(data.tools);
                    thiss.closest('tr').find('select.unit').html(data.unittype);
                    //$(this).closest(".code").html(data.tools);
                    console.log(data);
                }
            });

        });
        $(document).on('change', '.code', function (event) {
            var id = $(this).val();
            var code = $(".code option:selected").text();//alert(code);
            var thiss = $(this);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/gettooldetails'); ?>',
                success: function (data) {
                    // alert(data.name);
                    // $(".newcode").val(code);
                    //$(".item_name").val(data.name);
                    thiss.closest('tr').find('.newcode').val(code);
                    thiss.closest('tr').find('.item_name').val(data.name);

                    console.log(data);
                }
            });
        });

    });

</script>


<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    .find{
        cursor: pointer;
    }
</style>
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-md"></div>
</div>
