<?php
/* @var $this ToolTransferRequestController */
/* @var $model ToolTransferRequest */

/* $this->breadcrumbs=array(
  'Tool Transfer Requests'=>array('index'),
  'Manage',
  );

  $this->menu=array(
  array('label'=>'List ToolTransferRequest', 'url'=>array('index')),
  array('label'=>'Create ToolTransferRequest', 'url'=>array('create')),
  );

  Yii::app()->clientScript->registerScript('search', "
  $('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
  });
  $('.search-form form').submit(function(){
  $.fn.yiiGridView.update('tool-transfer-request-grid', {
  data: $(this).serialize()
  });
  return false;
  });
  "); */
?>

<h1>Item to be Return
</h1>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->
<?php /*
  <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
  <div class="search-form" style="display:none">
  <?php $this->renderPartial('_search',array(
  'model'=>$model,
  )); ?>
  </div><!-- search-form -->
 */ ?>
<?php if($page == 'site') {
    $role = 10;
}else{
     $role = 9;
}
?>

<?php
$res1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Transfer to vendor'))           
                        ->queryRow();
             $id1 = $res1['sid'];

$res2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Received back to stock'))           
                        ->queryRow();
             $id2 = $res2['sid'];

?>

<div class="table-responsive">
<?php

     $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Transfer Requested'))           
                        ->queryRow();
     
    $treq = $qryres1['sid'];
    $qryres2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Approved'))           
                        ->queryRow();
	
    $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-transfer-request-grid',
    'itemsCssClass' => 'table table-bordered',
    'dataProvider' => $model->requestitems(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        'request_no',
       //'req_date',
        //'request_to',
        //'request_from',
        //'transfer_to',
        'request_date',
        //'return_date',
        array(
            'name' => 'return_date',
            'type' => 'raw',
            'value' => '$data->getReturndatecolor($data->return_date)',
           // 'htmlOptions' => array('width' => '80px'),            
        ),  
        'code',
        'item_name',
        
       /* array(
            'name' => 'tool_id',
             'value' => '$data->getToolstatus("$data->tool_id")',
            'type' => 'raw',
           // 'htmlOptions' => array('width' => '200px'),            
        ), */
        
        array(
            'name' => 'request_from',
            'value' => 'isset($data->request_from) ? $data->requestFrom[\'name\'] : "--"',
        ),
        array(
            'name' => 'request_to',
           'value' => 'isset($data->request_to) ? $data->requestTo[\'name\'] : "--"',
        ),        
       /* array(
            'name' => 'request_status',
            'value' => 'isset($data->request_status) ? $data->requestStatus[\'caption\'] : "--"',
            'visible'=> ($page == 'site') ? true :false,
        ), */
       /* array(
            'name' => 'location',
           'value' => 'isset($data->location) ? $data->location0[\'name\'] : "--"',
        ),*/
       
    ),
));
	
	
?>
</div>
<!-- Add tools Popup -->
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function(){
$(".finalapprove").click(function (event) { 
$("#loading_icon").show();
event.preventDefault();
 var url = $(this).attr("href");
		$.ajax({
			type: "POST",
			url: url,
			success: function (response)
			{			
                           location.reload();
			}
		});
});


/*		$(".createtools").click(function (event) {
			 event.preventDefault();			
			$.ajax({
				type: "GET",
				url:"' . Yii::app()->createUrl('ToolTransferRequest/create&page='.$page) . '",
				success: function (response)
				{					
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
		});
		
		 
                
            
	$(".editAllocation").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
            url = url + "&page='.$page . '",
          //  alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addtools").html(response);
                        $("#addtools").css({"display":"block"})
                    }

                });
            
        });
		

		
*/
	});
');
?>
