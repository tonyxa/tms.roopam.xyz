
<h4><u>Transfer Request Details</u></h4>
<?php $req_detailes = ToolTransferRequest::model()->findByPk($insert_id);  ?>
<table>
    <tr><th style="text-align:left">Requested Date </th><td>: <?php echo date('d-m-Y',strtotime($req_detailes->request_date));?></td></tr>
    <tr><th style="text-align:left">Requested From </th><td>: <?php echo (isset($req_detailes->Requestowner) ? $req_detailes->Requestowner->first_name :""); ?></td></tr>
    <!--tr><th style="text-align:left">Requested From </th><td>: <?php // echo $req_detailes->requestFrom->name;?></td></tr-->
    <tr><th style="text-align:left">Requested To </th><td>: <?php echo isset($req_detailes->requestTo->name)?$req_detailes->requestTo->name:"";?></td></tr>
    <tr><th style="text-align:left">Transfer To </th><td>: <?php echo $req_detailes->transfer_to;?></td></tr>
     <tr><th style="text-align:left">Location </th><td>: <?php echo (isset($req_detailes->location0) ? $req_detailes->location0->name : ""); ?></td></tr>
     <tr><th style="text-align:left">Created By </th><td>: <?php echo $req_detailes->createdBy->first_name;?></td></tr>
</table>
<h4><u>Requested Items</u></h4>
<?php $req_items = ToolTransferInfo::model()->findAll(array('condition'=>'tool_transfer_id ='.$req_detailes->id));  ?>
<table style="border:1px solid #495a6d;width: 100%;border-collapse: collapse;">
    <tr><th style="border:1px solid #495a6d;">Sl No</th><th style="border:1px solid #495a6d;">TOOL NAME</th><th style="border:1px solid #495a6d;">Duration</th><th style="border:1px solid #495a6d;">Quantity</th></tr> 
    <?php  
    $k = 1;
    foreach($req_items as $items){ ?>
    <tr><td style="border:1px solid #495a6d;"><?php echo $k;?></td><td style="border:1px solid #495a6d;"><?php echo $items->item_name;?></td><td style="border:1px solid #495a6d;"><?php echo $items->duration_in_days.' '.$items->durationType->caption;?></td><td style="border:1px solid #495a6d;"><?php echo $items->qty.' '.$items->unit0->unitname;?></td></td></tr> 
    <?php $k++;} ?>
</table>

<?php 
 $trans_items = Yii::app()->db->createCommand('SELECT * FROM tms_tool_transfer_items WHERE tool_transfer_id='.$insert_id)->queryAll();
//$trans_items = ToolTransferItems::model()->findAll(array('condition'=>'tool_transfer_id ='.$insert_id));  ?>
<?php
if(!empty($trans_items)) {
 ?>
<h4><u>Transfer Items</u></h4>
<table style="border:1px solid #495a6d;width: 100%;border-collapse: collapse;">
    <tr><th style="border:1px solid #495a6d;">Sl No</th><th style="border:1px solid #495a6d;">CODE</th><th style="border:1px solid #495a6d;">TOOL NAME</th><th style="border:1px solid #495a6d;">Duration</th><th style="border:1px solid #495a6d;">Quantity</th></tr> 
    <?php  
    $k = 1;
    foreach($trans_items as $items){ 
	?>
    <tr><td style="border:1px solid #495a6d;"><?php echo $k;?></td><td style="border:1px solid #495a6d;"><?php echo $items['code'];?></td><td style="border:1px solid #495a6d;"><?php echo $items['item_name'];?></td><td style="border:1px solid #495a6d;"><?php echo $items['duration_in_days'];?></td><td style="border:1px solid #495a6d;"><?php echo $items['qty'];?></td></td></tr> 
    <?php $k++;} ?>
</table>
<?php } ?>

<br>
<br><p>Regards,</p><p><?php echo Yii::app()->name;?></p>

