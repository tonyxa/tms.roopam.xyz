

<div class="clearfix">
    <div class="pull-left">
        <h1>View Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1>
    </div>

</div>

<div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Site: </span><b><?php echo  (isset($model->request_from) ? $model->requestFrom->name : "---") ?></b></div>
    <div class="rightspace"><span>Office Staff: </span><b><?php echo(isset($model->request_owner_id) ? $model->Requestowner->first_name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>

    <?php



    $qryres1 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
            ->queryRow();

    $treq = $qryres1['sid'];

     $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Acknowledged'))
            ->queryRow();

    $treq1 = $qryres2['sid'];


    ?>


     <?php if(!empty($items)) {   ?>

		 <br><br/>
        <h2>Requested Items</h2> <br/>


  <!-- <div class="row" style="margin-bottom: 10px;">
    <div class="col-md-2" style="font-size: 100% !important;">

        <select class="form-control"  id="selectitem" style="margin-top: 5px;">
             <option value="">Please choose</option>
              <option value="1">Damage</option>
              <option value="2">Breakdown</option>
              <option value="3">Running</option>
        </select>
        </div>
        <div class="col-md-3 text-left" style="font-size: 100% !important;">

        <button class="btn blue add" type="submit">Add</button>
        </div>

    </div> -->

		<table  width="500" border="1" class=" table table-bordered newtable">
                    <thead>
                        <tr>
                            <!-- <th><input type="checkbox" id="allcb" name="allcb"/></th> -->
                            <th>S.No.</th>
                            <th>Code</th>
                            <th>Item Name</th>
                            <th>Units</th>
                            <th>Quantity</th>
                            <th>Return Quantity</th>
                            <th>Physical condition</th>
                        </tr>
                    </thead>

		   <?php
          foreach( $items as $key => $value ){

              $allitems = Yii::app()->db->createCommand("SELECT caption,qty FROM `tms_approve_itemqty`
                left join tms_status on tms_status.sid = tms_approve_itemqty.physical_condition
                WHERE `tool_transfer_id`= ".$id." and `transfer_item_id`= ".$value['id'])->queryAll();
         ?>
        <tbody>

                  <tr class="worktr">
                      <!-- <td><input type="checkbox" id="cb1" name="cb1" value="<?php // echo $value['id'] ?>" class="checkBoxClass"/></td> -->
                      <td><?php echo $key+1 ; ?>
                         <input type="hidden" class="itmid"  value="<?php echo $value['id']; ?>">
                      </td>
                      <td><?php echo $value['code']; ?></td>
                      <td><?php echo $value['item_name']; ?></td>
                      <td><?php
                          $tblpx = Yii::app()->db->tablePrefix;
                          if($value['unit'] !=''){
                          $data =  Yii::app()->db->createCommand("SELECT unitname FROM {$tblpx}unit WHERE id = ".$value['unit']."")->queryRow();
                          echo (!empty($data))?$data['unitname']:"";
                        }
                           ?>
                      </td>
                      <td><?php echo $value['qty']; ?>  </td>
                      <td style="width:120px;" class="quantity"><?php echo $value['return_qty']; ?></td>
                      <?php if(empty($allitems)) { ?>
                         <td style="font-size:13px;width:330px;">
                             <?php foreach($status as $stat){ ?>
                                    <div class="phycon">
                                        <label><?php  echo $stat['caption']; ?></label>
                                        <input type="text" name="<?php  echo $stat['caption']; ?>" class="form-control number_field <?php  echo strtolower($stat['caption']); ?>" autocomplete="off">
                                    </div>
                                <?php } ?>
                              <div class="phycon"><button class="btn blue btn-xs addsts">Add</button></div>
                              <span class="err" style="color:red;"></span>
                          </td>
                       <?php }else{ ?>
                         <td>
                           <?php foreach($allitems as $stat){ ?>
                                  <div class="phycon">
                                      <label><?php  echo $stat['caption']; ?></label><br/>
                                      <span><?php  echo $stat['qty']; ?></span>
                                  </div>
                              <?php } ?>
                         </td>

                      <?php  } ?>


                      </tr>
              </tbody>

			<?php } ?>
		</table>


    <button class="btn blue submit">Submit</button>
    <?php } ?>

  </div>
  <input type="hidden" id="request_id" value="<?= $id ?>">

<script type="text/javascript">


$( document ).ready(function() {

  $(document).on('click','.addsts',function(){

      var id   = $('#request_id').val();

      var damage    = 0;
      var breakdown = 0;
      var running   = 0;
      var qty       = 0;
      var error     = 0;
      var item_id =  $(this).parents().closest('tr').find('.itmid').val();
      if($('.damage').val()!=''){
        damage     = $('.damage').val();
      }
      if($('.breakdown').val()!=''){
        breakdown  = $('.breakdown').val();
      }
      if($('.running').val()!=''){
        running    = $('.running').val();
      }
      total = parseFloat(damage) + parseFloat(breakdown) + parseFloat(running);

      //  qty   = $(this).parents().siblings().find('.quantity').text();

      qty   = $(this).parents().closest('tr').find('.quantity').text();

      if(damage=='' && breakdown =='' && running== ''){
          error = 'Please add quantity';
      }
      if(total > qty){
          error = "Can't exceed return quantity";
      }

      if(total < qty){
          error = "Return quantity not matching";
      }


      if(error != 0){
          $(this).closest('td').find('.err').html(error);
          $(this).closest('td').find('.err').show();
      }

      if(error == 0){

          $.ajax({
              method: "POST",
              url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/StockApprove&id='.$id); ?>' ,
              data: { Damage : damage, Breakdown: breakdown, Running:running,total:total,item_id:item_id,req_id:id },
              success: function (data) {
                alert('Success');
                  if(data == 3){
                    location.reload();
                  }
              }
          });
      }
   });

 $(document).on('click','#allcb',function() {
    var checked=this.checked;

    if(checked == true){

        $(".checkBoxClass").each(function() {

            $(this).parent('span').addClass("checked");
            $(this).attr('checked','checked');

        });
    }else{
       $(".checkBoxClass").each(function() {

            $(this).parent('span').removeClass("checked");
            $(this).attr('checked','');

        });

    }
});

$(document).on('click', '.add', function (event) {

        var type = $( "#selectitem option:selected" ).val();
        var id   = $('#request_id').val();

        var selected = [];
        $('.worktr input:checked').each(function(){
            selected.push($(this).val());
        });

        if(type != '' && selected !='')
        {
            $.ajax({
                method: "POST",
                url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/StockApprove&id='.$id); ?>' ,
                data: { id : id, type: type, items:selected,  },
                success: function (data) {
                  alert('Success');
                  if(data == 3){
                    location.reload();
                  }
                }
            });
        }else {
            if(type == ''){
                alert('Please choose Physical conditions');
            }if( type != '' && selected == ''){
                 alert('Please choose items');
            }


        }
   });
$(document).on('click', '.submit', function (event) {

    var id   = $('#request_id').val();
        $.ajax({
            method: "POST",
            url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/Stocksubmit&id='.$id); ?>' ,
            data: { id : id },
            success: function (data) {
              if(data == 1 ){
                  $('.number_field').css('border-color','red');
                  alert('Add Physical condition for all items');
              }else{
                 window.location = "<?php  echo Yii::app()->createUrl('toolTransferRequest&page=stock')?>";
              }
           }
      });
    });
});

</script>
<style>
    div.checker input{
        opacity: 1;
    }

 .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;}
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    .grid-view table tbody tr{background: #eee;}
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}

 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}

  .btn-primary:hover{
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
.table .btn{margin:0px;}

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
.number_field{width: 80px;}
.phycon{
  display: inline-block;
  margin-right: 8px;
}
</style>
