<?php
Yii::app()->clientScript->registerScript('myScript', "
    $('#tool-transfer-request-form').submit(function() {
        $('#loading_icon').show();
    });
");
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>
<style>
.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
</style>

<?php

Yii::app()->clientScript->registerScript('myjquery', "

$(document).ready(function () {

 $(document).on('keyup', '.img_comp_class', function (event) {
	$('.ui-helper-hidden-accessible').hide(); event.preventDefault();

           var this_id = $(this).attr('id');
           var type = $('#type').val();
		 $.ajax({
			type: 'POST',
                       dataType: 'text',
      data:{type:type},
			url:'".Yii::app()->createUrl('ToolTransferRequest/getvendoritemscode')."',
			success:function(data){
			//alert(data);
			console.log(data);
			var arrResponse = JSON.parse(data);
				test(arrResponse,this_id);

			}
		 });

	 function test(data,this_id){
		var items = data;
		console.log(items);

		  $('.img_comp_class').autocomplete({

			  mustMatch: true,
			  source: items,
			  autoFocus: false,
			  change: function (event, ui) {
				  if (!ui.item) {
					  $('#'+this_id+'').find('.img_comp_class').val('');
				  }
			  },
                           focus: function (event, ui) {
                          $('.ui-helper-hidden-accessible').hide(); event.preventDefault();
                          },response: function(event, ui) {
                                if (!ui.item) {
                                    $('#message').text('No results found');
                                } else {
                                    $('#message').empty();
                                }
                            },
			  select: function(event,ui) {
				  event.preventDefault();
				  if(ui.item.label !=''){
				  $('#'+this_id+'').val(ui.item.label);
				  var id = ui.item.value;

				  $.ajax({
                        type : 'POST',
                        dataType : 'JSON',
                        data: {id: id},
                        url: '". $this->createUrl('Tools/gettallooldetails2')."',
                        success: function (data) {
                        //alert(data);

							 $('.newcode').val(id);
							 $('.total_quantity').val(data.total_qty);
                                                         $('.item_name').val(data.name);
							 $('.unit').html(data.units);
							 $('.unit').val(data.unit);
							 $('.quantity').val(data.qty);
               $('.damage').val(data.damage);
               $('.breakdown').val(data.breakdown);
                        }
					});
                                        $('#message').empty();
				}else{
					 $('#'+this_id+'').val('');
			    }

			  }
		  });
		}

});

  });
    $('#code').focusout(function() {
   if($('#message').text().length > 0){
    $(this).val('');
   }
   });
 ");
?>
<?php
if ($model->isNewRecord) {
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/createvendor&page=' . $page),
            /* 'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
} else {
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/updatevendor',array('id'=>$_GET['id'],'t_id'=>$_GET['t_id'],'page'=>$page)),
            /* 'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
}
$trans_id=isset($_GET['tr_id'])?  $_GET['tr_id'] :  0;
if($trans_id !=0){
    $model= ToolTransferRequest::model()->findBypk($trans_id);
}
?>
<input type="hidden" name="trans_id" value="<?php echo $trans_id; ?>"/>


        <input type="hidden" id="check_uniqueallocation" value="<?php echo ($model->isNewRecord) ? 0 : $model->id; ?>">
        <div class="topsec">
        <div class="row">
            <div class="col-md-2">
                 <?php
                $max = Yii::app()->db
                                ->createCommand("SELECT MAX(request_no) FROM tms_tool_transfer_request")->queryScalar();
                ?>

                <?php echo $form->labelEx($model, 'request_no'); ?>
                <?php echo $form->textField($model, 'request_no', array('class' => 'form-control', 'value' => $max + 1, 'readonly' => true)); ?>
                <?php echo $form->error($model, 'request_no'); ?>
            </div>

             <div class="col-md-3">
                <?php if (Yii::app()->user->role == 1) { ?>
                    <?php if ($page == 'site') { ?>
                        <span>Supervisor</span><br>
                        <?php
                        echo CHtml::activeDropDownList($model, 'request_owner_id', CHtml::listData(Users::model()->findAll(
                                                array(
                                                    'select' => array('userid,first_name'),
                                                    'condition' => 'user_type = 10',
                                        )), "userid", "first_name"), array('empty' => 'Select User', 'class' => 'form-control'));
                    } else {
                        ?>
                        <span>Office Staff</span><br>
                        <?php
                        echo CHtml::activeDropDownList($model, 'request_owner_id', CHtml::listData(Users::model()->findAll(
                                                array(
                                                    'select' => array('userid,first_name'),
                                                    'condition' => 'user_type = 9',
                                        )), "userid", "first_name"), array('empty' => 'Select User', 'class' => 'form-control'));
                    }
                } else {
                    ?>
                    <span style="float: left">Request Date<br><i><?php echo date('d-M-Y'); ?></i></span>
                <?php } ?>

            </div>

        <?php
        $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Site'))
                ->queryRow();

        $qryres2 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Stock'))
                ->queryRow();

        $qryres3 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Vendor'))
                ->queryRow();

        $siteid = $qryres1['sid'];
        $stockid = $qryres2['sid'];
        if ($page == 'site') {
            $condition1 = 'location_type IN(' . $siteid . ',' . $stockid . ')';
            $condition = 'location_type = ' . $qryres1['sid'];
            //$condition = 'location_type = 13';
        } else if ($page == 'stock') {
            //$condition = 'location_type = 12';
            $condition1 = 'location_type = ' . $qryres1['sid'];
            $condition = 'location_type = ' . $qryres2['sid'];
        } else if ($page == 'vendor') {
            //$condition = 'location_type = 14';
            //$condition1 = 'location_type IN(12,13)';
            $condition1 = 'location_type IN(' . $siteid . ',' . $stockid . ')';
            $condition = 'location_type = ' . $qryres3['sid'];
        }
        ?>
            <div class="col-md-3">

                 <?php echo $form->labelEx($model, 'request_to'); ?>
                <?php //echo $form->textField($model, 'request_to', array('maxlength' => 50, 'class' => 'form-control'));  ?>
                <?php
                echo $form->dropDownList($model, 'request_to', CHtml::listData(LocationType::model()->findAll(
                                        array(
                                            'select' => array('id,name'),
                                            'order' => 'name',
                                            'condition' => $condition.' and active_status ="1"',
                                            'distinct' => true
                                )), "id", "name"), array('class' => 'form-control', 'empty' => 'Please choose'));
                ?>
                <?php echo $form->error($model, 'request_to'); ?>
            </div><br>

            </div>
         </div>
        <br>
<?php
          //update section
               if(isset($_GET['id'])){

                   $itemid=$_GET['id'];
                  $datainfo= Yii::app()->db->createCommand()
					->select('*')
					->from('tms_tool_transfer_items')
					->where('id=:id', array(':id' => $itemid))
					->queryRow();
             $tid=$_GET['t_id'];
                       //echo $itemid;die;
               // print_r($datainfo);
                ?>

         <form>
            <div class="itemadd">
                <h3>Add Items</h3>
                <div class=""><label>Type</label>
                        <?php
                            $options = array('0' => 'Tool', '1' => 'Tool Set');
                            echo CHtml::dropDownList('type', '', $options, array('class' => 'form-control type'));
                            ?>
                        <div class="">
                    <label>Code</label>
                        <input type="hidden" name="code" class="inputs form-control target code" id="code_1" value="test03" style="display:none"/>
                    <input name="code" id="code" class="inputs img_comp_class form-control target code"   value="<?php echo isset($datainfo['code'])? $datainfo['code'] :'' ; ?>" >
                    <span id="message"></span>
                    <input type="text" name="newcode" class="inputs form-control target newcode" id="newcode_1" style="display:none"/>
                </div>
                <div class="">
                    <label>Item Name</label>


                        <input name="item_name" id="item_name_1" class="inputs  form-control target item_name" readonly value="<?php echo isset($datainfo['item_name'])? $datainfo['item_name'] :'' ; ?>"/>
                </div>
                <div class="">
                    <label>Unit</label>

                            <select class="inputs form-control target unit" name="unit" id="unit_1">
                                <option value="">Please choose</option>
                                <?php foreach ($units as $unit) { ?>
                                    <option value="<?php echo $unit['id']; ?>" <?php echo ($unit['id'] == $datainfo['unit'])?'selected':""; ?>><?php echo $unit['unitname']; ?></option>
                                <?php } ?>
                            </select>
                         </div>

                <div class="">
                        <?php if ($page == 'stock') {
                         }else{ ?>
                       <label>Duration</label>
                            <input type="text" name="duration" class="inputs form-control target duration" id="duration_1" value="<?php echo ($datainfo['duration_in_days'] != "")?$datainfo['duration_in_days']:""; ?>"/>
                            <?php
                            $durationtype = CHtml::listData($duration_type, "sid", "caption");
                            echo CHtml::dropDownList('duration_type', '', $durationtype, array('class' => 'inputs form-control target duration_type', 'id' => 'duration_type_1', 'empty' => 'Please choose'));
                            ?>
                         <?php } ?>
                        <?php if ($page == 'stock') { ?>
                            <select class="inputs form-control target physical_condition" name="physical_condition" id="physical_condition_1">
                                    <option value="">Please choose</option>
                                    <?php foreach ($status as $stat) { ?>
                                        <option value="<?php echo $stat['sid']; ?>" <?php echo ($unit['id'] == $datainfo['physical_condition'])?'selected':""; ?>><?php echo $stat['caption']; ?></option>
                                    <?php } ?>
                                </select>
                        <?php } ?>
                            </div>
               <div class="">
                    <label>Quantity</label>
                    <input type="text" class="form-control quantity" name="quantity" readonly value="<?php echo isset($datainfo['qty'])? $datainfo['qty'] :'' ; ?>">
                </div>

                <div>
                    <label>&nbsp;</label>
                    <button class="btn btn-sm btn-primary btnadd" type="submit">Save</button>
                    <button type="reset" class="btn btn-sm btn-default btnrst update_reset">Cancel</button>
                </div>
            </div>
         </form>
                <?php } else {
                    ?>

                    <form>
            <div class="itemadd">
                <h3>Add Items</h3>
                <div class="">
                    <label>Type</label>
                     <select class="form-control" name="type" id="type">
                        <option value="">Choose Type</option>
                        <option value="0" >Tool</option>
                        <option value="1" >Tool Set</option>

                    </select>
                </div>
                <div class="">
                    <label>Code</label>
                    <input type="hidden" name="code" class="inputs form-control target code" id="code_1" value="test03" style="display:none"/>
                    <input name="code" id="code" class="inputs img_comp_class form-control target code"   value="<?php //echo isset($datainfo['item_name'])? $datainfo['item_name'] :'' ; ?>" >
                    <span id="message"></span>
                    <input type="text" name="newcode" class="inputs form-control target newcode" id="newcode_1" style="display:none"/>
                </div>
                <div class="">
                    <label>Item Name</label>
                    <input name="item_name" id="item_name_1" class="inputs  form-control target item_name" readonly/>
                </div>
                <div class="">
                    <label>Unit</label>
                    <select class="inputs form-control target unit" name="unit" id="unit_1">
                                <option value="">Please choose</option>
                                <?php foreach ($units as $unit) { ?>
                                    <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitname']; ?></option>
                                <?php } ?>
                     </select>
                </div>


                        <?php if ($page == 'stock') {
                         }else{ ?>
                          <div class="">
                            <label>Duration</label>
                            <input type="text" name="duration" class="inputs form-control target duration number_field" id="duration_1" value="<?php //echo $new['duration_in_days']; ?>"/>
                          </div>
                          <div class="">
                            <label>&nbsp;&nbsp;</label>
                            <?php /*
                              $durationtype = CHtml::listData($duration_type,"sid","caption");
                              echo CHtml::dropDownList('duration_type[]', '',$durationtype,array('class'=>'inputs form-control target duration_type','id'=>'duration_type_1','empty'=>'Please choose')); */ ?>
                                <select class="inputs form-control target duration_type" name="duration_type" id="duration_type_1">
                                    <option value="">Please choose</option>
                                    <?php
                                    foreach ($duration_type as $duration) {
                                        //if ($duration['sid'] == $new['duration_type']) {
                                            //echo "hi";
                                            ?>
                                            <!--option value="<?php //echo $duration['sid']; ?>" selected><?php //echo $duration['caption']; ?></option-->
                                        <?php //} else { ?>
                                            <option value="<?php echo $duration['sid']; ?>"><?php echo $duration['caption']; ?></option>
                                            <?php
                                        }
                                    //}
                                    ?>
                                </select>
                            </div>

                         <?php  } ?>

                        <?php if ($page == 'stock') { ?>
                          <div class="">

                             <label>Physical Condition</label>
                                <select class="inputs form-control target physical_condition" name="physical_condition" id="physical_condition_1">
                                        <option value="">Please choose</option>
                                        <?php
                                        foreach ($status as $stat) {
                                            //if ($stat['sid'] == $new['physical_condition']) {
                                                ?>
                                                <!--option value="<?php //echo $stat['sid']; ?>" selected><?php //echo $stat['caption']; ?></option--><?php //} else { ?>
                                                <option value="<?php echo $stat['sid']; ?>"><?php echo $stat['caption']; ?></option>
                                                <?php
                                            //}
                                        }
                                        ?>
                                    </select>
                                </div>
                            <?php } ?>

                <div class="">
                    <label>Quantity</label>
                    <input type="text" class="form-control quantity number_field" name="quantity" readonly>
                </div>
                <div class="">
                    <label>Damage</label>
                    <input type="text" class="form-control damage number_field" name="damage" readonly>
                </div>
                <div class="">
                    <label>Breakdown</label>
                    <input type="text" class="form-control breakdown number_field" name="breakdown" readonly>
                </div>

                <div>
                    <label>&nbsp;</label>
                    <button class="btn btn-sm btn-primary btnadd" type="submit">Add</button>
                    <button type="reset" class="btn btn-sm btn-default btnrst">Cancel</button>
                </div>
            </div>
         </form>
                <?php } ?>

        <table class="table table-bordered" id="workdetails">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Type</th>
                    <th>Item Name</th>
                    <th>Units</th>
                    <th>Duration</th>
                    <th>Duration Type</th>
                    <th>Quantity</th>
                    <th></th>
                    </tr>
            </thead>
            <tbody>
<?php
        if(isset($_GET['t_id'])){
                     $trans_id=$_GET['t_id'];
                 }
                 if(isset($_GET['tr_id'])){
                     $trans_id=$_GET['tr_id'];
                 }
                 //echo $trans_id;
                 $list=Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_tool_transfer_items')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $trans_id))
                        ->queryAll();


//                echo '<pre>';
//                 print_r($list);exit;
                 $i=1;
                 if(!empty($list)){


                foreach($list as $l){
                            ?>
                <tr>
                    <td><?php echo $i++; ?><input type="hidden" name="id" id="hid" value="<?php echo $l['id']; ?>" /></td>
                                    <?php
                    if($l['type'] == 0){
                        $type='Tool';
                    }
                    elseif($l['type'] == 1){
                        $type='Tool Set';
                    }
                                            ?>
                    <td><?php echo $type; ?></td>
                    <td><?php echo $l['item_name']; ?></td>
                                            <?php
                                        if($l['unit']==1){
                        $unit='NOS';
                                        }
                    else{
                        $unit='SET';
                                    }
                                    ?>
                    <td><?php echo $unit; ?></td>
                    <td><?php echo $l['duration_in_days']; ?></td>

                                    <?php
                                                   $d_type =  Status::model()->findByPk($l['duration_type']);
//

                                    ?>
                    <td><?php echo isset($d_type) ? $d_type['caption'] : ''; ?></td>
                    <td><?php echo $l['qty']; ?></td>
                    <td style="width:60px;">
                        <?php if(isset($_GET['req_no'])){
                            $rno=$_GET['req_no'];
                          }?>
                        <?php echo CHtml::link('',array('toolTransferRequest/updatestock','id' => $l['id'],'t_id'=>$trans_id,'page'=>$page),array('class'=>'fa fa-edit btn btn-xs')); ?>
                        <?php echo CHtml::link('',array(),array('class'=>'fa fa-trash btn btn-xs deleteinfo')); ?>

                            </td>
                </tr>

                                <?php }
                 ?>

            </tbody>
                 <?php } else{
                     ?>
            <tfoot>
                <tr>
                    <td colspan="8">No records found.</td>
                </tr>
            </tfoot>
                <?php
                 }?>
            </table>

        <div class="row save-btnHold">
			<div class="col-md-12 text-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue submit','style' => 'display:none')); ?>
            <!--<button data-dismiss="modal" class="btn default" onclick="javascript:window.location.reload()">Close</button>-->
            <div id="loading_icon" style="display:none">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>



<script type="text/javascript">

    if($('#workdetails tbody tr').length > 0){
    $('.submit').css("display","block");
}

  $(document).on('click', '.update_reset', function () {
     window.location.href = "<?php  echo Yii::app()->createUrl('toolTransferRequest/createvendor',array('tr_id'=>$trans_id)); ?>";
  });
         $(document).on('click', '.submit', function () {
           if($('#ToolTransferRequest_request_owner_id').val() == ''){
               alert("Please select Supervisor");
               $('#ToolTransferRequest_request_owner_id').focus();
               return false;
           }
           else if($('#ToolTransferRequest_request_to').val() == ''){
               alert('Please select Site');
               $('#ToolTransferRequest_request_to').focus();
               return false;
           }else{
            window.location.href = "<?php  echo Yii::app()->createUrl('toolTransferRequest&page=vendor'); ?>";
        }
         });
    $(document).on('keypress', '.target', function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $(document).on('keydown', '.inputs', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var index = $('.inputs').index(this) + 1;
            $('.inputs').eq(index).focus();
        }
    });
    $(document).on('click', '.delete', function () {
        var dataid = $(this).attr('data-id');
        $(this).parent().parent('tr').remove();
        AutoNumber();
    });
    function AutoNumber() {
        var j = 1;
        $('.worktr').each(function () {
            $(this).find("td:first").text(j);
            j++;
        });
    }
    jQuery.validator.addMethod("unique",
            function (request_no, element) {
                var val = document.getElementById('check_uniqueallocation').value;               // alert(val);
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/checkvalidation'); ?>',
                    data: {request_no: request_no, val: val},
                    success: function (data) {
                        result = (data == "true") ? true : false;
                    }
                });

                // return true if username is exist in database
                return result;
            }
    );

    $(document).on('click', '.deleteitem', function (event) {
        var newthis = $(this);
        var id = $(this).attr("id");
        //alert(id);
        var answer = confirm("Are you sure you want to delete?");
        if (answer)
        {
            $.ajax({
                method: "POST",
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/deleteitem&id='); ?>' + id,
                data: {id: id},
                success: function (data) {
                    newthis.parent().parent("tr").remove();
                    //console.log(data);

                }
            });
        }
    });


 $(document).on('click', '.deleteinfo', function () {
          var infoid=$(this).closest('tr').find('#hid').val();
         // alert(infoid);
          var answer = confirm("Are you sure you want to delete?");
        if (answer)
        {
          $.ajax({
                    type: "POST",

                    url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/deletevendor&id='); ?>' + infoid,
                    data: {infoid: infoid},
                    success: function (data) {
                        result = (data == "true") ? true : false;
	}
    });
                }
         });


    $(document).ready(function () {
        $("#tool-transfer-request-form").validate({
            rules: {
                'ToolTransferRequest[request_no]': {
                    required: true,
                    //unique: true,
                },
                'ToolTransferRequest[request_date]': {
                    required: true

                },
                'ToolTransferRequest[request_from]': {
                    required: true,
                },
                'ToolTransferRequest[request_to]': {
                    required: true

                },
                'ToolTransferRequest[request_owner_id]': {
                    required: true

                },
                'ToolTransferRequest[paid_amount]': {
                    number: true
                },
                'ToolTransferRequest[rating]': {
                    number: true
                },

                'code': {
                    required: true,
                    //unique: true,
                },
                'item_name': {
                    required: true,
                },
                'unit': {
                    required: true,
                },
                'serial_no': {
                    required: true,
                },
                'duration': {
                    required: true,
                    number: true,
                },
                'duration_type': {
                    required: true,
                },
                'quantity': {
                    required: true,
                    number: true,
                },
                'physical_condition':{
					required: true,
				}

            },
            messages: {
                'ToolTransferRequest[request_no]': {
                    required: "Request No is required",
                    //unique: "Request No is unique",
                },
                'ToolTransferRequest[request_date]': {
                    required: "Request Date is required",
                },
                'ToolTransferRequest[request_from]': {
                    required: "Request From is required",
                },
                'ToolTransferRequest[request_to]': {
                    required: "Request To is required",
                },
                'ToolTransferRequest[request_owner_id]': {
                    required: "Required",
                },
                'ToolTransferRequest[paid_amount]': {
                    number: "Paid Amount must be an integer",
                },
                'ToolTransferRequest[rating]': {
                    number: "Rating must be an integer",
                },
                'code': {
                    required: "Required",
                },
                'item_name': {
                    required: "Required",
                },
                'unit': {
                    required: "Required",
                },
                'serial_no': {
                    required: "Required",
                },
                'duration': {
                    required: "Required",
                    number: "Duration must be an integer",
                },
                'duration_type': {
                    required: "Required",
                },
                'quantity': {
                    required: "Required",
                    number: "Quantity must be an integer",
                },
                'physical_condition': {
                    required: "Required",
                },

            },
            /* submitHandler: function () {
             var formData = JSON.stringify($("#tool-transfer-request-form").serializeArray());
             $.ajax({
             method: "POST",
             data: {formdata: formdata},
             url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/create'); ?>',
             success: function (data) {
             console.log(data);
             }
             });
             }, */

        });

        $.validator.addMethod("unique", function(value, element) {
    var parentForm = $(element).closest('form');
    var timeRepeated = 0;
    if (value != '') {
        $(parentForm.find(':text')).each(function () {
            if ($(this).val() === value) {
                timeRepeated++;
            }
        });
    }
    return timeRepeated === 1 || timeRepeated === 0;

}, "Duplicate");

        $(document).on('change', '#ToolTransferRequest_request_from', function (event) {
            $('.code').val("");
            $('.item_name').val("");
            $('.unit').val("");
            $('.duration').val("");
            $('.duration_type').val("");
            $('.quantity').val("");
        });
        $(document).on('change', '.type', function (event) {
            $(this).closest('tr').find('.code').val("");
            $(this).closest('tr').find('.item_name').val("");
            $(this).closest('tr').find('.unit').val("");
            $(this).closest('tr').find('.duration').val("");
            $(this).closest('tr').find('.duration_type').val("");
            $(this).closest('tr').find('.quantity').val("");
        });
//        $(document).on('click', '.code', function (event) {
//            var code = $(this).closest('tr').find('.find').attr('id');
//            var type = $(this).closest('tr').find('.type').val();
//            getdetails(code,type);
//        });
        $(document).on('click', '.find', function (event) {
            var code = $(this).attr('id');
            var type = $(this).closest('tr').find('.type').val();
            getdetails(code,type);
        });
        function getdetails(code,type) {
            var id = 1;//$('#ToolTransferRequest_request_from option:selected').val();
            var code = code;
            var type = type;
            var page_type = '<?php echo $page; ?>';

            if(id) {
                $.ajax({
                    type: "GET",
                    data: {id: id, code: code,type:type, page_type:page_type},
                    url: '<?php echo Yii::app()->createUrl('Tools/toolsview'); ?>',
                    success: function (response)
                    {
                        $("#addtools").html(response);
                        $("#addtools").css({"display": "block"});
                    }
                });
            } else {
                alert("Please Select Request from");
            }
        }
        ;

        $(document).on('change', '.type', function (event) {
            var id = $(this).val();
            var thiss = $(this);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/gettool'); ?>',
                success: function (data) {
                    //alert(data.tools);
                    // $(".code").html(data.tools);
                    thiss.closest('tr').find('select.code').html(data.tools);
                    thiss.closest('tr').find('select.unit').html(data.unittype);
                    //$(this).closest(".code").html(data.tools);
                    console.log(data);
                }
            });

        });
        $(document).on('change', '.code', function (event) {
           /* var id = $(this).val();
            var code = $(".code option:selected").text();//alert(code);
            var thiss = $(this);
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php //echo Yii::app()->createUrl('ToolTransferRequest/gettooldetails'); ?>',
                success: function (data) {
                    // alert(data.name);
                    // $(".newcode").val(code);
                    //$(".item_name").val(data.name);
                    thiss.closest('tr').find('.newcode').val(code);
                    thiss.closest('tr').find('.item_name').val(data.name);

                    console.log(data);
                }
            });*/
        });


       // $( document ).ready(function() {

    $("#tool_dis").click(function(){

            var id = $('#ToolTransferRequest_request_from option:selected').val();
            var page_type = '<?php echo $page; ?>';
            var code = 'find_1';
            var type = 0;

            if (id) {
                $.ajax({
                    type: "GET",
                    data: {id: id, code: code,type:type, page_type:page_type},
                    url: '<?php echo Yii::app()->createUrl('Tools/tooldisplay'); ?>',
                    success: function (response)
                    {
                        $("#addtools").html(response);
                        $("#addtools").css({"display": "block"});
                    }
                });
            } else {
                alert("Please Select Request from");
            }

    });


    //});

    });

</script>


<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    .find{
        cursor: pointer;
    }
</style>
<style>
    .fa {
    color: #555;
    font-size: 12px;
}
.fa:hover{color:#999;}
    .table .btn{margin:0px;}

    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    .find{
        cursor: pointer;
    }
    .itemadd{display:inline-block;border: 1px solid #eee;
    padding: 10px;
    box-shadow: 2px 3px 4px #eee;width:100%}
    .itemadd div{float:left;margin-right:30px;}
    select{width:200px;}
    .btnadd,.btnrst{margin-top:15px;}
    .itemadd h3{background:#eee;margin:-10px -10px 10px -10px;padding:7px;}
    table{border-collapse:collapse;}
    .number_field {
        width: 80px;
    }
</style>
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-md">

    </div>
</div>
