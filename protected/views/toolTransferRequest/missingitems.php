
<h1>Missing Items</h1><br/>

<div class="">



    <table  width="100%" border="1" class=" table table-bordered newtable">
                    <thead>
                        <tr>

                            <th>S.No.</th>
                            <th>Code</th>
                            <th>Item Name</th>
                            <th>From</th>
                            <th>Units</th>
                            <th>Duration</th>
                            <th>Duration Type</th>
                            <th>Quantity</th>
                            <th>Remarks</th>
                            <th>Status</th>
                            <th></th>

                        </tr>
                    </thead>

    <?php

        if(!empty($model)) {
//echo "<pre>";print_r($model);exit;
       foreach( $model as $key => $value ) {  ?>
        <tr class="worktr">


              <td><?php echo $key+1 ; ?></td>
                        <td><?php echo $value['code']; ?></td>
                        <td><?php echo $value['item_name']; ?></td>
                         <td><?php
                         $loc = LocationType::model()->findByPk($value['location']);
                         echo $loc['name']; ?></td>
                        <td><?php
                            $tblpx = Yii::app()->db->tablePrefix;
                            $data =  Yii::app()->db->createCommand("SELECT unitname FROM {$tblpx}unit WHERE id =".$value['unit']."")->queryRow();

                        echo $data['unitname']; ?>
                        </td>
                        <td><?php echo $value['duration_in_days']; ?></td>
                        <td><?php
                        $tblpx = Yii::app()->db->tablePrefix;
                        $du_data =  Yii::app()->db->createCommand("SELECT caption FROM {$tblpx}status WHERE sid =".$value['duration_type']." AND status_type='duration_type'")->queryRow();

                        echo $du_data['caption']; ?>
                        </td>
                        <td><?php echo $value['qty']; ?></td>
                        <td><?= $value['item_remarks'] ?></td>
                        <td><?= $value['item_current_status'] ?></td>
                        <td><a href="<?php  echo Yii::app()->createUrl('ToolTransferRequest/MissingItemsView&id='.$value['id']); ?>" style="color:#5d5d5d;"><i class="fa fa-pencil" aria-hidden="true"></i></a> </td>


        </tr>



      <?php } } else{ ?>
      <tr> <td colspan="11">No Result found </td></tr>
      <?php } ?>
    </table>



</div>
