<?php
Yii::app()->clientScript->registerScript('checkBoxSelect', "
$(document).ready(function(){
 $('#tool-transfer-items-form').validate({
            rules: {
            'paid_amount[]': {
                    required: true,
                    number: true
                },
             },
            messages: {
            'paid_amount[]': {
                    required: 'Required',
                    number: 'Paid amount must be an integer',
                },
            },
 });
});
");
$tool_details = Tools::model()->find(array('condition' => 'vendor_request_id =' . $id));

?>

<div class="clearfix">
    <div class="pull-left">
        <h1>Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1>
    </div>
    <?php if($model->request_status == 6) { ?>
    <div class="pull-right">
        <button class="btn btn-primary acknowledge">ACKNOWLEDGE</button>
    </div>
    <?php } ?>
</div>

<div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Vendor: </span><b><?php echo (isset($model->request_to) ? $model->requestTo->name : "---") ?></b></div>
    <div class="rightspace"><span>Request Status: </span><b><?php echo(isset($model->request_status) ? $model->requestStatus->caption : "---")?></b></div>
    <div class="rightspace"><span>Transfer To: </span><b><?php echo  $model->transfer_to;?></b></div>
</div><div class="table-responsive"><br/>
<?php
/*$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
       // 'id',
        'request_no',
        'request_date',
        //'request_to',
        array(
            'name' => 'request_to',
            'type' => 'raw',
            'value' => $model->requestTo->name,
        ),
//        array(
//            'name' => 'request_from',
//            'type' => 'raw',
//            'value' => $model->requestFrom->name,
//        ),
        array(
            'name' => 'request_status',
            'type' => 'raw',
            'value' => $model->requestStatus->caption,
        ),
        //'request_from',
        'transfer_to',
    //'paid_amount',
    //'rating',
    //'remarks',
    //'created_by',
    //'created_date',
    //'updated_by',
    //'updated_date',
    ),
));*/

   $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Transfer Requested'))
                        ->queryRow();

    $treq = $qryres1['sid'];


//$visible = (($model->request_status == 6) && (Yii::app()->user->role ==9)) ? true : false;
$visible = (($model->request_status == $treq) && (Yii::app()->user->role ==9)) ? true : false;
?>


    <h2><span>ToolTransferRequest Items</span></h2>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'tool-transfer-items-form',
    'action' => Yii::app()->createUrl('//ToolTransferRequest/updateitem&id=' . $model->id),
)); ?>

    <table >
     <?php foreach ($items as $item){
       $unit = Unit::model()->findByPk($item->unit);
         ?>
        <tr>

            <td><b>Code : <?php echo isset($tool_details->ref_no)?$tool_details->ref_no:$item->code;;?></b></td>
            <td><b>Item : <?php echo $item->item_name;?></b></td>
            <td><b>Unit : <?php echo $unit->unitname;?></b></td>
            <!--<td><b>Serial No : <?php echo $item->serial_no;?></b></td>-->
            <td><b>Quantity : <?php echo $item->qty;?></b></td>
            <td><b>Status : <?php echo $item->itemStatus->caption;?></b></td>
        </tr>
        <tr>
            <td>Paid Amount : <input type="text" name="paid_amount[]" class="inputs form-control" value="<?php echo $item->paid_amount; ?>"/></td>
            <td>Rating :
            <select name="rating[]" class="inputs form-control">
			<option value=""></option>
            <option value="1" <?php echo ($item->rating ==1)?'selected':''; ?>>1</option>
            <option value="2" <?php echo ($item->rating ==2)?'selected':''; ?>>2</option>
             <option value="3" <?php echo ($item->rating ==3)?'selected':''; ?>>3</option>
              <option value="4" <?php echo ($item->rating ==4)?'selected':''; ?>>4</option>
               <option value="5" <?php echo ($item->rating ==5)?'selected':''; ?>>5</option>
            </select>
            <!--<input type="text" name="rating[]" class="inputs form-control" value="<?php // echo $item->rating; ?>" /> -->

            </td>
            <td>Remarks : <input type="text" name="remarks[]" class="inputs form-control" value="<?php echo $item->remarks; ?>"/></td>
            <td>Service Report : <input type="text" name="service_report[]" class="inputs form-control" value="<?php echo $item->service_report; ?>"/></td>
            <td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" value="<?php echo $item->id; ?>"></td>
        </tr>
        <?php
     } ?>
        <input type="hidden" class="inputs form-control" name="transferid" value="<?php echo $model->id; ?>">
        <tr><td><?php
         $qryres2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Transfer to vendor'))
                        ->queryRow();




        //if($model->request_status == 10){
    if($model->request_status == $qryres2['sid']){

        echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue itemsave' ,'name'=> 'ToolTransferRequest'));
        } ?></td></tr>
    </table>
    <?php $this->endWidget(); ?>
</div>

<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }

</style>
<style>
    .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;}
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    .grid-view table tbody tr{background: #eee;}
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}

 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}

  .btn-primary:hover{
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
/*.table .btn{margin:0px;}*/

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
</style>
