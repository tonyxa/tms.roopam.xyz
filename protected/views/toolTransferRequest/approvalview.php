

<div class="clearfix">
    <div class="pull-left">
        <h1>Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1>
    </div>
    <?php if($model->request_status == 6) { ?>
    <div class="pull-right">
        <button class="btn btn-primary acknowledge">ACKNOWLEDGE</button>
    </div>
    <?php } ?>
</div>
<?php if($model->transfer_to=="stock"){?>
    <div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Request To: </span><b><?php echo (isset($model->transfer_to) ? $model->transfer_to : "---") ?></b></div>
    <div class="rightspace"><span>Supervisor: </span><b><?php echo(isset($model->request_from) ? $model->Transfer_from->name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>
<?php }else {?>
<div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Site: </span><b><?php echo (isset($model->request_to) ? $model->requestTo->name : "---") ?></b></div>
    <div class="rightspace"><span>Supervisor: </span><b><?php echo(isset($model->request_owner_id) ? $model->Requestowner->first_name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>
<?php } ?>

    <?php



    $qryres1 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
            ->queryRow();

    $treq = $qryres1['sid'];

     $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Acknowledged'))
            ->queryRow();

    $treq1 = $qryres2['sid'];

    $visible = ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) ? true : false;
//$visible = (($model->request_status == 6) && (Yii::app()->user->role ==9)) ? true : false;
    $visible1 = ($page == 'site') ? true : false;
    ?>



		<br><br/>
        <h2>Requested Items</h2> <br/>
        <div class="row" style="margin-bottom: 10px;">
            <?php if($model->request_status != 8){
               ?>
    <div class="col-md-2" style="font-size: 100% !important;">

        <select class="form-control"  id="selectitem" style="margin-top: 5px;">
             <option value="">Please choose</option>
              <option value="Accepted">Accepted</option>
              <option value="Defective">Defective</option>
              <option value="Missing">Missing</option>
              <option value="Not requested">Not Requested</option>
        </select>
  </div>
        <!-- <div class="col-md-2" style="font-size: 100% !important;">
Accepted : <input type="text" name="accepted" id="accepted" class="checking">
</div>
<div class="col-md-2" style="font-size: 100% !important;">
Defective : <input type="text" name="defective" id="defective" class="checking">
</div>
<div class="col-md-2" style="font-size: 100% !important;">
Missing : <input type="text" name="missing" id="missing" class="checking">
</div>
<div class="col-md-2" style="font-size: 100% !important;">
Not Requested : <input type="text" name="not_requested" id="not_requested" class="checking">
</div> -->
      <?php } ?>

        <div class="col-md-3 text-left" style="font-size: 100% !important;">

        <button class="btn blue assignstatus" type="submit">Assign Status</button>
        </div>
    </div>
    
       <div class="itemtable">
        <?php 
        
       
        $this->widget('zii.widgets.CListView', array(
	    'dataProvider'=>$items->search($id),'template' => '<table class="table table-bordered">{items}</table>',
	    'itemView'=>'_approvalview','viewData' => array( 'trns_items' => $newmodel,'model' => $model),
        )); ?>

    </div>

    </div>

    <button class="btn blue submit">Submit</button>
  </div>
  <input type="hidden" id="request_id" value="<?= $id ?>">
  <input type="hidden" name="txtRequestTo" id="txtRequestTo" value="<?php echo (isset($model->request_to) ? $model->request_to : ""); ?>"/>
<script type="text/javascript">
/* var serial_no = '<?php // echo $qryres['serialno_status']; ?>' */;

$( document ).ready(function() {
$(document).on('click', '.serialnumber_checked', function (event) {
    var data_id = $(this).attr("data-id");
    var item_id = $(this).val();
    if($(this).is(':checked')) {
        $("#item_id"+data_id).val(item_id);
    } else {
        $("#item_id"+data_id).val("");
    }
});

$(document).on('click','#allcb',function() {
    var checked=this.checked;

    if(checked == true){

        $(".checkBoxClass").each(function() {

            $(this).parent('span').addClass("checked");
            $(this).attr('checked','checked');

        });
    }else{
       $(".checkBoxClass").each(function() {

            $(this).parent('span').removeClass("checked");
            $(this).attr('checked','');

        });

    }
});



 $(document).on('click', '.assignstatus', function (event) {
        var type = $( "#selectitem option:selected" ).val();
        var id   = $('#request_id').val();

        /* if(serial_no == 'Y'){ */
        var selected        = [];
        var item_serial     = [];
        var item_nonserial  = [];
        var accepted        = [];
        var defective       = [];
        var missing         = [];
        var not_requested   = [];
        var serialno_status = [];
        var request_item    = [];
        var j = 0;
        var k = 0;
        $('.transfer_info').each(function(){
            var info_id     = $(this).val();
            var serial_no   = $(this).attr("data-id");
            if(info_id != "") {
                var status  = $("#serialno_status"+serial_no).val();
                serialno_status.push($("#serialno_status"+serial_no).val());
                selected.push(info_id);
                if(status == "Y") {
                    var i = 0;
                    item_serial[j] = [];
                    $('input.serialnumber_checked'+serial_no+':checked').each(function(){
                        item_serial[j].push($(this).val());
                        i = i + 1;
                    });
                    j = j + 1;
                    //alert(item_serial[0]);
                }
                if(status == "N") {
                    var l               = 0;
                    item_nonserial[k]   = [];
                    request_item[k]     = [];
                    accepted[k]         = [];
                    defective[k]        = [];
                    missing[k]          = [];
                    not_requested[k]    = [];
                    $('input.nonserialnumber_checked'+serial_no+':checked').each(function(){
                        var curr_id = $(this).attr("data-id");
                        curr_id         = curr_id.replace("-", ",");
                        var array       = curr_id.split(',');
                        var data_id     = array[0];
                        var row_id      = array[1];
                        item_nonserial[k].push($("#item_non"+data_id+""+row_id).val());
                        request_item[k].push($("#request_item"+data_id+""+row_id).val());
                        accepted[k].push($("#accepted"+data_id+""+row_id).val());
                        defective[k].push($("#defective"+data_id+""+row_id).val());
                        missing[k].push($("#missing"+data_id+""+row_id).val());
                        not_requested[k].push($("#not_requested"+data_id+""+row_id).val());
                        l= l + 1;
                    });
                    k = k  + 1;
                }
            }
        });

        /*$('.worktr input:checked').each(function(){
            var data_id = $(this).attr("data-id");
            var status  = $("#serialno_status"+data_id).val();
            serialno_status.push($("#serialno_status"+data_id).val());
            selected.push($(this).val());
            if(status == "Y") {
                item_serial.push($("#item_id"+data_id).val());
            } else if(status = "N") {
                item_nonserial.push($(this).val());
                accepted.push($("#accepted"+data_id).val());
                defective.push($("#defective"+data_id).val());
                missing.push($("#missing"+data_id).val());
                not_requested.push($("#not_requested"+data_id).val());
            }
        });*/
        var serialnumber_item       = 0;
        $('input.itemcheck:checked').each(function(){
            serialnumber_item = serialnumber_item + 1;
        });
        var nonserialnumber_item    = 0;
        $('input.nonitemcheck:checked').each(function(){
            nonserialnumber_item = nonserialnumber_item + 1;
        });
        if((type != '' && serialnumber_item > 0) || nonserialnumber_item > 0)
        //if(selected != '')
        {
            $.ajax({
                method: "POST",
                url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/AssignStatus&id='.$id); ?>' ,
                data: { id : id, type: type, items:selected, item_nonserial:item_nonserial, item_serial:item_serial, accepted:accepted, defective:defective, missing:missing, not_requested:not_requested, serialno_status:serialno_status, request_item:request_item},
                success: function (data) {
                    //alert(data)
                    alert("Success");
                    //alert('Succes');
                    location.reload();


                }
            });
        } else {
            if(serialnumber_item == 0) {
                if(type == ''){
                    alert('Please choose Type');
                }
                if( type != '' && serialnumber_item == 0){
                     alert('Please choose items');
                }
            }
            if(nonserialnumber_item == 0) {
                alert('Please enter quantity');
            }

        }
     /* }  else{

        var accepted = $("#accepted").val();
        var defective = $("#defective").val();
        var missing = $("#missing").val();
        var not_requested = $("#not_requested").val();
        var selected = [];

        $('.worktr input:checked').each(function(){
            selected.push($(this).val());
        });


        var accepted = [];

        $('.table #accepted').each(function(){
            accepted.push($(this).val());
        });

        var missing = [];

        $('.table #missing').each(function(){
            missing.push($(this).val());
        });

        var not_requested = [];

        $('.table #not_requested').each(function(){
            not_requested.push($(this).val());
        });

        var defective = [];

        $('.table #defective').each(function(){
            defective.push($(this).val());
        });




        if(selected !='')
        {
            $.ajax({
                method: "POST",
                url: '<?php  //echo Yii::app()->createUrl('ToolTransferRequest/AssignStatus&id='.$id); ?>' ,
                data: { id : id, type: type, items:selected,accepted:accepted, defective:defective,missing:missing,not_requested:not_requested },
                success: function (data) {
                  alert('Succes');
                   location.reload();
                }
            });
        }else {
            if(selected == ''){
                alert('Please choose items');
            }if(accepted !='' || defective !='' || missing !='' || not_requested !=''){
                 alert('Please choose items');
            }


        }


      } */

    });



 $(document).on('click', '.submit', function (event) {

            var id   = $('#request_id').val();
            var request_to = $("#txtRequestTo").val();
                $.ajax({
                        method: "GET",
                        url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/Approval&id='.$id); ?>' ,
                        data: { id : id, request_to: request_to },
                        success: function (data) {
                            if(data == 1 ){
                                alert('Add status for all items');
                            }else{
                               window.location = "<?php  echo Yii::app()->createUrl('ToolTransferRequest/site')?>";
                            }




                        }
                 });






   });









});

$(".checking").keyup(function(){
    var data_ids    = $(this).attr("data-id");
    data_ids        = data_ids.replace("-", ",");
    var array       = data_ids.split(',');
    var data_id     = array[0];
    var row_id      = array[1];

    var aloc_qty      = $("#aloc_qty"+data_id+""+row_id).val();
    var accepted      = $("#accepted"+data_id+""+row_id).val();
    var defective     = $("#defective"+data_id+""+row_id).val();
    var missing       = $("#missing"+data_id+""+row_id).val();
    var not_requested = $("#not_requested"+data_id+""+row_id).val();
    var total         = Number(accepted)+Number(defective)+Number(missing)+Number(not_requested);
    if(aloc_qty == total) {
        $("#validateQuantity"+data_id+""+row_id).text("");
        $("#item_non"+data_id+""+row_id).prop("disabled",false);
        $("#item_non"+data_id+""+row_id).click();

    } else {
        $("#item_non"+data_id+""+row_id).click();
        $("#item_non"+data_id+""+row_id).prop("disabled",true);
        if(aloc_qty < total){
            //$(this).val("");
            $("#validateQuantity"+data_id+""+row_id).text("You can enter maximum "+aloc_qty+" items.");
        } else {
            $("#validateQuantity"+data_id+""+row_id).text("");
        }
    }
    var ncount = 0;
    $('.nonserialnumber_checked'+data_id+':checked').each(function(){
        ncount = ncount + 1;
    });
    if(ncount > 0) {
        var transfer_id = $("#transfer_id"+data_id).val();
        $("#transfer_info"+data_id).val(transfer_id);
    } else {
        $("#transfer_info"+data_id).val("");
    }
});
$(".itemcheck").click(function(){
    var data_id         = $(this).attr("data-id");
    var selected_count  = 0;
    $('.serialnumber_checked'+data_id+':checked').each(function(){
        selected_count = selected_count + 1;
    });
    var transfer_id = $("#transfer_id"+data_id).val();
    if(selected_count > 0) {
        $("#transfer_info"+data_id).val(transfer_id);
    } else {
        $("#transfer_info"+data_id).val("");
    }
});
</script>











<style>
    div.checker input{
        opacity: 1;
    }

 .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;}
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    .grid-view table tbody tr{background: #eee;}
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}

 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}

  .btn-primary:hover{
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
.table .btn{margin:0px;}

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
</style>
