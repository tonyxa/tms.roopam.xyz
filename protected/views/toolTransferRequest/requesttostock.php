

<h1> ToolTransferRequest &nbsp;&nbsp;
    <span id="loading_icon" style="display:none">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
    </span>
</h1>
<div class="table-responsive">
	
    <?php
    
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            //'id',
            'request_no',
            'request_date',
            //'request_to',
            array(
                'name' => 'request_to',
                'type' => 'raw',
                'value' => isset($model->request_to) ? $model->requestTo->name : "---",
            ),
            array(
                'name' => 'request_from',
                'type' => 'raw',
                'value' => isset($model->request_from) ? $model->requestFrom->name : "---",
            ),
            'transfer_date',
            'received_date',
            array(
                'name' => 'request_owner_id',
                'type' => 'raw',
                'value' => isset($model->request_owner_id) ? $model->Requestowner->first_name : "---",
            ),
       
        ),
    ));

    $qryres1 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
            ->queryRow();

    $treq = $qryres1['sid'];
    
     $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Acknowledged'))
            ->queryRow();

    $treq1 = $qryres2['sid'];

    $visible = ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) ? true : false;
//$visible = (($model->request_status == 6) && (Yii::app()->user->role ==9)) ? true : false;
    $visible1 = ($page == 'site') ? true : false;
    ?>
    <hr/>
</div>




    <div>
     
    
		
		<h2><span>Dispatch Items</span></h2>   
        <?php if(isset($dispatch->comment)){?>

            <table  width="500" border="1" class=" table table-bordered newtable">
                    <thead>
                        <tr>
                           
                            <th>S.No.</th>
                            <th>Date</th>
                            <th>Mode Of Transport</th>
                            <th>Mode Name</th>
                            <th>Person Name</th>
                            <th>Comment</th>
                            
                    </thead>
        <tbody>

                    
        <?php 
       
         if(!empty($dispatch)) { 

             $key=0;   
        // foreach( $dispatch as $key => $value ) { print_r($key);die; ?>
       				<tr class="worktr">
                    

					    <td><?php echo $key+1 ; ?></td>
                        <td><?php echo $dispatch->date;?></td>
                        <td><?php echo $dispatch->modeOfTransport->caption; ?></td>
                        <td><?php echo $dispatch->mode_name;?></td>

                        <td><?php echo $dispatch->person_name; ?></td>
                        
                        </td>
                            <td><?php echo $dispatch->comment;?></td>                        
                            

				</tr>

           

			<?php  } else{  ?>
             <tr><td>Nothing found</td></tr>
            <?php } 
            ?>
             </tbody>   
		</table>
           <?php  }else{?>
		
		<table  width="500" border="1" class=" table table-bordered newtable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="allcb" name="allcb"/></th>
                            <th>S.No.</th>
                            <th>Code</th>
                            <th>Item Name</th>
                            <th>Units</th>
                            <th>Duration</th>
                            <th>Quantity</th>
                            <th>Status</th>
                        </tr>
                    </thead>
        <tbody>

                    
		<?php 
         if(!empty($newmodel)) {   
        foreach( $newmodel as $key => $value ) {  ?>
       				<tr class="worktr">
                    <td>
                      <input type="checkbox" id="cb1" name="cb1" value="<?= $value['id'] ?>" class="checkBoxClass"/>
                    </td>

					    <td><?php echo $key+1 ; ?></td>
                        <td><?php echo $value['code']; ?></td>
                        <td><?php echo $value['item_name']; ?></td>
                        <td><?php 
                            $tblpx = Yii::app()->db->tablePrefix;
                            $data =  Yii::app()->db->createCommand("SELECT unitname FROM {$tblpx}unit WHERE id =".$value['unit']."")->queryRow();
                        
                        echo $data['unitname']; ?>
                        </td>
                        <td><?php echo $value['duration_in_days']; ?></td>
                        
                        </td>
                            <td><?php echo $value['qty']; ?></td>                        
                            <td class="cur_status"  style="font-weight: bold;font-size: 13px;"  >
                              <?= ucfirst($value['item_current_status'] ) ?></td>

				</tr>

           

			<?php } } else{  ?>
             <tr><td>Nothing found</td></tr>
            <?php } 
            ?>
             </tbody>   
		</table>
            <?php } ?>
		 
   <?php if(!empty($newmodel)) { ?>
    <button class="btn blue submitreq">Submit</button>
    <?php } ?>
    
    
  </div>


  <input type="hidden" id="request_id" value="<?= $id ?>">

<script type="text/javascript">


$( document ).ready(function() {


$(document).on('click','#allcb',function() {
    var checked=this.checked;

    if(checked == true){

        $(".checkBoxClass").each(function() {

            $(this).parent('span').addClass("checked");
            $(this).attr('checked','checked');

        });
    }else{
       $(".checkBoxClass").each(function() {

            $(this).parent('span').removeClass("checked");
            $(this).attr('checked','');

        });

    }
});


$(document).on('click', '.submitreq', function (event) {

       
        var id   = $('#request_id').val();
        var selected = [];

        $('.worktr input:checked').each(function(){
            selected.push($(this).val());
        });

        if(selected != ''){



            $.ajax({
                        method: "POST",
                        url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/RequesttoStock&id='.$id); ?>' ,
                        data: { id : id , items : selected ,},
                        success: function (data) {

                          window.location = "<?php  echo Yii::app()->createUrl('toolTransferRequest&page=stock')?>";

                        }
            });
        }else{

            alert('Please check items');
        }
   });

});

</script>

<style>
    div.checker input{
        opacity: 1;
    }    
</style>
