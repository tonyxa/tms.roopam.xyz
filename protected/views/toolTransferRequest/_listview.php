
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

    <?php
    if($index == 0){
    ?>
    <thead>
        <tr>
            <th id="tool-transfer-items-grid_c0"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=item_name');?>">Item Name</a></th>
            <th id="tool-transfer-items-grid_c1"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=unit');?>">Unit</a></th>
            <th id="tool-transfer-items-grid_c2"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=duration_in_days');?>">Duration In Days</a></th>
            <th style="display:none" id="tool-transfer-items-grid_c3"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=tool_id');?>">Tool</a></th>
            <th id="tool-transfer-items-grid_c4">Tool Category</th>
            <th id="tool-transfer-items-grid_c5">Parent Category</th>
            <th id="tool-transfer-items-grid_c6"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=qty');?>">Qty</a></th>
            <th id="tool-transfer-items-grid_c7"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=item_status');?>">Item Status</a></th>
            <th style="display:none" id="tool-transfer-items-grid_c3"><a class="sort-link" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/siteview&amp;id=867&amp;ToolTransferInfo_sort=tool_id');?>">Tool</a></th>
        </tr>
    </thead>
    <?php } ?>
    <tr style="background:#eee;">
        <td><?php echo $data['item_name'] ?></td>
        <td><?php echo isset($data['unit0']->unitname) ? $data->unit0->unitname : "---"; ?></td>
        <td><?php echo $data->getDuration($data->id); ?></td>
        <td style="display:none"><?php echo $data->tool_id; ?></td>
        <td><?php echo $data->getcategory($data->tool_id); ?></td>
        <?php
        $tool = Tools::model()->findByPk($data->tool_id);
        ?>
        <input type="hidden" name="serialno_status" id="serialno_status" value="<?php echo $tool->serialno_status; ?>">
        <td><?php echo $data->getcategoryparent($data->tool_id); ?></td>
        <td>
       <?php   $toolsItem=ToolTransferInfo::model()->findByAttributes(array('tool_id'=>$data->tool_id,'tool_transfer_id'=>$_GET['id']));?>

        
        <input type="hidden" name="requested_qty" id="requested_qty" value="<?php echo $toolsItem->qty; ?>"> <?php echo $toolsItem->qty; ?></td>
        <td><?php  if($data->item_status != 8 && $data->item_status != 29 && $model->request_status != 9 && Yii::app()->user->role != 10 && $model->request_status != 28 && $model->request_status != 27 ){
            if($model->ack_user_id==NULL){
            $st = '<button" class="pull-right rejectitem btn btn-xs" disabled data-val="'.$data->id.'">Reject</button>';
            $alc = '<input type="button" class="pull-right btn btn-xs btn_alloc" disabled data-backdrop="static" data-keyboard="false"  value="ALLOCATE" data-id="'.$data->id.'"/>';
            }
            else{
                $st = '<button" class="pull-right rejectitem btn btn-xs" data-val="'.$data->id.'">Reject</button>';
                $alc = '<input type="button" class="pull-right btn btn-xs btn_alloc" data-backdrop="static" data-keyboard="false"  value="ALLOCATE" data-id="'.$data->id.'"/>';
    
            }
        }else{
            $st = '';
            $alc = '';
        }
            echo (isset($data->item_status) ? $data->itemStatus->caption : "---").$alc.'&nbsp'.$st;
          ?>
        </td>
        <td style="display:none"><?php echo $data->id; ?></td>
    </tr>
    <?php

    ?>
    <?php
    $j = 0;
    if(!empty($trns_items))
    { 
    ?>
        <?php
        foreach($trns_items as $key => $value){
            $stst = Status::model()->findByPk($value['item_status']);
            if($data->id == $value['parent_info_id']){
            $tools = Tools::model()->findByPk($value['tool_id']);
            $location = LocationType::model()->findByPk($tools->location);
        ?>
        <tr class="worktr">
        <td><?php  echo $j+1 ; ?></td>
        <td><?php echo $value['code']; ?></td>
        <td><?php echo $value['item_name']; ?></td>

            <td>&nbsp;&nbsp;&nbsp;&nbsp; For : <b><?php echo $value['duration_in_days']; ?></b>
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        if(isset($value['duration_type']) && $value['duration_type']!=''){
        $du_data =  Yii::app()->db->createCommand("SELECT caption FROM {$tblpx}status WHERE sid =".$value['duration_type']." AND status_type='duration_type'")->queryRow();

        echo $du_data['caption'].'(s)';
        } ?></td>
        <td>From:  <b><?php echo (isset($location->name) ? $location->name : "---") //echo (isset($value->location) ? $value->location0->name : "---") ?></b></td>
        <td>
        <?php
        if($data->tool->serialno_status == 'N') {
            echo $value["qty"];
        }
        ?>
        </td>
        <td class="stock_status"><?php echo (isset($value['item_status'])) ? ($value['item_current_status'] != '') ? $value['item_current_status'] : $stst->caption : '';?></td>
        <td class="text-right">
            <!--<a class="fa fa-edit btn btn-xs"></a>-->
            <?php
            if($model->request_status == 8 || $model->request_status == 21){
                ?>
             <a class="fa fa-trash btn btn-xs" href="<?php echo Yii::app()->createUrl('ToolTransferRequest/deleteallocateditem&id='.$value->id); ?>"></a>
            <?php
            }
            ?>

        </td>
        </tr>
        <?php
         $j++;
            }
            
            } ?>
<?php } ?>

    <style>
        .table tbody tr.worktr td{border:none;}
        .table{border-collapse:collapse;}
    </style>
