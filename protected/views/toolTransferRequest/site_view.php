
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/themes/btheme2/assets/admin/layout3/scripts/autocomplete.js', CClientScript::POS_END); ?>
<?php

Yii::app()->clientScript->registerScript('myjquery', "

$(document).ready(function () {

 $(document).on('keyup', '.img_comp_class', function () {
	var this_id = $(this).attr('id');
            $('.ui-helper-hidden-accessible').hide(); event.preventDefault();
		 $.ajax({
			type: 'GET',
			dataType: 'text',
			url:'".Yii::app()->createUrl("ToolTransferRequest/getitems")."',
			success:function(data){
			// alert(data);
			console.log(data);
			var arrResponse = JSON.parse(data);
				test(arrResponse,this_id);

			},
		 });

	 function test(data,this_id){
		var items = data;
		console.log(items);

		  $('.img_comp_class').autocomplete({

			  mustMatch: true,
			  source: items,
			  autoFocus: false,
			  change: function (event, ui) {
				  if (!ui.item) {
					  $('.img_comp_class').val('');
				  }
			  },
                           focus: function (event, ui) {
                          $('.ui-helper-hidden-accessible').hide(); event.preventDefault();
                          },response: function(event, ui) {
                                if (!ui.item) {
                                    $('#message').text('No results found');
                                } else {
                                    $('#message').empty();
                                }
                            },
			  select: function(event,ui) {
				  event.preventDefault();
				  if(ui.item.label !=''){
				  $('#'+this_id+'').val(ui.item.label);
				  var id = ui.item.value;
				  $('#item_id').val(id);

				  $.ajax({
                        type : 'GET',
                        data: {item_id: id},
                        url: '". $this->createUrl('Tools/getsitesbyitemid')."',
                        success: function (data) {
				if(data){
                                    $('#ToolTransferRequest_request_to').empty();
                                    $('#ToolTransferRequest_request_to').append(data);
                                }
                        }
					});$('#message').empty();
				}else{
					 $('#'+this_id+'').val('');
			    }

			  }
		  });
		}

});

  });
      $('#item_name_1').focusout(function() {
   if($('#message').text().length > 0){
    $(this).val('');
    $('#item_id').val('');
   }
   });
 ");
?>
<?php
$page = 'site';
Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedItems(){
var itemids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            itemids[i]= $(this).val();
          });
       return itemids;
   }

 $('#selected_tasks_all').click(function(){
                if(this.checked){
                $('div.checker span').addClass('checked');
                }else{
                $('div.checker span').removeClass('checked');
                }
                return true;
                });


function getUnSelectedItems(){
var uncheckeditemids = [];
 $(\"input[name='selected_tasks[]']:not(:checked)\").each(function(i) {
            uncheckeditemids[i]= $(this).val();
          });
       return uncheckeditemids;
   }
$(document).ready(function(){
$(\".approve\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;

    $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
        selected_tasks = 1;

    });
    if(selected_tasks==0)
        {
            alert(\"Please select task(s)\");
            return false;
        }
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{ids:getSelectedItems(),unchecked:getUnSelectedItems(),remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/Appovestatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});


$(\".acknowledge\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/acknowldgestatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});


$(\".reject\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/rejectstatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});

$(\".rejectitem\").click(function (event) {
        $('#loading_icon').show();
        var infoid = $(this).attr('data-val');
		$.ajax({
			type: \"POST\",
                        data :{info:infoid},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/rejectitem') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});


$(\".finalapprove\").click(function (event) {

            $('#loading_icon').show();
            var id = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/Finalapproval') . "',
			success: function (response)
			{
                           location.reload();
			}
		});
});

$(\".finalreject\").click(function (event) {

            $('#loading_icon').show();
            var id = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/finalreject') . "',
			success: function (response)
			{
                           location.reload();
			}
		});
});
});
$('.btn_alloc1').click(function(){
   
    var chked_vals = $(\"input[name='selectedIds[]']:checkbox:checked\").map(function(){
      return $(this).val();
    }).get();
    // var chked_reqqty = $(\"input[name='item_quantity[]'].val()!= ''\").map(function(){
    //   return $(this).val();
    // }).get();

    var chked_reqqty = [];

    $('input[name=\'item_quantity[]\']').each(function() {
        var value = $(this).val();
        if (value) {
            chked_reqqty.push(value);
        }
    });

    if($('#item_name_1').val() == ''){
        alert(\"Please select item\");
        return false;
    }
    else if($('#ToolTransferRequest_request_to').val() == ''){
        alert(\"Please select site\");
        return false;
    }
    else if($('#duration_1').val() == ''){
        alert(\"Enter duration\");
        return false;
    }
    else if($('#duration_type_1').val() == ''){
        alert(\"Please select duration type\");
        return false;
    }
    else if(chked_vals == '' ){
        alert('Please select items');
        return false;
    }else{
       
        
        var duration_type=$('#duration_type').val();


        var requested_id = $('#requested_id').val();
        var site = $('#txtRequestTo').val();
        var duration_days=$('#duration').val();
        var duration = $('#duration_1').val();

        
        var diif_dur=duration_days-duration;
        if(diif_dur<0  ){

            alert('Invalid duration');
            return false;

        }
        var qty=$('#item_quantity').val();
        

    
        var dur_type = $('#duration_type_1').val();
        var iteminfoid = $('#infoid').val();
        if(chked_vals.length > 0){
            $.ajax({
                type: 'POST',
                data: {chked_vals:chked_vals,request_id:requested_id,site:site,duration:duration,duration_type:dur_type,iteminfoid:iteminfoid,chked_reqqty:chked_reqqty},
                url: '". Yii::app()->createUrl('ToolTransferRequest/siteviewallocate&id='.$model->id)."',
                success: function (response)
                {
                    location.reload();
                }
            });
        }
    }
})
");
?>

<div class="clearfix">
    <div class="pull-left">
        <h1>Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1>
    </div>
    <?php if($model->request_status == 6 && Yii::app()->user->role!=10) { ?>
    <div class="pull-right">
        <button class="btn btn-primary acknowledge">ACKNOWLEDGE</button>
    </div>
    <?php } ?>
</div>
<?php if($model->transfer_to=="stock"){?>
    <div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Request To: </span><b><?php echo (isset($model->transfer_to) ? $model->transfer_to : "---") ?></b></div>
    <div class="rightspace"><span>Supervisor: </span><b><?php echo(isset($model->request_from) ? $model->Transfer_from->name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>
<?php }else {?>
<div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Site: </span><b><?php echo (isset($model->request_to) ? $model->requestTo->name : "---") ?></b></div>
    <div class="rightspace"><span>Supervisor: </span><b><?php echo(isset($model->request_owner_id) ? $model->Requestowner->first_name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>
<?php } ?>
<input type="hidden" name="txtRequestTo" id="txtRequestTo" value="<?php echo (isset($model->request_to) ? $model->request_to : ""); ?>"/>
<div class="table-responsive">

    <?php


    $qryres1 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
            ->queryRow();

    $treq = $qryres1['sid'];

     $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Acknowledged'))
            ->queryRow();

    $treq1 = $qryres2['sid'];

    $visible = ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) ? true : false;
//$visible = (($model->request_status == 6) && (Yii::app()->user->role ==9)) ? true : false;
    $visible1 = ($page == 'site') ? true : false;
    ?>
    <br>
    <?php if($model->request_to==NULL){?>

        <h2>Requested Items</h2>

       <div class="itemtable">

        <?php
        $tool_id=ToolTransferItems::model()->find('tool_transfer_id='.$id);
        $tool=Tools::model()->findByPk($tool_id->tool_id);
        if($tool->serialno_status=='Y'){
        $tool_allocated=ToolTransferItems::model()->find('tool_id='.$tool_id->tool_id.' and item_status=29 and return_qty IS NULL');
        }
        else{
            $tool_allocated=ToolTransferItems::model()->find('tool_id='.$tool_id->tool_id.' and item_status=9 ');
        }
        $tool_info_id=ToolTransferInfo::model()->find('tool_transfer_id='.$tool_allocated->tool_transfer_id);
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'tool-transfer-items-grid',
            'dataProvider' => $items->search($tool_info_id->tool_transfer_id),
            'itemsCssClass' => 'table table-bordered',
            // 'filter' => $items,
            'columns' => array(
               // 'tool_id',
                // 'tool_transfer_id',
                //'code',
                'item_name',
                // 'unit',
                array(
                    'name' => 'unit',
                    'type' => 'raw',
                    'value' => 'isset($data->unit0->unitname) ? $data->unit0->unitname : "---"',
                ),
                // 'serial_no',
                //'duration_in_days',
                array(
                    'name' => 'duration_in_days',
                    'type' => 'raw',
                    'value' => '$data->getDuration($data->id)',
                    'visible' => $visible1,
                ),
                array(
                    'name' => 'tool_id',
                    'type' => 'raw',
                    'headerHtmlOptions' => array('style' => 'display:none'),
                    'htmlOptions' => array('style' => 'display:none', 'class'=>'toolid'),
                ),
                 array(
                    'header' => 'Tool Category',
                    'type' => 'raw',
                    'value' => '$data->getcategory($data->tool_id)',

                ),
                array(
                    'header' => 'Parent Category',
                    'type' => 'raw',
                    'value' => '$data->getcategoryparent($data->tool_id)',

                ),
                'qty',
                // array(
                //     'name' => 'item_status',
                //     'type' => 'raw',
                //     'value' => function($data){
                //     if($data->item_status != 8 && $data->item_status != 29 && Yii::app()->user->role != 10){
                //         $st = '<button" class="pull-right rejectitem btn btn-xs" data-val="'.$data->id.'">Reject</button>';
                //         $alc = '<input type="button" class="pull-right btn btn-xs btn_alloc" value="ALLOCATE"/>';
                //     }else{
                //         $st = '';
                //         $alc = '';
                //     }
                //         return (isset($data->item_status) ? $data->itemStatus->caption : "---").$alc.'&nbsp'.$st;
                //     },
                //     'htmlOptions'=>array('class'=>'allocbtn'),
                // ),

            ),
        ));
     }else{ ?>
           <?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$items->search($id),'template' => '<table class="table table-bordered">{items}</table>',
	'itemView'=>'_listview','viewData' => array( 'trns_items' => $trns_items,'model' => $model),
    ));
    } ?>
    </div>








    </div>











<?php
if(!empty($newmodel)) {
?>

<?php
?>

<?php
if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {

?>
<?php } ?>

<?php
} else {
?>
<?php
if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {

?>
<?php } ?>
<?php
    }

?>
<br>
<?php 


?>
<?php if($model->request_status != 6) {

 $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/siteview&id='.$id.''),

    ));
?>
<input type="hidden" id="check_uniqueallocation" value="<?php echo ($model->isNewRecord) ? 0 : $model->id; ?>">
        <div class="row">
			<input type="hidden" name="request_id" value="<?php echo $id; ?>" >
        </div>
<div class="">
    <?php
    if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {
?>
    <textarea  class="form-control" rows="2" cols="50" placeholder="Remarks" name="remark"></textarea>
<?php    }
if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1)) {

        ?>

      <input type="submit" value="APPROVE" name="Submit" class="btn btn-warning"/>
    <button class="btn btn-red reject">REJECT</button> <?php
    } else {
        echo "Comments :- ".$model->staff_remarks;
    }
    ?>


</div>
 <?php $this->endWidget(); ?>
<?php  } ?>
<style>
    .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;}
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    .grid-view table tbody tr{background: #eee;}
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}

 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}

  .btn-primary:hover{
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
/*.table .btn{margin:0px;}*/

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
</style>
<script>
//    var status = '<?php echo $model->request_status; ?>';
//    if(status == 6 || status == 21){
//var r = $('<input type="button" class="pull-right btn btn-xs btn_alloc" value="ALLOCATE"/>');
//}else{
//    var r = '';
//}
//$(".allocbtn").append(r);
var tool = '';
$(document).on('click', '.btn_alloc',function(){
    tool = $(this).closest('tr').find('td:nth-child(4)').text();
    var nme = $(this).closest('tr').find('td:nth-child(1)').text();
		var requested_qty = $(this).closest('tr').find('#requested_qty').val();
        if(requested_qty<=0){
            alert('Cannot allocate item');
            return false;
        }
    var serialno_status = $(this).closest('tr').find('#serialno_status').val();

    $('#infoid').val($(this).attr('data-id'));
     var req_id = '<?php echo $model->id; ?>';
     $('.modal').show();
     $('#item_name_1').val(nme);
     $('#item_id').val(tool);

    /*$.ajax({
        type: "GET",
//        data: {id: id, code: code,type:type , item_id:item_id },
        data:{req_id:req_id,itemid:tool},
        url: '<?php //echo Yii::app()->createUrl('Tools/getitemsbysite'); ?>',
        success: function (response)
        {
            $("#addtools").html(response);
            $("#addtools").css({"display": "block"});

            if ($(".allocbtn").find('input').length) {
                $(".allocbtn input").remove();
            }*/
//     $(".allocbtn").append(r);
          //  $(".allocbtn").append(r);
            $.ajax({
                type: "GET",
                data: {item_id:tool },
                url: '<?php echo Yii::app()->createUrl('Tools/getsitedatabyitemid'); ?>',
                success: function (response)
                {
                    if(response){
                        $('#ToolTransferRequest_request_to').empty();
                        $('#ToolTransferRequest_request_to').append(response);
                    }
                    var val = '';
                    var item = $('#item_id').val();
                    var req_id = '<?php echo $model->id; ?>';
                    var info = $('#infoid').val();   
                    $.ajax({
                        type: "GET",
                        data: {siteid:val,itemid:item,req_id:req_id,info:info,requested_qty:requested_qty},
                        url: '<?php echo Yii::app()->createUrl('Tools/getitemdetailsbysite'); ?>',
                        success: function (response)
                        {       
                            $("#tableresult").html('');
                            $("#tableresult").html(response);
                            var i =1;
                            if(serialno_status == 'N'){
                              
  														$("#tools-grid tbody").find('tr').each (function() {
  															var quantity = $(this).find('.allc_qty').text();
                                var item_quantity = $(this).find('.item_quantity').val();
                              if(i == 1){
                                if(quantity > requested_qty){
                                    
  															}else{
                                                                
                                $(this).find('td input[type=checkbox]').attr('disabled','true');
  															}
                              }else{
                                if(quantity > requested_qty){
  															}else{
                                $(this).find('td input[type=checkbox]').attr('disabled','true');
  															}
                              }
                                i ++;
  														});
                            }
                            var sum = 0;
                            $(".item_quantity").keyup(function(){
                              $("#tools-grid tbody").find('tr').each (function() {
                                var item_quantity = Number($(this).find('.item_quantity').val());
                                sum += item_quantity;
                              });
                              var val = Number($(this).val());
                            //   var quantity = $(this).closest('tr').find('.allc_qty').text();
                            var quantity=Number($('#quantity_indent').val());
       
                              var total = Number(sum);
                              if(quantity >=val){
                              }else{
                                $(this).val('');
                              }
                              sum = 0;
                            })


                        }
                    });
                }
            });



        /*}
                });*/
});



$(document).on('change', '#ToolTransferRequest_request_to',function(){
    var val = $(this).val();
    var item = $('#item_id').val();
    var req_id = '<?php echo $model->id; ?>';
    var info = $('#infoid').val();
    if(val > 0){
        $.ajax({
            type: "GET",
            data: {siteid:val,itemid:item,req_id:req_id,info:info},
            url: '<?php echo Yii::app()->createUrl('Tools/getitemdetailsbysite'); ?>',
            success: function (response)
            {
                $("#tableresult").html('');
                $("#tableresult").html(response);
            }
        });
    }
});
//$(document).ajaxComplete(function(){
//    if ($(".allocbtn").find('input').length) {
//            $(".allocbtn input").remove();
//    }
//     $(".allocbtn").append(r);
//})

$(document).on('change','#tools-grid tbody td input[type="checkbox"]',function(){
  var quantity = $(this).closest('tr').find('.item_quantity').val();
  var serialno_status = $(this).closest('tr').find('.serialno_status').val();
  if(serialno_status == 'N'){
    if(quantity == ''){
      $(this).prop('checked',false);
      $(this).attr('disabled',true);
    }else{
      $(this).prop('checked',true);
      $(this).attr('disabled',false);
    }
  }
})

$(document).on('keyup','#tools-grid tbody td .item_quantity',function(){
  var quantity = $(this).val();
  if(quantity !=''){
    var tool_id = $(this).closest('tr').find('input[type="checkbox"]').val();
    if($(this).closest('tr').find('input[type="checkbox"]').attr("disabled", true)){
        $(this).closest('tr').find('input[type="checkbox"]').attr('disabled',false);
        $(this).closest('tr').find('input[type="checkbox"]').prop('checked',true);
    }else{
        $(this).closest('tr').find('input[type="checkbox"]').attr('disabled',true);
        $(this).closest('tr').find('input[type="checkbox"]').prop('checked',false);
    }
  }else{
      $(this).closest('tr').find('input[type="checkbox"]').attr('disabled',true);
      $(this).closest('tr').find('input[type="checkbox"]').prop('checked',false);
  }
});


</script>
<?php if($model->transfer_to!="stock"){?>

<div id="addtools" class="modal" role="dialog">
        <div class="modal-dialog modal-lg itempop">
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <div class="col-md-3" style="display:inline-flex">
                    <label>Item &nbsp;&nbsp;&nbsp;</label>
                    <input type="hidden" name="item_id" id="item_id" class="form-control inputs form-control item_id"   value="" >
                    <input type = "text" name="item_name" id="item_name_1" class="form-control inputs img_comp_class form-control target item_name"   value="" >
                    <span id="message"></span>
                    <input type="hidden" name="requested_id" id="requested_id" value="<?php echo $model->id; ?>" />
                    
                </div>
                <!-- <div class="col-md-3" style="display:inline-flex;">
                    <span>Site &nbsp;&nbsp;&nbsp;</span>
                    <?php
                    /* if(Yii::app()->user->role == 1){
                    $Locations =  Yii::app()->db->createCommand("SELECT id,name FROM `tms_location_type`
                   WHERE active_status = '1' ")->queryAll();
                   } else{
                    $Locations =  Yii::app()->db->createCommand("SELECT id,name FROM `tms_location_assigned`
                   join tms_location_type on tms_location_type.id = `tms_location_assigned`.`site_id` WHERE active_status = '1' and `user_id`= ". Yii::app()->user->id)->queryAll();
                    }
                    ?>

                    <select id="ToolTransferRequest_request_to" name="ToolTransferRequest[request_to]" class="form-control">
                      <option value="">Please choose</option>
                    <?php  foreach($Locations as $data) { ?>
                      <option value="<?= $data['id'] ?>"><?=  $data['name'] ?></option>
                    <?php } */ ?>
                    </select>
                </div> -->
                <div class="col-md-5" style="display:inline-flex;">&nbsp;
                    <span>Duration &nbsp;&nbsp;&nbsp;</span>
                    <input type="text" name="duration" class="inputs form-control target duration" id="duration_1" style="margin:0px 5px;" />
                    <?php
                    $durationtype = CHtml::listData($duration_type, "sid", "caption");
                    echo CHtml::dropDownList('duration_type', '', $durationtype, array('class' => 'inputs form-control target duration_type', 'id' => 'duration_type_1', 'empty' => 'Please choose'));
                    ?>
                </div>
                <?php $duration_days=ToolTransferInfo::model()->findByAttributes(array('tool_transfer_id'=>$model->id));?>
                <input type="hidden" name="duration" id="duration" value="<?php echo $duration_days['duration_in_days'];?>" />
                <input type="hidden" name="duration_type" id="duration_type" value="<?php echo $duration_days['duration_type'];?>" />
                <input type="hidden" name="quantity_indent" id="quantity_indent" value="<?php echo $duration_days['qty'];?>" />


								<!-- <div class="col-md-3" style="display:inline-flex">
                    <label>Quantity &nbsp;&nbsp;&nbsp;</label>
                    <input type = "text" name="quantity" id="quantity" class="form-control inputs img_comp_class form-control target item_name"   value="" >
                    <span id="message"></span>
                </div> -->
                <input type="hidden" name="infoid" id="infoid" />
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button class="btn btn_alloc1 btn-sm">ALLOCATE</button>
                </div>
            </div>
            <button type="button" class="close topspace" data-dismiss="modal" onclick='$("#addtools").css({"display":"none"})'>&times;</button>
        </div>
        <div class="modal-body" id="tableresult">   </div>
    </div>
</div>
</div>
<?php }
?>
<style>
    .close{
        position:absolute;
        top:18px;
        right:18px;
    }
    #selectedIds_all{
      display:none;
    }
</style>
