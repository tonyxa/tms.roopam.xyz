<?php
/* @var $this ToolTransferRequestController */
/* @var $model ToolTransferRequest */
/*
  $this->breadcrumbs=array(
  'Tool Transfer Requests'=>array('index'),
  $model->id,
  );

  $this->menu=array(
  array('label'=>'List ToolTransferRequest', 'url'=>array('index')),
  array('label'=>'Create ToolTransferRequest', 'url'=>array('create')),
  array('label'=>'Update ToolTransferRequest', 'url'=>array('update', 'id'=>$model->id)),
  array('label'=>'Delete ToolTransferRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
  array('label'=>'Manage ToolTransferRequest', 'url'=>array('admin')),
  ); */
?>
<?php
Yii::app()->clientScript->registerScript('checkBoxSelect', "
function getSelectedItems(){
var itemids = [];
 $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
            itemids[i]= $(this).val();
          });
       return itemids;
   }

 $('#selected_tasks_all').click(function(){
                if(this.checked){
                $('div.checker span').addClass('checked');
                }else{
                $('div.checker span').removeClass('checked');
                }
                return true;
                });


function getUnSelectedItems(){
var uncheckeditemids = [];
 $(\"input[name='selected_tasks[]']:not(:checked)\").each(function(i) {
            uncheckeditemids[i]= $(this).val();
          });
       return uncheckeditemids;
   }
$(document).ready(function(){
$(\".approve\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;

    $(\"input[name='selected_tasks[]']:checked\").each(function(i) {
        selected_tasks = 1;

    });
    if(selected_tasks==0)
        {
            alert(\"Please select task(s)\");
            return false;
        }
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{ids:getSelectedItems(),unchecked:getUnSelectedItems(),remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/Appovestatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});


$(\".acknowledge\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/acknowldgestatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});


$(\".reject\").click(function (event) {
$('#loading_icon').show();
var selected_tasks = 0;
		var remarks = $('.remarks').val();
                var ack_user = '" . Yii::app()->user->id . "';
                var transferid = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{remarks:remarks,ack_user:ack_user,transferid:transferid},
			url:'" . Yii::app()->createUrl('ToolTransferItems/rejectstatus') . "',
			success: function (response)
			{
                            location.reload();
			}
		});
	});




$(\".finalapprove\").click(function (event) {

            $('#loading_icon').show();
            var id = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/Finalapproval') . "',
			success: function (response)
			{
                           location.reload();
			}
		});
});

$(\".finalreject\").click(function (event) {

            $('#loading_icon').show();
            var id = '" . $model->id . "';
		$.ajax({
			type: \"POST\",
                        data :{id:id},
			url:'" . Yii::app()->createUrl('ToolTransferRequest/finalreject') . "',
			success: function (response)
			{
                           location.reload();
			}
		});
});
});
");
?>
<div class="clearfix">
    <div class="pull-left">
        <h1>View Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1>
    </div>

</div>

<div class="rowspace">
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Site: </span><b><?php echo (isset($model->request_from) ? $model->requestFrom->name : "---") ?></b></div>
    <div class="rightspace"><span>Office Staff: </span><b><?php echo(isset($model->request_owner_id) ? $model->Requestowner->first_name : "---")?></b></div>
    <div class="rightspace"><span>Transfer Date: </span><b><?php echo (isset($model->transfer_date) ? $model->transfer_date:'Not Set')?></b></div>
    <div class="rightspace"><span>Received Date: </span><b><?php echo(isset($model->received_date) ? $model->received_date: 'Not Set')?></b></div>
</div>
    <?php


    $qryres1 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
            ->queryRow();

    $treq = $qryres1['sid'];

     $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Acknowledged'))
            ->queryRow();

    $treq1 = $qryres2['sid'];

    $visible = ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) ? true : false;
//$visible = (($model->request_status == 6) && (Yii::app()->user->role ==9)) ? true : false;
    $visible1 = ($page == 'site') ? true : false;
    ?>
    <br><br/>
        <h2>Requested Items</h2> <br/>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'tool-transfer-items-grid',
            'dataProvider' => $items->search($id),
            'itemsCssClass' => 'table table-bordered',
            // 'filter' => $items,
            'columns' => array(
                //'id',
                // 'tool_transfer_id',
                array(
                    'id' => 'selected_tasks',
                    'class' => 'CCheckBoxColumn',
                    'selectableRows' => '50',
                    'visible' => $visible,
                ),
                'code',
                'item_name',
                // 'unit',
                array(
                    'name' => 'unit',
                    'type' => 'raw',
                    'value' => 'isset($data->unit0->unitname) ? $data->unit0->unitname : "---"',
                ),
                // 'serial_no',
                //'duration_in_days',
                array(
                    'name' => 'duration_in_days',
                    'type' => 'raw',
                    'value' => '$data->getDuration($data->id)',
                    'visible' => $visible1,
                ),
                 array(
                    'header' => 'Tool Category',
                    'type' => 'raw',
                    'value' => '$data->getcategory($data->tool_id)',

                ),
                array(
                    'header' => 'Parent Category',
                    'type' => 'raw',
                    'value' => '$data->getcategoryparent($data->tool_id)',

                ),
                'qty',
                'return_qty',
                array(
                    'name' => 'item_status',
                    'type' => 'raw',
                    'value' => 'isset($data->item_status) ? $data->itemStatus->caption : "---"',
                ),

                // array(
                //     'name' => 'tool_status',
                //     'type' => 'raw',
                //     'value' => '(!empty($data->physical_condition))?$data->gettoolstatus($data->physical_condition):NULL ',
                //
                // ),

                array(
                    'name' => 'tool_status',
                    'type' => 'raw',
                    'value' => '$data->getitemsqty($data->id)',
                ),


            ),
        ));
        ?>
    </div>
    <h4><span><i>Remarks :</i></span></h4>
    <?php
    if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) {
        echo '<textarea cols="50" rows="3" class="remarks"></textarea><br>';
    }
    //if(($model->request_status == 6)  && (Yii::app()->user->role ==9)){
    if (($model->request_status == $treq) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) {
        ?>

         <input type="submit" value="ACKNOWLEDGE" name="Submit" class="btn btn-info acknowledge"/>
        <?php
    }
    //if(($model->request_status == 6)  && (Yii::app()->user->role ==9)){
    if ((($model->request_status == $treq) || ($model->request_status == $treq1)) && (Yii::app()->user->role == 9 || Yii::app()->user->role == 1) && ($page == 'site')) {
        ?>
        <input type="submit" value="APPROVE" name="Submit" class="btn btn-warning approve"/>
        <input type="submit" value="REJECT" name="Submit" class="btn btn-danger reject"/>
        <?php
    } else {
        echo $model->staff_remarks;
    }


    $qryres2 = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tms_status')
            ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Approved'))
            ->queryRow();

//if((Yii::app()->user->role == 1 || Yii::app()->user->role == 10) && $model->request_status == 7){
    if ((Yii::app()->user->role == 1 || Yii::app()->user->role == 10) && ($model->request_status == $qryres2['sid']) && ($page == 'site')) {
        ?>
        <br><br> <input type="submit" value="TRANSFER APPROVE" name="Submit" class="btn btn-info finalapprove"/>
        <input type="submit" value="TRANSFER REJECT" name="Submit" class="btn btn-danger finalreject"/>
    <?php }
        $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/scheduleddate&id=' . $model->id),
    )); ?>Scheduled Return Date :
            <?php echo CHtml::activeTextField($model, 'received_date', array('class'=>'form-control',"id" => "received_date",'readonly' => true,'style' => 'width:10%' )); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'received_date',
                'ifFormat' => '%Y-%m-%d',
            ));
            ?>
            <?php echo $form->error($model, 'received_date'); ?>


<br><br> <input type="submit" value="Submit" name="Submit" class="btn btn-info finalapprove"/>
        <?php $this->endWidget(); ?>
<style>
    div.checker input{
        opacity: 1;
    }

 .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;}
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    .grid-view table tbody tr{background: #eee;}
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}

 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}

  .btn-primary:hover{
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
.table .btn{margin:0px;}

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
</style>
