<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-transfer-request-form',
        'action' => Yii::app()->createUrl('//toolTransferRequest/singledispatch&id='.$id),
            // 'enableAjaxValidation' => true,
              'enableClientValidation' => true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), 
    ));

?>

<?php echo $form->hiddenField($model, 'request_id_fk', array('class' => 'form-control','value' => $id)); ?>
        <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                    <?php echo $form->labelEx($model, 'date'); ?>
                    <?php echo $form->textField($model, 'date', array('class' => 'form-control start_date',)); ?>
                    <?php echo $form->error($model, 'date'); ?>
               </div>
            </div>

            <div class="col-md-6"> 
                <div class="form-group">
                    <?php 
                    $options = CHtml::listData(Status::model()->findAll(array('condition' => 'status_type = "mode_of_transport"')),"sid","caption");
                    echo $form->labelEx($model, 'mode_of_transport'); ?>
                    <?php echo $form->dropDownList($model, 'mode_of_transport', $options, array('class' => 'form-control'));
 ?>
                    <?php echo $form->error($model, 'mode_of_transport'); ?>
                </div>
            </div>
        </div>
       
        <div class="row">
           
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'mode_name'); ?>
                    <?php echo $form->textField($model, 'mode_name', array('class' => 'form-control',)); ?>
                    <?php echo $form->error($model, 'mode_name'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'person_name'); ?>
                    <?php echo $form->textField($model, 'person_name', array('class' => 'form-control',)); ?>
                    <?php echo $form->error($model, 'person_name'); ?>
                </div>
            </div>

            
        </div>
        <div class="row">
           
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'comment'); ?>
                    <?php echo $form->textArea($model, 'comment', array('class' => 'form-control',)); ?>
                    <?php echo $form->error($model, 'comment'); ?>
                </div>
            </div>
            

            
        </div>
<br/>
        <div class="row save-btnHold">
			<div class="col-md-12 text-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class' => 'btn blue')); ?>
            </div>
            </div>

        <?php $this->endWidget(); ?>



<script type="text/javascript">

$( document ).ready(function() {

    $('.start_date').datepicker({
        dateFormat: 'yy-mm-dd'
        
    });$('.start_date').datepicker('setDate', 'today');
});
</script>


<style>
    .has-error {
        border-style: solid;
        border-color: #ff0000;
    }

    .error{
        color:#ff0000;
    }
    .find{
        cursor: pointer;
    }
</style>

