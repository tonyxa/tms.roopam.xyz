<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tool_transfer-item-form',
	 'enableAjaxValidation' => true,
       'clientOptions' => array(
           'validateOnSubmit' => true,
           'validateOnChange' => true,
           'validateOnType' => false,
           ),
)); ?>



	

	<div class="row">
       


    <div class="row">
       <div class="col-md-4">
            <label>Code</label>
            <?php echo $form->textField($model,'code' , array('class'=>'form-control','readonly'=> true)); ?>
            <?php echo $form->error($model,'code'); ?>
        </div>

        <div class="col-md-4">
        <label>Found Location</label>
        <select class="form-control " name="item_location" id="item_location"  >

							<option value="">Please choose Location</option>';
		 <?php 
		
		 foreach ($location as $location1) {

			  echo '<option value='.$location1['id'].'>'.$location1['name'].'</option>';
			  

		  }  ?>
		  
							</select>
        </div>

    </div>
    <div class="row">
       <div class="col-md-4">
             <label>Item Name</label>
              <?php echo $form->textField($model,'item_name' , array('class'=>'form-control','readonly'=> true)); ?>
              <?php echo $form->error($model,'item_name'); ?>
         
       </div>

       <div class="col-md-4 hiddenbox" style="display:none">
        <label>Physical Condition</label>
        <select class="form-control " name="condition" id="condition"  >

						
		 <?php 
		
		 foreach ($condition as $condition1) {

			  echo '<option value='.$condition1['sid'].'>'.$condition1['caption'].'</option>';
			  

		  }  ?>
		  
							</select>
        </div>
    </div>

     </div>

     <div class="row">
      <div class="col-md-4">
            <?php echo $form->labelEx($model,'qty'); ?>
            <?php echo $form->textField($model,'qty' , array('class'=>'form-control','readonly'=> true)); ?>
            <?php echo $form->error($model,'qty'); ?>
        </div>

        <div class="col-md-4 hiddenbox" style="display:none">
        <?php echo $form->labelEx($model,'req_date'); ?>
                  <?php echo CHtml::activeTextField($model, 'req_date', array('class'=>'form-control datepicker',"id" => "ToolTransferItems_req_date","value"=>date('Y-m-d') )); ?>
                  <?php
                //   $this->widget('application.extensions.calendar.SCalendar', array(
                //   'inputField' => 'ToolTransferItems_req_date',
                //   'ifFormat' => '%Y-%m-%d',
                //   ));
                  ?>
                  <?php echo $form->error($model,'req_date'); ?> 

        </div>

        </div>
  

    <div class="row">
      <div class="col-md-4">
            <?php echo $form->labelEx($model,'item_current_status'); ?>
            <?php echo $form->textField($model,'item_current_status' , array('class'=>'form-control','readonly'=> true)); ?>
            <?php echo $form->error($model,'item_current_status'); ?>
        </div>
    </div>

     <div class="row">
        <div class="col-md-4">
            <?php echo $form->labelEx($model,'item_remarks'); ?>
            <?php echo $form->textarea($model,'item_remarks' , array('class'=>'form-control',)); ?>
            <?php echo $form->error($model,'item_remarks'); ?>
        </div>

        

       

    </div>

    



<div class="row buttons ">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save' ,
         array('class'=>'btn blue submit' , 'style'=>'margin-left: 24px; margin-top: 20px;' )); ?>

	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">

$('#item_location').change(function(){
            var val = $(this).val();
           
                if(val != ''){
                    $('.hiddenbox').show();
                }else{
               
                    $('.hiddenbox').hide();
               
            }
        })



$(document).ready(function () {

    $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd',changeYear:true});
    
/* submit function */

          $("#tool_transfer-item-form").validate({
            rules: {
              
                'ToolTransferItems[item_remarks]': {
                    required: true

                },
                
                
            },
            messages: {

                'ToolTransferItems[item_remarks]': {
                    required: "Please add Remarks",
                },
                
                
            },


     
            submitHandler: function () {
      /*  alert('hi'); */
                var formData = JSON.stringify($("#tool_transfer-item-form").serializeArray());
               
               // alert(formData);
                $.ajax({
                    method: "POST",
                    data: {formdata: formdata},
                    datatype:'JSON',
                    url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/MissingItemsView'); ?>',
                    success: function (data) {
//alert(data);
                    }
                });
            }
        });
  


    });
    
</script>




<style type="text/css">

  .error{
    color: red;
  }
</style>


