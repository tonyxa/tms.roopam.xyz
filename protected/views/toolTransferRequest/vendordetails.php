<style>
    .btn:hover{color:#fff;}
    textarea.form-control{
        height:auto !important;
        max-width:50%;
        display:inline-block;
        vertical-align: middle;
        padding:5px 5px;
        margin-right:5px;
    }
    .btn-brown{background:#e29522;border:1px solid #e29522;color:#fff;}
    .btn-red{background:#ff0000bf;color:white;border:1px solid #ff0000bf;}
    .rowspace{padding: 10px 20px;font-size:13px;display: inline-flex;background:#eee;width:100%;margin-bottom: 15px;}  
    .rowspace span{color:grey;}
    .rightspace{margin-right:35px;}
    div.checker input{
        opacity: 1;
    }    
    h2{margin-bottom:0px;}
    .grid-view{
        margin-bottom: -20px;
        padding-bottom: 0px;
    }
    .modaltable{border: 1px solid #ddd;}
    td, caption {
    padding: 9px 10px 4px 5px;}
    .btn_alloc,.btn_alloc1{background: green;border:green;color:#fff;border-radius: 2px;padding:3px 10px 3px;}
    /* .grid-view table tbody tr{background: #eee;} */
    .stock_status{color:#27c127;text-transform: uppercase;font-size:11px;}
    .fa{color:#555;font-size: 12px;}
    .fa:hover{color:#999;}
  
 .btn-primary {
    color: #fff;
    background-color: #87ceebe6;
    border-color: #87ceebe6;
}   
    
  .btn-primary:hover{    
    color: #fff;
    background-color: #44aad4;
    border-color: #44aad4;
  }  
  .grid-view {
    padding: 0px !important;
}
.grid-view .table-bordered{border-bottom: none;margin-bottom: 1.4em;}
/*.table .btn{margin:0px;}*/

.ui-autocomplete {
    background: #f7f7f7;
    max-width: 380px;
    padding: 0;
    margin: 0;
}
ul#ui-id-3 {
    /* max-height: 320px; */
    overflow: auto;
}
ul{
	list-style-type: none;
}
.ui-autocomplete li {
    cursor: pointer;
    list-style: none;
    padding: 2px 8px;
}
.modal{
    z-index: 0;
}
.rejectitem{
    background: #e6353d;
    border: #e6353d;
    color: #fff;
    border-radius: 2px;
    padding: 3px 10px 3px;
}
</style>

<div class="clearfix">
    <div class="pull-left">
        <h1>Tool Transfer Request <span style="color:#e69545;">#<?php echo $model->request_no;?></span>
            <span id="loading_icon" style="display:none"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
            </span>
        </h1> 
    </div>
    <?php if($model->request_status == 6) { ?>
    <div class="pull-right">
        <button class="btn btn-primary acknowledge">ACKNOWLEDGE</button>
    </div>
    <?php } ?>
</div>

<div class="rowspace">   
    <div class="rightspace"><span>Request Date: </span><b><?php echo $model->request_date?></b></div>
    <div class="rightspace"><span>Vendor: </span><b><?php echo (isset($model->request_to) ? $model->requestTo->name : "---") ?></b></div>
    <div class="rightspace"><span>Request Status: </span><b><?php echo(isset($model->request_status) ? $model->requestStatus->caption : "---")?></b></div>
    <div class="rightspace"><span>Transfer To: </span><b><?php echo  $model->transfer_to;?></b></div>
</div>

<?php
/*$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
       // 'id',
        'request_no',
        'request_date',
        //'request_to',
        array(
            'name' => 'request_to',
            'type' => 'raw',
            'value' => $model->requestTo->name,
        ),
        
        //'request_from',
      //  'transfer_to',
    //'paid_amount',
    //'rating',
    //'remarks',
    //'created_by',
    //'created_date',
    //'updated_by',
    //'updated_date',
    ),
));
*/
?>
<div>
    <h2><span>ToolTransferRequest Items</span></h2>    
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'tool-transfer-items-grid',
        'itemsCssClass' => 'table table-bordered',
        'dataProvider' => $model1->search1($id),
        // 'filter' => $items,
        'columns' => array(
            
            'code',
            'item_name',
            /*array(
                    'name' => 'unit',
                    'type' => 'raw',
                    'value' => 'isset($data->unit0->unitname) ? $data->unit0->unitname : "---"',
                ),*/
             array(
                    'name' => 'duration_in_days',
                    'type' => 'raw',
                    'value' => '$data->getDuration($data->id)',
                ),
            'qty',
            'paid_amount',
            'rating',
            'remarks',
            'service_report',
            array(
                'name' => 'item_status',
                'type' => 'raw',
                'value' => 'isset($data->item_status) ? $data->itemStatus->caption : "---"',
            ),
        /* 'physical_condition',
         */
        /* array(
          'class'=>'CButtonColumn',
          ), */
        ),
    ));
    ?>
</div>
    