<style>
.inline_field{display: inline;padding-left: 19px;}
table{border-collapse:collapse;}
.grid-view{padding-top: 8px;}
</style>
<?php if($page == 'site') {
    $role = 10;
}else{
     $role = 9;
}
?>
<div class="clearfix">

    <?php if( isset(Yii::app()->user->role) and Yii::app()->user->role == 1 ){ ?>
        <div class="pull-right">
            <button class="btn blue assg">Assign Tools</button>

        </div>
    <?php } ?>

    <div class="pull-right">
        <?php if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2 || Yii::app()->user->role == $role) { ?>
            <!--<button data-toggle="modal" data-target="#addtools"  class="btn blue createtools">Add Transfer Requests</button>-->
            <?php
            echo CHtml::link('Add Transfer Requests', $this->createAbsoluteUrl('ToolTransferRequest/createsite'), array('class' => 'btn blue','style'=>'margin-right: 5px;'));
        }
        ?>
    </div>
    <h1>Manage Tool Transfer Requests To <?php echo $page; ?>
        <span id="loading_icon" style="display:none">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
        </span>
    </h1>

     <div id="msg"></div>


    <!--   new assign tools   -->

          <div class="assg_form" style="display: none;">
            <div class="row form-inline">
                <div class="form-group inline_field">
                    <?php
                      $Locations =  Yii::app()->db->createCommand("SELECT id,name FROM `tms_location_type`
                      WHERE active_status = '1' and location_type= 13 ")->queryAll();

                    ?>
                    <select id="site" name="site" class="form-control">
                      <option value="">Please select site </option>
                      <?php  foreach($Locations as $data) { ?>
                      <option value="<?= $data['id'] ?>"><?=  $data['name'] ?></option>
                      <?php } ?>

                    </select>

                </div>

                <div class="form-group inline_field " id="seleng">
                    <select id ="siteeng" name="siteeng" class="form-control">
                       <option value="">Please select</option>
                    </select>

                </div>

                <div class="form-group inline_field">
                    <input type="number" id="duration" name="duration" class="form-control" placeholder="Duration" min="1" style="width: 68px;">

                </div>

                <div class="form-group inline_field " >

                    <select id="dur_type" name="dur_type" class="form-control">
                      <option value="">Duration type</option>
                      <option value="15">Days</option>
                      <option value="16">Months</option>
                    </select>

                </div>


              <div class="form-group inline_field">
                  <span class="icon-eye icon-comn" id="tool_dis" title="view"></span>
              </div>

            <div class="inline_field">
                <button class="btn btn-sm btn-primary" id="sub">Submit</button>
            </div>



              </div>

          </div>


    <div id="addtools" class="modal" role="dialog">
        <div class="modal-dialog modal-md">

        </div>
    </div>


    <!--  end  -->





</div>
<?php
$res1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Transfer to vendor'))
                        ->queryRow();
             $id1 = $res1['sid'];

$res2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Received back to stock'))
                        ->queryRow();
             $id2 = $res2['sid'];

?>

<div class="table-responsive">
<?php if($page == 'vendor') {?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-transfer-request-grid',
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->sitesearch(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        'request_no',
       'request_date',
        //'request_to',
        //'request_from',
        //'transfer_to',
        array(
            'name' => 'request_from',
            'value' => 'isset($data->request_from) ? $data->requestFrom[\'name\'] : "--"',
        ),
        array(
            'name' => 'request_to',
           'value' => 'isset($data->request_to) ? $data->requestTo[\'name\'] : "--"',
        ),
        array(
            'name' => 'location',
           'value' => 'isset($data->location) ? $data->location0[\'name\'] : "--"',
        ),
        /*'paid_amount',
        'rating',
        'remarks',
         'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:80px;'),
            //'template' => '{update}{got_back}{delete}{view}',
            'template' => '{edit}{got_back}{view}',
            'buttons' => array(
                'edit' => array(

                    'label' => '',
                    'options' => array('class' => 'editAllocation icon-pencil icon-comn','title'=>'Edit'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/update",array("id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '($data->request_status == '.$id1.')'

                ),
                'got_back' => array(
                    'label' => 'Got Back',
                    'options' => array('class' => ''),
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/back.png',
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/vendorview",array("id"=>$data->id,))',
                    'visible'=> '($data->request_status == '.$id1.')'

                ),
                /*'delete' => array(
                    'visible'=> '($data->request_status == '.$id1.')'

                ),*/
                'view' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'view'),
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/eye.png',
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/vendordetails",array("id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '($data->request_status == '.$id2.')'

                ),
            ),
        ),
    ),
));
?>
<?php } else{

     $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Transfer Requested'))
                        ->queryRow();

    $treq = $qryres1['sid'];
    $qryres2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Approved'))
                        ->queryRow();

    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'tool-form',
            //'enableAjaxValidation'=>false,
            'action'=>Yii::app()->createUrl('//toolTransferRequest/site'),
        'method' => 'post',
    ));

    if(Yii::app()->user->role!=10){

        echo CHtml::submitButton('Create Bulk Dispatch',array('buttonType'=>'submit','name' => 'dispatch_create', 'onclick' => 'myfunction();','style'=>'','class' => 'btn btn-primary dispatchbut'));
    }

    ?>
    <input type="hidden" name="hidden" id="hidden_field_id"/>
    <?php

    $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-transfer-request-grid',
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ', 'selectedPageCssClass'=>'active', ),
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
     'ajaxUpdate'=>false,
    'dataProvider' => $model->sitesearch(),
    'filter' => $model,
    'columns' => array(
        array(
                'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => '100',
                'cssClassExpression'=>'$data->requestStatus[\'caption\']=="Approved" ? "" : "statuscondition"',
                'visible'=>(Yii::app()->user->role!=10)?true:false,
            ),
         array(
				'headerHtmlOptions' => array('style' => 'display:none'),
				'htmlOptions' => array('style' => 'display:none', 'class'=>'customer[]'),
				'filterHtmlOptions'=>array('style' => 'display:none'),
				'name'=>'id',
				'type'=>'raw',
				'value'=>'CHtml::textField("$data->id",$data->id,array("style"=>"width:50px;","class" => "hdncustomer"))',
		),
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        'request_no',
       'request_date',
        //'request_to',
        //'request_from',
        //'transfer_to',
//        array(
//            'name' => 'request_from',
//            'value' => 'isset($data->request_from) ? $data->requestFrom[\'name\'] : "--"',
//        ),
        array(
            'header' => 'Site',
            'name' => 'request_to',
           'value' => 'isset($data->request_to) ? $data->requestTo[\'name\'] : "--"',
        ),
        array(
            'name' => 'request_status',
            'value' => 'isset($data->request_status) ? $data->requestStatus[\'caption\'] : "--"',
            'visible'=> ($page == 'site') ? true :false,
            'cssClassExpression' => '$data->addColor($data->request_status)',
        ),

//        array(
//            'name' => 'location',
//           'value' => 'isset($data->location) ? $data->location0[\'name\'] : "--"',
//        ),
       // 'paid_amount',
       // 'rating',
       // 'remarks',
        /* 'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:80px;'),
            //'template' => '{update}{delete}{view}{finalapprove}',
            'template' => '{delete}{view}{finalapprove}{dispatch}',
            'buttons' => array(
              /*  'update' => array(
                    'label' => 'edit',
                    'options' => array('class' => 'editAllocation'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/update",array("id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==10)) &&($data->request_status == '.$treq.')',

                ), */
                'delete' => array(
                    'label'=>'',
                    'visible'=> '((Yii::app()->user->role == 1)) &&($data->request_status == '.$treq.')',
                    'imageUrl'=>false,
                    'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete'),
                ),
                'finalapprove' => array(
                    'label' => '',
                    'options' => array('class' => 'finalapprove  icon-like icon-comn','title'=>'Finalapprove'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/AssignStatus",array("id"=>$data->id))',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/thumbs.png',
                    'imageUrl'=>false,
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==10)) && ($data->request_status == 27 || $data->request_status == 28)',

                ),
                'dispatch' => array(
                    'label' => '',
                    'options' => array('class' => 'singledispatch icon-plus icon-comn','title'=>'Dispatch'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/singledispatch",array("id"=>$data->id))',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/add.png',
                    'imageUrl'=>false,
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==9)) && ($data->request_status == 7)',

                ),
                'view' => array(
                    'label' => '',
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/siteview",array("id"=>$data->id))',
                    'imageUrl'=>false,
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                ),
            ),
        ),
    ),
));

 echo CHtml::endForm();



}?>
     <?php $this->endWidget(); ?>
</div>
<!-- Add tools Popup -->
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function(){


	$("#selectedIds_all").click(function(){
            if(this.checked){
                    $(".table tr td:first-child").each(function() {
                        if(!$(this).hasClass("statuscondition")){
                           $(this).find("span").addClass("checked");
                        }
                        else{
                               $(this).find("span").removeClass("checked");
                               $(this).find("input").attr("checked",false);
                        }
                    });
            }else
            {
                $("div.checker span").removeClass("checked");
            }
             return true;
	});

/*$(".finalapprove").click(function (event) {
$("#loading_icon").show();
event.preventDefault();
 var url = $(this).attr("href");
		$.ajax({
			type: "POST",
			url: url,
			success: function (response)
			{
                           location.reload();
			}
		});
});*/


/*		$(".createtools").click(function (event) {
			 event.preventDefault();
			$.ajax({
				type: "GET",
				url:"' . Yii::app()->createUrl('ToolTransferRequest/create&page='.$page) . '",
				success: function (response)
				{
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
		});




	$(".editAllocation").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
            url = url + "&page='.$page . '",
          //  alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addtools").html(response);
                        $("#addtools").css({"display":"block"})
                    }

                });

        });



*/
	});
');
?>
 <script type="text/javascript">

     function myfunction(){

	var myarray = [];
	var statusarr = [];
	//$("[name='selectedIds[]']:checked").each(function () {
             $(".table tr").each(function() {
                if($(this).find("td:first-child span").hasClass("checked")){
		  myarray.push($(this).find("input.hdncustomer").val());
                }
	});
	 $("input[id=hidden_field_id]").val(myarray);

       // return false;
	var arr=myarray;
    if(myarray.length > 0){
		if(confirm("Are you sure you want to create dispatch?")){
			$("form").attr("action", "<?php echo Yii::app()->createUrl('//toolTransferRequest/dispatchitem')?>");
		}else{
			$("form").submit(function(){
				return false;
			});
		}
	}else{
			alert("Please select request");
			location.reload();
			  $("form").submit(function(){
				return false;
			});
   }



}

    $(".assg").click(function(){

      $('.assg_form').slideDown();

    });

      $(document).on('change', '#site', function (event) {

            var id = $(this).val();

            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                url: '<?php echo Yii::app()->createUrl('ToolTransferRequest/getengineer'); ?>',
                success: function (data) {



                  $('#seleng').html(data);

                }
            });

      });


      $("#sub").click(function(){

            var id = $('#site option:selected').val();
            var eng = $('#siteeng option:selected').val();
            var duration = $('#duration').val();
            var dur_type = $('#dur_type').val();


            if (id && eng && duration && dur_type) {

                if (confirm('Are you sure to submit?')) {

              $('#loading_icon').show();

                $.ajax({
                    type: "POST",
                    data: {id: id, eng:eng, duration:duration, dur_type:dur_type },
                    url: '<?php  echo Yii::app()->createUrl('ToolTransferRequest/assignsubmit'); ?>',
                    success: function (response)
                    {
                         $('#loading_icon').hide();
                          if(response==1){
                            $('#msg').html('<div class="alert alert-success" style="width: 302px;    height: 43px;">Changed Successfully</div>');
                          }else{
                            $('#msg').html('<div class="alert alert-danger" style="width: 302px;    height: 43px;">No item to transfer</div>');
                          }
                    }
                });
            }
            }  else {

                 $('#msg').html('<div class="alert alert-danger" style="width: 302px;height: 43px;">Please Select All Fields</div>');
            }

    });

    $("#tool_dis").click(function(){

            var id = $('#site option:selected').val();
            var page_type = 'site';
            var code = 'find_1';
            var type = 0;

            if (id) {
                $.ajax({
                    type: "GET",
                    data: {id: id, code: code,type:type, page_type:page_type},
                    url: '<?php echo Yii::app()->createUrl('Tools/displaytools'); ?>',
                    success: function (response)
                    {
                        $("#addtools").html(response);
                        $("#addtools").css({"display": "block"});
                    }
                });
            } else {
                alert("Please Select Site");
            }

    });
  </script>
  <style>
      .statuscondition .checker{
          display:none;

      }
  </style>
