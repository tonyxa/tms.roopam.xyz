



<?php
/* @var $this ToolTransferRequestController */
/* @var $model ToolTransferRequest */

/* $this->breadcrumbs=array(
  'Tool Transfer Requests'=>array('index'),
  'Manage',
  );

  $this->menu=array(
  array('label'=>'List ToolTransferRequest', 'url'=>array('index')),
  array('label'=>'Create ToolTransferRequest', 'url'=>array('create')),
  );

  Yii::app()->clientScript->registerScript('search', "
  $('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
  });
  $('.search-form form').submit(function(){
  $.fn.yiiGridView.update('tool-transfer-request-grid', {
  data: $(this).serialize()
  });
  return false;
  });
  "); */
?>


<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->
<?php /*
  <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
  <div class="search-form" style="display:none">
  <?php $this->renderPartial('_search',array(
  'model'=>$model,
  )); ?>
  </div><!-- search-form -->
 */ ?>
<?php if($page == 'site') {
    $role = 10;
}else{
     $role = 9;
}
?>
<div class="clearfix">
<?php if($page == 'vendor') { ?> <div class="pull-right">
            <?php if ( Yii::app()->user->role == 1 || Yii::app()->user->role == 9 ) { ?>

           <?php

        echo CHtml::link('Add Transfer Requests', $this->createAbsoluteUrl('ToolTransferRequest/createstock',array('page'=>$page)),array('class' => 'btn blue'));
             } ?>
        </div> <?php } else{?>

            <div class="pull-right">
            <?php if ( Yii::app()->user->role == 1 || Yii::app()->user->role == 10 ) { ?>

           <?php

        echo CHtml::link('Add Transfer Requests', $this->createAbsoluteUrl('ToolTransferRequest/createstock',array('page'=>$page)),array('class' => 'btn blue'));
             } ?>
        </div> <?php }  ?>
            <?php if(($page == 'vendor')&& (Yii::app()->user->role == 10)) { 
                         ?> <h1> Error 403</h1> <?php }
                  else { ?> <h1>Manage Tool Transfer Requests To <?php if($page=='vendor') {echo 'Vendor';}else if($page=='stock')  {echo 'Stock';} else {echo $page;}}?>
        <span id="loading_icon" style="display:none">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading_icon2.gif" alt="" style ="height: 32px; width: 32px;"/>
                </span>
        </h1>
    </div>
<?php
$res1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Transfer to vendor'))
                        ->queryRow();
             $id1 = $res1['sid'];

$res2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'vendor_status',':caption'=>'Received back to stock'))
                        ->queryRow();
             $id2 = $res2['sid'];


$res3 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'stock_transfer', ':caption' => 'Stock Approved'))
                ->queryRow();

$res4 = Yii::app()->db->createCommand()
        ->select('*')
        ->from('tms_status')
        ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Approved'))
        ->queryRow();



?>

<div class="table-responsive">
<?php if($page == 'vendor') {  if ( Yii::app()->user->role == 1 || Yii::app()->user->role == 9 ) { ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-transfer-request-grid',
    'itemsCssClass' => 'table table-bordered',
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->search($page),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        'request_no',
       'request_date',
        //'request_to',
        //'request_from',
        //'transfer_to',
//        array(
//            'name' => 'request_from',
//            'value' => 'isset($data->request_from) ? $data->requestFrom[\'name\'] : "--"',
//        ),
        array(
            'name' => 'request_to',
           'value' => 'isset($data->request_to) ? $data->requestTo[\'name\'] : "--"',
        ),
//        array(
//            'name' => 'location',
//           'value' => 'isset($data->location) ? $data->location0[\'name\'] : "--"',
//        ),
        /*'paid_amount',
        'rating',
        'remarks',
         'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:80px;'),
            //'template' => '{update}{got_back}{delete}{view}',
            'template' => '{edit}{got_back}{view}',
            'buttons' => array(
                'edit' => array(
                    'label' => '',
                    'options' => array('class' => 'editAllocation icon-pencil icon-comn','title'=>'Edit'),
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/createvendor",array("tr_id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '($data->request_status == '.$id1.')'

                ),
                'got_back' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-action-undo icon-comn','title'=>'Got Back'),
                    'imageUrl'=>false,
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/back.png',
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/vendorview",array("id"=>$data->id,))',
                    'visible'=> '($data->request_status == '.$id1.')'

                ),
                /*'delete' => array(
                    'visible'=> '($data->request_status == '.$id1.')'

                ),*/
                'view' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                    'imageUrl'=>false,
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/eye.png',
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/vendordetails",array("id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '($data->request_status == '.$id2.')'

                ),
            ),
        ),
    ),
));
?>
<?php } else { echo "You are not authorized to perform this action.";}} else{

     $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Transfer Requested'))
                        ->queryRow();

    $treq = $qryres1['sid'];
    $qryres2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Approved'))
                        ->queryRow();
    $qryres4 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Full Transit'))
                        ->queryRow();

    $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-transfer-request-grid',
    'itemsCssClass' => 'table table-bordered',
        'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers',
    'dataProvider' => $model->search($page),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'S.No.',),
        'request_no',
       'request_date',
        //'request_to',
        //'request_from',
        //'transfer_to',
        array(
            'header' => 'Site',
            'name' => 'request_from',
            'value' => 'isset($data->request_from) ? $data->requestFrom->name : "--"',
        ),
//        array(
//            'name' => 'request_to',
//           'value' => 'isset($data->request_to) ? $data->requestTo[\'name\'] : "--"',
//        ),
        // array(
        //     'name' => 'request_status',
        //     'value' => 'isset($data->request_status) ? $data->requestStatus[\'caption\'] : "--"',
        //     'visible'=> ($page == 'site') ? true :false,
        // ),
       
        array(
            'name' => 'location',
           'value' => 'isset($data->location) ? $data->location0[\'name\'] : "--"',
        ),
        array(
            'name' => 'request_status',
            'value' => 'isset($data->request_status) ? $data->requestStatus[\'caption\'] : "--"',
            // 'visible'=> ($page == 'stock') ? true :false,
            'cssClassExpression' => '$data->addColor($data->request_status)',
        ),

       // 'paid_amount',
       // 'rating',
       // 'remarks',
        /* 'created_by',
          'created_date',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:110px;'),
            'template' => '{view}{update}{delete}{finalapprove}{stockapprove}{dispatch}{request}',
            'buttons' => array(
                'update' => array(
                    'label' => '',
                    'imageUrl'=>false,
                    'options' => array('class' => 'editAllocation icon-pencil icon-comn', 'title'=>'Edit'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/createstock",array("tr_id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==10)) &&($data->request_status == '.$treq.')',

                ),
                'delete' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-trash icon-comn', 'title'=>'Delete'),
                    'imageUrl'=>false,
                    'visible'=> '((Yii::app()->user->role == 1)) && ($data->request_status == '.$treq.')',

                ),
                'finalapprove' => array(
                    'label' => '',
                    'options' => array('class' => 'finalapprove  icon-like icon-comn','title'=>'Finalapprove'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/stockapprove",array("id"=>$data->id))',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/thumbs.png',
                    'imageUrl'=>false,
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==9) ) && ($data->request_status == 27 || $data->request_status == 28)',

                ),
                'view' => array(
                    'label' => '',
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/view",array("id"=>$data->id,"page"=>"'.$page.'"))',
                    'visible' =>'(Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10 )',

                ),

                'request' => array(
                    'label' => '',
                    'options' => array('class' =>'icon-cursor icon-comn','title'=>'Dispatched Item'),
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/dispatchrequesttoStock1",array("id"=>$data->id ,))',
                    'visible' =>'( (Yii::app()->user->role == 10 && $data->request_status == 27 || $data->request_status==28))',

                ),

                'stockapprove' => array(
                    'label' => '',
                    'options' => array('class' =>'icon-like icon-comn','title'=>'Request'),
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/siteview",array("id"=>$data->id ,))',
                    'visible' =>'( (Yii::app()->user->role == 1 || Yii::app()->user->role == 9) && (($data->request_status == 6)||($data->request_status == 21) ))',

                ),
                'dispatch' => array(
                    'label' => '',
                    'options' => array('class' => 'singledispatch','title'=>'Dispatch'),
                    'url' => 'Yii::app()->createUrl("ToolTransferRequest/singledispatch",array("id"=>$data->id))',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/add.png',
                   // 'imageUrl'=>false,
                    'visible'=> '((Yii::app()->user->role == 1) || (Yii::app()->user->role ==9)) && ($data->request_status == 7)',

                ),



            ),
        ),
    ),
));




}?>
</div>
<!-- Add tools Popup -->
<div id="addtools" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

    </div>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function(){
    /* $(".finalapprove").click(function (event) {
        $("#loading_icon").show();
        event.preventDefault();
         var url = $(this).attr("href");
                $.ajax({
                    type: "POST",
                    url: url,
                    success: function (response)
                    {
                                   location.reload();
                    }
                });
        });


		$(".createtools").click(function (event) {
			 event.preventDefault();
			$.ajax({
				type: "GET",
				url:"' . Yii::app()->createUrl('ToolTransferRequest/create&page='.$page) . '",
				success: function (response)
				{
					$("#addtools").html(response);
					$("#addtools").css({"display":"block"});
				}
			});
		});




	$(".editAllocation").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("href");
            url = url + "&page='.$page . '",
          //  alert(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response)
                    {
						//alert(response);
                        $("#addtools").html(response);
                        $("#addtools").css({"display":"block"})
                    }

                });

        });



*/
	});
');
?>
