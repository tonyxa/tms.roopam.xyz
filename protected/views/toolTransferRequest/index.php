<?php
/* @var $this ToolTransferRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tool Transfer Requests',
);

$this->menu=array(
	array('label'=>'Create ToolTransferRequest', 'url'=>array('create')),
	array('label'=>'Manage ToolTransferRequest', 'url'=>array('admin')),
);
?>

<h1>Tool Transfer Requests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
