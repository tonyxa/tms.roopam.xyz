<?php
/* @var $this ToolSetCategoryController */
/* @var $model ToolSetCategory */

$this->breadcrumbs=array(
	'Tool Set Categories'=>array('index'),
	$model->tool_set_id=>array('view','id'=>$model->tool_set_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ToolSetCategory', 'url'=>array('index')),
	array('label'=>'Create ToolSetCategory', 'url'=>array('create')),
	array('label'=>'View ToolSetCategory', 'url'=>array('view', 'id'=>$model->tool_set_id)),
	array('label'=>'Manage ToolSetCategory', 'url'=>array('admin')),
);
?>

<h1>Update ToolSetCategory <?php echo $model->tool_set_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>