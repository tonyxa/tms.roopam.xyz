<?php
/* @var $this ToolSetCategoryController */
/* @var $data ToolSetCategory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('tool_set_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->tool_set_id), array('view', 'id'=>$data->tool_set_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tool_cat_name')); ?>:</b>
	<?php echo CHtml::encode($data->tool_cat_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>