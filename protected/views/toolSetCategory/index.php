<?php
/* @var $this ToolSetCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tool Set Categories',
);

$this->menu=array(
	array('label'=>'Create ToolSetCategory', 'url'=>array('create')),
	array('label'=>'Manage ToolSetCategory', 'url'=>array('admin')),
);
?>

<h1>Tool Set Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
