<?php
/* @var $this ToolSetCategoryController */
/* @var $model ToolSetCategory */

$this->breadcrumbs=array(
	'Tool Set Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ToolSetCategory', 'url'=>array('index')),
	array('label'=>'Manage ToolSetCategory', 'url'=>array('admin')),
);
?>

<h1>Create ToolSetCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>