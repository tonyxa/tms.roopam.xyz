<?php
/* @var $this ToolSetCategoryController */
/* @var $model ToolSetCategory */

$this->breadcrumbs=array(
	'Tool Set Categories'=>array('index'),
	'Manage',
);

/* $this->menu=array(
	array('label'=>'List ToolSetCategory', 'url'=>array('index')),
	array('label'=>'Create ToolSetCategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tool-set-category-grid', {
		data: $(this).serialize()
	});
	return false;
});
"); */
?>

<h1>Manage Tool Set Categories</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div><!-- search-form -->
<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-set-category-form',
        'enableAjaxValidation' => false,
            /* 'enableAjaxValidation' => true,
              'enableClientValidation'=>true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
    ?>
    <div class="addelement">
        <div>
<?php echo $form->labelEx($model, 'tool_cat_name'); ?>
            <?php echo $form->textField($model, 'tool_cat_name', array('class' => 'form-control', 'style' => 'width:250px;height:30px;margin-bottom:1px')); ?>
            <span class="errorMessage"></span>
        </div>        
        <div>
            <label>&nbsp;</label>
<?php echo CHtml::Button('Add', array('class' => 'addbutton_form btn green')); ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tool-set-category-grid',
	'dataProvider'=>$model->search(),
        'itemsCssClass' => 'table table-bordered',
	'filter'=>$model,
        'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
	'columns'=>array(
         array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('style' => 'width:20px;'),),
            array(
               'name' => 'tool_set_id',
               'headerHtmlOptions' => array('style' => 'display:none'),
               'htmlOptions' => array('style' => 'display:none'),
               'filterHtmlOptions' => array('style' => 'display:none'),       
            ),
            array(
            'name' => 'tool_cat_name',
            'htmlOptions' => array('class' => 'editable', 'title' => 'Update')
            ),
             /*array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:60px;'),
            'template' => '{deletecat}',
            'buttons' => array(
                'deletecat' => array(
                    'label' => 'Delete',
                    'url' => 'Yii::app()->createUrl("toolSetCategory/delete", array("id"=>$data->tool_set_id))',
                    'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                    'options' => array('class' => 'deleteicon'),
                ),
            ),
        ),*/
		
//		'created_by',
//		'created_date',
//		'updated_by',
//		'updated_date',
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>

<?php
Yii::app()->clientScript->registerScript('myjquery', ' 
		
$(document).ready(function () {
	
        
$(".addbutton_form").on("click", function () {
	//alert("hi");
    var data=$("#tool-set-category-form").serialize();
	//alert(data);
    $.ajax({
    type: "POST",
	dataType: "JSON",
    url:"' . Yii::app()->createUrl('toolSetCategory/create') . '",
    data:data,
    success:function(response){
					
					if(response == null){
						 //window.parent.$.fn.yiiGridView.update("unit-grid");
						 
						  location.reload();
						
					 }
					 else{
                                         
						var obj = eval(response);
						$(".errorMessage").text(obj[\'tool_cat_name\']);                                                
					}
				
					
					
             },
    
 

   });
 
});

var flag = 0;

jQuery("#tool-set-category-grid table tbody tr .editable").click(function(){ 
if(flag == 0){
flag = 1;
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   var label = $(this);
		   label.after("<input class=\'tool_cat_name singletextbox\' type = \'text\' style = \'display:none\' name=\'tool_cat_name\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  
		  
		     
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-set-category-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var value = $(".tool_cat_name").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolSetCategory/update') . '",
				data:{id:id,value: value},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj[\'tool_cat_name\']);
							}
						 },

			   });
			
			});   
		   
            }
         });
         
     $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					
					
					location.reload();
				}
				
				else
				{
					alert("Category is already in use,Cannot delete! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
		
jQuery("#tool-category-grid table tbody tr .editable1").click(function(e){
if(flag == 0){
flag = 1;
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   var label = $(this);
		   label.after("<input class=\'cat_code singletextbox\' type = \'text\' style = \'display:none\' name=\'cat_code\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage3\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  
		  
		     
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var cat_code = $(".cat_code").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/update') . '",
				data:{id:id,cat_code: cat_code},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage3").text(obj[\'cat_code\']);
							}
						 },

			   });
			
			});   
		   
		}   
         });
         
    
		

	});
        
      
   ');
?> 

