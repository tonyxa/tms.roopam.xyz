<?php
/* @var $this ToolSetCategoryController */
/* @var $model ToolSetCategory */

$this->breadcrumbs=array(
	'Tool Set Categories'=>array('index'),
	$model->tool_set_id,
);

$this->menu=array(
	array('label'=>'List ToolSetCategory', 'url'=>array('index')),
	array('label'=>'Create ToolSetCategory', 'url'=>array('create')),
	array('label'=>'Update ToolSetCategory', 'url'=>array('update', 'id'=>$model->tool_set_id)),
	array('label'=>'Delete ToolSetCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->tool_set_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ToolSetCategory', 'url'=>array('admin')),
);
?>

<h1>View ToolSetCategory #<?php echo $model->tool_set_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tool_set_id',
		'tool_cat_name',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
