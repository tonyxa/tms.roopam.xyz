<?php
/* @var $this WorkSiteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Work Sites',
);

$this->menu=array(
	array('label'=>'Create WorkSite', 'url'=>array('create')),
	array('label'=>'Manage WorkSite', 'url'=>array('admin')),
);
?>

<h1>Work Sites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
