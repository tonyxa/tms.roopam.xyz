<?php
/* @var $this WorkSiteController */
/* @var $model WorkSite */

$this->breadcrumbs=array(
	'Work Sites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WorkSite', 'url'=>array('index')),
	array('label'=>'Manage WorkSite', 'url'=>array('admin')),
);
?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add WorkSite</h4>
        </div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
   </div>

</div>
