<?php
/* @var $this WorkSiteController */
/* @var $model WorkSite */

$this->breadcrumbs=array(
	'Work Sites'=>array('index'),
	'Manage',
);
/*
$this->menu=array(
	array('label'=>'List WorkSite', 'url'=>array('index')),
	array('label'=>'Create WorkSite', 'url'=>array('create')),
); */


?>

<h1>Manage Work Sites</h1>


<?php  // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div--><!-- search-form -->


<div class="">
<div class="table-responsive">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'work-site-form',
         'enableAjaxValidation'=>false,
       
       /* 'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,), */
    ));
    ?>
		<div class="addelement">
			<div>
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name', array('class' => 'form-control','style' => 'width:250px;margin-bottom:1px')); ?>
				<span class="errorMessage"></span>
			</div>
			<div>
				<label>&nbsp;</label>
			<?php  echo CHtml::Button('Add',array('class'=>'addbutton_form btn blue btn-sm', "style"=>"margin-top:20px;")); ?>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
		


<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'work-site-grid',
	'dataProvider'=>$model->search($page),
	'itemsCssClass' => 'table table-bordered',
	'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
    'nextPageLabel'=>'Next ' ),    
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'filter'=>$model,
	'columns'=>array(
		array('class' => 'IndexColumn', 'header' => 'S.No.','htmlOptions'=>array('style'=>'width:20px;'),),
		
			array(
			'name' => 'id',
			'headerHtmlOptions' => array('style' => 'display:none'),
			'htmlOptions' => array('style' => 'display:none'),
			'filterHtmlOptions' => array('style' => 'display:none'),
			//'visible' => Yii::app()->user->role == 1,
			//'htmlOptions' => array('style' => 'display:none'),
		
		
		),
		array(
		'name' => 'name',
		'htmlOptions' => array('class' => 'editable','title' => 'Update')
		),
		//'created_by',
		//'created_date',
		//'updated_by',
		//'updated_date',
		array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:150px;'),
			//'template' => '{deletesite}{active}{inactive}',
                        'template' => '{assigned}{Make Inactive}{Make Active}', 
			'buttons' => array(

			'assigned' => array(
                            'label' => '',
                            'url' => '"#".Yii::app()->createUrl("WorkSite/assigntouser",array("id"=>$data->id,"asDialog"=>1,"gridId"=>$this->grid->id))',
                            'click' => 'function(){$("#cru-frame").attr("src",($(this).attr("href")).substring(1)); $("#cru-dialog").dialog("open");return false;}',
//                            'imageUrl' => Yii::app()->baseUrl . '/images/users.png',
                            'imageUrl'=>false,
                            'options' => array('class' => 'actionitem viewicon icon-target icon-comn','title'=>'Assigned' ),
                            'visible' => '((Yii::app()->user->role==1) ? true: false)',
            ),
			
			'deletesite' => array(
			'label' => 'Delete',
                        'url'=>'Yii::app()->createUrl("workSite/delete", array("id"=>$data->id))',
			'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'deleteicon'),                            
                        ),
                            
                        // Active section starts from here     
                        'Make Active' => array(			    
                        'url'=>'Yii::app()->createUrl("workSite/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-primary'), 
                        'visible' => '($data->active_status == 0)',    
			),
			// Active section end here 
                            
                         //Inactive section starts from here     
                        'Make Inactive' => array(			    
                        'url'=>'Yii::app()->createUrl("workSite/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-warning'),  
                        'visible' => '($data->active_status == 1)',     
			),
			// Inactive section end here     
				
               
                // 'update' => array(
                   // 'label'=>'edit',
                   // 'options' => array('class' => 'editWorksite'),
                  //  'url' => 'Yii::app()->createUrl("workSite/update",array("id"=>$data->id,))',
                //),
                
              
            ),
		), 
	),
)); ?>

</div>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'User Form',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="555" height="620" frameborder="0" style="min-height:325px;"></iframe>

<?php
$this->endWidget();
?>




<?php

Yii::app()->clientScript->registerScript('myjquery', ' 
		
$(document).ready(function () {
	
        
$(".addbutton_form").on("click", function () {
	//alert("hi");
    var data=$("#work-site-form").serialize();
	//alert(data);
    $.ajax({
    type: "POST",
	dataType: "JSON",
    url:"'.Yii::app()->createUrl('workSite/create').'",
    data:data,
    success:function(response){
					
					if(response == null){
						 //window.parent.$.fn.yiiGridView.update("work-site-grid");
						  //$("#WorkSite_site_name").val("");
						  //$(".errorMessage").css({"display":"none"});
						  location.reload();
						
					 }
					 else{

						var obj = eval(response);
						$(".errorMessage").text(obj);
					}
				
					
					
             },
    
 

   });
 
});


$(document).on("click","#work-site-grid table tbody tr .editable",function() {
//jQuery("#work-site-grid table tbody tr .editable").click(function(){
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		  // alert(id);
		   var label = $(this);
		   label.after("<input class=\'work_site_name singletextbox\' type = \'text\' style = \'display:none\' name=\'site_name\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  
		  
		     
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("work-site-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var value = $(".work_site_name").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"'.Yii::app()->createUrl('workSite/update').'",
				data:{id:id,value: value},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj);
							}
						 },

			   });
			
			});   
		   
		   
         });
         
     $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					
					//alert("Work Site Deleted");
					location.reload();
				}
				else
				{
					alert("Work Site is already in use,Cannot delete! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
       
//active status script starts from here
$(document).on("click",".changestatusicon",function() {
//$(".changestatusicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){					
					
					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
//active status script end here
		

	

	});
      
   ');
?>
