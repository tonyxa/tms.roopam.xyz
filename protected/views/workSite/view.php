<?php
/* @var $this WorkSiteController */
/* @var $model WorkSite */

$this->breadcrumbs=array(
	'Work Sites'=>array('index'),
	$model->id,
);
/*
$this->menu=array(
	array('label'=>'List WorkSite', 'url'=>array('index')),
	array('label'=>'Create WorkSite', 'url'=>array('create')),
	array('label'=>'Update WorkSite', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WorkSite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WorkSite', 'url'=>array('admin')),
); */
?>

<h1>View WorkSite #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_name',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
