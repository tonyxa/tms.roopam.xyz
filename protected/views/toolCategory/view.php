<?php
/* @var $this ToolCategoryController */
/* @var $model ToolCategory */

$this->breadcrumbs=array(
	'Tool Categories'=>array('index'),
	$model->cat_id,
);

$this->menu=array(
	array('label'=>'List ToolCategory', 'url'=>array('index')),
	array('label'=>'Create ToolCategory', 'url'=>array('create')),
	array('label'=>'Update ToolCategory', 'url'=>array('update', 'id'=>$model->cat_id)),
	array('label'=>'Delete ToolCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ToolCategory', 'url'=>array('admin')),
);
?>

<h1>View ToolCategory #<?php echo $model->cat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cat_id',
		'cat_name',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
