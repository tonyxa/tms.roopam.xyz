
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/scripts/filetree.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/admin/layout3/css/filetree.css');
$category = ToolCategory::model()->findAll();
$slectbox = '';
$slectbox.='<select class=\"catselect form-control\">';
foreach ($category as $cat) {
    $slectbox.='<option value=\"' . $cat['cat_id'] . '\">' . $cat['cat_name'] . '</option>';
}
$slectbox.='</select>';

Yii::app()->clientScript->registerScript('toolcategory', "
/*$('li').click(function(e) {

    var path = [];
    var el = $(this);
    do {
        path.unshift(el.html());
        el = el.parent().closest('li');
    } while(el.length != 0);

    alert(path.join('/'));
    e.stopPropagation();
});*/

$('#reset').on('click',function(){
        $('.tree-title').removeClass('activeli');
        $('#ToolCategory_parent_id').val('');
        $('#uniform-ToolCategory_tool_type span').removeClass('checked');
        $('#ToolCategory_tool_type').attr('checked', false);
        $('.tree').hide();
});

$('.tree-title').click(function(){
            $('#reset').parent().removeClass('checked');
            $('.tree-title').removeClass('activeli');
            var catid= $(this).attr('data-id');
            $('#ToolCategory_parent_id').val(catid);
            $.ajax({
                    method: 'POST',
                    dataType: 'html',
                     url:'".Yii::app()->createUrl('toolCategory/gettooltype')."',
                    data: {catid:catid},
                    success: function (data) {
                      if(data == 1){
                        $('#uniform-ToolCategory_tool_type span').addClass('checked');
                        $('#ToolCategory_tool_type').attr('checked', true);
                        //$('#ToolCategory_tool_type').prop('readonly', true);
                        $('#ToolCategory_tool_type').attr('disabled', false);
                      }else{

                        $('#uniform-ToolCategory_tool_type span').removeClass('checked');
                        $('#ToolCategory_tool_type').attr('checked', false);
                        //$('#ToolCategory_tool_type').prop('readonly', true);
                        $('#ToolCategory_tool_type').attr('disabled', true);

                       //$('#ToolCategory_tool_type').click(function() { return false; });
                      }
                    }
                });


           $(this).addClass('activeli');
       /*
        alert(catid);
        var lengthstr = $(this).parents('ul').length;
        alert(lengthstr);
        if(lengthstr>0){
            var parentEls = $(this).parents('ul')
            .each(function() {
               alert($(this).parent('li').html());
             //  return this.html();
            })
            .get()
            .join( ',');
            $('.path').append('<strong>'+ parentEls + '</strong>');


            var obj = $(this).parents('li').add(this);
        }**/

});


");


?>
<div class="container">
    <h1 class="text-center">Manage Tool Categories</h1>
    <br><br>
    <div class="row">

        <div class="col-md-4 left-panel">
            <div class="checkbox chehck">
               <!-- <label><input type="checkbox" value="" id="reset"><b>RESET</b></label> -->
                <button class="btn btn-sm" id="reset" style="color: #5d5d5d;">RESET</button>
            </div>
            <span id="path"></span>

            <?php
            $parents = ToolCategory::model()->findAll(
                    array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id IS NULL',
                              'order'=>'cat_name'
                             )
                    );
            foreach ($parents as $parent) {
                echo '<ul class="main-tree">';
                echo '<li class="tree-title" data-id="' . $parent->cat_id . '">' . $parent->cat_name . '</li>';
                echo $this->getChilds($parent->cat_id);
                echo '</ul>';
            }
            ?>

        </div>
        <div class="col-md-8 right-panel">
            <div class="table-responsive">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'tool-category-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => false,),
            ));
            ?>

            <div class="clearfix">
                <div class="row addRow">
                    <div class="col-md-3">
                        <?php echo $form->hiddenField($modelnew, 'parent_id', array('class' => 'form-control')); ?>
                        <?php echo $form->labelEx($modelnew, 'cat_name'); ?>
                        <?php echo $form->textField($modelnew, 'cat_name', array('class' => 'form-control')); ?>
                        <?php echo $form->error($modelnew, 'cat_name'); ?>
                        <br>
                    </div>
                    <div class="col-md-3">
                        <?php echo $form->labelEx($modelnew, 'cat_code'); ?>
                        <?php echo $form->textField($modelnew, 'cat_code', array('class' => 'form-control')); ?>
                        <?php echo $form->error($modelnew, 'cat_code'); ?>
                        <br>
                    </div>
                    <div class="col-md-2">
                        <?php echo $form->labelEx($modelnew, 'Tool Set'); ?><br>
                        <?php echo $form->checkBox($modelnew,'tool_type'); ?>
                        <?php echo $form->error($modelnew, 'tool_type'); ?>
                        <br>
                    </div>
                    <div class="save-btnHold col-md-4">
                        <?php echo CHtml::submitButton($modelnew->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm blue addbutton_form')); ?>
                        <input type="button" data-dismiss="modal"  class="btn btn-sm default" onclick="javascript:window.location.reload()" value="Reset">
                    </div>

                    <?php $this->endWidget(); ?>


                </div>
            </div>



            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'tool-category-grid',
                'dataProvider' => $model->search(),
                'itemsCssClass' => 'table table-bordered',
                'filter' => $model,
                'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
                'nextPageLabel' => 'Next '),
                'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
                'columns' => array(
                    array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('style' => 'width:20px;'),),
                    array(
                        'name' => 'cat_id',
                        'headerHtmlOptions' => array('style' => 'display:none'),
                        'htmlOptions' => array('style' => 'display:none'),
                        'filterHtmlOptions' => array('style' => 'display:none'),
                    //'visible' => Yii::app()->user->role == 1,
                    //'htmlOptions' => array('style' => 'display:none'),
                    ),
                    array(
                        'name' => 'cat_name',
                        'htmlOptions' => array('class' => ' ', 'title' => 'Update'),
                        'value'=>'"<div ><span class=\'editabletext\'>".$data->cat_name."</span><i class=\'editable icon-pencil icon-comn\'></i></div>"',
                        'type'=> 'raw',

                    ),
                    array(
                        'name' => 'parent_id',
                        'type'=> 'raw',
                        'value' => '(!isset($data->parentId["cat_name"]) ? "ROOT CATEGORY" : $data->parentId["cat_name"] ).CHtml::link( CHtml::tag("img"),"",array("class" => "editable2 icon-pencil icon-comn ", "title"=>"Edit","style"=>"margin-left: 3px;"))',
                        //'value' => '(!isset($data->parentId["cat_name"]) ? "ROOT CATEGORY" : $data->parentId["cat_name"] ).CHtml::link( CHtml::tag("img", array( "src" => "'.Yii::app()->request->baseUrl . '/images/update.png")),"",array("class" => "editable2"))',
                        //'value' => '($data->parent_id != "" ? $data->parentId["cat_name"].CHtml::link( CHtml::tag("img", array( "src" => "'.Yii::app()->request->baseUrl . '/images/update.png")),"",array("class" => "editable2")) : $data->parentId["cat_name"])',
//                        'filter' => CHtml::listData(ToolCategory::model()->findAll(
//                                        array(
//                                            'select' => array('cat_id,cat_name'),
//                                            'order' => 'cat_name',
//                                            'distinct' => true
//                                )), "cat_id", "cat_name"),
                     'filter'      => CHtml::dropDownList( 'ToolCategory[parent_id]', $model->parent_id,  $testmodel, array( 'empty' => 'All' )),
                    'class'=>'DataColumn',
                    'evaluateHtmlOptions'=>true,
                  'htmlOptions' => array('id'=>'"maincatedit{$data->cat_id}"'),
                    ),
                    array(
                        'name' => 'cat_code',
                        'htmlOptions' => array('class' => '', 'title' => 'Update') //editable1
                    ),
                     array(
                        'name' => 'tool_type',
                        'value' => '$data->toolType["caption"]',
                        'type' => 'raw',
                        'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'order' => 'caption',
                                'condition'=>'status_type ="tool_type"',
                                'distinct' => true
                            )), "sid", "caption"),
                    ),
                /* 'created_by',
                  'created_date',
                  'updated_by',
                  'updated_date', */
                /* array(
                  'class' => 'CButtonColumn',
                  'htmlOptions' => array('style' => 'width:60px;'),
                  'template' => '{update}',
                  'buttons' => array(
                  'addrate' => array(
                  'label' => 'Update',
                  'url' => 'Yii::app()->createUrl("tasks/taskaddrate", array("tid"=>$data->tskid))',
                  'imageUrl' => Yii::app()->request->baseUrl . '/images/rate.png',
                  'options' => array('id' => 'addrate', 'class' => 'addrateicon', 'data-toggle' => 'modal', 'data-target' => '#myModal'),
                  'visible' => '($data->acknowledge_status == 1) && ($data->report_to==' . Yii::app()->user->id . ')',
                  ),
                  ),
                  ),
                  */

                       	array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:130px;'),
			'template' => '{Make Inactive}{Make Active}{view}',
                        //'template' => '{update}{delete}',
			'buttons' => array(

//                'update' => array(
//                    'label'=>'edit',
//                    //'options' => array('class' => 'editVendor'),
//                    'options' => array('class' => ''),
//                    'url' => 'Yii::app()->createAbsoluteUrl("purchase/update",array("id"=>$data->cat_id,))',
//                    'visible' => '($data->active_status == 0)',
//                ),

                 /* Active section starts from here */
                        'Make Active' => array(
                        'url'=>'Yii::app()->createUrl("toolCategory/changestatus", array("id"=>$data->cat_id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-success'),
                        'visible' => '($data->active_status == 0)',
			),
			/* Active section end here */

                        /* Inactive section starts from here */
                        'Make Inactive' => array(
                        'url'=>'Yii::app()->createUrl("toolCategory/changestatus", array("id"=>$data->cat_id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-warning'),
                        'visible' => '($data->active_status == 1)',
			),
                        'view' => array(
                        'label'=>'',
                        'url'=>'Yii::app()->createUrl("tools/categoryview", array("id"=>$data->cat_id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'imageUrl' =>false,
                        'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                        'visible' => '($data->active_status == 1)',
			),
			/* Inactive section end here */
                /*'delete' => array(
                    'label'=>'Delete',
                    'visible'=>'$data->chkitems($data->id)',

                ),*/
            ),
		),

                ),
            ));
            ?>
            </div>


            <?php
            Yii::app()->clientScript->registerScript('myjquery', '

$(document).ready(function () {
var flag = 0;
$(document).on("click","#tool-category-grid table tbody tr .editable",function() {
//$("#tool-category-grid table tbody tr .editable").click(function(){
if(flag == 0){
flag = 1;
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   var label = $(this);
       var elem =  $(this).siblings(".editabletext");
		   label.after("<input class=\'cat_name singletextbox\' type = \'text\' style = \'display:none\' name=\'cat_name\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn blue btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
       console.log(label);
		   textbox.val(elem.html());
			   $(this).siblings(".editabletext").hide();
         $(this).hide();
			   $(this).next().show();


		  $(document).on("click",".cancel",function() {
		  //$(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();


			});

		  $(document).on("click",".save",function() {

		  //$(".save").click(function(){

				var value = $(".cat_name").val();

				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/update') . '",
				data:{id:id,value: value},
				success:function(response){

							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj[\'cat_name\']);
							}
						 },

			   });

			});

            }
         });


var flag2 = 0;
$(document).on("click","#tool-category-grid table tbody tr .editable2",function() {
if(flag == 0){
flag = 1;


		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   //var label = $(this);
                   //var catres = "<select name=\'parent\' class=\'form-control singletextbox\' style = \'display:none\' ><option value=\'phone\'>phone</option></select>";
                   //var catres = "<select name="parent" class="form-control singletextbox" style = "display:none" ><option value="phone">phone</option></select>";
		   //label.after("catres&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><a class=\'save btn blue btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
                   // var textbox = $(this).next();
                   // $(this).hide();
                   // $(this).next().show();

                   //var selectcontent = $("#maincategory").html();

                   $.ajax({
                   method:"get",
                   data:{id:id},
                   url:"' . Yii::app()->createUrl('toolCategory/categoryshow') . '",
                   }).done(function(data){
                   if(data != 1){
                   $("#maincatedit"+id).html(data);
                   }
                   });




$(document).on("click",".cancel",function() {

				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();


			});


$(document).on("click",".save",function() {
//jQuery("#availability option:selected").val();
var r = "";
 r= $("select#ToolCategory_parent_id option:selected").val();
//alert(id);
alert(r);
return false;



				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/categoryupdate') . '",
				data:{id:id,value: value},
				success:function(response){

							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj[\'cat_name\']);
							}
						 },

			   });

			});


}
});




     $(".deleteicon").on("click", function (event) {

		event.preventDefault();
		var url = $(this).attr("href");

		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){


					location.reload();
				}

				else
				{
					alert("Category is already in use,Cannot delete! ");
				}

						   }
					   });

			}

       });


$(document).on("click","#tool-category-grid table tbody tr .editable1",function() {
//$("#tool-category-grid table tbody tr .editable1").click(function(e){
if(flag == 0){
flag = 1;
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   var label = $(this);
		   label.after("<input class=\'cat_code singletextbox\' type = \'text\' style = \'display:none\' name=\'cat_code\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn blue btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage3\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();


		    $(document).on("click",".cancel",function() {
		 // $(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();


			});

		   $(document).on("click",".save",function() {
		//  $(".save").click(function(){

				var cat_code = $(".cat_code").val();

				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/update') . '",
				data:{id:id,cat_code: cat_code},
				success:function(response){

							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage3").text(obj[\'cat_code\']);
							}
						 },

			   });

			});

		}
         });




	});




//active status script starts from here
//$(".changestatusicon").on("click", function (event) {
$(document).on("click",".changestatusicon",function() {

		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{

				if(response.response == "success"){

					$.fn.yiiGridView.update("tool-category-grid");
				}
				else
				{
					alert("Cannot change the status! ");
				}

						   }
					   });

			}

       });
//active status script end here



   ');

            ?>

        </div>
    </div>
</div>
<style type="text/css">

    .left-panel .tree-title.activeli{
        background: #c3d9ff;
        color:#09f;
    }
    .left-panel {
        border-right: 1px solid #BFBFBF;
        height: 730px;
        max-height: 730px;
        overflow-y: auto;
    }
    .addbutton_form{
        margin: 0;
    }
    .chehck{
        margin-left: -20px;
        color:#06c;
    }
    .default, .blue{
        margin-top: 13px;
    }
    .tree-title, .tree-title.activeli:before {
        color: #72255a;
    }
    .left-panel .tree-title.activeli {
        background: #72255a;
        color: #fff;
        padding: 2px 6px;
    }
</style>
<script type="text/javascript">
function cTrig(parent,id){
        var box= confirm("Are you sure you want to change this category ?");
        if (box==true){

        $.ajax({
        method:'get',
        data:{parent:parent,id:id},
        url:'<?php echo Yii::app()->createUrl('toolCategory/Categoryupdate'); ?>',
        //url:"' . Yii::app()->createUrl('toolCategory/update') . '",
        }).done(function(data){
        if(data == 1){
        //RefreshTable();
        location.reload();
        }else{
        location.reload();
        }
        });
        return false;
        }else{
        location.reload();
        }
}

function RefreshTable() {
        $( "#tool-category-grid" ).load( "toolCategory/categoryview #tool-category-grid" );
        }
    </script>
