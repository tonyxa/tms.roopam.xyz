


<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'tool-category-form',
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

<div class="clearfix">
    <div class="row addRow">
        <div class="col-md-12">
            <?php echo $form->labelEx($model,'cat_name'); ?>
			<?php echo $form->textField($model,'cat_name', array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'cat_name'); ?>
        </div>
      
    
    <br />

    
	</div>
</div>

<div class="modal-footer save-btnHold">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn green')); ?> 
        <button data-dismiss="modal"  class="btn default" onclick="javascript:window.location.reload()">Close</button>
</div>
 <?php $this->endWidget(); ?>
</div>
