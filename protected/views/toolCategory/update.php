<?php
/* @var $this ToolCategoryController */
/* @var $model ToolCategory */

$this->breadcrumbs=array(
	'Tool Categories'=>array('index'),
	$model->cat_id=>array('view','id'=>$model->cat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ToolCategory', 'url'=>array('index')),
	array('label'=>'Create ToolCategory', 'url'=>array('create')),
	array('label'=>'View ToolCategory', 'url'=>array('view', 'id'=>$model->cat_id)),
	array('label'=>'Manage ToolCategory', 'url'=>array('admin')),
);
?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Update Tool Category</h4>
        </div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div>

</div>
