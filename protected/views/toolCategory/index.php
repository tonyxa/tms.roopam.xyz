<?php
/* @var $this ToolCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tool Categories',
);

$this->menu=array(
	array('label'=>'Create ToolCategory', 'url'=>array('create')),
	array('label'=>'Manage ToolCategory', 'url'=>array('admin')),
);
?>

<h1>Tool Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
