<?php
/* @var $this ToolCategoryController */
/* @var $model ToolCategory */

$this->breadcrumbs = array(
    'Tool Categories' => array('index'),
    'Manage',
);
/*
  $this->menu=array(
  array('label'=>'List ToolCategory', 'url'=>array('index')),
  array('label'=>'Create ToolCategory', 'url'=>array('create')),
  );
 */
?>

<h1>Manage Tool Categories</h1>


<div class="">
<div class="table-responsive">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tool-category-form',
        'enableAjaxValidation' => false,
            /* 'enableAjaxValidation' => true,
              'enableClientValidation'=>true,
              'clientOptions' => array(
              'validateOnSubmit' => true,
              'validateOnChange' => true,
              'validateOnType' => false,), */
    ));
    ?>
    <div class="addelement">
        <div>
<?php echo $form->labelEx($model, 'cat_name'); ?>
            <?php echo $form->textField($model, 'cat_name', array('class' => 'form-control', 'style' => 'width:250px;height:30px;margin-bottom:1px')); ?>
            <span class="errorMessage"></span>
        </div>
        <div>
<?php echo $form->labelEx($model, 'cat_code'); ?>
            <?php echo $form->textField($model, 'cat_code', array('class' => 'form-control', 'style' => 'width:250px;height:30px;margin-bottom:1px')); ?>
            <span class="errorMessage2"></span>
        </div>
         <div>
<?php echo $form->labelEx($model, 'parent_id'); ?>
            <?php 
            echo $form->dropDownList($model, 'parent_id', $testmodel, array('prompt' => 'ROOT','class' => 'form-control', 'style' => 'width:250px;height:30px;margin-bottom:1px')); 
            //echo $form->textField($model, 'parent_id', array('class' => 'form-control', 'style' => 'width:250px;height:30px;margin-bottom:1px')); ?>
            <span class="errorMessage3"></span>
        </div>
        <div>
            <label>&nbsp;</label>
<?php echo CHtml::Button('Add', array('class' => 'addbutton_form btn green')); ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
</div>



<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tool-category-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array('id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => 'Previous ',
        'nextPageLabel' => 'Next '),
    'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('style' => 'width:20px;'),),
        array(
            'name' => 'cat_id',
            'headerHtmlOptions' => array('style' => 'display:none'),
            'htmlOptions' => array('style' => 'display:none'),
            'filterHtmlOptions' => array('style' => 'display:none'),
        //'visible' => Yii::app()->user->role == 1,
        //'htmlOptions' => array('style' => 'display:none'),
        ),
        array(
            'name' => 'cat_name',
            'htmlOptions' => array('class' => 'editable', 'title' => 'Update')
        ),
        array(
            'name' => 'cat_code',
            'htmlOptions' => array('class' => 'editable1', 'title' => 'Update')
        ),
        array(
            'name' => 'parent_id',
            'value' => 'is_null($data->parent_id)? "ROOT": $data->parentId->cat_name',
            'filter'      => CHtml::dropDownList( 'ToolCategory[parent_id]', $model->parent_id,  $testmodel, array( 'empty' => 'All' )
            ),
        ),
        /* 'created_by',
          'created_date',
          'updated_by',
          'updated_date', */
        /*array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:60px;'),
            'template' => '{deletecat}',
            'buttons' => array(
                'deletecat' => array(
                    'label' => 'Delete',
                    'url' => 'Yii::app()->createUrl("toolCategory/delete", array("id"=>$data->cat_id))',
                    'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                    'options' => array('class' => 'deleteicon'),
                ),
            ),
        ),*/
    ),
));
?>
</div>


<?php
Yii::app()->clientScript->registerScript('myjquery', ' 
		
$(document).ready(function () {
	
        
$(".addbutton_form").on("click", function () {
	//alert("hi");
    var data=$("#tool-category-form").serialize();
	//alert(data);
    $.ajax({
    type: "POST",
	dataType: "JSON",
    url:"' . Yii::app()->createUrl('toolCategory/create') . '",
    data:data,
    success:function(response){
					
					if(response == null){
						 //window.parent.$.fn.yiiGridView.update("unit-grid");
						 
						  location.reload();
						
					 }
					 else{
                                         
						var obj = eval(response);
						$(".errorMessage").text(obj[\'cat_name\']);
                                                $(".errorMessage2").text(obj[\'cat_code\']);
					}
				
					
					
             },
    
 

   });
 
});

var flag = 0;

jQuery("#tool-category-grid table tbody tr .editable").click(function(){ 
if(flag == 0){
flag = 1;
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   var label = $(this);
		   label.after("<input class=\'cat_name singletextbox\' type = \'text\' style = \'display:none\' name=\'cat_name\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  
		  
		     
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var value = $(".cat_name").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/update') . '",
				data:{id:id,value: value},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj[\'cat_name\']);
							}
						 },

			   });
			
			});   
		   
            }
         });
         
     $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					
					
					location.reload();
				}
				
				else
				{
					alert("Category is already in use,Cannot delete! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
		
jQuery("#tool-category-grid table tbody tr .editable1").click(function(e){
if(flag == 0){
flag = 1;
		   //alert("hi");
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   //alert(id);
		   var label = $(this);
		   label.after("<input class=\'cat_code singletextbox\' type = \'text\' style = \'display:none\' name=\'cat_code\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn green btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage3\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  
		  
		     
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("tool-category-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var cat_code = $(".cat_code").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"' . Yii::app()->createUrl('toolCategory/update') . '",
				data:{id:id,cat_code: cat_code},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage3").text(obj[\'cat_code\']);
							}
						 },

			   });
			
			});   
		   
		}   
         });
         
    
		

	});
        
      
   ');
?> 

