<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-form',
                'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

<!--    <p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php //echo $form->errorSummary($model); ?>


    <div class="row">
         <div class="subrow">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="subrow">
        <?php echo $form->labelEx($model, 'client_id'); ?>
        <?php echo $form->dropDownList($model, 'client_id', CHtml::listData(Clients::model()->findAll(array('order' => 'name ASC')), 'cid', 'name'), array('empty' => '--', 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'client_id'); ?>
    </div> 
    </div>
     
    <div class="row">
         <div class="subrow">
        <?php echo $form->labelEx($model, 'billable'); ?>
        <div class="radio_btn">
            <?php
            echo $form->radioButtonList($model, 'billable', CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="yesno"',
                                        'order' => 'caption',
                                        'distinct' => true
                            )), 'sid', 'caption'), array(  'labelOptions'=>array('style'=>'display:inline'),'separator' => ''));
            ?>
        </div>
        <?php echo $form->error($model, 'billable'); ?>
    </div>  
    
    <div class="subrow">
        <?php echo $form->labelEx($model, 'status'); ?>
        <div class="radio_btn">
            <?php
            echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="active_status"',
                                        'order' => 'caption',
                                        'distinct' => true
                            )), 'sid', 'caption'), array(  'labelOptions'=>array('style'=>'display:inline'),'separator' => ''));
            ?>
        </div>
        <?php echo $form->error($model, 'status'); ?>
    </div>  
    </div>
    <div class="row">
        <div class="subrow">
        <?php echo $form->labelEx($model, 'budget'); ?>
        <?php echo $form->textField($model, 'budget', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'budget'); ?>
    </div>
    </div>
    <div class="row">
        <div class="subrow subrowlong">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('class'=>'form-control','rows' => 3)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>
    <br/>
    <div class="row">

        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue')); ?>
        <?php echo CHtml::resetButton('Reset', array('class' => 'btn default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    #content{
        width: 500px; 
        /*border: 1px solid #ddd;*/
    }   
    div.form .row{ 
        width: 500px;
    }
/*    .row{
        margin-left: -15px;
        margin-right: -15px;
        width: 500px;
    }*/
    .subrow{
        width: 50%;
        float:left;
    }
    form .subrow {
        padding: 0 12px !important;
    }
    div.form input, div.form textarea, div.form select {
        margin: 0.2em 0 0.5em 0;
    }
    .subrowlong{
        width: 500px;
        float:left;
    }
    .blue.btn {
        margin-left: 30px;
    }
    

</style>
