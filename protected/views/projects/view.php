<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->name,
);

$this->menu = array(
//    array('label' => 'List Projects', 'url' => array('index')),
//    array('label' => 'Create Projects', 'url' => array('create')),
//    array('label' => 'Update Projects', 'url' => array('update', 'id' => $model->pid)),
//    array('label' => 'Delete Projects', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->pid), 'confirm' => 'Are you sure you want to delete this item?')),
//    array('label' => 'Manage Projects', 'url' => array('admin')),
);
?>

<h1>Project #<?php echo $model->pid; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(

        'name',
//        array(
//            'name' => 'client_id',
//            'type' => 'raw',
//            'value' => $model->client->name,
//        ),
        array(
            'name' => 'billable',
            'type' => 'raw',
            'value' => $model->billable0->caption,
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => $model->status0->caption,
        ),
        'budget',
        'description',
        'created_date',
        array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => $model->createdBy->first_name,
        ),

    ),
));
?>
<br/><br/>

<!--<h2><span> Payment Report</span></h2>-->
<hr/>

<table cellpadding="3" cellspacing="0" class="timehours" style=display:none;>
    <thead>
        <tr class="tr_title">
            <th width="50">Slno</th>
            <th width="550">Invoice Title</th><th width="100">Invoice No</th>
            <th width="100">Payment Date</th><th width="400">Remarks</th><th width="50">Project Amount</th><th width="50">Remaining/Balance Amount</th><th width="50">Payment status</th><th width="50">Amount Paid</th>
        </tr>
    </thead>
   <?php
 if(!empty($getdata)){
?>
    <?php
    $k = 1;
  
   foreach($getdata as $pro){
       $balamnts= $pro['balance_amnt'];
        if($balamnts >= 0){
          
           $retvals="<span style='color:red'>$".$balamnts."</span>";

        $paystatus="Partial";
            } 
            else if($balamnts == 0){
                 $paystatus="Paid";
            }
             
            else {
           $x=  str_replace("-","+$", $balamnts);
           
           $retvals="<span style='color:green'>".$x."</span>";
            $paystatus="Paid";
       }
     
    
        ?>
        <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
            <td><?php echo $k;?></td>
            <td><?php echo Invoice::model()->findByAttributes(array('invoice_id'=>$pro['invid']))->invoice_title; ?></td>
            <td><?php echo Invoice::model()->findByAttributes(array('invoice_id'=>$pro['invid']))->invoice_no; ?></td>
            <td><?php echo $pro['paymentadded_date']; ?></td>
             <td><?php echo $pro['remarks']; ?></td>
            <td><?php echo "$".$pro['project_total_amnt']; ?></td>
             <td><?php echo $retvals; ?></td>
             <td><?php echo $paystatus; ?></td>
            <th class="totaltr"><?php echo "$".$pro['paid_amnt']; ?></th>
           
         
            <!--<td width="72"><?php //echo CHtml::ajaxLink('Delete', array("cdelete", "id" => $tdentry->teid), array('complete' => 'js:function(request){$("#yt1").click();}')); ?> </td>-->
        </tr>

        <?php
        $k++;
    }
    ?>
        
    <tr class="totaltr">
        <td colspan="7" class="tbl_title" >&nbsp;</td>
        <th width="200px">Total Invoice Amount<br/>Balance Amount<br/></th>
        <th><?php echo "$".Invoice::model()->getprjctamnt($pid);?><br />
            <?php echo  "$".number_format((Invoice::model()->getprjctamnt($pid) - Invoice::model()->getinnerprjct($pid)), 2);?><br />
            <?php //echo number_format($total_hours, 2); ?>
        </th>
    </tr>
    <?php
 }
?>
</table>





<style type="text/css">
    th.button-column{
        width:120px !important;
    }    
</style>



