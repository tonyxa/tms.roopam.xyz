<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->name=>array('view','id'=>$model->pid),
	'Update',
);

 
?>

<h2>Update Project: <span><?php echo $model->name; ?></span></h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>