<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Projects', 'url'=>array('index')),
//	array('label'=>'Manage Projects', 'url'=>array('admin')),
);
?>

 
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>