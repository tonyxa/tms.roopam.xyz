<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);

if(yii::app()->user->role<=2){
	$this->menu = array(
//	    array('label' => 'Create Projects', 'url' => array('create')),
//	    array('label' => 'Manage Projects', 'url' => array('admin')),
	);
}
?>

<div class="clearfix">
    <div class="add link pull-right">
        <?php
        $createUrl = $this->createUrl('addproject', array("asDialog" => 1, "gridId" => 'address-grid'));
        echo CHtml::link('Add Project', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        ?>
    </div>  
    <h1>Projects</h1>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate'=>false,
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
   'nextPageLabel'=>'Next ' ),
     
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
    'columns' => array(
        array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
      array(
        'name'  => 'name',
        'value' => 'CHtml::link($data->name, Yii::app()->createUrl("projects/view",array("id"=>$data->pid)))',
        'type'  => 'raw',
         'filter'=> $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'Projects[name]',
                'source'=>$this->createUrl('Projects/autocomplete'),
                'value' => isset($model->name) ? $model->name: "",
                'options'=>array(
                    'focus'=>'js:function(event, ui) { 
                       $("#Projects_name").val(ui.item.value);
                    }',
                    'minLength'=>'1',
                    'showAnim'=>'fold',
                    'select'=>'js:function(event, ui) {  $("#Projects_name").val(ui.item.value); }'
               
                   ),
                 ),true),
    ),
       array(
            'name' => 'client_id',
            'value' => 'isset($data->client) ? $data->client->name :  "N/A"',
            'type' => 'raw',
            'filter' => CHtml::listData(Clients::model()->findAll(
                            array(
                                'select' => array('cid,name'),
                                'order' => 'name',
                    )), "cid", "name"),
            'type'  => 'raw',
            'visible'=>(yii::app()->user->role==1),
        ),
          
        array(
            'name' => 'billable',
            'value' => '$data->billable0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="yesno"',
                                'order' => 'status_type',
                    )), "sid", "caption")
        ),
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                    )), "sid", "caption")
        ),
        array(
            'name' => 'budget',
            'htmlOptions' => array('width' =>100),
        ),
        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */
        array(
            'htmlOptions' => array('style'=>'width:80px;'),
            'class' => 'CButtonColumn',
              'template' => '{view}{update}'.((isset(Yii::app()->user->role) && Yii::app()->user->role==1 )?"{delete}": ""),
             
            'buttons'=>array(
                'view' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                   ),
                'update' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit',),
                   ),
                   'delete'=>array(
                       'label'=>'',
                       'imageUrl' =>false,
                       'options' => array('class' => 'icon-trash icon-comn','title'=>'Delete',),
                       'visible'=> '$data->checkdelete($data->pid) ? true: false'
                    )
                 )
                    
        ),
    ),
));
?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add project',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  style="min-height:400px;"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );

');


?>




