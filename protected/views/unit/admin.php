<?php
/* @var $this UnitController */
/* @var $model Unit */

$this->breadcrumbs=array(
	'Units'=>array('index'),
	'Manage',
);

/*
$this->menu=array(
	array('label'=>'List Unit', 'url'=>array('index')),
	array('label'=>'Create Unit', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('unit-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
 
 */
?>

<h1>Manage Units</h1>



<div class="">
<div class="table-responsive">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'unit-form',
         'enableAjaxValidation'=>false,
       
       /* 'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,), */
    ));
    ?>
		<div class="addelement">
                    <div>
                        <?php echo $form->labelEx($model,'unitname'); ?>
                        <?php echo $form->textField($model,'unitname', array('class' => 'form-control','style' => 'width:250px;margin-bottom:1px')); ?>
                        <span class="errorMessage"></span>
                    </div>
                    <div>
                            <label>&nbsp;</label>
                    <?php  echo CHtml::Button('Add',array('class'=>'addbutton_form btn blue btn-sm')); ?>
                    </div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
		



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unit-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-bordered',
       // 'rowCssClassExpression'=>'$data->unitname == "Set" ? "hiddenclass" : "normalclass"',
        'rowCssClassExpression'=>'"normalclass"',
	'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
        'nextPageLabel'=>'Next ' ),    
        'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
	'filter'=>$model,
	'columns'=>array(
            array('class' => 'IndexColumn', 'header' => 'S.No.','htmlOptions'=>array('style'=>'width:20px;'),),
		
			array(
			'name' => 'id',
			'headerHtmlOptions' => array('style' => 'display:none'),
			'htmlOptions' => array('style' => 'display:none'),
			'filterHtmlOptions' => array('style' => 'display:none'),
			//'visible' => Yii::app()->user->role == 1,
			//'htmlOptions' => array('style' => 'display:none'),
		
		
			),
			array(
			'name' => 'unitname',
			'htmlOptions' => array('class' => 'editable','title' => 'Update')
			),
			/*'created_by',
			'created_date',
			'updated_by',
			'updated_date',
					 * 
					 */
			array(
			'class'=>'CButtonColumn',
			'htmlOptions'=>array('style'=>'width:60px;'),
			//'template' => '{deleteunit}',
                        'template' => '{Make Inactive}{Make Active}',    
			'buttons' => array(			
			
			'deleteunit' => array(
			'label' => 'Delete',
                        'url'=>'Yii::app()->createUrl("unit/delete", array("id"=>$data->id))',
			'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'deleteicon'),                   
			),
			
			/* Active section starts from here */    
                        'Make Active' => array(			    
                        'url'=>'Yii::app()->createUrl("unit/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-success'), 
                        'visible' => '($data->active_status == 0)',    
			),
			/* Active section end here */
                            
                        /* Inactive section starts from here */    
                        'Make Inactive' => array(			    
                        'url'=>'Yii::app()->createUrl("unit/changestatus", array("id"=>$data->id))',
			//'imageUrl' => Yii::app()->theme->baseUrl . '/assets/admin/layout3/img/delete.jpg',
                        'options' => array('class' => 'changestatusicon btn btn-xs btn-warning'),  
                        'visible' => '($data->active_status == 1)',     
			),
			/* Inactive section end here */  	
             
              
            ),
		), 
	),
)); ?>
</div>
 
 
 

<?php

Yii::app()->clientScript->registerScript('myjquery', ' 
		
$(document).ready(function () {
	
        
$(".addbutton_form").on("click", function () {
	//alert("hi");
    var data=$("#unit-form").serialize();
	//alert(data);
    $.ajax({
    type: "POST",
	dataType: "JSON",
    url:"'.Yii::app()->createUrl('unit/create').'",
    data:data,
    success:function(response){
					
					if(response == null){
						 //window.parent.$.fn.yiiGridView.update("unit-grid");
						 
						  location.reload();
						
					 }
					 else{

						var obj = eval(response);
						$(".errorMessage").text(obj);
					}
				
					
					
             },
    
 

   });
 
});



jQuery("#unit-grid table tbody tr .editable").click(function(){
		   var id = $(this).closest("tr").find("td:eq(1)").text();
		   var label = $(this);
		   label.after("<input class=\'unit_name singletextbox\' type = \'text\' style = \'display:none\' name=\'unitname\' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=\'save btn blue btn-xs\'>Save</a>&nbsp;&nbsp;&nbsp;<a class=\'cancel btn default btn-xs\'>Cancel</a><br><span class=\'errorMessage1\'></span>");
		   var textbox = $(this).next();
		   textbox.val(label.html());
			   $(this).hide();
			   $(this).next().show();
		  jQuery(".cancel").click(function(){
				//window.parent.$.fn.yiiGridView.update("unit-grid");
				location.reload();
				
			
			}); 
		  
		 
		  jQuery(".save").click(function(){
		  
				var value = $(".unit_name").val();
				
				$.ajax({
				type: "POST",
				dataType: "json",
				url:"'.Yii::app()->createUrl('unit/update').'",
				data:{id:id,value: value},
				success:function(response){
							
							if(response == null){
							location.reload();
							}
							else{
							//alert("hi");
							var obj = eval(response);
							$(".errorMessage1").text(obj);
							}
						 },

			   });
			
			});   
		   
		   
         });
         
     $(".deleteicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		
		var answer = confirm ("Are you sure you want to delete?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){
					
					//alert("Unit Deleted");
					location.reload();
				}
				
				
				else
				{
					alert("Unit is already in use,Cannot delete! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
		
//active status script starts from here
$(".changestatusicon").on("click", function (event) {
			
		event.preventDefault();
		var url = $(this).attr("href");
		//alert(url);
		var answer = confirm ("Are you sure you want to change the status?");
		if (answer)
		{
			$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			success: function (response)
			{
				if(response.response == "success"){					
					
					location.reload();
				}
				else
				{
					alert("Cannot change the status! ");
				}  
				
						   }
					   });
					   
			}
				  
       });
//active status script end here
		

	});
      
   ');
?> 
<style>
    .hiddenclass{
        display: none;
    } 
    .addbutton_form{margin-top:20px;}
</style>
    