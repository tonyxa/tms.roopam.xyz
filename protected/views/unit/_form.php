<?php
/* @var $this UnitController */
/* @var $model Unit */
/* @var $form CActiveForm */
?>

<?php /*
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unit-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

*/
?>



<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'unit-form',
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


<div class="clearfix">
    <div class="row addRow">
        <div class="col-md-12">   
            <?php echo $form->labelEx($model,'unitname'); ?>
		<?php echo $form->textField($model,'unitname',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'unitname'); ?>
        </div>
      
    
    <br />

    <div class="modal-footer save-btnHold col-md-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn green')); ?> 
        <button data-dismiss="modal" class="btn default"  onclick="javascript:window.location.reload()">Close</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
</div><!-- form -->