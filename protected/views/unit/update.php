<?php
/* @var $this UnitController */
/* @var $model Unit */

$this->breadcrumbs=array(
	'Units'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

//$this->menu=array(
//	array('label'=>'List Unit', 'url'=>array('index')),
//	array('label'=>'Create Unit', 'url'=>array('create')),
//	array('label'=>'View Unit', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage Unit', 'url'=>array('admin')),
//);
?>

<!--<h1>Update Unit <?php //echo $model->id; ?></h1>-->






<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Update Unit</h4>
        </div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>

</div>