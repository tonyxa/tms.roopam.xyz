<?php
/* @var $this UnitController */
/* @var $model Unit */

$this->breadcrumbs=array(
	'Units'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Unit', 'url'=>array('index')),
	array('label'=>'Manage Unit', 'url'=>array('admin')),
);
?>

<h1>Create Unit</h1>


<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Unit</h4>
        </div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
   </div>

</div>

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>