<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'clients-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>


    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="subrow">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
   
    <div class="subrow">
        <?php echo $form->labelEx($model, 'nick_name'); ?>
        <?php echo $form->textField($model, 'nick_name', array('class'=>'form-control')); ?>
        <?php echo $form->error($model, 'nick_name'); ?>
    </div>
    </div>
    <div class="row">
        <div class="subrow">
        <?php echo $form->labelEx($model, 'project_type'); ?>
        <?php echo $form->dropDownList($model, 'project_type', CHtml::listData(ProjectType::model()->findAll(array('order' => 'project_type ASC')), 'ptid', 'project_type'), array('empty' => '--', 'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'project_type'); ?>
    </div>

    <div class="subrow">
        <?php echo $form->labelEx($model, 'status'); ?>
        <div class="radio_btn">
            <?php
            echo $form->radioButtonList($model, 'status', CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'caption',
                                'distinct' => true
                    )),'sid','caption') , array(  'labelOptions'=>array('style'=>'display:inline'),'separator' => ''));
            ?>
        </div>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    </div>
  
    <div class="row">
        <div class="subrow subrowlong">
         
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6,'class'=>'form-control')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>        
    </div>
 

<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn blue')); ?>
        <?php echo CHtml::button('Cancel',array('class' => 'btn default','onclick'=>"window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    #content{
        width: 500px; 
        /*border: 1px solid #ddd;*/
    }   
    div.form .row{ 
        width: 500px;
    }
    .subrow{
        width: 50%;
        float:left;
    }
    form .subrow {
        padding: 0 12px !important;
    }
    div.form input, div.form textarea, div.form select {
        margin: 0.2em 0 0.5em 0;
    }
    .subrowlong{
        /*        width: 500px;
                float:left;*/
    }
    .radio_btn label {
        display: inline-block !important;
        margin-right: 15px;
    }
    .blue.btn {
        margin-left: 30px;
    }
        


</style>
