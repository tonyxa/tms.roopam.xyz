<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->name=>array('view','id'=>$model->cid),
	'Update',
);

 
?>

<h2>Update Client: <span><?php echo $model->name; ?></span></h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
