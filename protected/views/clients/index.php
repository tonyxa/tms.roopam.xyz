
<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients',
);

if(yii::app()->user->role<=2){
	$this->menu = array(
//	    array('label' => 'Create Projects', 'url' => array('create')),
//	    array('label' => 'Manage Projects', 'url' => array('admin')),
	);
}

?>
<div class="clearfix">
    <div class="add link pull-right">
        <?php
        $createUrl = $this->createUrl('addclient', array("asDialog" => 1, "gridId" => 'address-grid'));
        echo CHtml::link('Add Client', '', array('class' => 'btn blue', 'onclick' => "$('#cru-frame').attr('src','$createUrl '); $('#cru-dialog').dialog('open');"));
        ?>
    </div>
    <h1>Clients</h1>
</div>

<?php 
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate'=>false,
    'itemsCssClass' => 'table table-bordered',
    'filter' => $model,
    'pager' => array( 'id'=>'dataTables-example_paginate',  'header' => '', 'prevPageLabel'=>'Previous ',
   'nextPageLabel'=>'Next ' ),
     
    'pagerCssClass'=>'dataTables_paginate paging_simple_numbers', 
    'columns' => array(
        array(
            'header' => 'S.No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),
       array('name'=>'cid', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')), 
       array(
        'name'  => 'name',
        'value' => 'CHtml::link($data->name, Yii::app()->createUrl("clients/view",array("id"=>$data->cid)))',
        'type'  => 'raw',
        'visible'=>(yii::app()->user->role==1),
        'filter'=> $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'Clients[name]',
                'source'=>$this->createUrl('Clients/autocomplete'),
                'value' => isset($model->name) ? $model->name: "",
                'options'=>array(
                    'focus'=>'js:function(event, ui) { 
                       $("#Clients_name").val(ui.item.value);
                    }',
                    'minLength'=>'1',
                    'showAnim'=>'fold',
                    'select'=>'js:function(event, ui) {  $("#Clients_name").val(ui.item.value); }'
               
                   ),
                 ),true),

    ),
          
        'nick_name',
	
        array(
            'name' => 'project_type',
            'value' => '$data->projectType->project_type',
            'type' => 'raw',
            'filter' => CHtml::listData(ProjectType::model()->findAll(
                            array(
                                'select' => array('ptid,project_type'),
                                'order' => 'project_type',
                                'distinct' => true
                    )), "ptid", "project_type")
        ),
        'description',
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                    )), "sid", "caption")
        ),
       

        array(
            'name'=>'created_date',
            //'value'=>'Yii::app()->dateFormatter->format("Y-m-d",strtotime($data->created_date))',
            'filter'=> $this->widget('zii.widgets.jui.CJuiDatePicker', array(
              'model'=>$model,
              'attribute'=>'created_date',
              'htmlOptions' => array(
              'id' => 'created_date'
              ),
              'options' => array(
              'showOn' => 'focus',
              'dateFormat' => 'yy-mm-dd',
               )
             ), true)
            
        ),
        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}',
            'buttons'=>array(
                'view' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-eye icon-comn','title'=>'View'),
                   ),
                'update' => array(
                    'label'=>'',
                    'imageUrl' =>false,
                    'options' => array('class' => 'icon-pencil icon-comn','title'=>'Edit',),
                   ),
           ), 
        ),
    ),
));
?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'cru-dialog',
    'options' => array(
        'title' => 'Add Client',
        'autoOpen' => false,
        'modal' => false,
        'width' => 590,
        'height' => "auto",
    ),
));
?>
<iframe id="cru-frame" width="550" height="auto" frameborder="0"  style="min-height:325px;"></iframe>

<?php
$this->endWidget();
?>
<div id="id_view"></div>

<?php
    Yii::app()->clientScript->registerScript('myjavascript', '
    
    $( function() {

         (function($) {
            if (!$.curCSS) {
            $.curCSS = $.css;
        }
        })(jQuery);

        jQuery.fn.extend({
        propAttr: $.fn.prop || $.fn.attr
        });
   
   
  } );

');


?>


