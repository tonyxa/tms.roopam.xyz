<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Create',
);

?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>