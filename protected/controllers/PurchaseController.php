<?php


class PurchaseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','create1','update','update1','purchaselist','toollist','uniqunes','addpurchase','updatepurchase'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','deleteitem','chkpnoexists','changestatus'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$newmodel = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$id'"));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'newmodel' => $newmodel,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //$this->layout = false;
		$model=new Purchase;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Purchase']))
		{

      $model->attributes		= $_POST['Purchase'];
			$model->created_by 		= Yii::app()->user->id;
			$model->created_date 	= date('Y-m-d');
			$model->updated_by 		= Yii::app()->user->id;
			$model->updated_date 	= date('Y-m-d');
			$model->attributes=$_POST['Purchase'];

			if($model->save()){

               //echo sizeof($sl_No);
                $insert_id = Yii::app()->db->getLastInsertID();

                /*
                $sl_No = $_POST['unit'];
                for ($i = 0; $i < sizeof($sl_No); $i++) {

                        $purchase_id = $insert_id;
                        $item_code = 0;//$_POST['item_code'];
                        $item_name = $_POST['item_name'];
                        $unit      = $_POST['unit'];
                        $model_no  = $_POST['model_no'];
                        $batch_no  = $_POST['batch_no'];
                        $make      = $_POST['make'];
                        $serial_no = $_POST['serial_no'];
                        $qty       = $_POST['qty'];
                        $rate      = $_POST['rate'];
                        $tax       = $_POST['tax'];
                        $amount    = $_POST['amount'];

                        $newmodel = new PurchaseItems;
                        $newmodel->purchase_id = $insert_id;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name[$i];
                        $newmodel->unit = $unit[$i];
                        $newmodel->model_no  = $model_no[$i];
                        $newmodel->batch_no  = $batch_no[$i];
                        $newmodel->make = $make[$i];
                        $newmodel->serial_no = $serial_no[$i];
                        $newmodel->qty = $qty[$i];
                        $newmodel->rate = $rate[$i];
                        $newmodel->tax = $tax[$i];
                        $newmodel->amount = $amount[$i];
                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;
                        $newmodel->remaining_qty = $qty[$i];
                        if(!empty($insert_id) && !empty($item_name[$i]) && !empty($unit[$i]) && !empty($model_no[$i])
                                && !empty($batch_no[$i]) && !empty($make[$i]) && !empty($serial_no[$i]) && !empty($qty[$i])&&!empty($rate[$i])
                                && !empty($rate[$i]) && !empty($amount[$i])){
                        $newmodel->save();
                        }


                        //$insert_id = Yii::app()->db->getLastInsertID();
                        /*

                        $newmodel1 = new Tools;
                        $newmodel1->unsetAttributes();
                        $newmodel1->tool_code   =   0;//$item_code[$i];
                        $newmodel1->tool_name   =   $item_name[$i];
                        $newmodel1->tool_category=  '';
                        $newmodel1->unit        =   $unit[$i];
                        $newmodel1->make        =   $make[$i];
                        $newmodel1->model_no    =   $model_no[$i];
                        $newmodel1->serial_no   =   $serial_no[$i];
                        $newmodel1->ref_no      =   '';
                        $newmodel1->batch_no    =   $batch_no[$i];
                        $newmodel1->created_by 		= Yii::app()->user->id;
			$newmodel1->created_date 	= date('Y-m-d');
			$newmodel1->updated_by 		= Yii::app()->user->id;
			$newmodel1->updated_date 	= date('Y-m-d');
                        $statusmodel = Status::model()->findAll(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"));
                        $newmodel1->pending_status 	=   $statusmodel[0]['sid'];
                        $newmodel1->qty = $qty[$i];
                        $newmodel1->item_id             =   $insert_id;
                    if(!empty($item_name[$i]) && !empty($unit[$i]) && !empty($make[$i])
                    && !empty($model_no[$i]) && !empty($serial_no[$i]) && !empty($batch_no[$i])
                    && !empty($qty[$i]) && !empty($statusmodel[0]['sid']) && !empty($insert_id)){
                        $newmodel1->save();
                                }*/


                       // }

                        $this->redirect(array('admin'));
                        }
		}

                $res = Unit::model()->findAll(array('condition'=>'active_status = "1" and id!=1'));
               //print_r($res); die;
		$this->render('create',array(
			'model'=>$model,'unitRes'=>$res,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

    public function actionCreate1()
	{
                //$this->layout = false;
		$model=new Purchase;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                $data    = $_POST["result"];
                $data    = json_decode("$data", true);
				extract($data); 
				
							
				$unitRes = Unit::model()->findAll(array('condition'=>'active_status = "1" and id!=1'));
		if(isset($data))
		{			
                        $newmodel = new PurchaseItems;
                        $newmodel->purchase_id = $id;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name;
                        $newmodel->unit = $unit;
                        $newmodel->model_no  = $model_no;
                        $newmodel->batch_no  = $batch_no;
						$newmodel->make = $make;
						if(empty($serial_no)||($serial_no=="")||($serial_no==NULL)){
							$newmodel->serial_no=NULL;
						}else
						{
							$newmodel->serial_no = $newmodel->serial_no;
	
						}  
                        // $newmodel->serial_no = ($serial_no==NULL)?"":;
                        $newmodel->warranty_date = (!empty($warranty_date) ? $warranty_date : NULL);
                        $newmodel->qty = $qty;
                        $newmodel->rate = $rate;
                        $newmodel->tax = $tax;
                        $newmodel->amount = $amount;
						
                        $newmodel->prev_main = ($prev_main==1)?"1":"0";

						$newmodel->save();
                        if($prev_main==1){
                             $newmodel->prev_hrs  = isset($prev_hrs)?$prev_hrs:NULL;
                             //$newmodel->prev_date = date('Y-m-d H:i:s');
														 if($prev_hrs != NULL){
	 															$hours =  $prev_hrs/24;
	 															$days = (int)$hours;
	                             $newmodel->prev_date = date('Y-m-d H:i:s',strtotime("".$days." days"));
	 													}else{
	 														$newmodel->prev_date = NULL;
	 													}
                        }else{

                             $newmodel->prev_hrs  = NULL;
                             $newmodel->prev_date = NULL;
						}
						
                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;
                        $newmodel->remaining_qty = $qty;
//                        if(!empty($id) && !empty($item_name) && !empty($unit) && !empty($model_no)
//                                && !empty($batch_no) && !empty($make) && !empty($serial_no) && !empty($qty)&&!empty($rate)
//                                && !empty($rate) && !empty($amount)){


                        if($newmodel->save()){
												$ids = $newmodel->id;
                        //echo json_encode(array('status' =>1, 'id' => $id));
												$newmodel = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$id'"));
												$result = '';
												$i = 1;
										    foreach ($newmodel as $new) {
										        if (!empty($new['id']) && !empty($arr)) {
										            for ($k = 0; $k < count($arr); $k++) {
										                if ($new['id'] == $arr[$k]) {
										                    $read = 'readonly';
										                    break;
										                } else {
										                    $read = '';
										                }
										            }
										        } else {
										            $read = '';
										        }
														$result .='<tr class="itemtr">';
		                        $result .='<td>'.$i.'</td>';
		                        $result .='<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="piid_'.$i.'" value="'.$new['id'].'" ></td>';
		                        $result .='<td><input type="text" name="item_name[]" class="inputs form-control target" id="item_name_'. $i.'" value="'.$new['item_name'].'" '.$read.' /></td>';
		                        $result .='<td> <select class="inputs form-control target" name="unit[]" id="unit_'.$i.'" '.$read.'>
		                                    <option value="">Please choose</option>';
															        foreach ($unitRes as $uRes) {
															            if ($uRes['id'] == $new['unit']) {
															                $selected = 'selected';
															            } else {
															                $selected = '';
															            }
		                                        $result .='<option value="'.$uRes['id'].'" '.$selected.'>'.$uRes['unitname'].'</option>';
		                                    }
		                            $result .='</select></td>';
		                            $result .='<td><input type="text" name="model_no[]" class="inputs form-control target" id="model_no_'.$i.'" value="'.$new['model_no'].'" '.$read.' /></td>';
		                            $result .='<td><input type="text" name="batch_no[]" class="inputs form-control target" id="batch_no_'.$i.'" value="'.$new['batch_no'].'"  '.$read.' /></td>';
		                            $result .='<td><input type="text" name="make[]" class="inputs form-control target" id="make_'.$i.'" value="'.$new['make'].'" '.$read.' /></td>';
		                            $result .='<td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_'.$i.'" value="'.$new['serial_no'].'" '.$read.' /></td>';
		                            $result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$i.'" value="'.$new['warranty_date'].'" '.$read.'/></td>';
		                            $result .='<td><input type="text" name="qty[]" onkeyup="chkinteger(this.id);"  class="inputs form-control target qty" id="qty_'.$i.'"  value="'.$new['qty'].'"  '.$read.' /></td>';
		                            $result .='<td><input type="text" name="rate[]" onkeyup="chknumber1(this.id);" class="inputs form-control target rate" id="rate_'.$i.'" value="'.$new['rate'].'" '.$read.' /></td>';
		                            $result .='<td><input type="text" name="tax[]" onkeyup="chknumber1(this.id);" class="inputs form-control target tax" id="tax_'.$i.'" value="'.$new['tax'].'" '.$read.' /></td>';
		                            $result .='<td><input type="text" name="amount[]" readonly="true" class="inputs lst form-control amount" id="amount_'.$i.'" value="'.$new['amount'].'" '.$read.' /></td>';
																if($new['prev_main']==1){
																	$checked = 'checked';
																	$class = '';
																}else{
																	$checked = '';
																	$class = 'check_class';
																}
		                            $result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$i.'" value='.$new['prev_main'].'  '.$checked.'></td>';
		                            $result .='<input type="hidden" id="prev_db_'.$i.'" value="'.$new['prev_main'].'">';
																$result .='<td><input type="text" name="hrs[]" class="inputs form-control target hrs '.$class.'" value="'.$new['prev_hrs'].'"  id="hrs_'.$i.'"></td>';
		                            $result .='<td></td>';
		        										if ($read != 'readonly') {
		                                $result .='<td id="delete_'.$i.'"><input type="button" class="btn red btn-xs delitem" id="'.$new['id'].'" value="Delete"></td>';
		        										}
		                        $result .='</tr>';
														$i++;
												}
												$j = count($newmodel)+1;
												$result .='<tr class="itemtr">';
												$result .='<td>'.$j.'</td><td style="display:none"><input class="inputs form-control" name="ids[]" id="ids'.$j.'" type="hidden"></td>';
												$result .='<td><input class="inputs form-control target" name="item_name[]" id="item_name_'.$j.'" type="text"></td>';
												$result .='<td><select class="inputs form-control target" name="unit[]" id="unit_'.$j.'">';
													$result .='	<option value="">Please choose</option>';
													foreach ($unitRes as $uRes) {
																$result .='<option value="'.$uRes['id'].'">'.$uRes['unitname'].'</option>';
														}
														$result .='</select></td>';
												$result .='<td><input class="inputs form-control target" name="model_no[]" id="model_no_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs form-control target" name="batch_no[]" id="batch_no_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs form-control target" name="make[]" id="make_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs form-control target" name="serial_no[]" id="serial_no_'.$j.'" type="text"></td>';
												$result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$j.'" value="" /></td>';
												$result .='<td><input class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs form-control target rate" onkeyup="chknumber1(this.id);" name="rate[]" id="rate_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs form-control target tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_'.$j.'" type="text"></td>';
												$result .='<td><input class="inputs lst form-control amount" name="amount[]" id="amount_'.$j.'" readonly="true" type="text"></td>';
												$result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$j.'" value=""></td>';
												$result .='<td> <input name="hrs[]" class="inputs form-control hrs target" id="hrs_'.$j.'" style="display: none;" type="text"></td>';
												$result .='<td></td>';
												// $result .='<td><input class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_'.$j.'" type="text"></td>';

												$result .='<td><input class="btn green btn-xs lst1" data-id="'.$j.'" value="Create" type="button"></td>';
												$result .='</tr>';
												echo json_encode(array('status' =>1, 'id' => $ids,'html'=>$result));
                        }else{
                        echo json_encode(array('status' =>2));
                        }
                        //}
                        //echo "herre1"; die;
                        //$model->attributes		= $_POST['Purchase'];
			//$model->created_by 		= Yii::app()->user->id;
			//$model->created_date 	= date('Y-m-d');
			//$model->updated_by 		= Yii::app()->user->id;
			//$model->updated_date 	= date('Y-m-d');
			//$model->attributes=$_POST['Purchase'];
			//if($model->save()){
               //echo sizeof($sl_No);
                //$insert_id = Yii::app()->db->getLastInsertID();
                /*
                $sl_No = $_POST['unit'];
                for ($i = 0; $i < sizeof($sl_No); $i++) {
                        $purchase_id = $insert_id;
                        $item_code = 0;//$_POST['item_code'];
                        $item_name = $_POST['item_name'];
                        $unit      = $_POST['unit'];
                        $model_no  = $_POST['model_no'];
                        $batch_no  = $_POST['batch_no'];
                        $make      = $_POST['make'];
                        $serial_no = $_POST['serial_no'];
                        $qty       = $_POST['qty'];
                        $rate      = $_POST['rate'];
                        $tax       = $_POST['tax'];
                        $amount    = $_POST['amount'];

                        $newmodel = new PurchaseItems;
                        $newmodel->purchase_id = $insert_id;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name[$i];
                        $newmodel->unit = $unit[$i];
                        $newmodel->model_no  = $model_no[$i];
                        $newmodel->batch_no  = $batch_no[$i];
                        $newmodel->make = $make[$i];
                        $newmodel->serial_no = $serial_no[$i];
                        $newmodel->qty = $qty[$i];
                        $newmodel->rate = $rate[$i];
                        $newmodel->tax = $tax[$i];
                        $newmodel->amount = $amount[$i];
                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;
                        $newmodel->remaining_qty = $qty[$i];
                        if(!empty($insert_id) && !empty($item_name[$i]) && !empty($unit[$i]) && !empty($model_no[$i])
                                && !empty($batch_no[$i]) && !empty($make[$i]) && !empty($serial_no[$i]) && !empty($qty[$i])&&!empty($rate[$i])
                                && !empty($rate[$i]) && !empty($amount[$i])){
                        $newmodel->save();
                        }


                        //$insert_id = Yii::app()->db->getLastInsertID();
                        /*

                        $newmodel1 = new Tools;
                        $newmodel1->unsetAttributes();
                        $newmodel1->tool_code   =   0;//$item_code[$i];
                        $newmodel1->tool_name   =   $item_name[$i];
                        $newmodel1->tool_category=  '';
                        $newmodel1->unit        =   $unit[$i];
                        $newmodel1->make        =   $make[$i];
                        $newmodel1->model_no    =   $model_no[$i];
                        $newmodel1->serial_no   =   $serial_no[$i];
                        $newmodel1->ref_no      =   '';
                        $newmodel1->batch_no    =   $batch_no[$i];
                        $newmodel1->created_by 		= Yii::app()->user->id;
			$newmodel1->created_date 	= date('Y-m-d');
			$newmodel1->updated_by 		= Yii::app()->user->id;
			$newmodel1->updated_date 	= date('Y-m-d');
                        $statusmodel = Status::model()->findAll(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"));
                        $newmodel1->pending_status 	=   $statusmodel[0]['sid'];
                        $newmodel1->qty = $qty[$i];
                        $newmodel1->item_id             =   $insert_id;
                    if(!empty($item_name[$i]) && !empty($unit[$i]) && !empty($make[$i])
                    && !empty($model_no[$i]) && !empty($serial_no[$i]) && !empty($batch_no[$i])
                    && !empty($qty[$i]) && !empty($statusmodel[0]['sid']) && !empty($insert_id)){
                        $newmodel1->save();
                                }*/


                       // }

                        //$this->redirect(array('admin'));
                       // }
		}

               // $res = Unit::model()->findAll(array('condition'=>'active_status = "1"'));
               //print_r($res); die;
		//$this->render('create',array(
			//'model'=>$model,'unitRes'=>$res,
		//));
	}

     public function actionUpdate1()
	{
                $this->layout = false;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
				$data    = $_POST["result"];				
                $data    = json_decode("$data", true);
                extract($data);
		if(isset($data))
		{

                        $newmodel = PurchaseItems::model()->findByPk($id);
                        $newmodel->purchase_id = $pid;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name;
                        $newmodel->unit = $unit;
                        $newmodel->model_no  = $model_no;
                        $newmodel->batch_no  = $batch_no;
                        $newmodel->make = $make;
                        $newmodel->serial_no = $serial_no;
                        $newmodel->warranty_date = (!empty($warranty_date) ? $warranty_date : NULL);
                        $newmodel->qty = $qty;
                        $newmodel->rate = $rate;
                        $newmodel->tax = $tax;
						$newmodel->amount = $amount;
						
                        $newmodel->prev_main = ($data['prev_main']==1)?"1":"0";
						$newmodel->save();
                        if($prev_main == 1){
                            $newmodel->prev_hrs  = isset($prev_hrs)?$prev_hrs:NULL;
														if($prev_hrs != NULL){
															$hours =  $prev_hrs/24;
															$days = (int)$hours;
                            $newmodel->prev_date = date('Y-m-d H:i:s',strtotime("".$days." days"));
													}else{
														$newmodel->prev_date = NULL;
													}
                        }else{

                            $newmodel->prev_hrs  = NULL;
                            $newmodel->prev_date = NULL;
                        }



                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;
                        $newmodel->remaining_qty = $qty;
//                        if(!empty($id) && !empty($item_name) && !empty($unit) && !empty($model_no)
//                                && !empty($batch_no) && !empty($make) && !empty($serial_no) && !empty($qty)&&!empty($rate)
//                                && !empty($rate) && !empty($amount)){

                        if($newmodel->save()){
													$unitRes = Unit::model()->findAll(array('condition'=>'active_status = "1" and id!=1'));
													$newmodel = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
													$result = '';
													$i = 1;
											    foreach ($newmodel as $new) {
											        if (!empty($new['id']) && !empty($arr)) {
											            for ($k = 0; $k < count($arr); $k++) {
											                if ($new['id'] == $arr[$k]) {
											                    $read = 'readonly';
											                    break;
											                } else {
											                    $read = '';
											                }
											            }
											        } else {
											            $read = '';
											        }
															$result .='<tr class="itemtr">';
			                        $result .='<td>'.$i.'</td>';
			                        $result .='<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="piid_'.$i.'" value="'.$new['id'].'" ></td>';
			                        $result .='<td><input type="text" name="item_name[]" class="inputs form-control target" id="item_name_'. $i.'" value="'.$new['item_name'].'" '.$read.' /></td>';
			                        $result .='<td> <select class="inputs form-control target" name="unit[]" id="unit_'.$i.'" '.$read.'>
			                                    <option value="">Please choose</option>';
																        foreach ($unitRes as $uRes) {
																            if ($uRes['id'] == $new['unit']) {
																                $selected = 'selected';
																            } else {
																                $selected = '';
																            }
			                                        $result .='<option value="'.$uRes['id'].'" '.$selected.'>'.$uRes['unitname'].'</option>';
			                                    }
			                            $result .='</select></td>';
			                            $result .='<td><input type="text" name="model_no[]" class="inputs form-control target" id="model_no_'.$i.'" value="'.$new['model_no'].'" '.$read.' /></td>';
			                            $result .='<td><input type="text" name="batch_no[]" class="inputs form-control target" id="batch_no_'.$i.'" value="'.$new['batch_no'].'"  '.$read.' /></td>';
			                            $result .='<td><input type="text" name="make[]" class="inputs form-control target" id="make_'.$i.'" value="'.$new['make'].'" '.$read.' /></td>';
			                            $result .='<td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_'.$i.'" value="'.$new['serial_no'].'" '.$read.' /></td>';
			                            $result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$i.'" value="'.$new['warranty_date'].'" '.$read.'/></td>';
			                            $result .='<td><input type="text" name="qty[]" onkeyup="chkinteger(this.id);"  class="inputs form-control target qty" id="qty_'.$i.'"  value="'.$new['qty'].'"  '.$read.' /></td>';
			                            $result .='<td><input type="text" name="rate[]" onkeyup="chknumber1(this.id);" class="inputs form-control target rate" id="rate_'.$i.'" value="'.$new['rate'].'" '.$read.' /></td>';
			                            $result .='<td><input type="text" name="tax[]" onkeyup="chknumber1(this.id);" class="inputs form-control target tax" id="tax_'.$i.'" value="'.$new['tax'].'" '.$read.' /></td>';
			                            $result .='<td><input type="text" name="amount[]" readonly="true" class="inputs lst form-control amount" id="amount_'.$i.'" value="'.$new['amount'].'" '.$read.' /></td>';
																	if($new['prev_main']==1){
																		$checked = 'checked';
																		$class = '';																$style1="";
																	}else{
																		$checked = '';
																		$class = 'check_class';
																		$style1="display:none;";
																	}
			                            $result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$i.'" value='.$new['prev_main'].'  '.$checked.'></td>';
			                            $result .='<input type="hidden" id="prev_db_'.$i.'" value="'.$new['prev_main'].'">';
										$result .='<td><input type="text" name="hrs[]" class="inputs form-control target hrs " style="'.$style1.'" value="'.$new['prev_hrs'].'"  id="hrs_'.$i.'"></td>';
										$result .='<td></td>';									
			        										if ($read != 'readonly') {
			                                $result .='<td id="delete_'.$i.'"><input type="button" class="btn red btn-xs delitem" id="'.$new['id'].'" value="Delete"></td>';
			        										}
			                        $result .='</tr>';
															$i++;
													}
													$j = count($newmodel)+1;
													$result .='<tr class="itemtr">';
													$result .='<td>'.$j.'</td><td style="display:none"><input class="inputs form-control" name="ids[]" id="ids'.$j.'" type="hidden"></td>';
													$result .='<td><input class="inputs form-control target" name="item_name[]" id="item_name_'.$j.'" type="text"></td>';
													$result .='<td><select class="inputs form-control target" name="unit[]" id="unit_'.$j.'">';
														$result .='	<option value="">Please choose</option>';
														foreach ($unitRes as $uRes) {
																	$result .='<option value="'.$uRes['id'].'">'.$uRes['unitname'].'</option>';
															}
															$result .='</select></td>';
													$result .='<td><input class="inputs form-control target" name="model_no[]" id="model_no_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs form-control target" name="batch_no[]" id="batch_no_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs form-control target" name="make[]" id="make_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs form-control target" name="serial_no[]" id="serial_no_'.$j.'" type="text"></td>';
													$result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$j.'" value="" /></td>';
													$result .='<td><input class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs form-control target rate" onkeyup="chknumber1(this.id);" name="rate[]" id="rate_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs form-control target tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_'.$j.'" type="text"></td>';
													$result .='<td><input class="inputs lst form-control amount" name="amount[]" id="amount_'.$j.'" readonly="true" type="text"></td>';
													$result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$j.'" value=""></td>';
													// $result .='<td> <input name="hrs[]" class="inputs form-control hrs target" id="hrs_'.$j.'" style="display: none;" type="text"></td>';
												
													$result .='<td> <input name="hrs[]" class="inputs form-control hrs target" id="hrs_'.$j.'" style="display: none;" type="text"></td>';
													$result .='<td></td>';
													$result .='<td><input class="btn green btn-xs addbtn lst1" data-id="'.$j.'" value="Create" type="button"></td>';
													$result .='</tr>';
													echo json_encode(array('status' =>1, 'id' => $pid,'html'=>$result));
                        }else{
                        echo json_encode(array('status' =>2, 'id' => $pid));
                        }
                }
        }
	public function actionUpdate($id)
	{
               // $this->layout = false;

		$model=$this->loadModel($id);
                $newmodel = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$id'"));

                $arr = array();
                $qryres2 = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from('tms_purchase_items u')
                        ->where('purchase_id=:id', array(':id'=>$id))
                        ->queryAll();

               foreach ($qryres2 as $key => $value) {
               $itid = $value['id'];
               $qryres3 = Yii::app()->db->createCommand()
                        ->select('item_id')
                        ->from('tms_tools u')
                        ->where('item_id=:itid AND ref_value !="" ', array(':itid'=>$itid))
                        ->queryRow();

               if(!empty($qryres3['item_id'])){
               $arr[] = $qryres3['item_id'];
               }
               }



		// Uncomment the following line if AJAX validation is needed
                $res = Unit::model()->findAll(array('condition'=>'active_status = "1" and id!=1 '));

                $this->performAjaxValidation($model);


//                print_r($_POST);
//                die;
//
		if(isset($_POST['Purchase']))
		{


			$model->attributes=$_POST['Purchase'];
                        $model->updated_by = yii::app()->user->id;
						$model->updated_date = date('Y-m-d');
						
                        $ids       = isset($_POST['ids'])?$_POST['ids']:"";
                        $item_code = 0;//$_POST['item_code'];
                        $item_name = isset($_POST['item_name'])?$_POST['item_name']:"";
                        $unit      = isset($_POST['unit'])?$_POST['unit']:"";
                        $model_no  = isset($_POST['model_no'])?$_POST['model_no']:"";
                        $batch_no  = isset($_POST['batch_no'])?$_POST['batch_no']:"";
                        $make      = isset($_POST['make'])?$_POST['make']:"";
                        $serial_no = isset($_POST['serial_no'])?$_POST['serial_no']:"";
                        $warranty_date = isset($_POST['warranty_date'])?$_POST['warranty_date']:"";
                        $qty       = isset($_POST['qty'])?$_POST['qty']:"";
                        $rate      = isset($_POST['rate'])?$_POST['rate']:"";
                        $tax       = isset($_POST['tax'])?$_POST['tax']:"";
                        $amount    = isset($_POST['amount'])?$_POST['amount']:"";
                        $prev_main  = ($_POST['prev_main']==1)?"1":"0";

                        if(($_POST['prev_main']==1)){
                            $prev_hrs  = isset($_POST['prev_hrs'])?$_POST['prev_hrs']:NULL;
                            $prev_date = date('Y-m-d H:i:s');
                        }else{
                            $prev_hrs  = NULL;
                            $prev_date = NULL;
                        }




                        if($model->save()){
                             foreach($ids as $i=>$idnew){


                        if($idnew!=NULL)
                        {
                        if(isset($qty[$i])){
                        //$newmodel = ToolItems::model()->findAll(array("condition" => "tool_allocated_id = '$id'"));
                        $newmodel = PurchaseItems::model()->findByPk($idnew);

                        $oldqty = $newmodel->qty;
                        $newqty = $qty[$i];
                        $oldremainqty = $newmodel->remaining_qty;

                        if($newqty > $oldqty){
                        $balance =  $newqty - $oldqty;
                        $newmodel->qty = $qty[$i];
                        $newmodel->remaining_qty = $oldremainqty + $balance;
                        }else if($newqty < $oldqty){
                        $balance =  $oldqty - $newqty;
                        if($balance <= $oldremainqty ){
                        $newmodel->qty = $qty[$i];
                        $newmodel->remaining_qty  = $oldremainqty - $balance;
                        }
                        }else{
                        $newmodel->qty = $qty[$i];
                        }


                        $newmodel->purchase_id = $id;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name[$i];
                        $newmodel->unit = $unit[$i];
                        $newmodel->model_no  = $model_no[$i];
                        $newmodel->batch_no  = $batch_no[$i];
                        $newmodel->make = $make[$i];
                        $newmodel->serial_no = $serial_no[$i];
                        $newmodel->warranty_date = (!empty($warranty_date[$i]) ? $warranty_date[$i] : NULL);
                        $newmodel->rate = $rate[$i];
                        $newmodel->tax = $tax[$i];
                        $newmodel->amount = $amount[$i];
                        $newmodel->prev_main = $prev_main[$i];
                        $newmodel->prev_hrs = $prev_hrs[$i];
                        $newmodel->prev_date = $prev_date[$i];

                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;



                        $newmodel->save();

                         /*$qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_tools u')
                        ->where('item_id=:id', array(':id'=>$idnew))
                        ->queryRow();
                         $tid = $qryres1['id'];

//                        echo "<pre>";
//                        print_r($qryres1);
//                        die;
                        $newmodel1 = Tools::model()->findByPk($tid);
                        if($newmodel1!=NULL){
                            //$newmodel1->tool_code   =   0;//$item_code[$i];
                            $newmodel1->tool_name   =   $item_name[$i];
                            //$newmodel1->tool_category=  '';
                            $newmodel1->unit        =   $unit[$i];
                            $newmodel1->make        =   $make[$i];
                            $newmodel1->model_no    =   $model_no[$i];
                            $newmodel1->serial_no   =   $serial_no[$i];
                            //$newmodel1->ref_no      =   '';
                            $newmodel1->qty         =   $qty[$i];
                            $newmodel1->batch_no    =   $batch_no[$i];
                            $newmodel1->created_by 		= Yii::app()->user->id;
                            $newmodel1->created_date 	= date('Y-m-d');
                            $newmodel1->updated_by 		= Yii::app()->user->id;
                            $newmodel1->updated_date 	= date('Y-m-d');
    //                        $statusmodel = Status::model()->findAll(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"));
    //                        $newmodel1->pending_status 	=   $statusmodel[0]['sid'];
                            $newmodel1->save();
                            }*/
                        }
                        }else{
//                      echo "<pre>";
//                      print_r($_POST);die;
                        $newmodel = new PurchaseItems;
                        $newmodel->purchase_id = $id;
                        $newmodel->item_code = 0;//$item_code[$i];
                        $newmodel->item_name = $item_name[$i];
                        $newmodel->unit = $unit[$i];
                        $newmodel->model_no  = $model_no[$i];
                        $newmodel->batch_no  = $batch_no[$i];
                        $newmodel->make = $make[$i];
                        $newmodel->serial_no = $serial_no[$i];
                         $newmodel->warranty_date = (!empty($warranty_date[$i]) ? $warranty_date[$i] : NULL);
                        $newmodel->qty = $qty[$i];
                        $newmodel->rate = $rate[$i];
                        $newmodel->tax = $tax[$i];
                        $newmodel->amount = $amount[$i];
                        $newmodel->tool_status = Status::model()->find(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"))->sid;
                        $newmodel->remaining_qty = $qty[$i];

                        $newmodel->save();
                        /* $insert_id = Yii::app()->db->getLastInsertID();

                        $newmodel1 = new Tools;
                        $newmodel1->unsetAttributes();
                        $newmodel1->tool_code   =   0;//$item_code[$i];
                        $newmodel1->tool_name   =   $item_name[$i];
                        $newmodel1->tool_category=  '';
                        $newmodel1->unit        =   $unit[$i];
                        $newmodel1->make        =   $make[$i];
                        $newmodel1->model_no    =   $model_no[$i];
                        $newmodel1->serial_no   =   $serial_no[$i];
                        $newmodel1->ref_no      =   '';
                        $newmodel1->qty         =   $qty[$i];
                        $newmodel1->batch_no    =   $batch_no[$i];
                        $newmodel1->created_by 		= Yii::app()->user->id;
			$newmodel1->created_date 	= date('Y-m-d');
			$newmodel1->updated_by 		= Yii::app()->user->id;
			$newmodel1->updated_date 	= date('Y-m-d');
                        $statusmodel = Status::model()->findAll(array("condition" => "caption = 'Pending' AND status_type = 'pending_status'"));
                        $newmodel1->pending_status 	=   $statusmodel[0]['sid'];

                        $newmodel1->item_id             =   $insert_id;
                        $newmodel1->save();
                            */
                        }


                    }
                                         //echo "enter";
                                         //die;
                            $this->redirect(array('admin'));
                        }

		}


		$this->render('update',array(
			'model'=>$model,'unitRes'=>$res,'newmodel'=>$newmodel,'arr'=>$arr
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{


         $this->loadModel($id)->delete();
         // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	 if(!isset($_GET['ajax']))
	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

  public function actionDeleteitem($id)
	{
		$pid = $_POST['pid'];
		if($pid !=0){
    	if(PurchaseItems::model()->deleteByPk($id)){
				$unitRes = Unit::model()->findAll(array('condition'=>'active_status = "1"'));
				$newmodel = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
				$result = '';
				$i = 1;
				foreach ($newmodel as $new) {
						if (!empty($new['id']) && !empty($arr)) {
								for ($k = 0; $k < count($arr); $k++) {
										if ($new['id'] == $arr[$k]) {
												$read = 'readonly';
												break;
										} else {
												$read = '';
										}
								}
						} else {
								$read = '';
						}
						$result .='<tr class="itemtr">';
						$result .='<td>'.$i.'</td>';
						$result .='<td style="display:none"><input type="hidden" class="inputs form-control" name="ids[]" id="piid_'.$i.'" value="'.$new['id'].'" ></td>';
						$result .='<td><input type="text" name="item_name[]" class="inputs form-control target" id="item_name_'. $i.'" value="'.$new['item_name'].'" '.$read.' /></td>';
						$result .='<td> <select class="inputs form-control target" name="unit[]" id="unit_'.$i.'" '.$read.'>
												<option value="">Please choose</option>';
											foreach ($unitRes as $uRes) {
													if ($uRes['id'] == $new['unit']) {
															$selected = 'selected';
													} else {
															$selected = '';
													}
														$result .='<option value="'.$uRes['id'].'" '.$selected.'>'.$uRes['unitname'].'</option>';
												}
								$result .='</select></td>';
								$result .='<td><input type="text" name="model_no[]" class="inputs form-control target" id="model_no_'.$i.'" value="'.$new['model_no'].'" '.$read.' /></td>';
								$result .='<td><input type="text" name="batch_no[]" class="inputs form-control target" id="batch_no_'.$i.'" value="'.$new['batch_no'].'"  '.$read.' /></td>';
								$result .='<td><input type="text" name="make[]" class="inputs form-control target" id="make_'.$i.'" value="'.$new['make'].'" '.$read.' /></td>';
								$result .='<td><input type="text" name="serial_no[]" class="inputs form-control target" id="serial_no_'.$i.'" value="'.$new['serial_no'].'" '.$read.' /></td>';
								$result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$i.'" value="'.$new['warranty_date'].'" '.$read.'/></td>';
								$result .='<td><input type="text" name="qty[]" onkeyup="chkinteger(this.id);"  class="inputs form-control target qty" id="qty_'.$i.'"  value="'.$new['qty'].'"  '.$read.' /></td>';
								$result .='<td><input type="text" name="rate[]" onkeyup="chknumber1(this.id);" class="inputs form-control target rate" id="rate_'.$i.'" value="'.$new['rate'].'" '.$read.' /></td>';
								$result .='<td><input type="text" name="tax[]" onkeyup="chknumber1(this.id);" class="inputs form-control target tax" id="tax_'.$i.'" value="'.$new['tax'].'" '.$read.' /></td>';
								$result .='<td><input type="text" name="amount[]" readonly="true" class="inputs lst form-control amount" id="amount_'.$i.'" value="'.$new['amount'].'" '.$read.' /></td>';
								if($new['prev_main']==1){
									$checked = 'checked';
									$class = '';
								}else{
									$checked = '';
									$class = 'check_class';
								}
								$result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$i.'" value='.$new['prev_main'].'  '.$checked.'></td>';
								$result .='<input type="hidden" id="prev_db_'.$i.'" value="'.$new['prev_main'].'">';
								$result .='<td><input type="text" name="hrs[]" class="inputs form-control target hrs '.$class.'" value="'.$new['prev_hrs'].'"  id="hrs_'.$i.'"></td>';
								$result .='<td></td>';
								if ($read != 'readonly') {
										$result .='<td id="delete_'.$i.'"><input type="button" class="btn red btn-xs delitem" id="'.$new['id'].'" value="Delete"></td>';
								}
						$result .='</tr>';
						$i++;
				}
				$j = count($newmodel)+1;
				$result .='<tr class="itemtr">';
				$result .='<td>'.$j.'</td><td style="display:none"><input class="inputs form-control" name="ids[]" id="ids'.$j.'" type="hidden"></td>';
				$result .='<td><input class="inputs form-control target" name="item_name[]" id="item_name_'.$j.'" type="text"></td>';
				$result .='<td><select class="inputs form-control target" name="unit[]" id="unit_'.$j.'">';
					$result .='	<option value="">Please choose</option>';
					foreach ($unitRes as $uRes) {
								$result .='<option value="'.$uRes['id'].'">'.$uRes['unitname'].'</option>';
						}
						$result .='</select></td>';
				$result .='<td><input class="inputs form-control target" name="model_no[]" id="model_no_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs form-control target" name="batch_no[]" id="batch_no_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs form-control target" name="make[]" id="make_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs form-control target" name="serial_no[]" id="serial_no_'.$j.'" type="text"></td>';
				$result .='<td><input type="text" name="warranty_date[]" readonly="readonly" class="inputs form-control target datepicker" id="warranty_date_'.$j.'" value="" /></td>';
				$result .='<td><input class="inputs form-control target qty" onkeyup="chkinteger(this.id);" name="qty[]" id="qty_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs form-control target rate" onkeyup="chknumber1(this.id);" name="rate[]" id="rate_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs form-control target tax" onkeyup="chknumber1(this.id);" name="tax[]" id="tax_'.$j.'" type="text"></td>';
				$result .='<td><input class="inputs lst form-control amount" name="amount[]" id="amount_'.$j.'" readonly="true" type="text"></td>';
				$result .='<td><input type="checkbox" class="prev_main target" name="prev_main[]" id="prev_main_'.$j.'" value=""></td>';
				$result .='<td> <input name="hrs[]" class="inputs form-control hrs target" id="hrs_'.$j.'" style="display: none;" type="text"></td>';
				$result .='<td></td>';
				//$result .='<td><input class="btn red btn-xs delete" data-id="'.$j.'" value="Delete" type="button"></td>';
				$result .='</tr>';
				echo json_encode(array('status' =>1, 'id' => $pid,'html'=>$result));
			}else{
				echo json_encode(array('status' =>2));
			}
		}else{
			echo json_encode(array('status' =>3));
		}

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Purchase');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Purchase('search');
		$model->unsetAttributes();  // clear any default values


		if(isset($_GET['Purchase']))
			$model->attributes=$_GET['Purchase'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Purchase::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='purchase-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


          public function actionChkpnoexists(){

        $pno = $_REQUEST['pno'];
        $id = $_POST['val'];
        if(!empty($pno)){

            if ($id == 0) {
             $result = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_purchase')
                        ->where('purchase_no=:pno', array(':pno'=>$pno))
                        ->queryRow();
        if(!empty($result['purchase_no'])){
        echo "false";
        }else{
        echo "true";
        }
        } else {


              $result = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_purchase')
                        ->where('purchase_no=:pno and id!= :id', array(':pno'=>$pno,':id'=>$id))
                        ->queryRow();
        if(!empty($result['purchase_no'])){
        echo "false";
        }else{
        echo "true";
        }
        }
        }
        }
         public function actionChangestatus($id)
	{
                ////////////////////////////////////////select the current active status

                $active_status      = '';
                $new_status         = '';
                $criteria           = new CDbCriteria;
                $criteria->condition='id=:Id';
                $criteria->params   = array(':Id'=>$id);
                $model              = Purchase::model()->find($criteria);
                $active_status      = $model->active_status;
                if($active_status == 1)
                {
                    $new_status = 0;
                }
                else
                {
                    $new_status = 1;
                }
                ////////////////////////////////////////
                $site = Purchase::model()->findByPk($id);
                $site->active_status = $new_status;

		if($site->update())
		{
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{
 			echo json_encode(array('response'=> 'fail'));
 		}



	}
public function actionPurchaselist($id)
	{
        $finaldata = array();
        $tbl = Yii::app()->db->tablePrefix;
        $qryres2 = Yii::app()->db->createCommand()
               ->select('*')
               ->from("{$tbl}view_purchase_list u")
               ->where('id=:id', array(':id'=>$id))
               ->queryAll();

        $i = 0;
            foreach ($qryres2 as $key => $value) {
                extract($value);
                $finaldata[$i][]= $purchase_no;
                $finaldata[$i][]= $purchase_date;
                $finaldata[$i][]= $invoice_no;
                $finaldata[$i][]= $invoice_date;
                $finaldata[$i][]= $supplier_name;
                $finaldata[$i][]= $item_name;
                $finaldata[$i][]= $unit;
                $finaldata[$i][]= $model_no;
                $finaldata[$i][]= $batch_no;
                $finaldata[$i][]= $make;
                $finaldata[$i][]= $serial_no;
                $finaldata[$i][]= $qty;
                $finaldata[$i][]= $rate;
                $finaldata[$i][]= $tax;
                $finaldata[$i][]= $amount;
                $finaldata[$i][]= $warranty_date;
                $i++;
            }
        $arraylabel = array('Purchase No.','Purchase Date','Invoice No.','Invoice Date','Supplier Name','Item Name','Unit','Model No.','Batch No.','Make','Serial No.','Quantity','Rate','Tax','Amount','Warranty Date');
        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'purchase_list' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();

          }
    public function actionToollist($id)
    {
        $finaldata = array();
         $tbl = Yii::app()->db->tablePrefix;
        $qryres2 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from("{$tbl}view_tool_list u")
                        ->where('id=:id', array(':id'=>$id))
                        ->queryAll();
        $i = 0;
        foreach ($qryres2 as $key => $value) {
            extract($value);
            $finaldata[$i][]= $name;
            $finaldata[$i][]= $purchase_no;
            $finaldata[$i][]= $purchase_date;
            $finaldata[$i][]= $invoice_no;

            $finaldata[$i][]= $invoice_date;
            $finaldata[$i][]= $supplier_name;
            $finaldata[$i][]= $tool_name;
            $finaldata[$i][]= $cat_code;
            $finaldata[$i][]= $ref_no;
            $finaldata[$i][]= $unit;

            $finaldata[$i][]= $model_no;
            $finaldata[$i][]= $batch_no;
            $finaldata[$i][]= $make;
            $finaldata[$i][]= $serial_no;

            $finaldata[$i][]= $qty;
            $finaldata[$i][]= $rate;
            $finaldata[$i][]= $tax;
            $finaldata[$i][]= $amount;
            $finaldata[$i][]= $warranty_date;

            $i++;
        }

        $arraylabel = array('Site Name','Purchase No.','Purchase Date','Invoice No.','Invoice Date','Supplier Name','Item Name','Category Code','Tool Code','Unit','Model No.','Batch No.','Make','Serial No.','Quantity','Rate','Tax','Amount','Warranty Date');


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'tool_list' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();

    }

		public function actionuniqunes(){
	    $purchase_no = $_POST['Purchase']['purchase_no'];
			$data = Yii::app()->db->createCommand()
			 				->select('id')
			 				->from('tms_purchase')
			 				->where('purchase_no=:purchase_no', array(':purchase_no'=>$purchase_no))
			 				->queryRow();
				if(isset($data['id']) || $data['id'] != ''){
					echo "false";
				 exit;
				}else{
					echo "true";
				 exit;
				}
		}

		public function actionaddpurchase(){
			parse_str($_REQUEST['data'],$data);
			$id = $data['check_uniqueallocation'];
			if(isset($data['Purchase']))
			{
				if($id == 0){
						$model = new Purchase;
				}else{
					  $model = $this->loadModel($id);
				}

	      $model->attributes		= $data['Purchase'];
				if($id == 0){
					$model->created_by 		= Yii::app()->user->id;
					$model->created_date 	= date('Y-m-d');
				}
				$model->updated_by 		= Yii::app()->user->id;
				$model->updated_date 	= date('Y-m-d');
				if($model->save()){
	         $insert_id = Yii::app()->db->getLastInsertID();
					 echo json_encode(array('status'=>'success', 'id'=>$insert_id));
				}else{
					echo json_encode(array('status'=>'error', 'id'=>0));
				}
			}
		}

		public function actionupdatepurchase(){
			parse_str($_REQUEST['data'],$data);
			$id = $data['check_uniqueallocation'];
			if(isset($data['Purchase']))
			{
				if($id == 0){
						$model = new Purchase;
				}else{
					  $model = $this->loadModel($id);
				}

	      $model->attributes		= $data['Purchase'];
				if($id == 0){
					$model->created_by 		= Yii::app()->user->id;
					$model->created_date 	= date('Y-m-d');
				}
				$model->updated_by 		= Yii::app()->user->id;
				$model->updated_date 	= date('Y-m-d');
				if($model->save()){
	         $insert_id = Yii::app()->db->getLastInsertID();
					 echo json_encode(array('status'=>'success', 'id'=>$insert_id));
				}else{
					echo json_encode(array('status'=>'error', 'id'=>0));
				}
			}
		}
}
