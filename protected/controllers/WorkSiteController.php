<?php

class WorkSiteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','changestatus','AssigntoUser', 'AjaxReturn'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
		
		
		
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
            //echo "here"; 
		$this->layout = false;
		$model=new LocationType;
		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);
            if(isset($_POST['LocationType']))
            {
            
            
//                echo "<pre>";
//                print_r($_POST);
//                die;
            $model->attributes=$_POST['LocationType'];
            $model->name = $_POST['LocationType']['name'];
            $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption!=:title', array(':type'=>'location_type',':title'=>'Site'))           
                        ->queryRow();
             
            $model->location_type = $qryres1['sid'];
            $model->created_by = yii::app()->user->id;
            
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'location_type',':caption'=>'Site'))           
                        ->queryRow();
            $model->location_type = $qryres1['sid'];
                        
            if($model->validate() && $model->save())
			{
			$data = null;
			print_r(json_encode($data));
			}
			else
			{
			$error = $model->getErrors();
			print_r(json_encode($error['name']));
			}
			
		}

		
	}
        
        /* created by arun on 26-5-17 */
        public function actionChangestatus($id)
	{	
                ////////////////////////////////////////select the current active status               
                
                $active_status      = '';
                $new_status         = '';
                $criteria           = new CDbCriteria;     
                $criteria->condition='id=:Id';
                $criteria->params   = array(':Id'=>$id);
                $model              = LocationType::model()->find($criteria); 
                $active_status      = $model->active_status;
                if($active_status == 1)
                {
                    $new_status = 0;
                }
                else
                {
                    $new_status = 1;
                }               
                ////////////////////////////////////////
                $site = LocationType::model()->findByPk($id);
                $site->active_status = $new_status;               		
                
		if($site->update())
		{ 			
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{			
 			echo json_encode(array('response'=> 'fail'));
 		}
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$id = $_POST['id'];
		$value = $_POST['value'];
		//echo $value; die;
		$model=$this->loadModel1($id);
		$model->name = $value;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->save())
		{
			$data = null;
			print_r(json_encode($data));
		}else
		{
			$error = $model->getErrors(); 
			print_r(json_encode($error['name']));
		}
		
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{	
                		
		$ws1 = Yii::app()->db->createCommand()
                    ->select(['work_site'])
                    ->from('tms_stock_details')
                    ->where('work_site ='.$id)
                    ->queryAll();
                        
		$ws2 = Yii::app()->db->createCommand()
					->select(['allocated_site'])
					->from('tms_tool_allocation')
					->where('allocated_site ='.$id)
					->queryAll();

                
		if($ws1 == NULL && $ws2 == NULL)
		{  
			//echo "hi"; die;
			$this->loadModel1($id)->delete();
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{	
			//echo "hello"; die;
 			echo json_encode(array('response'=> 'fail'));
 		}
		
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(array('workSite/admin'));
	}
        
        

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('WorkSite');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                 $page = 'Site';
		$model=new LocationType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LocationType']))
			$model->attributes=$_GET['LocationType'];

		$this->render('admin',array(
			'model'=>$model,'page'=>$page
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=WorkSite::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadModel1($id)
	{
		$model= LocationType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-site-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	/* assign to user */



    public function actionAssigntoUser() {


        $id = $_REQUEST['id'];
        $check = "Select user_id from tms_location_assigned where site_id=$id";
        $command = Yii::app()->db->createCommand($check);
        $command->execute();
        $check = $command->queryAll();
        $userEx = array();
        foreach ($check as $value) {
            $userEx[] = $value['user_id'];
        }
        $model = new LocationType;
        $query = "SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `whole_name`,userid FROM `tms_users` WHERE status=0 ORDER BY whole_name";
		
        $command = Yii::app()->db->createCommand($query);
        $command->execute();
        $users = $command->queryAll();

        if (isset($_POST['LocationType'])) {

        	

            $model->attributes = $_POST['LocationType'];
            //$model->updated_by = yii::app()->user->id;
            //$model->updated_date</td> = date('Y-m-d');

            if ($model->validate() && $model->save())
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script(" parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index'));
                }
        }

        //echo "<pre>";print_r($users);echo "</pre>";die();
        // Uncomment the following line if AJAX validation is needed


        if (!empty($_GET['asDialog']))
            $this->layout = false;
        $this->render('assignuser', array(
            'model' => $model,
            'users' => $users,
            'id' => $id,
            'check' => $userEx
        ));
    }


      public function actionAjaxReturn() {
        $id = ($_REQUEST['id']);
        $check = "Select user_id from tms_location_assigned where site_id=$id";
        $command = Yii::app()->db->createCommand($check);
        $command->execute();
        $check = $command->queryAll();
        $arr = array();
        foreach ($check as $chk) {
            $arr[] = $chk['user_id'];
        }

        $user = array();
        if (isset($_REQUEST['users']))
            $user = ($_REQUEST['users']);
        foreach ($user as $users) {
            if (!in_array($users, $arr)) {
                $query = "INSERT INTO tms_location_assigned (site_id,user_id) values('$id', '$users')";
                $parameters = array(":id" => $id, ":user" => $users);
                Yii::app()->db->createCommand($query)->execute();
            }
        }
        foreach ($arr as $array) {

            if (!in_array($array, $user)) {
                echo "Are you sure You want Delete the assigned users";
                $query = "DELETE FROM tms_location_assigned WHERE site_id=$id AND user_id=$array";
                $parameters = array(":id" => $id, ":user" => $array);
                Yii::app()->db->createCommand($query)->execute();
            }
        }
        if ($user == "")
            foreach ($check as $checked) {
                $query = "DELETE FROM tms_location_assigned WHERE site_id=$id AND user_id=$checked";
                $parameters = array(":id" => $id, ":user" => $checked);
                Yii::app()->db->createCommand($query)->execute();
            }
    }


}
