<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    private $access_rules = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xEBF4FB,
                'fontFile' => dirname(dirname(__FILE__)) . '\extensions\fonts\nimbus.ttf',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        $this->access_rules = array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('register', 'pwdrecovery', 'pwdreset', 'message_page', 'activation'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('myprofile', 'changepass', 'passchange', 'editprofile'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('create', 'update',  'delete', 'index', 'view','adduser','Userlog','Autocomplete','Autouser','Autoemail','Autoemployee','autodesig'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow super admin user to perform this shift user option
                'actions' => array('usershift',),
                'users' => array('@'),
                'expression' => 'yii::app()->user->mainuser_role==1',
            ),            
            
            
        );

        //this will include in the register user. because captcha is not working if we use this.
        //if remove, other pages wont work.
        if (trim(strtolower(Yii::app()->controller->action->id) != 'captcha')) {
            // deny all users
            $this->access_rules[] = array('deny', 'users' => array('*'),);
        }

        return $this->access_rules;
    }

    public function actionEditprofile() {
        $model = $this->loadModel(Yii::app()->user->id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET first_name='" . $model->first_name . "',last_name='" . $model->last_name . "',email='" . $model->email . "', last_modified=now() WHERE userid=" . Yii::app()->user->id);

            if ($command->execute())
                Yii::app()->user->setFlash('success', 'Successfully updated.');
            else
                Yii::app()->user->setFlash('error', 'Update failed. Please try again.');

            $this->redirect(array('myprofile'));
        }

        $this->render('editprofile', array(
            'model' => $model,
        ));
    }

    public function actionUsershift()
    {
         $sftuser = Users::model()->findByPk($_POST['shift_userid']);
         //Set current user to new user details
         Yii::app()->user->id = $_POST['shift_userid'];
         Yii::app()->user->role = $sftuser->user_type;
         Yii::app()->user->firstname = $sftuser->first_name;
         Yii::app()->user->name = $sftuser->username;
         
         echo "session_changed";
        
    }   
    
    public function actionPasschange() {
        $model = new Users;

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $newpasword = md5(base64_encode($_POST['Users']['password']));
                $old_password = md5(base64_encode($_POST['Users']['old_password']));

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE userid = '" . Yii::app()->user->id . "' AND password='$old_password'");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Your new password updated successfully.');
                    $this->redirect(array('users/myprofile'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ');
                    $this->redirect(array('users/message_page'));
                }
                Yii::app()->end();
            }
        }
        $this->render("_change_pwd_form", array('model' => $model));
        Yii::app()->end();
    }

    public function actionMessage_page() {
        $this->render('message_page');
    }

    public function actionMyProfile() {
        $model = new Users;
        $myid = Yii::app()->user->id;
        $this->render('myprofile', array('model' => $this->loadModel($myid),));
    }

    public function actionRegister() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
        }

        $model1 = new UserRegister;
        if (isset($_POST['UserRegister'])) {
            $model1->attributes = $_POST['UserRegister'];
            if ($model1->validate()) {
                $model = new Users;
                $current_date = date('Y-m-d H:i:s');

                $_POST['UserRegister']['password'] = md5(base64_encode($_POST['UserRegister']['password']));
                $model->attributes = $_POST['UserRegister'];
                $model->reg_ip = $_SERVER['REMOTE_ADDR'];
                $model->reg_date = $current_date;
                $activation_key = md5(base64_encode($_POST['UserRegister']['email'] . "-" . Yii::app()->params['enc_key']));
                $model->activation_key = $activation_key;

                if ($model->save()) {
                    $activation_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->createUrl('users/activation', array("avkey" => $activation_key));
                    $subject = "Please verify your email address to activate your tourcorner account";
                    $message = 'Hello,';
                    $message .= "Your activation link to activate and to use Tourcorner.com account: " . $activation_url . ".<br />";
                    $message .= "If clicking the link above does not work, copy and paste the URL in a new browser window instead. ";
                    $message .= Yii::app()->params['mail_footer_msg'];

                    $email = Yii::app()->email;
                    $email->to = $_POST['UserRegister']['email'];
                    $email->subject = $subject;
                    $email->view = '/template1';
                    $email->viewVars = array('message' => $message);
                    if ($email->send()) {
                        Yii::app()->user->setFlash('flash_msg', 'Please check your email. For email verification we have sent instructions to your email address. <b>' . $_POST['UserRegister']['email'] . "</b>");
                    } else {
                        Yii::app()->user->setFlash('flash_msg', 'Mail sending error. Please try again.');
                    }
                }

                $this->redirect(array('users/message_page'));
                return;
            }
        }
        $this->render('register', array('model' => $model1));
    }

    public function actionActivation() {
        if (!Yii::app()->user->isGuest || !isset($_GET['avkey']))
            $this->redirect(array('/'));
        $model = new Users;

        if (isset($_GET['avkey']) && strlen($_GET['avkey']) == 32) {
            $activation_key = $_GET['avkey'];
            $row = Users::model()->find("activation_key = '$activation_key' AND status IS NULL AND email_activation IS NULL");

            if (count($row) == 1) {
                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET email_activation='1',status=1, last_modified=now() WHERE activation_key = '$activation_key' AND status IS NULL");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Successfully verified your email and now activated your account.');
                    $this->redirect(array('site/login'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while email verification. Please try again with same link. <br /> ' . CHtml::link('Go Home', array('/')));
                    $this->redirect(array('users/message_page'));
                }
            } else {
                Yii::app()->user->setFlash('error', 'Wrong activation URL. Please try again with same link  from your email. <br /> ' . CHtml::link('Go Home', array('/')));
                $this->redirect(array('users/message_page'));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Wrong activation URL. Please try again with same link from your email. <br /> ' . CHtml::link('Go Home', array('/')));
            $this->redirect(array('users/message_page'));
        }
    }

    public function actionPwdrecovery() {

        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
            Yii::app()->end();
        }

        $model = new Users;

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET activation_key=md5(now()), last_modified=now() WHERE userid=" . $model->ruser_id);
                $command->execute();

                $user = $model->findbyPk($model->ruser_id);

                //Creating activation id
                $key_part1 = md5(base64_encode($user->email . "-" . $user->activation_key . "-" . $user->last_modified));
                $key_part2 = md5(Yii::app()->params['enc_key'] . "^" . $user->email);
                $new_activation_key = $key_part1 . $key_part2;
                //End activation id
                $activation_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->createUrl('users/pwdreset', array("vkey" => $new_activation_key));
                $subject = "You have requested the password recovery site " . Yii::app()->name;
                $message = 'Hello,';
                $message .= "You have requested the password recovery site " . Yii::app()->name . ". To receive a new password, go to " . $activation_url . ".<br />";
                $message .= "If clicking the link above does not work, copy and paste the URL in a new browser window instead. The URL will expire in 24 hours for security reasons.";
                $message .= Yii::app()->params['mail_footer_msg'];

                $email = Yii::app()->email;
                $email->to = $user->email;
                $email->subject = $subject;
                $email->view = '/template1';
                $email->viewVars = array('message' => $message);
                if ($email->send()) {
                    Yii::app()->user->setFlash('flash_msg', 'Please check your email. An instructions was sent to your email address. ' . CHtml::link('Go to home', array('/')));
                } else {
                    Yii::app()->user->setFlash('flash_msg', 'Mail sending error. Please try again.');
                }
                $this->refresh();

                // form inputs are valid, do something here
                // return;
            }
//            echo Yii::app()->ruserid;
        }
        $this->render('pwdrecovery', array('model' => $model));
    }

    public function actionPwdreset() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('/'));
        }

        $model = new Users;
        $show_form = 0;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-pwdrest-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Users'])) {
            $show_form = 2;
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {

                $vkey_email = (substr($_GET['vkey'], 32));

                $newpasword = md5(base64_encode($_POST['Users']['new_pwd']));

                $command = Yii::app()->db->createCommand('UPDATE ' . ($model->tableName()) . " SET password='$newpasword', last_modified=now() WHERE md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");
                if ($command->execute()) {
                    Yii::app()->user->setFlash('success', 'Your new password updated successfully. Now you can login with your new password.');
                    $this->redirect(array('site/login'));
                } else {
                    Yii::app()->user->setFlash('error', 'Error occurred while updating your new password. ' . CHtml::link('Please try again', array('users/pwdrecovery')) . ' OR ' . CHtml::link('Go Home', array('/')));
                    $this->redirect(array('users/message_page'));
                }
                Yii::app()->end();
            }
        }

        if (isset($_GET['vkey']) && strlen($_GET['vkey']) == 64) {

            $vkey_email = (substr($_GET['vkey'], 32));
            $verification_key = (substr($_GET['vkey'], 0, 32));

            $row = Users::model()->find("status = 1 AND email_activation=1 AND md5( concat( \"" . Yii::app()->params['enc_key'] . "\", \"^\", `email` )) = '$vkey_email'");

            if (count($row) == 1) {
                $vkey_match = md5(base64_encode($row->email . "-" . $row->activation_key . "-" . $row->last_modified));
                if ($vkey_match == $verification_key) {
                    $show_form = 1;
                    $this->render('pwdreset', array('model' => $model, 'act_status' => $show_form));
                }
            }
        }

        if ($show_form == 0) {
            Yii::app()->user->setFlash('error', "Verification code is not matching with any record. Please try again.");
            $this->redirect(array('users/pwdrecovery'));
            Yii::app()->end();
        } elseif ($show_form == 2) {
            $this->render('pwdreset', array('model' => $model));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array('model' => $this->loadModel($id),));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Users;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {

            $current_date = date('Y-m-d H:i:s');

            $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            $model->attributes = $_POST['Users'];
            
            $model->reg_ip = $_SERVER['REMOTE_ADDR'];
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($_POST['Users']['email'] . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;
            $model->email_activation = 1;

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }
    
    public function actionadduser()
    {
        $model = new Users;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
             $current_date = date('Y-m-d H:i:s');

            $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));
            $model->attributes = $_POST['Users'];
            
            $model->reg_ip = $_SERVER['REMOTE_ADDR'];
            $model->reg_date = $current_date;
            $activation_key = md5(base64_encode($_POST['Users']['email'] . "-" . Yii::app()->params['enc_key']));
            $model->activation_key = $activation_key;
            $model->email_activation = 1;
            
            //$model->created_by = yii::app()->user->id;
           // $model->created_date = date('Y-m-d');
            //$model->updated_by = yii::app()->user->id;
            //$model->updated_date = date('Y-m-d');
			
            if ($model->validate() && $model->save())
			{
				 
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index', 'id' => $model->userid));
                }
        }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])){
         
            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);


        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model);
         
        if (isset($_POST['Users'])) {
            
            if(trim($_POST['Users']['password'])=='')
                $_POST['Users']['password']=$model->password;
            else
                $_POST['Users']['password'] = md5(base64_encode($_POST['Users']['password']));

            
            $model->attributes = $_POST['Users'];
                        
            if ($model->validate()){
                $model->save();
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values

         // new filter back and forth
        $class_name= get_class($model);
        if (isset($_GET[ $class_name])){
           Yii::app()->user->setState('prevfilter' , $_GET[$class_name]);
        }

        if(isset(Yii::app()->user->prevfilter)){
           $model->attributes = Yii::app()->user->prevfilter;
        }

        // end 
        
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
             

        ));
    }


    public function actionUserlog(){

        

        $model = new Users('search');
        $model->unsetAttributes(); 
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('userlog', array(
            'model' => $model, ));
    }
     

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAutocomplete() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT CONCAT_WS(' ',first_name,last_name) as full_name FROM {{users}} WHERE first_name LIKE :first_name"; $command =Yii::app()->db->createCommand($qtxt); $command->bindValue(":first_name", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }

    public function actionAutouser() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT username as full_name FROM {{users}} WHERE username LIKE :username"; $command =Yii::app()->db->createCommand($qtxt); $command->bindValue(":username", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }

    public function actionAutoemail() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT email as email FROM {{users}} WHERE email LIKE :email"; $command =Yii::app()->db->createCommand($qtxt); $command->bindValue(":email", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }

    public function actionAutodesig() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT designation as designation FROM {{users}} WHERE designation LIKE :designation ";
       print_r( $qtxt);exit;
        $command =Yii::app()->db->createCommand($qtxt); 
        $command->bindValue(":designation", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }

    public function actionAutoemployee() { 
       $res =array();

      
      if (isset($_GET['term'])) { 
       
       $qtxt ="SELECT employee_id as employee_id FROM {{users}} WHERE employee_id LIKE :employee_id"; $command =Yii::app()->db->createCommand($qtxt); $command->bindValue(":employee_id", '%'.$_GET['term'].'%', PDO::PARAM_STR); 
       $res =$command->queryColumn();
     } 
       echo CJSON::encode($res); 
       Yii::app()->end(); 
    }



}
