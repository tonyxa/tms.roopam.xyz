<?php

class ToolsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('preventiveinfomail'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'deleteitem', 'toolreport', 'toolhistory', 'gettoolcode', 'toolsview', 'gettooldetails', 'changestatus', 'createtoolset','toolsetadmin','categoryview','toolsetview', 'toolsviewsite', 'alltoolsview', 'gettallooldetails','gettallooldetails2','tooldisplay','Addtoolmultiple','toolLog','servicehistory','displaytools','transferalltools','getsitesbyitemid','getsitedatabyitemid','getitemsbysite','getitemdetailsbysite','deleteolddata','serialnumberrename'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'toolcreate', 'pending', 'checkvalidation','servicereport','getchildcategories','getchildcategories1','Prev_request','PrevApprove'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = false;
        $model = new Tools;

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        $work_sites = WorkSite::model()->findAll();


        if (isset($_POST['Tools'])) {




            $model->attributes = $_POST['Tools'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = Yii::app()->user->id;
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = Yii::app()->user->id;

            if ($model->save()) {

                $sl_No = $_POST['work_site'];

                $insert_id = Yii::app()->db->getLastInsertID();
                for ($i = 0; $i < sizeof($sl_No); $i++) {

                    $work_site = $_POST['work_site'];
                    $serial_no = $_POST['serial_no'];
                    $cost = $_POST['cost'];
                    $stock = $_POST['stock'];
                    $newmodel = new StockDetails;
                    $newmodel->tool_id = $insert_id;
                    $newmodel->work_site = $work_site[$i];
                    $newmodel->serial_no = $serial_no[$i];
                    $newmodel->cost = $cost[$i];
                    $newmodel->stock = $stock[$i];
                    $newmodel->save();
                }

                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model, 'work_sites' => $work_sites,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id,$type=null) {
        //$this->layout = false;
        $model = $this->loadModel($id);

        $newmodel = StockDetails::model()->findAll(array("condition" => "tool_id = '$id'"));

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $work_sites = WorkSite::model()->findAll();
        if (isset($_POST['Tools'])) {

          /* $ids       = $_POST['ids'];
              $work_site = $_POST['work_site'];
              $serial_no = $_POST['serial_no'];
              $cost      = $_POST['cost'];
              $stock     = $_POST['stock'];
             */


            $model->attributes = $_POST['Tools'];

            /* preventive maintenance */

            if(isset($_POST['prev_main'])){

              $future_date = date('Y-m-d H:i:s', strtotime('+7 days'));
              $model->prev_main =  '1';
              $model->prev_hrs  =   ($_POST['prev_hrs']!=NULL)?$_POST['prev_hrs']:NULL;
              if($_POST['prev_hrs'] != NULL){
                  $hours =  $_POST['prev_hrs']/24;
                  $days = (int)$hours;
                $model->prev_date = date('Y-m-d H:i:s',strtotime("".$days." days"));
              }else{
                $model->prev_date = NULL;
              }
              //$model->prev_date =   ($_POST['Tools']['prev_date']!=NULL)?$_POST['Tools']['prev_date'].' '.date('H:i:s'):$future_date;
            }else{

              $model->prev_main =  '0';
              $model->prev_hrs  =  NULL;
              $model->prev_date =  NULL;
            }



            if ($model->save()) {
                /*
                  foreach($ids as $i=>$idnew){

                  if($idnew!=NULL)
                  {

                  $newmodel = StockDetails::model()->findByPk($idnew);
                  $newmodel->tool_id = $id;
                  $newmodel->work_site = $work_site[$i];
                  $newmodel->serial_no = $serial_no[$i];
                  $newmodel->cost  = $cost[$i];
                  $newmodel->stock = $stock[$i];
                  $newmodel->save();

                  }
                  else
                  {

                  $newmodel = new StockDetails;
                  $newmodel->tool_id = $id;
                  $newmodel->work_site = $work_site[$i];
                  $newmodel->serial_no = $serial_no[$i];
                  $newmodel->cost  = $cost[$i];
                  $newmodel->stock = $stock[$i];
                  $newmodel->save();


                  }


                  }

                 */

                if($type==1){

                  $this->redirect(array('toolLog'));

                }else if($type==2){

                    $this->redirect(array('toolsetadmin'));
                }else{

                    $this->redirect(array('admin'));
                }



            }
        }

        $this->render('update', array(
            'model' => $model, 'work_sites' => $work_sites, 'newmodel' => $newmodel
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Tools');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {


        $page = 'tools';
        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values

        $catTree = Tools::toolcategories();

        $categories_list = self::visualTree($catTree, 0);

        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('admin', array(
            'model' => $model, 'page' => $page,'testmodel' => $categories_list,
        ));
    }


    public function actionToolsetadmin() {
        $page = 'toolset';
        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values

        $catTree = Tools::toolsetcategories();
        $categories_list = self::visualTree($catTree, 0);


        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('toolsetadmin', array(
            'model' => $model, 'page' => $page,'testmodel' => $categories_list,
        ));
    }




    public function actionPending() {
        /* $page = 'pending';
          $model = new Tools('search');
          $model->unsetAttributes();  // clear any default values
          if (isset($_GET['Tools']))
          $model->attributes = $_GET['Tools'];

          $this->render('pending', array(
          'model' => $model,'page'=>$page
          )); */
        $page = 'pending';
        $model = new PurchaseItems('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['PurchaseItems']))
            $model->attributes = $_GET['PurchaseItems'];

        $this->render('pending', array(
            'model' => $model, 'page' => $page
        ));
    }

    public function actionToolreport() {
        $tblpx = Yii::app()->db->tablePrefix;
        $modelcat = new ToolCategory();

        $catTree = ToolCategory::allcategories();
        $categories_list = ToolCategory::visualTree($catTree, 0);
        
        $page = 'toolreport';
        $model = new Tools('search');
      
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])){
        $model->child = (isset($_GET['childCategory'])? $_GET['childCategory'] : '');
        $model->subchild = (isset($_GET['subchildCategory'])? $_GET['subchildCategory'] : '');
        $model->attributes = $_GET['Tools'];
        //$model->status = (isset($_GET['Tools']['statuscaption'])? $_GET['Tools']['statuscaption'] : '');
        }
        $this->render('toolreport', array(
            'model' => $model,'page' => $page, 'categories_list' =>$categories_list,
        ));
    }

    // public function actionToolreport() {
    //     $tblpx = Yii::app()->db->tablePrefix;
    //     $modelcat = new ToolCategory();
    //
    //     $catTree = ToolCategory::allcategories();
    //     $categories_list = ToolCategory::visualTree($catTree, 0);
    //
    //     $page = 'toolreport';
    //     $model = new ViewToolreport('search');
    //     $model->unsetAttributes();  // clear any default values
    //     if (isset($_GET['ViewToolreport'])){
    //     $model->child = (isset($_GET['childCategory'])? $_GET['childCategory'] : '');
    //     $model->subchild = (isset($_GET['subchildCategory'])? $_GET['subchildCategory'] : '');
    //     $model->attributes = $_GET['ViewToolreport'];
    //     //$model->status = (isset($_GET['Tools']['statuscaption'])? $_GET['Tools']['statuscaption'] : '');
    //     }
    //     $this->render('toolreportnew', array(
    //         'model' => $model, 'page' => $page, 'categories_list' =>$categories_list,
    //     ));
    // }
    public function actionGetchildcategories(){
    $cid = $_REQUEST['cid'];
    $sel = $_REQUEST['sel'];
    $tblpx = Yii::app()->db->tablePrefix;
    $html = '';
    if(!empty($cid)){
    $childlist =  $sql = Yii::app()->db->createCommand("select cat_id, cat_name FROM {$tblpx}tool_category"
    . " WHERE parent_id = '$cid'")->queryAll();
    //if(empty($sel)){
    $html.='<option value="">Select</option>';
  //  }

    foreach($childlist as $r){
    $selected = '';
    if($sel == $r['cat_id']){
    $selected = 'selected';
    }
    $html.='<option value="'.$r['cat_id'].'" '.$selected.'>'.$r['cat_name'].' </option>';
    }
    }
    echo $html;
    }
    public function actionGetchildcategories1(){
    $cid = $_REQUEST['cid'];

    $tblpx = Yii::app()->db->tablePrefix;
    $html = '';
    if(!empty($cid)){
    $childlist =  $sql = Yii::app()->db->createCommand("select cat_id, cat_name FROM {$tblpx}tool_category"
    . " WHERE parent_id = '$cid'")->queryAll();
    $html.='<option value="">Select</option>';
    foreach($childlist as $r){
    $html.='<option value="'.$r['cat_id'].'">'.$r['cat_name'].' </option>';
    }
    }
    echo $html;
    }


    public function actionToolhistory() {
        $tool_name = '';
        $tool_code = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) || (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
            $where = '';
            if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) && (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
                $tool_name = $_POST['tool_name'];
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}' and {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            } else if ((isset($_POST['tool_name']) && !empty($_POST['tool_name']))) {
                $tool_name = $_POST['tool_name'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}'";
            } else if (isset($_POST['tool_code']) && !empty($_POST['tool_code'])) {
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            }
            // $sql = Yii::app()->db->createCommand("select tool_id, tool_transfer_id,request_from,request_to,
            // request_status,item_name,{$tblpx}tool_transfer_items.code,item_status,req_date,
            // {$tblpx}tool_transfer_items.location,c.caption,d.name,{$tblpx}tool_transfer_items.`paid_amount`,
            // {$tblpx}tool_transfer_items.serial_no,e.caption as type,item_current_status from {$tblpx}tool_transfer_items left join 
            // {$tblpx}tool_transfer_request on tool_transfer_id={$tblpx}tool_transfer_request.id  
            // left join {$tblpx}status c on c.sid = {$tblpx}tool_transfer_items.item_status 
            // left join {$tblpx}location_type d on d.id = {$tblpx}tool_transfer_items.location 
            // inner join {$tblpx}status e on d.location_type=e.sid {$where} 
            // order by {$tblpx}tool_transfer_items.id ASC ")->queryAll();
            

            
            $sql = Yii::app()->db->createCommand("select * from tms_tool_transfer_items left join tms_tools on tms_tool_transfer_items.tool_id=tms_tools.id  left join tms_status c on c.sid = tms_tool_transfer_items.item_status left join tms_tool_transfer_request on tms_tool_transfer_request.request_no=tms_tool_transfer_items.id left join tms_location_type d on d.id = tms_tool_transfer_items.location {$where} and item_status!=29")->queryAll();
           
            

        } 
        else 
        {
             $sql= array();     
        }
        $this->render('toolhistory', array(
              'sql' => $sql, 'tool_code' => $tool_code,'tool_name' => $tool_name,
        ));
    }


     public function actionServicehistory() {
        $tool_name = '';
        $tool_code = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) || (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
            $where = '';
            if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) && (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
                $tool_name = $_POST['tool_name'];
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}' and {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            } else if ((isset($_POST['tool_name']) && !empty($_POST['tool_name']))) {
                $tool_name = $_POST['tool_name'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}'";
            } else if (isset($_POST['tool_code']) && !empty($_POST['tool_code'])) {
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            }

         $tools = Yii::app()->db->createCommand(" SELECT  tool_transfer_id,{$tblpx}tools.warranty_date,{$tblpx}purchase_items.make,amount,supplier_name,{$tblpx}tool_transfer_items.item_name,{$tblpx}tool_transfer_items.tool_id ,{$tblpx}tool_transfer_items.code,{$tblpx}tool_transfer_items.serial_no,rate,{$tblpx}location_type.name , caption FROM {$tblpx}tool_transfer_items left join {$tblpx}tools on {$tblpx}tool_transfer_items.tool_id = {$tblpx}tools.id left join {$tblpx}purchase_items on {$tblpx}tools.item_id = {$tblpx}purchase_items.id left join {$tblpx}purchase on {$tblpx}purchase_items.purchase_id = {$tblpx}purchase.id  left join {$tblpx}location_type on {$tblpx}tools.location = {$tblpx}location_type.id  left join {$tblpx}status on sid = {$tblpx}tool_transfer_items.item_status {$where} ")->queryRow();


        $service = Yii::app()->db->createCommand("SELECT tool_id,{$tblpx}tool_transfer_request.request_date,{$tblpx}tools.`warranty_date`,`req_date`,`return_date`,{$tblpx}tool_transfer_items.`remarks`,{$tblpx}tool_transfer_items.`service_report`,{$tblpx}tool_transfer_items.`paid_amount`,{$tblpx}location_type.name FROM `{$tblpx}tool_transfer_items`
          left join {$tblpx}tools on {$tblpx}tool_transfer_items.tool_id = {$tblpx}tools.id left join {$tblpx}tool_transfer_request on {$tblpx}tool_transfer_items.`tool_transfer_id` = {$tblpx}tool_transfer_request.id left join {$tblpx}location_type on {$tblpx}tool_transfer_request.request_to = {$tblpx}location_type.id  {$where} and {$tblpx}tool_transfer_request.transfer_to='vendor' and   {$tblpx}tool_transfer_items.paid_amount is not NULL ")->queryAll();


        $pre_request = array();
        $service_details = array();

        foreach ($service as $key => $value){


           $pre_request  = Yii::app()->db->createCommand("SELECT tms_preventive_request.*,tms_location_type.name ,date as request_date,cost as paid_amount,warranty_date from tms_preventive_request
           left join tms_tools on tms_tools.id = tms_preventive_request.tool_id
           left join  tms_location_type on  tms_tools.location = tms_location_type.id
            where tool_id = ".$value['tool_id']." and `status` = 'approved' ")->queryAll();
        }


        if(!empty($pre_request)){
          $service_details = array_merge($service, $pre_request);
        }

    $projects = Yii::app()->db->createCommand("select tool_id, tool_transfer_id,request_from,request_to,request_status,item_name,{$tblpx}tool_transfer_items.code,item_status,req_date,{$tblpx}tool_transfer_items.location,caption,{$tblpx}location_type.name,{$tblpx}tool_transfer_items.`paid_amount`,{$tblpx}tool_transfer_items.serial_no from {$tblpx}tool_transfer_items left join {$tblpx}tool_transfer_request on tool_transfer_id={$tblpx}tool_transfer_request.id  left join {$tblpx}status on sid = {$tblpx}tool_transfer_items.item_status left join {$tblpx}location_type on {$tblpx}location_type.id = {$tblpx}tool_transfer_items.location {$where}  and {$tblpx}status.caption='Site' order by {$tblpx}tool_transfer_items.id ASC ")->queryAll();

      } else {

             $tools= array();
             $service=array();
             $projects=array();
             $pre_request = array();
             $service_details = array();

        }



        $this->render('servicehistory', array(
              'tool_name' => $tool_name, 'tool_code' => $tool_code,'tool' => $tools,'service' => $service,'projects'=>$projects, 'pre_request' => $pre_request, 'service_details' => $service_details ,
        ));
    }

    /* public function actionChangestatus(){
      $cnt  = count($_POST['selectedIds']);
      //       print_r($_POST['selectedIds']);
      //       die;
      if($cnt > 0){
      $arr = $_POST['selectedIds'];
      for($i=0;$i<$cnt;$i++){
      $id = $arr[$i];
      $tools =Tools::model()->findByPk($id);
      if($tools->pending_status == 2){
      $tools->pending_status = 1;
      }else{
      $tools->pending_status = 2;
      }
      $tools->save();
      }
      }
      $this->redirect(array('admin'));
      }
     */

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Tools::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tools-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDeleteItem($id) {
        StockDetails::model()->deleteByPk($id);
        echo true;
    }

    public function actionToolcreate($id=0, $type = "") {



        //$this->layout = false;
        $model = new Tools;

        $this->performAjaxValidation($model);
        $item_model = PurchaseItems::model()->findByPk($id);
        $tblpx = Yii::app()->db->tablePrefix;


        if(isset($_POST['Tools'])){


          $tool_category = isset($_POST['Tools']['parent_category'])?$_POST['Tools']['parent_category']:NULL;

          if(isset($_POST['childCategory']) && $_POST['childCategory']  != ''){
              $tool_category = $_POST['childCategory'];
          }
          if(isset($_POST['subchildCategory']) && $_POST['subchildCategory'] != ''){
              $tool_category = $_POST['subchildCategory'];
          }



        $type = isset($_POST['Tools']['type']) ? $_POST['Tools']['type'] : 'tool';
        

          if ($type == 'tool') {
            $serialno_status = $_POST['Tools']['serialno_status'];
            if($_POST['Tools']['serialno_status'] == 'Y'){

                $transaction = Yii::app()->db->beginTransaction();
                try {

                $total_quantity = $_POST['Tools']['qty'];
                for ($i = 0; $i < $total_quantity; $i++) {
                    $model = new Tools;
                    $model->attributes = $_POST['Tools'];
                    $model->tool_code = 0; //$item_code[$i];
                    $model->tool_name = $item_model->item_name;
                    $model->unit = $item_model->unit;
                    $model->make = $item_model->make;
                    $model->model_no = $item_model->model_no;
                    if(empty($item_model->serial_no)||($item_model->serial_no=="")||($item_model->serial_no==NULL)){
                        $model->serial_no="NULL";
                    }else
                    {
                        $model->serial_no = $item_model->serial_no;

                    }                    $model->warranty_date = $item_model->warranty_date;

                    $model->batch_no = $item_model->unit;
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d');
                    $model->updated_by = Yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    $model->item_id = $id;
                    $model->pending_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                    //$model->id                      =   $id;
                    //$model->tool_category = $_POST['Tools']['tool_category'];
                    $model->tool_category = $tool_category;

                    $model->location = $_POST['Tools']['location'];
                    $model->qty = 1;
                    $model->stock_qty = 1;

                   // $toolcat = ToolCategory::model()->findByPk($_POST['Tools']['tool_category']);
                    $toolcat = ToolCategory::model()->findByPk($tool_category);

                    $cat_id=$toolcat->cat_id;
                    $qryres = Yii::app()->db->createCommand()
                            ->select('MAX(ref_value) AS maxval')
                            ->from('tms_tools u')
                            ->where('tool_category=:tool_category', array(':tool_category'=>$cat_id))
                            ->queryRow();


                    if (!empty($qryres['maxval'])) {
                        $pr_id = sprintf("%03d", $qryres['maxval'] + 1);
                        $maxval = $pr_id;
                    } else {
                        $pr_id = sprintf("%03d", '001');
                        $maxval = $pr_id;
                    }

                    $model->ref_no = $toolcat['cat_code'] .'-'. $maxval;
                    $model->ref_value = $maxval;

                    // new preventive maintaince

                    $future_date = date('Y-m-d H:i:s', strtotime('+7 days'));

                    $model->prev_main = (isset($_POST['prev_main']))?"1":"0";
                    $model->prev_hrs  = (isset($_POST['prev_hrs']))?$_POST['prev_hrs']:NULL;
                    // $model->prev_date = ($item_model->prev_date!=NULL)?$item_model->prev_date:$future_date;

                    if($_POST['prev_hrs'] != NULL){
                        $hours =  $_POST['prev_hrs']/24;
                        $days = (int)$hours;
                      $model->prev_date = date('Y-m-d H:i:s',strtotime("".$days." days"));
                    }else{
                      $model->prev_date = NULL;
                    }




                    $qryres1 = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('tms_status')
                            ->where('status_type=:type and sid!=:id', array(':type' => 'pending_status', ':id' => $model->pending_status))
                            ->queryRow();


                    //$model->pending_status          =   $qryres1['sid'];
                    if ($model->save()) {
                        $toolid = Yii::app()->db->getLastInsertID();

                        $tool_item = new ToolsLocation;
                        $tool_item->qty = 1;
                        $tool_item->tool_id = $toolid;
                        $tool_item->location = $_POST['Tools']['location'];
                        $tool_item->created_by = Yii::app()->user->id;
                        $tool_item->created_date = date('Y-m-d');
                        $tool_item->save(false);


                        $item_model->remaining_qty = ($item_model->remaining_qty - 1);
                        $item_model->save();
                        if ($item_model->remaining_qty == 0) {
                            $item_model->tool_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                            $item_model->save();
                        }
                        $newmodel = new ToolTransferRequest;
                        //$newmodel->tool_id = $id;
                        $qryres2 = Yii::app()->db->createCommand()
                                ->select('MAX(request_no) AS maxval')
                                ->from('tms_tool_transfer_request u')
                                //->where('id=:id', array(':id'=>$id))
                                ->queryRow();
                        if (empty($qryres2['maxval'])) {
                            $rno = 1;
                        } else {
                            $rno = $qryres2['maxval'] + 1;
                        }

                        $newmodel->request_no = $rno;
                        $newmodel->request_from = $_POST['Tools']['location'];
                        $newmodel->request_to = $_POST['Tools']['location'];
                        $newmodel->request_date = date('Y-m-d');
                        $newmodel->location = $_POST['Tools']['location'];

                        $newmodel->created_by = Yii::app()->user->id;
                        $newmodel->created_date = date('Y-m-d');
                        $newmodel->updated_by = Yii::app()->user->id;
                        $newmodel->updated_date = date('Y-m-d');
                        $newmodel->save();
                        $insert_id = Yii::app()->db->getLastInsertID();

                        $newmodel1 = new ToolTransferItems;
                        $newmodel1->tool_transfer_id = $insert_id;
                        $newmodel1->tool_id = $toolid;
                        $newmodel1->code = $toolcat['cat_code'] .'-'. $maxval;
                        $qryres3 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_tools u')
                                ->where('id=:id', array(':id' => $toolid))
                                ->queryRow();

                        $newmodel1->item_name = $qryres3['tool_name'];
                        $newmodel1->unit = $qryres3['unit'];
                        $newmodel1->serial_no = $qryres3['serial_no'];
                        $newmodel1->req_date = date('Y-m-d');
                        // $newmodel1->duration_in_days = $qryres3['tool_name;'];
                        $newmodel1->qty = $qryres3['qty'];
                        //$newmodel1->physical_condition = $qryres3['tool_name;'];
                        $lid = $_POST['Tools']['location'];
                        $qryres4 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_location_type u')
                                ->where('id=:id', array(':id' => $lid))
                                ->queryRow();

                        $newmodel1->item_status = $qryres4['location_type'];
                        $newmodel1->location = $_POST['Tools']['location'];
                        $newmodel1->save();
                    }
                  }
                  $transaction->commit();
                  $this->redirect(array('admin'));
                 }
                 catch(Exception $e){
                    $transaction->rollBack();
                 }

            }else{

              // with out serial no
                    $tool_details = Tools::model()->find(array('condition' => 'tool_category ='.$tool_category.' AND location='.$_POST['Tools']['location'].' AND serialno_status="N"'));
                    // $tool_details =   Yii::app()->db->createCommand("Select t1.id, t1.qty as quantity,t2.qty as item_qty,t2.id as item_id FROM {$tblpx}tools as t1 INNER JOIN {$tblpx}tools_location as t2 ON t1.id=t2.tool_id WHERE t1.tool_category =".$tool_category." AND t2.location=".$_POST['Tools']['location']." AND t1.serialno_status='N'")->queryRow();
                    if(!empty($tool_details)){
                      $model = Tools::model()->findByPk($tool_details['id']);
                      $quantity = $tool_details['qty'];
                    }else{
                      $model = new Tools;
                      $quantity = 0;
                    }
                    $model->attributes = $_POST['Tools'];
                    $model->tool_code = 0; //$item_code[$i];
                    $model->tool_name = $item_model->item_name;
                    $model->unit = $item_model->unit;
                    $model->make = $item_model->make;
                    $model->model_no = $item_model->model_no;
                    $model->serial_no = $item_model->serial_no;
                    $model->warranty_date = $item_model->warranty_date;

                    $model->batch_no = $item_model->unit;
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d');
                    $model->updated_by = Yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    $model->item_id = $id;
                    $model->pending_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                    //$model->id                      =   $id;
                    //$model->tool_category = $_POST['Tools']['tool_category'];
                    $model->tool_category = $tool_category;

                    $model->location = $_POST['Tools']['location'];
                    $model->qty = $_POST['Tools']['qty']+$quantity;
                    $model->stock_qty = $_POST['Tools']['qty']+$quantity;
                   // $toolcat = ToolCategory::model()->findByPk($_POST['Tools']['tool_category']);
                    $toolcat = ToolCategory::model()->findByPk($tool_category);

                    $cat_id=$toolcat->cat_id;
                    $qryres = Yii::app()->db->createCommand()
                            ->select('MAX(ref_value) AS maxval')
                            ->from('tms_tools u')
                            ->where('tool_category=:tool_category', array(':tool_category'=>$cat_id))
                            ->queryRow();


                    if (!empty($qryres['maxval'])) {
                        $pr_id = sprintf("%03d", $qryres['maxval'] + 1);
                        $maxval = $pr_id;
                    } else {
                        $pr_id = sprintf("%03d", '001');
                        $maxval = $pr_id;
                    }

                    $model->ref_no = $toolcat['cat_code'];
                    $model->ref_value = NULL;

                    // new preventive maintaince

                    $future_date = date('Y-m-d H:i:s', strtotime('+7 days'));

                    $model->prev_main = "0";
                    $model->prev_hrs  = NULL;
                    // $model->prev_date = ($item_model->prev_date!=NULL)?$item_model->prev_date:$future_date;

                      $model->prev_date = NULL;

                    $qryres1 = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('tms_status')
                            ->where('status_type=:type and sid!=:id', array(':type' => 'pending_status', ':id' => $model->pending_status))
                            ->queryRow();


                    //$model->pending_status          =   $qryres1['sid'];
                    if ($model->save(false)) {
                        $toolid = Yii::app()->db->getLastInsertID();

                        // Tools location
                        $tool_item = new ToolsLocation;
                        $tool_item->qty = $_POST['Tools']['qty'];
                        $tool_item->tool_id = $model->id;
                        $tool_item->location = $_POST['Tools']['location'];
                        $tool_item->created_by = Yii::app()->user->id;
                        $tool_item->created_date = date('Y-m-d');
                        $tool_item->save(false);

                        $item_model->remaining_qty = ($item_model->remaining_qty - $_POST['Tools']['qty']);
                        $item_model->save();
                        if ($item_model->remaining_qty == 0) {
                            $item_model->tool_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                            $item_model->save();
                        }
                        $newmodel = new ToolTransferRequest;
                        //$newmodel->tool_id = $id;
                        $qryres2 = Yii::app()->db->createCommand()
                                ->select('MAX(request_no) AS maxval')
                                ->from('tms_tool_transfer_request u')
                                //->where('id=:id', array(':id'=>$id))
                                ->queryRow();
                        if (empty($qryres2['maxval'])) {
                            $rno = 1;
                        } else {
                            $rno = $qryres2['maxval'] + 1;
                        }

                        $newmodel->request_no = $rno;
                        $newmodel->request_from = $_POST['Tools']['location'];
                        $newmodel->request_to = $_POST['Tools']['location'];
                        $newmodel->request_date = date('Y-m-d');
                        $newmodel->location = $_POST['Tools']['location'];

                        $newmodel->created_by = Yii::app()->user->id;
                        $newmodel->created_date = date('Y-m-d');
                        $newmodel->updated_by = Yii::app()->user->id;
                        $newmodel->updated_date = date('Y-m-d');
                        $newmodel->save();
                        $insert_id = Yii::app()->db->getLastInsertID();

                        $newmodel1 = new ToolTransferItems;
                        $newmodel1->tool_transfer_id = $insert_id;
                        $newmodel1->tool_id = $toolid;
                        $newmodel1->code = $toolcat['cat_code'] .'-'. $maxval;
                        $qryres3 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_tools u')
                                ->where('id=:id', array(':id' => $toolid))
                                ->queryRow();

                        $newmodel1->item_name = $qryres3['tool_name'];
                        $newmodel1->unit = $qryres3['unit'];
                        $newmodel1->serial_no = $qryres3['serial_no'];
                        $newmodel1->req_date = date('Y-m-d');
                        // $newmodel1->duration_in_days = $qryres3['tool_name;'];
                        $newmodel1->qty = $qryres3['qty'];
                        //$newmodel1->physical_condition = $qryres3['tool_name;'];
                        $lid = $_POST['Tools']['location'];
                        $qryres4 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_location_type u')
                                ->where('id=:id', array(':id' => $lid))
                                ->queryRow();

                        $newmodel1->item_status = $qryres4['location_type'];
                        $newmodel1->location = $_POST['Tools']['location'];
                        $newmodel1->save();

                        $this->redirect(array('admin'));
                    }

                    $this->redirect(array('admin'));
                }
            } else if ($type == 'toolset') {
                $toolsetmodel = new ToolSet();
                $toolsetmodel->toolset_id = $_POST['Tools']['tool_name'];
                $toolsetmodel->category = Tools::model()->findByPk($_POST['Tools']['tool_name'])->tool_category;
                $toolsetmodel->qty = $_POST['Tools']['qty'];
                $toolsetmodel->item_name = $item_model->item_name;
                if($toolsetmodel->save()){

                $item_model->remaining_qty = ($item_model->remaining_qty - $_POST['Tools']['qty']);
                $item_model->save();
                if ($item_model->remaining_qty == 0) {
                    $item_model->tool_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                    $item_model->save();
                }
            }
                $this->redirect(array('tools/toolsetadmin'));
            }
        }


        $this->render('toolcreate', array(
            'model' => $model,'item_model'=> $item_model
        ));
     }


    public function actionGettoolcode() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
          $toolslist = CHtml::listData(Tools::model()->findAll(
                                    array(
                                        'select' => array('ref_no,ref_no'),
                                        'distinct' => true,
                                        'condition' => 'id = ' . $id,
                            )), "id", "ref_no");
            $tools = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($toolslist as $toolid => $toolcode)
                $tools .= CHtml::tag('option', array('value' => $toolid), CHtml::encode($toolcode), true);

            echo CJSON::encode(array('tools' => $tools));
        }
    }

    public function actionToolsview() {
        $this->layout = false;
        $page = 'toolsview';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page_type = isset($_REQUEST['page_type']) ? $_REQUEST['page_type'] : "";

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('toolsview', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type, 'page_type' => $page_type,
        ));
    }


    public function actionToolsviewsite() {
        $this->layout = false;
        $page = 'toolsviewsite';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";


        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('toolsviewsite', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type,
        ));
    }

    public function actionGettallooldetails() {
      
         $id = $_POST['id'];
        if (isset($_POST['id'])) {

            $toolslist = Tools::model()->findByPk($id);

            $name = $toolslist->tool_name;
            $refcode = $toolslist->ref_no;
            $unit = $toolslist->unit;
            $qty = $toolslist->qty;

            $parents = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id = tms_tools.id WHERE tms_approve_itemqty.physical_condition =1 AND tms_tools.id=".$id."")->queryRow();
            $damage  = ($parents['qty'] != null)?$parents['qty']:"";
            $parents2 = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id = tms_tools.id WHERE tms_approve_itemqty.physical_condition =2 AND tms_tools.id=".$id."")->queryRow();

            $brakedown = ($parents2['qty'] != null)?$parents2['qty']:"";


            $tblpx = Yii::app()->db->tablePrefix;

		    //$total_qty  = Yii::app()->db->createCommand('SELECT COUNT(*) AS count FROM tms_tools WHERE location='.$_POST['loc'].' AND active_status = "1" AND tool_name ="'.$toolslist->tool_name.'" GROUP BY tool_name')->queryRow();
            $total_qty  = Yii::app()->db->createCommand('SELECT COUNT(*) AS count FROM tms_tools WHERE  active_status = "1" AND tool_name ="'.$toolslist->tool_name.'" GROUP BY tool_name')->queryRow();
            $current_qty=ToolTransferItems::model()->findByAttributes(array('tool_id'=>$id,'item_status'=>'12'));
            if($toolslist->serialno_status=='N'){
            $current_stock_qty=$current_qty->qty;
             }
             else{
                $current_stock_qty=1;
             }
            $unitlist = CHtml::listData(Unit::model()->findAll(
                                    array(
                                        'select' => array('id,unitname'),
                                        'distinct' => true,
                                        'condition' => 'id = ' . $unit,
                            )), "id", "unitname");
            $units = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($unitlist as $unitid => $unitname)
                $units .= CHtml::tag('option', array('value' => $unitid), CHtml::encode($unitname), true);


            echo CJSON::encode(array('name' => $name,'id'=>$id,'current_stock_qty'=>$current_stock_qty, 'refcode' => $refcode, 'units' => $units,'qty' =>$qty,'unit' => $unit, 'total_qty' => $total_qty['count'],'damage' => $damage,'breakdown' => $brakedown));
        }
    }

    public function actionGettallooldetails2() {
         $id = $_POST['id'];
        if (isset($_POST['id'])) {

            $toolslist = Tools::model()->findByPk($id);
            $name = $toolslist->tool_name;
            $refcode = $toolslist->ref_no;
            $unit = $toolslist->unit;
            $qty = $toolslist->qty;

            $parents= Yii::app()->db->createCommand("SELECT tms_approve_itemqty.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id = tms_tools.id WHERE tms_approve_itemqty.physical_condition =1 AND tms_tools.id=".$id." ")->queryRow();
            $parents2 = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id = tms_tools.id WHERE tms_approve_itemqty.physical_condition =2 AND tms_tools.id=".$id."")->queryRow();
            // if($toolslist->serialno_status =='N'){
                // print_r($parents);die;
              $damage  = ($parents['qty'] != null)?$parents['qty']:0;
              $brakedown = ($parents2['qty'] != null)?$parents2['qty']:0;
              $qty = $damage+$brakedown;
            // }else{
            //   $qty = $toolslist->qty;
              
            //   $damage = "";
            //   $brakedown = "";
            // }


            $tblpx = Yii::app()->db->tablePrefix;

		    //$total_qty  = Yii::app()->db->createCommand('SELECT COUNT(*) AS count FROM tms_tools WHERE location='.$_POST['loc'].' AND active_status = "1" AND tool_name ="'.$toolslist->tool_name.'" GROUP BY tool_name')->queryRow();
		    $total_qty  = Yii::app()->db->createCommand('SELECT COUNT(*) AS count FROM tms_tools WHERE  active_status = "1" AND tool_name ="'.$toolslist->tool_name.'" GROUP BY tool_name')->queryRow();
            $unitlist = CHtml::listData(Unit::model()->findAll(
                                    array(
                                        'select' => array('id,unitname'),
                                        'distinct' => true,
                                        'condition' => 'id = ' . $unit,
                            )), "id", "unitname");
            $units = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($unitlist as $unitid => $unitname)
                $units .= CHtml::tag('option', array('value' => $unitid), CHtml::encode($unitname), true);


            echo CJSON::encode(array('name' => $name, 'refcode' => $refcode, 'units' => $units,'qty' =>$qty,'unit' => $unit, 'total_qty' => $total_qty['count'],'damage' => $damage,'breakdown' => $brakedown));
        }
    }

    public function actionGettooldetails() {
        $id = $_POST['id'];
        if (isset($_POST['id'])) {


            $toolslist = Tools::model()->findByPk($id);
            $name = $toolslist->tool_name;
            $refcode = $toolslist->ref_no;
            $unit = $toolslist->unit;
            $qty = $toolslist->qty;
            $unitlist = CHtml::listData(Unit::model()->findAll(
                                    array(
                                        'select' => array('id,unitname'),
                                        'distinct' => true,
                                        'condition' => 'id = ' . $unit,
                            )), "id", "unitname");
            $units = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($unitlist as $unitid => $unitname)
                $units .= CHtml::tag('option', array('value' => $unitid), CHtml::encode($unitname), true);


            echo CJSON::encode(array('name' => $name,'id'=>$id,'refcode' => $refcode, 'units' => $units,'qty' =>$qty,'unit' => $unit));
        }
    }

    public function actionAllToolsview() {
        $this->layout = false;
        $page = 'alltoolsview';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $item_id = isset($_REQUEST['item_id']) ? $_REQUEST['item_id'] : "";

        $tool_category = array();
        if(!empty($item_id)){
            $result = Yii::app()->db->createCommand("SELECT tool_category FROM `tms_tool_transfer_info` left join tms_tools on tms_tools.id = `tms_tool_transfer_info`.`tool_id`
          WHERE `tool_transfer_id` = ".$item_id)->queryAll();

          foreach ($result as $key => $value) {
             $tool_category[] = $value['tool_category'];
          }
           $tool_category = implode(',', $tool_category);

        }

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('alltoolsview', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type,'cate' => $tool_category,
        ));
    }

    public function getChildstool($id, $space) {
        $row = '';
        $pre = '';
        $res = ToolCategory::model()->findAll(array('condition'=>'active_status="1" AND parent_id = ' . $id,"order" => "cat_name"));
        if ($res != NULL) {

            foreach (ToolCategory::model()->findAll(array('condition'=>'active_status="1" AND parent_id = ' . $id,"order" => "cat_name")) as $model) {
                $next = $model->parent_id;
                if ($next != $pre) {
                    $space .= '&nbsp; &nbsp; &nbsp;';
                    $pre = $model->parent_id;
                }
                //$row .= ' <option value="' . $model->cat_id . '">' . $space . $model->cat_name . '</option>';
                $row .= ' <option value="' . $model->cat_id . '">' . Tools::model()->getparents($model->cat_id) .' ('.Tools::model()->getQty($model->cat_id).') </option>';
                $row .= $this->getChildstool($model->cat_id, $space);
            }
        }
        return $row;
    }

    public function actionChangestatus($id) {
        ////////////////////////////////////////select the current active status

        $active_status = '';
        $new_status = '';
        $criteria = new CDbCriteria;
        $criteria->condition = 'id=:Id';
        $criteria->params = array(':Id' => $id);
        $model = Tools::model()->find($criteria);
        $active_status = $model->active_status;
        if ($active_status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }
        ////////////////////////////////////////
        $site = Tools::model()->findByPk($id);
        $site->active_status = $new_status;

        if ($site->update()) {
            echo json_encode(array('response' => 'success'));
        } else {
            echo json_encode(array('response' => 'fail'));
        }
    }

    public function actionCheckvalidation() {
        $qty = $_POST['qty'];
        $id = $_POST['id'];

        if ($id < 0) {
            echo "false";
        } else {

            $row = PurchaseItems::model()->findByPk($id);
            if ($row->remaining_qty < $qty) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    public function actionCreatetoolset() {


        $id = $_POST['id'];
        $item_model = PurchaseItems::model()->findByPk($id);
       // $toolname = $_POST['Tools'][0]['value'];
        // $unit = $_POST['Tools'][0]['value'];
        $unit =1;
        $location = $_POST['Tools'][0]['value'];
        $category = $_POST['Tools'][1]['value'];

        $child_cat  = $_POST['Tools'][2]['value'];
        $subshild_cat = $_POST['Tools'][3]['value'];

        //echo '<pre>';print_r($_POST['Tools']);exit;

        if(!empty($child_cat)){
          $category = $child_cat;
        }
        if(!empty($subshild_cat)){
           $category = $subshild_cat;
        }



        if( isset($_POST['Tools'][4]['name']) && $_POST['Tools'][4]['name']=='prev_main1'){
            $prev_main = "1";
            $hrs  = isset($_POST['Tools'][5]['value'])?$_POST['Tools'][5]['value']:NULL;
        }else{
            $prev_main = NULL;
            $hrs  = NULL;
        }

     if (isset($_POST['Tools'])) {


            $model = new Tools;
            //$model->attributes  =   $_POST['Tools'];
            $model->tool_code = 0; //$item_code[$i];
            $model->unit = $unit;
            $model->make = $item_model->make;
            $model->model_no = $item_model->model_no;
            $model->serial_no = ($item_model->serial_no!='')?$item_model->serial_no:"None";
			      $model->warranty_date = $item_model->warranty_date;
            $model->batch_no = $item_model->unit;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            $model->item_id = $id;
            $status = Status::model()->find(array("condition" => "caption = 'Tool Set' AND status_type = 'pending_status'"));
            $model->pending_status = $status['sid'];
            //$model->id                      =   $id;
            $model->tool_category = $category;
            $model->location = $location;
            $model->qty = 1;

            /* prev main */

            if($prev_main != NULL){
              $future_date = date('Y-m-d H:i:s', strtotime('+7 days'));
              $model->prev_main =  '1';
              $model->prev_hrs  =  ($hrs!=NULL)?$hrs:24;
              $model->prev_date =   $future_date;
            }else{

              $model->prev_main =  '0';
              $model->prev_hrs  =  NULL;
              $model->prev_date =  NULL;
            }

            /* pre end */


            $toolcat = ToolCategory::model()->findByPk($category);

            $cat_id=$toolcat->cat_id;
            $qryres = Yii::app()->db->createCommand()
                    ->select('MAX(ref_value) AS maxval')
                    ->from('tms_tools u')
                    ->where('tool_category=:tool_category', array(':tool_category'=>$cat_id))
                    ->queryRow();



            if (!empty($qryres['maxval'])) {
                $pr_id = sprintf("%03d", $qryres['maxval'] + 1);
                $maxval = $pr_id;
            } else {
                $pr_id = sprintf("%03d", '001');
                $maxval = $pr_id;
            }

            $model->ref_no = $toolcat['cat_code'].'-' . $maxval;
            $model->ref_value = $maxval;
            $model->tool_name = $toolcat['cat_code'].'-' . $maxval;

            $qryres1 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and sid!=:id', array(':type' => 'pending_status', ':id' => $model->pending_status))
                    ->queryRow();


            //$model->pending_status          =   $qryres1['sid'];
            if ($model->save()) {
                $toolid = Yii::app()->db->getLastInsertID();
                /* $item_model->remaining_qty = ($item_model->remaining_qty - 1);
                  $item_model->save();
                  if($item_model->remaining_qty == 0){
                  $item_model->tool_status 	=    Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                  $item_model->save();
                  } */
                $newmodel = new ToolTransferRequest;
                //$newmodel->tool_id = $id;
                $qryres2 = Yii::app()->db->createCommand()
                        ->select('MAX(request_no) AS maxval')
                        ->from('tms_tool_transfer_request u')
                        //->where('id=:id', array(':id'=>$id))
                        ->queryRow();
                if (empty($qryres2['maxval'])) {
                    $rno = 1;
                } else {
                    $rno = $qryres2['maxval'] + 1;
                }

                $newmodel->request_no = $rno;
                $newmodel->request_from = $location;
                $newmodel->request_to = $location;
                $newmodel->request_date = date('Y-m-d');
                $newmodel->location = $location;

                $newmodel->created_by = Yii::app()->user->id;
                $newmodel->created_date = date('Y-m-d');
                $newmodel->updated_by = Yii::app()->user->id;
                $newmodel->updated_date = date('Y-m-d');
                $newmodel->save();
                $insert_id = Yii::app()->db->getLastInsertID();

                $newmodel1 = new ToolTransferItems;
                $newmodel1->tool_transfer_id = $insert_id;
                $newmodel1->tool_id = $toolid;
                $newmodel1->code = $toolcat['cat_code'] . $maxval;
                $qryres3 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_tools u')
                        ->where('id=:id', array(':id' => $toolid))
                        ->queryRow();

                $newmodel1->item_name = $qryres3['tool_name'];
                $newmodel1->unit = $qryres3['unit'];
                $newmodel1->serial_no = $qryres3['serial_no'];
                $newmodel1->req_date = date('Y-m-d');
                // $newmodel1->duration_in_days = $qryres3['tool_name;'];
                $newmodel1->qty = $qryres3['qty'];
                //$newmodel1->physical_condition = $qryres3['tool_name;'];
                $lid = $location;
                $qryres4 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_location_type u')
                        ->where('id=:id', array(':id' => $lid))
                        ->queryRow();

                $newmodel1->item_status = $qryres4['location_type'];
                $newmodel1->location = $location;
                $newmodel1->save();

                $tool_set = Tools::model()->findByPk($toolid);
                $data = '<option value="' . $tool_set->id . '">' . $tool_set->ref_no . '</option>';
                echo $data;
            }
        }
    }
    public function actionCategoryview($id) {
        //$this->layout = false;
        $page = 'categoryview';
        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('categoryview', array(
            'model' => $model, 'page' => $page,'id' => $id,
        ));
    }
    public function actionToolsetview($id,$type=NULL) {

        $page = 'toolsetview';
        $model = new ToolSet('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolSet'])) {
            $model->attributes = $_GET['ToolSet'];
        }

        $newmodel =  Tools::model()->findByPk($id);

        // new preventive maintaince

        if(isset($_POST['Tools'])){


          if(isset($_POST['prev_main'])){

            $future_date = date('Y-m-d H:i:s', strtotime('+7 days'));
            $prev_main = '1';
            $prev_hrs = (!empty($_POST['prev_hrs']))?$_POST['prev_hrs']:24;
            $prev_date = (!empty($_POST['Tools']['prev_date']))?$_POST['Tools']['prev_date'].' '.date('H:i:s'):$future_date;

            $command = Yii::app()->db->createCommand("UPDATE `tms_tools` SET `prev_main`= '".$prev_main."' , `prev_hrs`= '".$prev_hrs."' , `prev_date`='". $prev_date ."' where `id` = ".$id);
            $command->execute();

         }else{

             $command = Yii::app()->db->createCommand("UPDATE `tms_tools` SET `prev_main`= NULL , `prev_hrs`= NULL , `prev_date`= NULL where `id` = ".$id);
              $command->execute();

          }


              if($type==1){

                    $this->redirect(array('toolLog'));

                }else{

                    $this->redirect(array('toolsetadmin'));
                }



      }




        $this->render('toolsetview', array(
            'model' => $model, 'page' => $page,'id' => $id,'newmodel'=>$newmodel
        ));
    }
    public function actionServicereport(){
    //$this->layout = false;

        $tool_name = '';
        $tool_code = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) || (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
            $where = '';
            if ((isset($_POST['tool_name']) && !empty($_POST['tool_name'])) && (isset($_POST['tool_code']) && !empty($_POST['tool_code']))) {
                $tool_name = $_POST['tool_name'];
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}' and {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            } else if ((isset($_POST['tool_name']) && !empty($_POST['tool_name']))) {
                $tool_name = $_POST['tool_name'];
                $where = "where {$tblpx}tool_transfer_items.tool_id = '{$tool_name}'";
            } else if (isset($_POST['tool_code']) && !empty($_POST['tool_code'])) {
                $tool_code = $_POST['tool_code'];
                $where = "where {$tblpx}tool_transfer_items.code = '{$tool_code}'";
            }

        $tooldet  = Yii::app()->db->createCommand("select t.tool_name AS tname,t.ref_no AS company_slno,t.make AS make,tc.cat_name AS tcname,"
        . " if(tc.parent_id IS NULL,tc.cat_name,tc.parent_id) AS parent_category,pi.amount,p.supplier_name,"
        . "lt.id,s.caption AS present_status"
        . " FROM {$tblpx}tools t LEFT JOIN {$tblpx}tool_category tc"
        . " ON t.tool_category = tc.cat_id LEFT JOIN {$tblpx}purchase_items pi ON t.item_id = pi.id "
        . " LEFT JOIN {$tblpx}purchase p ON pi.purchase_id = p.id"
        . " LEFt JOIN {$tblpx}location_type lt ON t.location = lt.id "
        . " LEFT JOIN {$tblpx}status s ON lt.location_type = s.sid "
        . " WHERE t.id = '$tool_name'")->queryAll();

        $vendorrec = Yii::app()->db->createCommand("SELECT a.* FROM {$tblpx}tool_transfer_items a "
        . " LEFT JOIN {$tblpx}tool_transfer_request b ON a.id = b.request_no"
        . "  WHERE a.tool_id = '$tool_name' AND b.transfer_to = 'vendor' ")->queryAll();

        $tot_amt = 0;
        for($i=0;$i<count($vendorrec);$i++){
            $tot_amt = $tot_amt+$vendorrec[$i]['paid_amount'];
        }

        //echo "select tool_id, tool_transfer_id,request_from,request_to,request_status,item_name,{$tblpx}tool_transfer_items.code,item_status,req_date,{$tblpx}tool_transfer_items.location,caption,{$tblpx}location_type.name from {$tblpx}tool_transfer_items left join {$tblpx}tool_transfer_request on tool_transfer_id={$tblpx}tool_transfer_request.id  left join {$tblpx}status on sid = {$tblpx}tool_transfer_items.item_status left join {$tblpx}location_type on {$tblpx}location_type.id = {$tblpx}tool_transfer_items.location {$where} order by {$tblpx}tool_transfer_items.id ASC ";

        $sql = Yii::app()->db->createCommand("select tool_id, tool_transfer_id,request_from,request_to,request_status,item_name,{$tblpx}tool_transfer_items.code,item_status,req_date,{$tblpx}tool_transfer_items.location,caption,{$tblpx}location_type.name from {$tblpx}tool_transfer_items left join {$tblpx}tool_transfer_request on tool_transfer_id={$tblpx}tool_transfer_request.id  left join {$tblpx}status on sid = {$tblpx}tool_transfer_items.item_status left join {$tblpx}location_type on {$tblpx}location_type.id = {$tblpx}tool_transfer_items.location {$where} order by {$tblpx}tool_transfer_items.id ASC ")->queryAll();
        } else {

            $sql = array();
            $tooldet = array();
            $vendorrec = array();
            $tot_amt= '';
        }



        $this->render('servicereport', array(
            'sql' => $sql,'tooldet'=>$tooldet,'vendorrec'=>$vendorrec,'service_total'=>$tot_amt,'tool_name' => $tool_name, 'tool_code' => $tool_code,
        ));
    }


    /* cron job  for preventive notification */

    public function actionPreventiveinfomail(){



      $date        = date('Y-m-d H:i:s');
      $future_date = date('Y-m-d', strtotime('+7 days'));

      $tbl = Yii::app()->db->tablePrefix;
      $futu_tools= Yii::app()->db->createCommand("SELECT * FROM  {$tbl}tools WHERE `prev_main`='1' and `prev_date` BETWEEN '".$date ."' and '".$future_date."' ")->queryAll();
      $pre_tools = Yii::app()->db->createCommand("SELECT * FROM {$tbl}tools WHERE `prev_main`='1' and `prev_date` < '".$date."'"  )->queryAll();

       if( !empty($futu_tools)  || !empty($pre_tools) ){
           $this->sendMail($futu_tools, $pre_tools);
        }

    }

    public function sendMail($futu_tools, $pre_tools){

            $to_mail = array();

            $mail = new JPhpMailer;

           // $bodyContent = "<p>Hello,</p><p>Notification Tool Preventive maintainance.</p>";
            $bodyContent = $this->renderPartial('//tools/_emailform', array('fut_tools' =>$futu_tools,'pre_tools'=> $pre_tools), true);

           $tbl = Yii::app()->db->tablePrefix;

            $model =  Yii::app()->db->createCommand("SELECT email FROM {$tbl}mail_settings JOIN {$tbl}users on `tms_users`.`userid`= {$tbl}mail_settings.`user_id` " )->queryAll();

            if(!empty($model)){
              foreach ($model as $value) {
                 $to_mail[] = $value['email'];
              }
             }else{
                $model =  Yii::app()->db->createCommand("SELECT email FROM {$tbl}users where user_type=1" )->queryAll();
                foreach ($model as $value) {
                   $to_mail[] = $value['email'];
                }
           }

            /*$to_mails= Array ( 0=> 'anju.ba@bluehorizoninfotech.com',1=>'tony.xa@bluehorizoninfotech.com');*/


              foreach ($to_mail as $email) {
                  $mail->addAddress($email);
               }




             /* $mail->isSMTP();                                   // Set mailer to use SMTP
              $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                            // Enable SMTP authentication
              $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
              $mail->Password = 'Test2paSS()';                           // SMTP password
              $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = 465;
              $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);*/

              $mail->setFrom('info@bhiapp.com', Yii::app()->name);
              $mail->isHTML(true);

              $mail->Subject = Yii::app()->name." - Preventive Maintainance Notifications";
              $mail->Body = $bodyContent;
              $mail->Send();

             /* if($mail->Send()){
                 echo "success";exit;
              }else{
                echo "error ";exit;
              }*/

       }

       public function actiontooldisplay(){


        $this->layout = false;
        $page = 'toolsview';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page_type = isset($_REQUEST['page_type']) ? $_REQUEST['page_type'] : "";

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('toolsdisplay', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type, 'page_type' => $page_type,
        ));


    }

      public function actiondisplaytools(){


        $this->layout = false;
        $page = 'toolsview1';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page_type = isset($_REQUEST['page_type']) ? $_REQUEST['page_type'] : "";

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('dislaytools', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type, 'page_type' => $page_type,
        ));


    }


    public function actiontransferalltools(){


        $this->layout = false;
        $page = 'toolstransferall';
        $loc = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        $code = isset($_REQUEST['code']) ? $_REQUEST['code'] : "";
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page_type = isset($_REQUEST['page_type']) ? $_REQUEST['page_type'] : "";

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('dislaytools', array(
            'model' => $model, 'page' => $page, 'loc' => $loc, 'code' => $code,'type' => $type, 'page_type' => $page_type,
        ));


    }


    public function actionAddtoolmultiple() {

         // $id = $_POST['id'];
        if (isset($_POST['id'])) {

            $ids = NULL;
            $ids =$_POST['id'];

            $url = $_GET['r'];

            $page = substr($url, strrpos($url, '/') + 1);

            foreach ($ids as $id) {
                $toolslist = Tools::model()->findByPk($id);
                $name[] = $toolslist->tool_name;
                $refcode[] = $toolslist->ref_no;
                $unit = $toolslist->unit;
                $qty[] = $toolslist->qty;
                $tool_id[] = $toolslist->id;
                //$serial_no[] = $toolslist->serial_no;
            }


            /* unit */
            $unitlist = CHtml::listData(Unit::model()->findAll(
                                    array(
                                        'select' => array('id,unitname'),
                                        'distinct' => true,
                                        'condition' => 'id = ' . $unit,
                            )), "id", "unitname");
            $units = "<option value=''>Please choose </option>";

             foreach ($unitlist as $unitid => $unitname)
                $units .= CHtml::tag('option', array('value' => $unitid), CHtml::encode($unitname), true);
              /* unit end */

              /* status */

               $statuslist = CHtml::listData(TmsStatus::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'distinct' => true,
                                        'condition' => 'status_type = "physical_condition"',
                            )), "sid", "caption");


               $status = "<option value=''>Please choose </option>";
              foreach ($statuslist as $sid => $caption)
                $status .= CHtml::tag('option', array('value' => $sid), CHtml::encode($caption), true);

              /* status end */

              /* duration */
                  $duration_type = CHtml::listData(Status::model()->findAll(
                                    array(
                                        'select' => array('sid,caption'),
                                        'distinct' => true,
                                        'condition' => 'status_type = "duration_type"',
                            )), "sid", "caption");

                  $duration = "<option value=''>Please choose </option>";
                  foreach ($duration_type as $sid => $caption)
                   $duration .= CHtml::tag('option', array('value' => $sid), CHtml::encode($caption), true);


                   /*duration end */

                $head= ' <tr id="head"><td>No.</td><td>Type</td><td>Code</td><td></td> <td>Item Name</td>
                        <td>Units</td>';
                if ($page != 'stock') {
                $head .= '<td>Duration</td><td>Duration Type</td>';
                  }
                if ($page == 'stock') {
                  $head .= ' <td>Physical Condition</td>';
                }
                $head .= ' <td>Quantity</td>
                        <td></td>
                    </tr>';

              $table = "<tr class='worktr'>";

              foreach($ids as $a => $v) {

                  $no= $a+1;

                  $table  .= '<td>'.$no.'</td>';

                  $table .= '<td><select class="inputs form-control target type_'.$a.'" >'
                          . '<option value="0" selected>Tools</option>'
                          . '<option value="1">Tool Set</option>'
                          . '</select> </td>';
                  $table .= '<td><input type="text" name="code[]" class="inputs form-control target code" id="code_'.$a.'" value='.$refcode[$a].' readonly=true/></td>';
                   $table .='<td> </td>';
                  $table .= '<td><input type="text" name="item_name[]" class="inputs form-control target item_name" id="item_name_'. $a .'"  value='.$name[$a].' readonly=true/></td>';

                  $table .= '<td><select class="inputs form-control target unit" name="unit[]">'.$units.' </select></td>';
                  if($page!='stock'){
                     $table .= '<td><input type="text" name="duration[]" class="inputs form-control target duration" id="duration_' . $a . '" /></td>';

                  $table .='<td><select class="inputs form-control target duration_type" name="duration_type[]" id="duration_type_' . $a . '">'. $duration.'</select></td>';

                  }
                  if($page == 'stock'){
                   $table .='<td><select class="inputs form-control target physical_condition" name="physical_condition[]" id="physical_condition_' . $a . '">'. $status.'</select></td>';
                  }
                  $table .= '<td><input type="text" name="quantity[]" class="inputs lst form-control target quantity" id="quantity_' . $a . '"  readonly="true" value='.$qty[$a].' /></td>';
                  $table .='<td><input type="text" name="newcode[]"  value='.$tool_id[$a].' class="inputs form-control target newcode" id="newcode_' .$a . '" style="display:none"/> </td>';
                  $table .= '<td><input type="button" class="btn green btn-xs delete" data-id="' .$a. '" value="Delete"></td>';

                  $table .= "</tr><br> ";

                }
             echo CJSON::encode(array('name' => $name, 'refcode' => $refcode, 'units' => $units,'qty' =>$qty,'unit' => $unit, 'table'=> $table ,'head' =>$head ,));
        }


    }


    public function actiontoolLog(){

            $page = 'toollog';
            $model = new Tools('search');


            $catTree = Tools::allcategories();
            $categories_list = self::visualTree($catTree, 0);

            $model->unsetAttributes();
            if (isset($_GET['Tools'])) {
                $model->attributes = $_GET['Tools'];
            }

            $this->render('toollog', array(
                'model' => $model, 'page' => $page,'testmodel'=>$categories_list,
            ));

    }

    private static function visualTree($catTree, $level) {
            $res = array();
            foreach ($catTree as $item) {
                $res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['text'];
                if (isset($item['children'])) {
                    $res_iter = self::visualTree($item['children'], $level + 1);
                    foreach ($res_iter as $key => $val) {
                        $res[$key] = $val;
                    }
                }
            }
            return $res;
     }


    public function actionPrev_request($id){



        $model= new PreventiveRequest;

        $newmodel = $this->loadModel($id);

         // Uncomment the following line if AJAX validation is needed

        $this->performValidation1($model);

        if(isset($_POST['PreventiveRequest']))
        {

          $model->attributes = $_POST['PreventiveRequest'];
          $model->tool_id = $id;
          $model->status = "pending";
          $model->created_by = Yii::app()->user->id;
          $model->created_date = date('Y-m-d H:i:s');

          /* check exist */

          $check = PreventiveRequest::model()->find('tool_id = '. $id.' and status = "pending"  and created_by = '. Yii::app()->user->id);

          if(empty($check)){
            if($model->save())

              $insert_id = Yii::app()->db->getLastInsertID();

              $email = $this->sendemailrequest($insert_id, $id);

              Yii::app()->user->setFlash('success','Request send');
              $this->redirect(array('toollog'));
          }else{

            Yii::app()->user->setFlash('error','Request alredy sended ');

          }


        }

        $this->render('prevrequest',array(
          'model'=>$model,'newmodel'=>$newmodel
        ));




    }

    public function sendemailrequest($insertid, $tool_id){

            $to_mail = array();

            $mail = new JPhpMailer;

            $bodyContent = $this->renderPartial('//tools/_emailformrequest', array('id'=>$insertid,'tool_id'=>$tool_id), true);



           $tbl = Yii::app()->db->tablePrefix;

            $model =  Yii::app()->db->createCommand("SELECT email FROM {$tbl}mail_settings JOIN {$tbl}users on `tms_users`.`userid`= {$tbl}mail_settings.`user_id` " )->queryAll();

            if(!empty($model)){
              foreach ($model as $value) {
                 $to_mail[] = $value['email'];
              }
             }else{
                $model = Yii::app()->db->createCommand("SELECT email FROM {$tbl}users where user_type= 1" )->queryAll();
                foreach ($model as $value) {
                   $to_mail[] = $value['email'];
                }
            }

             foreach ($to_mail as $email) {
                  $mail->addAddress($email);
               }


              $mail->setFrom('info@bhiapp.com', Yii::app()->name);
              $mail->isHTML(true);
              $mail->Subject = Yii::app()->name." - Preventive Maintainance Service Request";
              $mail->Body = $bodyContent;
              $mail->Send();
              return true;
      }


      public function actionPrevApprove($id)
      {

         //$model = new PreventiveRequest;
        $check = PreventiveRequest::model()->find('tool_id = '.$id.' and status = "pending" ');


        if(empty($check)){
          $this->redirect(array('toolLog'));
        }

        $pr_id = $check->pr_id;
        $model = PreventiveRequest::model()->findByPk($pr_id);

        $newmodel = $this->loadModel($id);

        if(isset($_POST['PreventiveRequest'])){



            $model->remarks = $_POST['PreventiveRequest']['remarks'];
            $model->status  = 'approved';
            $date = $model->date;
            $hrs =  $newmodel->prev_hrs;
            $hid = 24;
             $days = round($hrs/$hid);
             if($days < 1){
               $count = 1;
             }else{
               $count = $days;
             }

          $prev_date = date('Y-m-d', strtotime($date . '+ '.$count.'day'));

          if($model->save()){

            $command = Yii::app()->db->createCommand("UPDATE `tms_tools` SET `prev_main`= '1' , `prev_hrs`= 24 , `prev_date`='". $prev_date ."' where `id` = ".$id);
            $command->execute();

            $this->redirect(array('toolLog'));

          }


        }

          $this->render('prevrequestapprove',array(
            'model'=>$model,'newmodel'=>$newmodel
          ));


      }


    protected function performValidation1($model)
    {
      if(isset($_POST['ajax']) && $_POST['ajax']==='preventive-request-form')
      {
        echo CActiveForm::validate($model);
        Yii::app()->end();
      }


    }

/*-----------------------------------*/
    public function actionGetsitesbyitemid(){
        if(isset($_REQUEST['item_id']) && $_REQUEST['item_id'] != ''){
            $tblpx = Yii::app()->db->tablePrefix;
            $id = $_REQUEST['item_id'];
            $getsites = Yii::app()->db->createCommand("SELECT lt.name,lt.id FROM {$tblpx}location_type lt JOIN {$tblpx}tools tti ON tti.location = lt.id WHERE tti.id = '".$id."' GROUP BY lt.id")->queryAll();
            $option = "<option value=''>Please choose</option>";
            if(!empty($getsites)){
                foreach ($getsites as $site){
                    $option .= "<option value='".$site['id']."'>".$site['name']."</option>";
                }
            }
            echo $option;
        }
    }
    public function actionGetsitedatabyitemid(){
        
        if(isset($_REQUEST['item_id']) && $_REQUEST['item_id'] != ''){
            $tblpx = Yii::app()->db->tablePrefix;
            $id = $_REQUEST['item_id'];
            $tools = Yii::app()->db->createCommand("SELECT tool_name FROM {$tblpx}tools WHERE id = {$id}")->queryRow();
            $toolname = $tools["tool_name"];

            $getsites = Yii::app()->db->createCommand("SELECT id,name FROM {$tblpx}location_type WHERE id IN(SELECT DISTINCT location FROM {$tblpx}tools WHERE tool_name = '{$toolname}')")->queryAll();
            $option = "<option value=''>Please choose</option>";
            if(!empty($getsites)){
                foreach ($getsites as $site){
                    $option .= "<option value='".$site['id']."'>".$site['name']."</option>";
                }
            }
            echo $option;
        }
    }
    public function actionGetitemsbysite(){
        $site = isset($_GET['siteid']) ? $_GET['siteid'] :'';
        $tool = isset($_GET['itemid']) ? $_GET['itemid'] :'';
        $reqid = isset($_GET['req_id']) ? $_GET['req_id'] :'';
        $info = isset($_GET['info']) ? $_GET['info'] :'';
        $this->layout = false;
        $tool_category = array();
        if(!empty($reqid)){
            $result = Yii::app()->db->createCommand("SELECT tool_category FROM `tms_tool_transfer_info` left join tms_tools on tms_tools.id = `tms_tool_transfer_info`.`tool_id`
          WHERE `tool_transfer_id` = ".$reqid." AND tool_id = ".$tool)->queryAll();

          foreach ($result as $key => $value) {
             $tool_category[] = $value['tool_category'];
          }
           $tool_category = implode(',', $tool_category);

        }
        $type = ToolTransferInfo::model()->findByPk($info);
        if(!empty($type)){
            $ty = $type['type'];
        }else{
            $ty = 0;
        }
        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('alltoolsview', array(
            'model' => $model, 'loc' => $site,'cate' => $tool_category,'toolid' => $tool,'type' => $ty
        ));

    }
    public function actionGetitemdetailsbysite(){
       
        $site = isset($_GET['siteid']) ? $_GET['siteid'] :'';
        $tool = isset($_GET['itemid']) ? $_GET['itemid'] :'';
        $reqid = isset($_GET['req_id']) ? $_GET['req_id'] :'';
        $info = isset($_GET['info']) ? $_GET['info'] :'';
        $requested_qty = isset($_GET['requested_qty']) ? $_GET['requested_qty'] :'';
        $this->layout = false;
        $tool_category = array();
        if(!empty($reqid)){
           
            $result = Yii::app()->db->createCommand("SELECT tool_category,duration_in_days FROM `tms_tool_transfer_info` left join tms_tools on tms_tools.id = `tms_tool_transfer_info`.`tool_id`
          WHERE `tool_transfer_id` = ".$reqid." AND tool_id = ".$tool)->queryAll();
            // print_r($result[0]['duration_in_days']);die;
          foreach ($result as $key => $value) {
             $tool_category[] = $value['tool_category'];
             $tool_duration[]=$value['duration_in_days'];
          }
           $tool_category = implode(',', $tool_category);
           $tool_duration=implode(',', $tool_duration);

        }
        $type = ToolTransferInfo::model()->findByPk($info);
        if(!empty($type)){
            $ty = $type['type'];
        }else{
            $ty = 0;
        }

        $new_model = Tools::model()->findByPk($type->tool_id);
        $tool_type = $new_model->serialno_status;

        $model = new Tools('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Tools'])) {
            $model->attributes = $_GET['Tools'];
        }

        $this->render('alltoolsview', array(
            'model' => $model, 'loc' => $site,'cate' => $tool_category,'duration'=>$tool_duration,'toolid' => $tool,'type' => $ty ,'requested_qty' => $requested_qty,'tool_type' => $tool_type,'reqid'=>$reqid
        ));

    }
    /*-----------------------------*/
    public function actionDeleteolddata() {
        $tblpx  = Yii::app()->db->tablePrefix;
        $sql    = "SELECT id FROM {$tblpx}tools t WHERE t.tool_category NOT IN (1,2,3,4,5) GROUP BY t.tool_category, t.location
                    UNION
                    SELECT id FROM {$tblpx}tools t WHERE t.tool_category IN (1,2,3,4,5)";
        $array  = Yii::app()->db->createCommand($sql)->queryAll();

        foreach($array as $arr)
        {
            $id[] = $arr["id"];
        }
        $newsql     = "SELECT count(*) as quantity, id, tool_category, location FROM {$tblpx}tools t WHERE t.tool_category NOT IN (1,2,3,4,5) GROUP BY t.tool_category, t.location";
        $data       = Yii::app()->db->createCommand($newsql)->queryAll();
        $i          = 0;
        foreach($data as $tool) {
            if($tool['quantity'] > 50) {
                $model              = Tools::model()->findByPk($tool["id"]);
                $model->qty         = $tool["quantity"];
                $model->stock_qty   = $tool["quantity"];
                if($model->save()) {
                    $i = $i + 1;
                }
            } else {
                $newsql1    = "SELECT *  FROM {$tblpx}tools t WHERE t.tool_category = {$tool["tool_category"]} AND location = {$tool["location"]}";
                $data1      = Yii::app()->db->createCommand($newsql1)->queryAll();
                foreach($data1 as $data) {
                    $id[] = $data["id"];
                }
            }
        }
        $id        = array_unique($id);
        $ids        = implode (", ", $id);
        if($i > 0) {
            $deleteSql  = "DELETE FROM tms_tools WHERE id NOT IN ({$ids})";
            $query      = Yii::app()->db->createCommand($deleteSql)->execute();
            if($query) {
                echo "Deleted!";
            } else {
                echo "Failed! Please try again.";
            }
        } else {
            echo "No item found";
        }

    }
    public function actionSerialnumberrename() {
        $tblpx  = Yii::app()->db->tablePrefix;
        $sql    = "SELECT * FROM {$tblpx}tools WHERE tool_category NOT IN(1,2,3,4,5) AND qty > 50 AND serialno_status = 'Y'";
        $data   = Yii::app()->db->createCommand($sql)->queryAll();
        $i      = 0;
        foreach($data as $tool) {
            $ref_no = $tool["ref_no"];
            if (strpos($ref_no, '-') !== false) {
                $ref_no = substr($ref_no, 0, strpos($ref_no, "-"));
                $model  = Tools::model()->findByPk($tool["id"]);
                $model->ref_no          = $ref_no;
                $model->serialno_status = 'N';
                if($model->save()) {
                    $i = $i + 1;
                }
            }
        }
        if($i == 0) {
            echo "No items found!";
        } else {
            echo "{$i} field are updated!";
        }
    }






}
