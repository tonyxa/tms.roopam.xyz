<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $defaultAction = 'login';

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

		if(!isset(Yii::app()->user->role))
		   $this->redirect(array('/site/login'));

        $this->render('index');
    }



    public function actionTable() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

		if(!isset(Yii::app()->user->role))
		   $this->redirect(array('/site/login'));

        $this->render('tables');//http://localhost/pmsyii/index.php?r=site/login
    }

    /**
     * This is the action to handle external exceptions.
     */
    /* public function actionError() {

        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    } */

    /* created by arun on 15-5-17 */
    public function actionError() {

        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else{
                 if(!Yii::app()->user->mainuser_id){
                     $this->render('error', $error);
                }else{

                $this->render('error', $error);
                }
            }
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }



       public function actionLogin() {

       		if(!Yii::app()->user->isGuest){
	       	    $this->redirect(array('site/index'));
	    	}

        $this->layout = "//layouts/login";

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                //$this->redirect(Yii::app()->user->returnUrl);
                $this->redirect(array('site/index'));
            }
        }
        // display the login form
        $this->render('login', array('model' => $model ));
    }
    public function actionIndex2() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

		if(!isset(Yii::app()->user->role))
		   $this->redirect(array('/site/login'));

        $this->render('index2');
    }


    /**
     * Displays the login page
     */
    public function actionLogin1() {


$this->layout = '//layouts/login';

        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('site/index'));
        }

        $model = new LoginForm;
         //echo "<pre>";print_r($_POST);echo "</pre>";

    //print_r($_POST);
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        // collect user input data
        if (isset($_POST['LoginForm'])) {


            $model->attributes = $_POST['LoginForm'];

            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())

                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));

    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }


    /* test mail */
    public function actionTestemail() {
	/*
         $from = 'info@bhiapp.com';
         $toname ='Testing';
         $tomail = 'tony.xa@bluehorizoninfotech.com';
         $subject = 'test mail subject';
         $body ='test mail content';
            $name='=?UTF-8?B?'.base64_encode($toname).'?=';
                $subject='=?UTF-8?B?'.base64_encode($subject).'?=';
                $headers="From: $name <{$from}>\r\n".
                    "Reply-To: {$from}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/html; charset=UTF-8";

                    mail($tomail,$subject,$body,$headers);
        */

      }

      public function actionApplicationlog($reset=0){

        if($reset==1){
            $applog = ('protected/runtime/application.log');

            $logfile = fopen($applog, "w") or die("Unable to open file!");
            $txt = "";
            fwrite($logfile, $txt);
            fclose($logfile);

            $this->redirect(array('/site/applicationlog'));
            exit;
        }

        $lines = file("protected/runtime/application.log");
        $log = '';
        foreach ($lines as $line_num => $line) {
            $log.= $line . "\n";
        }
    $this->render('applicationlog',array('log'=>$log));

    }              

}
