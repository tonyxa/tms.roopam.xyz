<?php

class ToolTransferRequestController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'admin', 'checkvalidation', 'deleteitem', 'finalapproval', 'vendorview', 'updateitem', 'gettool', 'gettooldetails', 'vendordetails', 'finalreject', 'site', 'createsite', 'siteview', 'items', 'returndate','dispatchitem','singledispatch','siteviewallocate','rejectitem','deleteallocateditem','updatesite','deletesite','getitemscode','createstock','updatestock','deletestock','scheduleddate','getvendoritemscode','createvendor','updatevendor','deletevendor','getsiteById','updatesitelocation'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'Approval' ,'AssignStatus','dispatchrequesttoStock1' ,'MissingItems', 'MissingItemsView' ,'RequesttoStock','StockApprove','Stocksubmit','changesupervisor','getengineer','Assignsubmit','Changeeng','getitems'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id, $page = '') {
        // $items = ToolTransferItems::model()->findAll(array('condition'=>'tool_transfer_id ='.$id));
        $items = new ToolTransferItems('search');



        $this->render('view', array(
            'model' => $this->loadModel($id), 'items' => $items, 'id' => $id, 'page' => $page,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($page) {

        //$this->layout = false;
        $model = new ToolTransferRequest;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));
        if (isset($_POST['ToolTransferRequest'])) {

            /*print_r($_POST['ToolTransferRequest']);exit;*/

            $model->attributes = $_POST['ToolTransferRequest'];
            $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
            $model->request_date = date('Y-m-d');
            if ($page == "site") {
                $model->location = $_POST['ToolTransferRequest']['request_from'];
            } else {
                $model->location = $_POST['ToolTransferRequest']['request_to'];
            }

            if (Yii::app()->user->role == 1) {
                $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
            } else {
                $model->request_owner_id = Yii::app()->user->id;
            }
            if ($page == "vendor") {
                //$model->request_status = 10;

                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                        ->queryRow();
                $model->request_status = $qryres1['sid'];
            } else {
                //$model->request_status = 6;

                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
                        ->queryRow();
                $model->request_status = $qryres1['sid'];
            }
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->transfer_to = $page;
            if ($model->save()) {
                $sl_No = $_POST['unit'];
                $insert_id = Yii::app()->db->getLastInsertID();
                for ($i = 0; $i < sizeof($sl_No); $i++) {
                    //print_r($_POST);die;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $newcode = isset($_POST['newcode'])?$_POST['newcode']:1;
                    $item_name = isset($_POST['item_name'])?$_POST['item_name']:'test';
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferItems;
                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                    $newmodel->unit = $unit[$i];
                    $newmodel->code = $code[$i];
                    $newmodel->tool_id = $newcode[$i];
                    $newmodel->item_name = $item_name[$i];
                    // $newmodel->serial_no = $serial_no[$i];
                    $newmodel->duration_in_days = isset($duration[$i]) ? $duration[$i] : '';
                    $newmodel->duration_type = isset($duration_type[$i]) ? $duration_type[$i] : '';
                    $newmodel->qty = $quantity[$i];
                    $newmodel->physical_condition = $physical_condition[$i];


                    if ($page == "vendor") {
                        $reqto = $_POST['ToolTransferRequest']['request_to'];
                        $location = LocationType::model()->findByPk($reqto)->location_type;
                        $newmodel->item_status = $location;
                        $newmodel->location = $reqto;
                        if($newcode[$i] != NULL) {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->location = $reqto;
$tools->save();
   }
                    } else if ($page == "site") {
                        $reqfrom = $_POST['ToolTransferRequest']['request_from'];
                        $location = LocationType::model()->findByPk($reqfrom)->location_type;
                        $newmodel->item_status = $location;
                        $newmodel->location = $reqfrom;
                        if($newcode[$i] != NULL) {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->location = $reqfrom;
$tools->save();
   }
                    } else if ($page == "stock") {
                        $reqto = $_POST['ToolTransferRequest']['request_to'];
                        $location = LocationType::model()->findByPk($reqto)->location_type;
                        $newmodel->item_status = $location;
                        $newmodel->location = $reqto;
                        if($newcode[$i] != NULL) {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->location = $reqto;
$tools->tool_condition = $physical_condition[$i];
$tools->save();
   }
                    }
                    $newmodel->save();
                }
                if ($page == "site") {


                    // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                    $tbl = Yii::app()->db->tablePrefix;
                    $getdata = "SELECT * FROM {$tbl}users
INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                    $users = Yii::app()->db->createCommand($getdata)->queryAll();

                    if ($users != NULL) {
                        $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                    }
                    if ($users != NULL) {
                        foreach ($users as $user) {
                            // require_once 'PHPMailer/class.phpmailer.php';
                            $mail = new JPhpMailer;
                            $newemail = $user->email;

                            $email = $newemail;
                            $subject = Yii::app()->name;
                            $headers = Yii::app()->name;

                            $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                            $bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $insert_id), true);

                            /*
                              $mail->isSMTP();                                   // Set mailer to use SMTP
                              $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
                              $mail->SMTPAuth = true;                            // Enable SMTP authentication
                              $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
                              $mail->Password = 'Test2paSS()';                           // SMTP password
                              $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                              $mail->Port = 465;

                              $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);

                             */
                            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                            $mail->addAddress($email);   // Add a recipient
                            $mail->isHTML(true);

                            $mail->Subject = Yii::app()->name;
                            $mail->Body = $bodyContent;
                            $mail->Send();
                            /* if (!$mail->Send()) {
                              Yii::app()->user->setFlash('error', 'There was an error sending the message!');
                              } else {

                              Yii::app()->user->setFlash('success', 'Message was sent successfully.');

                              } */
                        }
                    }
                }
                $this->redirect(array('/toolTransferRequest&page=' . $page));
            }
        }

        $this->render('create', array(
            'model' => $model, 'units' => $units, 'page' => $page, 'status' => $status, 'duration_type' => $duration_type,
        ));
    }

    public function actionCreatestock($page) {
        //$this->layout = false;
        $model = new ToolTransferRequest;
         $newmodel = new ToolTransferItems;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));
        if(isset($_POST['ToolTransferRequest'])) {
          // print_r($newcode);
          // exit();

            $req_no = $_POST['ToolTransferRequest']['request_no'];

            $transaction=$_POST['trans_id'];
            if($transaction == '' || $transaction == 0){

                $model->attributes = $_POST['ToolTransferRequest'];
                $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
                $model->request_date = date('Y-m-d');
                if ($page == "site") {
                    $model->location = $_POST['ToolTransferRequest']['request_from'];
                } else {
                    $model->location =1;
                }

            if (isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
                $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
            } else {
                $model->request_owner_id = Yii::app()->user->id;
            }
            if ($page == "vendor") {
                //$model->request_status = 10;

                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                        ->queryRow();
                $model->request_status = $qryres1['sid'];
            } else {
                //$model->request_status = 6;

                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
                        ->queryRow();
                $model->request_status = $qryres1['sid'];
            }
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->transfer_to = $page;
            if ($model->save()) {
                $sl_No = $_POST['unit'];
                $insert_id = Yii::app()->db->getLastInsertID();

                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $type=$_POST['type'];
                    $newcode = isset($_POST['newcode'])?$_POST['newcode']:1;
                    $item_name = isset($_POST['item_name'])?$_POST['item_name']:'test';
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : 3;
                    $itemexists =  Yii::app()->db->createCommand("SELECT * FROM tms_tool_transfer_items WHERE tool_transfer_id =".$transaction." and tool_id = ".$newcode." and location = 1")->queryRow();

                    if(!empty($itemexists)){
                        $newmodel =  ToolTransferItems::model()->findByPk($newcode);
                    }else{
                        $newmodel = new ToolTransferItems;
                    }

                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                    $newmodel->unit = $unit;
                    $newmodel->code = $code;
                    $newmodel->tool_id = $newcode;
                    $newmodel->item_name = $item_name;
                    $newmodel->type = $type;
                    // $newmodel->serial_no = $serial_no[$i];
                    $newmodel->duration_in_days   = isset($duration) ? $duration : '';
                    $newmodel->duration_type      = isset($duration_type) ? $duration_type : '';
                    $newmodel->qty                = $quantity;
                    $newmodel->physical_condition = $physical_condition;

                    $itemsts_qty = 0;
                    if($page == "stock"){

                        $sts_type_all =  Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));
                        foreach($sts_type_all as $dat){
                           if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=''){
                                $itemsts_qty+= $_POST[$dat['caption']];
                            }
                        }


                        $reqto                 = 1;
                        $location              = LocationType::model()->findByPk($reqto)->location_type;
                        // $newmodel->item_status = $location;
                        $newmodel->item_status = 29;
                        $newmodel->location    = $reqto;
                        if($newcode != NULL){
                            $tools  = Tools::model()->findByPk($newcode);
                            $check =  Yii::app()->db->createCommand("SELECT * FROM `tms_tools` WHERE location = 1 and  parent_id = ".$newcode)->queryRow();

                            if($itemsts_qty == $quantity){
                                if(!empty($check)){
                                   $checkqty =  $check['qty'];
                                   $tools->qty       = $checkqty + $tools->qty;
                                   $tools->stock_qty = $tools->qty;
                                   Yii::app()->db->createCommand("DELETE FROM tms_tools WHERE parent_id = ".$newcode)->execute();
                                }
                                // $tools->location       = $reqto;
                                $tools->tool_condition = 3;
                                $tools->save();
                                $curr_tool_id = ($check['id'] != NULL)?$check['id']:$newcode;
                                //$curr_tool_id = $check['id'];
                            }else{
                                  // $tools->location       = $reqto;
                                  $tools->qty            = $quantity-$itemsts_qty;
                                  $tools->stock_qty      = $quantity-$itemsts_qty;
                                  $tools->tool_condition = 3;
                                  $tools->save();

                                  if(empty($check)){

                                    $qryres1 = Yii::app()->db->createCommand()->select('*')->from('tms_status')
                                            ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Stock'))->queryRow();
                                    $qryres2 = Yii::app()->db->createCommand()->select('*')->from('tms_location_type')
                                            ->where('location_type=:type ', array(':type' =>$qryres1['sid'] ,))->queryRow();
                                    $newtoolsmodel  = new Tools;
                                    $newtoolsmodel->attributes = $tools->attributes;
                                    $newtoolsmodel->qty        = $itemsts_qty;
                                    $newtoolsmodel->stock_qty  = $itemsts_qty;
                                    // $newtoolsmodel->location   = $qryres2['id'];
                                    $newtoolsmodel->parent_id  = $newcode;
                                    $newtoolsmodel->save();
                                    $curr_tool_id = $newtoolsmodel->id;
                                  }else{
                                      $stocktools      = Tools::model()->findByPk($check['id']);
                                      $stocktools->qty = $check['qty']+$itemsts_qty;
                                      $stocktools->save();
                                      $curr_tool_id = ($check['id'] != NULL)?$check['id']:$newcode;
                                      //$curr_tool_id = $check['id'];
                                  }
                            }
                         }
                    }
                    $newmodel->tool_id = $curr_tool_id;
                    $newmodel->save();

                    // insert req items qty based on status
                    if(!empty($itemexists)){
                        $itemid =  $newcode;
                    }else{
                        $itemid = Yii::app()->db->getLastInsertID();
                    }

                    if($page == "stock"){

                      $sts_type_all =  Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));
                      foreach($sts_type_all as $dat){
                         if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=''){
                           $itemcheck = Yii::app()->db->createCommand("SELECT * FROM tms_reqst_itemqty WHERE tool_transfer_id = ".$insert_id." and transfer_item_id = ".$itemid." and physical_condition = ".$dat['sid'])->queryRow();

                              if(empty($itemcheck)){
                                $reqmodel = new ReqstItemqty;
                                $reqmodel->tool_transfer_id   = $insert_id;
                                $reqmodel->transfer_item_id   = $itemid;
                                $reqmodel->physical_condition = $dat['sid'];
                                $reqmodel->qty                = $_POST[$dat['caption']];
                                $reqmodel->save();
                              }else{
                                $reqmodel      =  ReqstItemqty::model()->findByPk($itemcheck['id']);
                                $reqmodel->qty  = $_POST[$dat['caption']]+ $reqmodel->qty;
                                $reqmodel->save();
                              }
                          }
                      }
                      $itemmodel =  ToolTransferItems::model()->findByPk($itemid);
                      $itemmodel->return_qty = $itemsts_qty;
                      $itemmodel->save();
                    }

                    $this->redirect(array('/toolTransferRequest/createstock','page'=>$page,'tr_id'=>$model->id));
                }

            }  else {
                $newcode = isset($_POST['newcode'])?$_POST['newcode']:1;
                $model = $this->loadModel($transaction);
                $model->attributes = $_POST['ToolTransferRequest'];
                if(isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
                    $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
                } else {
                    $model->request_owner_id=Yii::app()->user->id;
                }

                if ($model->save()) {
                    $insert_id = $transaction;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $type=$_POST['type'];
                    $newcode = isset($_POST['newcode'])?$_POST['newcode']:1;
                    $item_name = isset($_POST['item_name'])?$_POST['item_name']:'test';
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;


                    $itemexists =  Yii::app()->db->createCommand("SELECT * FROM tms_tool_transfer_items WHERE tool_transfer_id =".$transaction." and tool_id = ".$newcode." and location = 1")->queryRow();

                    if(!empty($itemexists)){
                       $newmodel =  ToolTransferItems::model()->findByPk($itemexists['id']);
                    }else{
                      $newmodel = new ToolTransferItems;
                      $newmodel->tool_transfer_id = $insert_id;
                      $newmodel->req_date = date('Y-m-d');
                      $newmodel->unit = $unit;
                      $newmodel->code = $code;
                      $newmodel->tool_id = $newcode;
                      $newmodel->item_name = $item_name;
                      $newmodel->type=$type;
                      // $newmodel->serial_no = $serial_no[$i];
                      $newmodel->duration_in_days = isset($duration) ? $duration : '';
                      $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                      $newmodel->qty = $quantity;
                      $newmodel->physical_condition = $physical_condition;
                        if($page == "stock"){
                          $reqto = 1;
                          $location = LocationType::model()->findByPk($reqto)->location_type;
                        //   $newmodel->item_status = $location;
                           $newmodel->item_status = 29;
                          $newmodel->location = $reqto;
                        }
                    }

                    $itemsts_qty = 0;
                    if($page == "stock") {
                          $sts_type_all =  Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));
                          foreach($sts_type_all as $dat){
                             if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=''){
                                  $itemsts_qty+= $_POST[$dat['caption']];
                               }
                             }
                         if($newcode != NULL) {
              $tools = Tools::model()->findByPk($newcode);
                            $check =  Yii::app()->db->createCommand("SELECT * FROM `tms_tools` WHERE location = 1 and  parent_id = ".$newcode)->queryRow();

                            if($itemsts_qty == $quantity){
                                  if(!empty($check)){
                                     $checkqty =  $check['qty'];
                                     $tools->qty       = $checkqty + $tools->qty;
                                     $tools->stock_qty = $tools->qty;
                                     Yii::app()->db->createCommand("DELETE FROM tms_tools WHERE parent_id = ".$newcode)->execute();
                                  }
                                //   $tools->location       = 1;
                                  $tools->tool_condition = 3;
                                  $tools->save();
                            }else{
                                // $tools->location       = $reqto;
                                $tools->qty            = $quantity-$itemsts_qty;
                                $tools->stock_qty      = $quantity-$itemsts_qty;
                                $tools->tool_condition = 3;
                                $tools->save();

                                if(empty($check)){

                                  $qryres1 = Yii::app()->db->createCommand()->select('*')->from('tms_status')
                                          ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Stock'))->queryRow();
                                  $qryres2 = Yii::app()->db->createCommand()->select('*')->from('tms_location_type')
                                          ->where('location_type=:type ', array(':type' =>$qryres1['sid'] ,))->queryRow();
                                  $newtoolsmodel  = new Tools;
                                  $newtoolsmodel->attributes = $tools->attributes;
                                  $newtoolsmodel->qty        = $itemsts_qty;
                                  $newtoolsmodel->stock_qty  = $itemsts_qty;
                                  $newtoolsmodel->location   = $qryres2['id'];
                                  $newtoolsmodel->parent_id  = $newcode;
                                  $newtoolsmodel->save();

                                }else{
                                    $stocktools      = Tools::model()->findByPk($check['id']);
                                    $stocktools->qty = $check['qty']+$itemsts_qty;
                                    $stocktools->save();
                                }
                          }
           }
                   }
                  $newmodel->save();

                  if(!empty($itemexists)){
                      $itemid =  $itemexists['id'];
                  }else{
                      $itemid = Yii::app()->db->getLastInsertID();
                  }

                  // insert req items qty based on status

                  if($page == "stock"){
                    $sts_type_all =  Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));
                      foreach($sts_type_all as $dat){
                         if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=''){

                             $itemcheck = Yii::app()->db->createCommand("SELECT * FROM tms_reqst_itemqty WHERE tool_transfer_id = ".$insert_id." and transfer_item_id = ".$itemid." and physical_condition = ".$dat['sid'])->queryRow();

                              if(empty($itemcheck)){
                                $reqmodel = new ReqstItemqty;
                                $reqmodel->tool_transfer_id   = $insert_id;
                                $reqmodel->transfer_item_id   = $itemid;
                                $reqmodel->physical_condition = $dat['sid'];
                                $reqmodel->qty                = $_POST[$dat['caption']];
                                $reqmodel->save();
                              }else{
                                $reqmodel =  ReqstItemqty::model()->findByPk($itemcheck['id']);
                                $reqmodel->qty                = $_POST[$dat['caption']]+ $reqmodel->qty;
                                $reqmodel->save();
                              }
                          }
                       }
                      $itemmodel =  ToolTransferItems::model()->findByPk($itemid);
                      $itemmodel->return_qty = $itemsts_qty + $itemmodel->return_qty;
                      $itemmodel->save();
                  }

                  $this->redirect(array('/toolTransferRequest/createstock','page'=>$page,'tr_id'=>$model->id));
                }
            }

        }
        if(isset($_REQUEST["tr_id"]) && !empty($_REQUEST["tr_id"])) {
            $trid = $_REQUEST["tr_id"];
        } else {
            $trid = "";
        }
        $this->render('create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'page' => $page, 'status' => $status, 'duration_type' => $duration_type, 'trid' => $trid,
        ));
    }


    public function actionupdatestock($page){
         $tran_id=$_GET['t_id'];
         $infoid=$_GET['id'];
         $model = $this->loadModel($tran_id);
         $newmodel= ToolTransferItems::model()->findBypk($infoid);
         $this->performAjaxValidation($model);
         $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
         $duration_type = Status::model()->findAll(
                  array(
                      'select' => array('sid,caption'),
                      'condition' => 'status_type = "duration_type"',
                      'distinct' => true
          ));
         $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));

         if(isset($_POST['ToolTransferRequest'])){


            $model->attributes = $_POST['ToolTransferRequest'];
            if(isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
                $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
            } else {
                $model->request_owner_id=Yii::app()->user->id;
            }

            $model->save();
            $type=$_POST['type'];
            $item_name=$_POST['item_name'];
            $unit=$_POST['unit'];

            $physical_condition= isset($_POST['physical_condition'])?$_POST['physical_condition']:3;
            $qty                 = $_POST['quantity'];
            $newmodel->type      = $type;
            $newmodel->item_name = $item_name;
            $newmodel->unit      = $unit;
            $newmodel->physical_condition = $physical_condition;
            $newmodel->qty                = $qty;

            if($page == 'stock'){

                Yii::app()->db->createCommand("DELETE FROM tms_reqst_itemqty WHERE tool_transfer_id = ".$tran_id." and transfer_item_id = ".$infoid )->execute();

                $itemsts_qty  = 0;
                $sts_type_all =  Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));
                foreach($sts_type_all as $dat){
                   if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=''){
                        $itemsts_qty+= $_POST[$dat['caption']];
                        $reqmodel = new ReqstItemqty;
                        $reqmodel->tool_transfer_id   = $tran_id;
                        $reqmodel->transfer_item_id   = $infoid;
                        $reqmodel->physical_condition = $dat['sid'];
                        $reqmodel->qty                = $_POST[$dat['caption']];
                        $reqmodel->save();
                    }
                }
                $newmodel->return_qty = $itemsts_qty;

                //insert to Tools
                $newcode = $newmodel['tool_id'];
                Yii::app()->db->createCommand("DELETE FROM tms_tools WHERE parent_id = ".$newcode)->execute();
                $tools                 = Tools::model()->findByPk($newcode);
                 if($itemsts_qty == $qty){
                    //   $tools->location       = 1;
                      $tools->qty            = $itemsts_qty;
                      $tools->tool_condition = 3;
                      $tools->save();
                 }else{
                    //   $tools->location       = $_POST['ToolTransferRequest']['request_from'];
                      $tools->qty            = $qty-$itemsts_qty;
                      $tools->stock_qty      = $qty-$itemsts_qty;
                      $tools->tool_condition = 3;
                      $tools->save();

                      $qryres1 = Yii::app()->db->createCommand()->select('*')->from('tms_status')
                              ->where('status_type=:type and caption =:caption', array(':type' => 'location_type', ':caption' => 'Stock'))->queryRow();
                      $qryres2 = Yii::app()->db->createCommand()->select('*')->from('tms_location_type')
                              ->where('location_type=:type ', array(':type' =>$qryres1['sid'] ,))->queryRow();

                      $newtoolsmodel             = new Tools;
                      $newtoolsmodel->attributes = $tools->attributes;
                      $newtoolsmodel->qty        = $itemsts_qty;
                      $newtoolsmodel->stock_qty  = $itemsts_qty;
                      $newtoolsmodel->location   = $qryres2['id'];
                      $newtoolsmodel->parent_id  = $newcode;
                      $newtoolsmodel->save();
                }
            }

            $newmodel->save();
            $this->redirect(array('/toolTransferRequest/createstock','tr_id'=>$tran_id,'page'=>$page));
        }


         $this->render('create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'page' => $page, 'status' => $status, 'duration_type' => $duration_type,'trid' =>$tran_id
        ));


    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $page) {
        //$this->layout = false;
        $model = $this->loadModel($id);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $newmodel = ToolTransferItems::model()->findAll(array("condition" => "tool_transfer_id = '$id'"));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ToolTransferRequest'])) {

            $ids = $_POST['ids'];
            $unit = $_POST['unit'];
            $code = $_POST['code'];
            $newcode = $_POST['newcode'];
            $item_name = $_POST['item_name'];
            // $serial_no = $_POST['serial_no'];
            //$duration = $_POST['duration'];
            $duration = isset($_POST['duration']) ? $_POST['duration'] : NULL;
            $quantity = $_POST['quantity'];
            //$duration_type = $_POST['duration_type'];
            $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : NULL;
            $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
            $model->attributes = $_POST['ToolTransferRequest'];
            $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
            $model->request_date = date('Y-m-d');
            $model->transfer_to = $page;
            $model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->save()) {

                foreach ($ids as $i => $idnew) {


                    if ($idnew != NULL) {


                        $newmodel = ToolTransferItems::model()->findByPk($idnew);
                        $newmodel->tool_transfer_id = $id;
                        $newmodel->unit = $unit[$i];
                        $newmodel->code = $code[$i];
                        $newmodel->item_name = $item_name[$i];
                        //  $newmodel->serial_no = $serial_no[$i];
                        $newmodel->duration_in_days = $duration[$i];
                        $newmodel->duration_type = $duration_type[$i];
                        $newmodel->qty = $quantity[$i];
                        $newmodel->physical_condition = $physical_condition[$i];
                        $newmodel->save();
                        if ($page == "stock") {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->tool_condition = $physical_condition[$i];
$tools->save();
}
                    } else {


                        $newmodel = new ToolTransferItems;
                        $newmodel->tool_transfer_id = $id;
                        $newmodel->unit = $unit[$i];
                        $newmodel->code = $code[$i];
                        $newmodel->item_name = $item_name[$i];
                        // $newmodel->serial_no = $serial_no[$i];
                        $newmodel->duration_in_days = $duration[$i];
                        $newmodel->duration_type = $duration_type[$i];
                        $newmodel->qty = $quantity[$i];
                        $newmodel->physical_condition = $physical_condition[$i];
                        $newmodel->save();
                        if ($page == "stock") {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->tool_condition = $physical_condition[$i];
$tools->save();
}
                    }
                }
                $this->redirect(array('/toolTransferRequest&page=' . $page));
            }
        }

        $this->render('update', array(
            'model' => $model, 'units' => $units, 'newmodel' => $newmodel, 'page' => $page, 'status' => $status, 'duration_type' => $duration_type,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

      Yii::app()->db->createCommand("DELETE FROM tms_reqst_itemqty WHERE tool_transfer_id = ".$id)->execute();

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 'site';
        }
        $model = new ToolTransferRequest('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolTransferRequest']))
            $model->attributes = $_GET['ToolTransferRequest'];

        $this->render('admin', array(
            'model' => $model, 'page' => $page,
        ));
    }

    public function actionSite() {

        $model = new ToolTransferRequest('sitesearch');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolTransferRequest']))
            $model->attributes = $_GET['ToolTransferRequest'];

        $this->render('site_admin', array(
            'model' => $model, 'page' => 'site',
        ));
}
        public function actionDispatchitem(){
            $model  = new DispatchTool();
            if (isset($_POST['DispatchTool'])){
                $ids=explode(",",$_POST['hids']);
                for($i=0;$i<count($ids);$i++){
                     $model = new DispatchTool();
                     $model->request_id_fk=$ids[$i];
                     $model->date=$_POST['DispatchTool']['date'];
                     $model->mode_of_transport=$_POST['DispatchTool']['mode_of_transport'];
                     $model->mode_name=$_POST['DispatchTool']['mode_name'];
                     $model->person_name=$_POST['DispatchTool']['person_name'];
                     $model->comment=$_POST['DispatchTool']['comment'];
                     $model->created_by =Yii::app()->user->id;
                     $model->created_date = date('Y-m-d H:i:s');
                     $model->save();

                      $qry1 = Yii::app()->db->createCommand()
                        ->select('sum(qty) as qty')
                        ->from('tms_tool_transfer_info')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $ids[$i]))
                        ->queryRow();
                      $qry2=Yii::app()->db->createCommand()
                        ->select('sum(qty) as qty')
                        ->from('tms_tool_transfer_items')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $ids[$i]))
                        ->queryRow();

                        $modelnew = ToolTransferRequest::model()->findByPk($ids[$i]);
                       if($qry2['qty']<$qry1['qty']){
                          // echo 'partial_transit';

                           $modelnew->request_status=28;
                           $modelnew->save();

                       }
                       elseif($qry2['qty']>=$qry1['qty']){
                           //echo 'full_transit';
                             $modelnew->request_status=27;
                             $modelnew->save();

                       }
                }
                   $this->redirect(array('site'));
               //exit();
            }
             $this->render('dispatchitem', array(
            'model' => $model, 'page' => 'site',
        ));

        }
public function actionSingledispatch($id){
       
        $transfer=ToolTransferRequest::model()->findByPK($id);
        // $tools_transfer_items=ToolTransferItems::model()->find('tool_transfer_id='.$id.' and item_status=29');
        $tools_transfer_items=Yii::app()->db->createCommand()
        ->select('*')
        ->from('tms_tool_transfer_items')
        ->where('tool_transfer_id=:tool_transfer_id and item_status=:item_status',array(':tool_transfer_id' => $id,':item_status'=>29))
        ->queryAll();
       
         $model  = new DispatchTool();
              if (isset($_POST['DispatchTool'])){
                   $model = new DispatchTool();
                     $model->request_id_fk=$id;
                     $model->date=$_POST['DispatchTool']['date'];
                     $model->mode_of_transport=$_POST['DispatchTool']['mode_of_transport'];
                     $model->mode_name=$_POST['DispatchTool']['mode_name'];
                     $model->person_name=$_POST['DispatchTool']['person_name'];
                     $model->comment=$_POST['DispatchTool']['comment'];
                     $model->created_by =Yii::app()->user->id;
                     $model->created_date = date('Y-m-d H:i:s');
                     $model->save();
                    foreach($tools_transfer_items as $tool_transfer){
                        $tool_id=Tools::model()->findByPk($tool_transfer['tool_id']);
                        if($tool_id->serialno_status=='Y'){
                         
                        $tool_transfer_model=new ToolTransferItems;
                        $tool_transfer_model->req_date=$_POST['DispatchTool']['date'];
                        $tool_transfer_model->tool_id=$tool_transfer['tool_id'];
                        $tool_transfer_model->tool_transfer_id=$id;
                        if(isset($tool_transfer['return_qty'])){
                            $tool_transfer_model->return_qty=$tool_transfer['return_qty'];
                        }
                        $tool_transfer_model->code=$tool_transfer['item_status'];
                        $tool_transfer_model->code=$tool_transfer['code'];
                        $tool_transfer_model->item_name=$tool_transfer['item_name'];
                        $tool_transfer_model->item_status=30;
                        $tool_transfer_model->location=NULL;
                        $tool_transfer_model->physical_condition=30;
                        $tool_transfer_model->save();
                        }
                        
                    }
                    $tools = Yii::app()->db->createCommand()
                        ->select('tool_id')
                        ->from('tms_tool_transfer_items')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $id))
                        ->queryAll();
                     foreach($tools as $tool_id){
                     
                        $tools_loc=Tools::model()->findByPk($tool_id['tool_id']);
                        if($tools_loc->serialno_status=='N'){
                            $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set location = NULL,updated_date = now(),
                            updated_by= ' . yii::app()->user->id .'
                            WHERE tool_id ='.$tool_id['tool_id'].' and item_current_status ="added"');
                        $num        = $command->execute();
                        // $command1    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set tool_condition = 30,updated_date = now(),
                        //     updated_by= ' . yii::app()->user->id .'
                        //     WHERE id ='.$tool_id['tool_id']);
                        // $num1       = $command1->execute();
                       
                        }
                        $tools_loc->tool_condition=30;
                        $tools_loc->location=NULL;
                        Yii::app()->db->createCommand()
                        ->update('tms_tool_transfer_items', array('physical_condition'=>'30','location'=>NULL),
                        'tool_transfer_id=:tool_transfer_id',array(':tool_transfer_id'=>$id));

                        $tools_loc->save();
                        
                       }
                       

                      $qry1 = Yii::app()->db->createCommand()
                        ->select('sum(qty) as qty')
                        ->from('tms_tool_transfer_info')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $id))
                        ->queryRow();
                      $qry2=Yii::app()->db->createCommand()
                        ->select('sum(qty) as qty')
                        ->from('tms_tool_transfer_items')
                        ->where('tool_transfer_id=:tool_transfer_id', array(':tool_transfer_id' => $id))
                        ->queryRow();

                        $modelnew = ToolTransferRequest::model()->findByPk($id);
                       if($qry2['qty']<$qry1['qty']){
                          // echo 'partial_transit';

                           $modelnew->request_status=28;
                           $modelnew->save();

                       }
                       elseif($qry2['qty']>=$qry1['qty']){
                           //echo 'full_transit';
                             $modelnew->request_status=27;
                             $modelnew->save();

                       }
                       if($transfer->transfer_to=="stock"){
                        $this->redirect(array('&page=stock'));

                       }
                       else{
                       $this->redirect(array('site'));
                    }
              }
            $this->render('singledispatchitem', array(
            'model' => $model, 'page' => 'site','id'=>$id
             ));

        }

public function actionCreatesite() {
//$this->layout = false;
        $model = new ToolTransferRequest;
        $newmodel = new ToolTransferInfo;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));

        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));
        if (isset($_POST['ToolTransferRequest'])) {


           // print_r($_POST);die;

            $req_no=$_POST['ToolTransferRequest']['request_no'];

            $transaction=$_POST['trans_id'];
            //echo  $transaction;die;
            if($transaction == '' || $transaction == 0){
            $model->attributes = $_POST['ToolTransferRequest'];
            $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
            $model->request_date = date('Y-m-d');
            // $model->location = $_POST['ToolTransferRequest']['request_from'];


            if(isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
                $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
            } else {
                $model->request_owner_id = Yii::app()->user->id;
            }
                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
                        ->queryRow();
$model->request_status = $qryres1['sid'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->transfer_to = 'site';
            if ($model->save()) {


                $insert_id = Yii::app()->db->getLastInsertID();
                  //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                       // print_r($_POST);die;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                        $type=$_POST['type'];
                    $newcode = $_POST['newcode'];
                    $item_name = $_POST['item_name'];
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferInfo;
                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                        $newmodel->unit = $unit;
                        $newmodel->type = $type;
                        $newmodel->tool_id = $newcode;
                        $newmodel->item_name = $item_name;
                    // $newmodel->serial_no = $serial_no[$i];
                        $newmodel->duration_in_days = isset($duration) ? $duration : '';
                        $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                        $newmodel->qty = $quantity;
                        $newmodel->physical_condition = $physical_condition;

                                                    if($newcode != NULL) {
                                                            $tools = Tools::model()->findByPk($newcode);

$location = LocationType::model()->findByPk($tools->location)->location_type;
$newmodel->item_status = $location;
$newmodel->location = $tools->location;

// $tools->location = $reqfrom;
$tools->save();
                                               // }

                       /* $reqfrom = $_POST['ToolTransferRequest']['request_from'];
                        $location = LocationType::model()->findByPk($reqfrom)->location_type;
                        $newmodel->item_status = $location;
                        $newmodel->location = $reqfrom;
                        if($newcode[$i] != NULL) {
                        $tools = Tools::model()->findByPk($newcode[$i]);
                        $tools->location = $reqfrom;
                        $tools->save();
   } */
                    $newmodel->save();
                    //print_r($newmodel->getErrors());
                }



                    // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                    $tbl = Yii::app()->db->tablePrefix;
                    $getdata = "SELECT * FROM {$tbl}users
INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                    $users = Yii::app()->db->createCommand($getdata)->queryAll();

                    if ($users != NULL) {
                        $users = Users::model()->findAll(array('condition' => 'user_type = 9 || user_type = 1'));
                    }
                    if ($users != NULL) {
                        foreach ($users as $user) {
                            // require_once 'PHPMailer/class.phpmailer.php';
                            // $mail = new JPhpMailer;
                            // $newemail = $user->email;

                            // $email = $newemail;
                            $subject = Yii::app()->name;
                            // $headers = Yii::app()->name;

                            $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                            $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);
                           
                           
           
            //$user_name = $model->usrId->firstname." ".$model->usrId->lastname." ( Emp Code: ".$model->usrId->employee_code." )";
             
                       
            $mailto = $user->email;
           
            if($this->SMTPMailer($subject, $bodyContent, $mailto)) {
                //   Yii::app()->user->setFlash('success', 'Message was sent successfully.');
            }
            else{
              //  Yii::app()->user->setFlash('error', 'There was an error sending the message!');
            }




                            /*
                              $mail->isSMTP();                                   // Set mailer to use SMTP
                              $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
                              $mail->SMTPAuth = true;                            // Enable SMTP authentication
                              $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
                              $mail->Password = 'Test2paSS()';                           // SMTP password
                              $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                              $mail->Port = 465;

                              $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);

                             */
                            // $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                            // $mail->addAddress($email);   // Add a recipient
                            // $mail->isHTML(true);

                            // $mail->Subject = Yii::app()->name;
                            // $mail->Body = $bodyContent;
                            // $mail->Send();
                            /* if (!$mail->Send()) {
                              Yii::app()->user->setFlash('error', 'There was an error sending the message!');
                              } else {

                              Yii::app()->user->setFlash('success', 'Message was sent successfully.');

                              } */
                        }
                    }


                        $this->redirect(array('/toolTransferRequest/createsite','tr_id'=>$model->id));
                    //$this->redirect(array('/toolTransferRequest/site'));
            }
            }

            else{
                $model = $this->loadModel($transaction);
                 $model->attributes = $_POST['ToolTransferRequest'];
                 if(isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
                    $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
                 } else {
                    $model->request_owner_id=Yii::app()->user->id;
                 }
                if ($model->save()) {


                $insert_id = $transaction;
              //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                    //print_r($_POST);die;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $type=$_POST['type'];
                    $newcode = $_POST['newcode'];
                    $item_name = $_POST['item_name'];
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferInfo;
                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                    $newmodel->unit = $unit;
                    $newmodel->type = $type;
                    $newmodel->tool_id = $newcode;
                    $newmodel->item_name = $item_name;
                    // $newmodel->serial_no = $serial_no[$i];
                    $newmodel->duration_in_days = isset($duration) ? $duration : '';
                    $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                    $newmodel->qty = $quantity;
                    $newmodel->physical_condition = $physical_condition;

if($newcode != NULL) {
$tools = Tools::model()->findByPk($newcode);

$location = LocationType::model()->findByPk($tools->location)->location_type;
$newmodel->item_status = $location;
$newmodel->location = $tools->location;

// $tools->location = $reqfrom;
$tools->save();
  // }

                       /* $reqfrom = $_POST['ToolTransferRequest']['request_from'];
                        $location = LocationType::model()->findByPk($reqfrom)->location_type;
                        $newmodel->item_status = $location;
                        $newmodel->location = $reqfrom;
                        if($newcode[$i] != NULL) {
                        $tools = Tools::model()->findByPk($newcode[$i]);
                        $tools->location = $reqfrom;
                        $tools->save();
   } */
                    $newmodel->save();
                    //print_r($newmodel->getErrors());
        }

            }
           
             $this->redirect(array('/toolTransferRequest/createsite','tr_id'=>$model->id));

            }


        }

        $this->render('site_create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'status' => $status, 'duration_type' => $duration_type,
        ));
}

        public function actionUpdatesite()
        {
          //$model = new ToolTransferRequest;
        $tran_id=$_GET['t_id'];

        $infoid=$_GET['id'];
         $model = $this->loadModel($tran_id);
         $newmodel= ToolTransferInfo::model()->findBypk($infoid);

           $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));

        if(isset($_POST['ToolTransferRequest'])){
          //   echo '<pre>';
       //print_r($_POST);

        $model->attributes = $_POST['ToolTransferRequest'];
        if(isset($_POST['ToolTransferRequest']['request_owner_id']) && !empty($_POST['ToolTransferRequest']['request_owner_id'])) {
            $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
        } else {
            $model->request_owner_id=Yii::app()->user->id;
        }
         //print_r($model->attributes);die;
        $model->save();
        $type=$_POST['type'];
        $item_name=$_POST['item_name'];
        $unit=$_POST['unit'];
        $duration=$_POST['duration'];
        $duration_type=$_POST['duration_type'];
        $qty=$_POST['quantity'];
        $newmodel->type=$type;
        $newmodel->item_name=$item_name;
        $newmodel->unit=$unit;
        $newmodel->duration_in_days=$duration;
        $newmodel->duration_type=$duration_type;
        $newmodel->qty=$qty;
        $newmodel->save();


         $this->redirect(array('/toolTransferRequest/createsite','tr_id'=>$tran_id));
        }


             $this->render('site_create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'status' => $status, 'duration_type' => $duration_type,
        ));

        }
 public function actionDeletesite($id){

        ToolTransferInfo::model()->deleteByPk($id);
        echo true;
    }
    public function actionDeletestock($id){

        $check = ToolTransferItems::model()->findByPk($id);
        Yii::app()->db->createCommand("DELETE FROM tms_reqst_itemqty WHERE tool_transfer_id = ".$check['tool_transfer_id']." and transfer_item_id = ".$id )->execute();
        ToolTransferItems::model()->deleteByPk($id);

        echo true;
    }

public function actionSiteView($id) {
        $transfer_to = ToolTransferRequest::model()->findAll(array('condition'=>'id ='.$id));
       
        $items = new ToolTransferInfo('search');

        $trns_items = new ToolTransferItems;

        $tblpx = Yii::app()->db->tablePrefix;
        $data =  Yii::app()->db->createCommand("SELECT SUM(qty) as total_qty FROM {$tblpx}tool_transfer_info WHERE tool_transfer_id =".$id."")->queryRow();
        //$exit_data =  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}tool_transfer_items WHERE tool_transfer_id =".$id."")->queryAll();
                $exit_data =  Yii::app()->db->createCommand("SELECT * FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$id."")->queryAll();

        if (isset($_POST['request_id'])) {


//print_r($_POST);





/*$sl_No = $_POST['unit'];
for ($i = 0; $i < sizeof($sl_No); $i++) {
                    //print_r($_POST);die;

                    $request_details = ToolTransferRequest::model()->findByPk($id);

                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $newcode = $_POST['newcode'];
                    $item_name = $_POST['item_name'];
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferItems;
                    $newmodel->tool_transfer_id = $_POST['request_id'];
                    $newmodel->req_date = date('Y-m-d');
                    $newmodel->unit = $unit[$i];
                    $newmodel->code = $code[$i];
                    $newmodel->tool_id = $newcode[$i];
                    $newmodel->item_name = $item_name[$i];
                    $newmodel->location = $request_details['request_to'];
                    $location = LocationType::model()->findByPk($request_details['request_to'])->location_type;
                    $newmodel->item_status = $location;
                    // $newmodel->serial_no = $serial_no[$i];
                    $newmodel->duration_in_days = isset($duration[$i]) ? $duration[$i] : '';
                    $newmodel->duration_type = isset($duration_type[$i]) ? $duration_type[$i] : '';
                    $newmodel->qty = $quantity[$i];
                    $newmodel->physical_condition = $physical_condition[$i];


                    if($newcode[$i] != NULL) {
$tools = Tools::model()->findByPk($newcode[$i]);
$tools->location = $request_details['request_to'];
$tools->save();
}



                    $newmodel->save();
                }
                */
                // approve code

                $reqdetails = ToolTransferRequest::model()->findByPk($_POST['request_id']);
$remarks = isset($_POST['remark']) ? $_POST['remark'] : "";
                $ack_user = Yii::app()->user->id;

$reqto = $reqdetails->request_to;

                $newmodel = ToolTransferRequest::model()->findByPk($_POST['request_id']);
$newmodel->staff_remarks = $remarks;
$newmodel->ack_user_id = $ack_user;
//$newmodel->request_status = 7;

                    $qryres1 = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Approved'))
                    ->queryRow();
                    if($reqdetails->request_status==8)
                    {
                        $newmodel->request_status = $reqdetails->request_status;
                    }
                    else
                    {
                        $newmodel->request_status = $qryres1['sid'];
                    }
                $newmodel->transfer_date = date('Y-m-d');
                $newmodel->save();


                //$mail = new JPhpMailer;
                $req_owner = ToolTransferRequest::model()->findByPk($_POST['request_id']);
                if($req_owner->request_owner_id == $req_owner->created_by){
                $users = Users::model()->findAll(array('condition'=>'userid='.$req_owner->request_owner_id || 'user_type = 1'));}
                else{
                    $users = Users::model()->findAll(array('condition'=>'userid='.$req_owner->request_owner_id || 'userid='.$req_owner->created_by|| 'user_type = 1'));
                   
                   
                }
                foreach ($users as $user)
                {
                //$newemail = Users::model()->find(array('condition'=>'userid='.$ack_user))->email;
                //$email = $newemail;
                $subject = Yii::app()->name;
                //$headers = Yii::app()->name;
                    if($reqdetails->request_status == 8)
                    {
                        $bodyContent = "<p>Hello,</p><p>This is a notification to let you know that  Tool Transfer Request has been rejected.</p>";
                    }
                    else
                    {
                        $bodyContent = "<p>Hello,</p><p>This is a notification to let you know that  Tool Transfer Request has been verified and approved.</p>";  
                    }
                $bodyContent .= $this->renderPartial('//toolTransferRequest/_emailformsite', array('insert_id' => $_POST['request_id']), true);




                //$mail->setFrom('info@bhiapp.com', Yii::app()->name);
                $mailto = $user->email;
               
                 
       
        //$user_name = $model->usrId->firstname." ".$model->usrId->lastname." ( Emp Code: ".$model->usrId->employee_code." )";
         
                   
        //$mailto = $user->email;
       
        if($this->SMTPMailer($subject, $bodyContent, $mailto)) {
           //    Yii::app()->user->setFlash('success', 'Message was sent successfully.');
        }
        else{
         //  Yii::app()->user->setFlash('error', 'There was an error sending the message!');
        }
   
    }
//$mail->addAddress($email);   // Add a recipient
// $mail->isHTML(true);

// $mail->Subject = Yii::app()->name;

// $mail->Body = $bodyContent;
// $mail->Send();

               // exit();

              if($transfer_to[0]['transfer_to']=="stock"){
                $this->redirect(array('&page=stock'));

              }else{
                $this->redirect(array('/toolTransferRequest/site'));
              }

}


         $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));

        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));

        $newmodel = new ToolTransferInfo;

         $model2 = new ToolTransferRequest;
      
         $trns_items = ToolTransferItems::model()->findAll(
                array(
                    'select' => array('*'),
                    'condition' => 'tool_transfer_id = "'.$id.'" and item_status=29',
        ));
        $this->render('site_view', array(
            'model' => $this->loadModel($id), 'items' => $items, 'id' => $id, 'units' => $units, 'status' => $status, 'duration_type' => $duration_type, 'model2' => $model2, 'total_qty' => $data['total_qty'], 'newmodel' => $exit_data , 'trns_items' => $trns_items
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ToolTransferRequest('search');

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolTransferRequest']))
            $model->attributes = $_GET['ToolTransferRequest'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ToolTransferRequest::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tool-transfer-request-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCheckvalidation() {
        $request_no = $_POST['request_no'];
        $id = $_POST['val'];

        if ($id == 0) {
            $row = ToolTransferRequest::model()->findAll(array('condition' => 'request_no =' . $request_no));
            $count = count($row);
            if ($count > 0) {
                echo "false";
            } else {
                echo "true";
            }
        } else {

            $row = ToolTransferRequest::model()->findAll(array('condition' => 'request_no =' . $request_no . ' and id !=' . $id));
            //print_r($row);die;
            $count = count($row);
            if ($count > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    public function actionDeleteItem($id) {

        ToolTransferItems::model()->deleteByPk($id);
        echo true;
    }

    public function actionFinalapproval() {
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        //$model->request_status = 9;

        $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Approved'))
                ->queryRow();
        $model->request_status = $qryres1['sid'];
        $model->received_date = date('Y-m-d');



        if ($model->save()) {

$date = date('Y-m-d');
            $tblpx = Yii::app()->db->tablePrefix;
            $row = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id =' . $id));

            if(!empty($row)){
foreach($row as $value) {
  $duration_type = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$value['duration_type'])->queryRow();
  echo $duration_type['caption'];
  if($duration_type['caption'] == 'Days') {
  $return_date = date("Y-m-d",strtotime($date."+".$value['duration_in_days']." day"));
  }else {
  $return_date = date("Y-m-d",strtotime($date."+".$value['duration_in_days']." month"));
  }

 $user=Yii::app()->db->createCommand()->update("{$tblpx}tool_transfer_items", array(
'return_date'=> $return_date,
), 'id=:id', array(':id'=>$value['id']));
}
   }

            $userid = ToolTransferRequest::model()->findByPk($id)->ack_user_id;
            $useremail = Users::model()->findByPk($userid);
            $mail = new JPhpMailer;
            $email = $useremail->email;
            $subject = Yii::app()->name;
            $headers = Yii::app()->name;

            $bodyContent = "<p>Hello $useremail->first_name,</p><p>This is a notification to let you know that  Tool Transfer Request has been Approved.</p>";
            $bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $id), true);


            /*
              $mail->isSMTP();                                   // Set mailer to use SMTP
              $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                            // Enable SMTP authentication
              $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
              $mail->Password = 'Test2paSS()';                           // SMTP password
              $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = 465;

              $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);

             */

            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
            $mail->addAddress($email);   // Add a recipient
            $mail->isHTML(true);

            $mail->Subject = Yii::app()->name;

            $mail->Body = $bodyContent;
            $mail->Send();
        }
    }

    public function actionVendorview($id, $page = '') {
        $items = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id =' . $id));
        $model1 = new ToolTransferItems('search');

        $this->render('vendorview', array(
            'model' => $this->loadModel($id), 'items' => $items, 'id' => $id, 'model1' => $model1, 'page' => $page,
        ));
    }

    public function actionVendordetails($id) {
        $items = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id =' . $id));
        $model1 = new ToolTransferItems('search');

        $this->render('vendordetails', array(
            'model' => $this->loadModel($id), 'items' => $items, 'id' => $id, 'model1' => $model1
        ));
    }

    public function actionUpdateitem($id) {
        $model = $this->loadModel($id);

        print_r($_POST);
      //  exit();
        //
        //  print_r($_POST);
        // //
        // $item_data = ToolTransferItems::model()->find(array('condition'=>"tool_transfer_id =10 AND tool_id=5"));
        // $transfer_items = ToolTransferItems::model()->findByPk($item_data->id);
        // $transfer_items->delete();
        //  exit();


        $query_data = Yii::app()->db->createCommand("SELECT tms_tools.id,tms_tools.parent_id FROM `tms_tools` INNER JOIN tms_tool_transfer_request ON tms_tools.vendor_request_id= tms_tool_transfer_request.id WHERE tms_tool_transfer_request.id=".$id."")->queryRow();
        // print_r($query_data);
        // if(empty($query_data)){
        //
        // }else{
        //
        // }
        // exit();

        if (isset($_POST['ToolTransferRequest'])) {

            $query_data = Yii::app()->db->createCommand("SELECT tms_tools.id,tms_tools.parent_id FROM `tms_tools` INNER JOIN tms_tool_transfer_request ON tms_tools.vendor_request_id= tms_tool_transfer_request.id WHERE tms_tool_transfer_request.id=".$id."")->queryRow();
          //  print_r($query_data);
          $query_tool_id = Yii::app()->db->createCommand("SELECT tool_id FROM tms_tool_transfer_items WHERE tool_transfer_id=".$id."")->queryRow();
            if(empty($query_data)){
              // single

              $ids = $_POST['ids'];
              $paid_amount = $_POST['paid_amount'];
              $rating = $_POST['rating'];
              $remarks = $_POST['remarks'];
              $service_report = $_POST['service_report'];
              //$model->request_status = 11;

              $qryres1 = Yii::app()->db->createCommand()
                      ->select('*')
                      ->from('tms_status')
                      ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Received back to stock'))
                      ->queryRow();
              $model->request_status = $qryres1['sid'];

              if ($model->save()) {

                  foreach ($ids as $i => $idnew) {
                      //$newmodel = ToolTransferItems::model()->findByPk($idnew);
                      $obj = ToolTransferItems::model()->findByPk($idnew);
                      $newmodel = new ToolTransferItems;
                      $newmodel->attributes = $obj->attributes;
  //                        if($paid_amount[$i] !=""){
                      $newmodel->paid_amount = $paid_amount[$i];
                      $newmodel->rating = $rating[$i];
                      $newmodel->remarks = $remarks[$i];
                      $newmodel->service_report = $service_report[$i];
                      $newmodel->req_date = date('Y-m-d');
                      $newmodel->physical_condition = 3;

                      $transferid = $_POST['transferid'];
                      $reqdetails = ToolTransferRequest::model()->findByPk($transferid);
                      $reqfrom = $reqdetails->request_from;
                      $location = LocationType::model()->findByPk(1)->location_type;
                      $newmodel->item_status = $location;
                      $newmodel->location = $reqfrom;
                      $tools = Tools::model()->findByPk($obj->tool_id);
                      //$tools->location = $reqfrom;
                      $tools->location = 1;
                      $tools->tool_condition = 3;
                      $command2 = Yii::app()->db->createCommand
                      ('UPDATE tms_tools set tool_condition=3 WHERE id ='.$query_tool_id['tool_id']);
              $num2  = $command2->execute();
                      $tools->save();

                      //$newmodel->item_status = $qryres1['sid'];

                      if ($newmodel->save()) {
                          // print_r($newmodel->getErrors());die;
                      }
                      /* }else{
                        $transferid = $_POST['transferid'];
                        $reqdetails = ToolTransferRequest::model()->findByPk($transferid);
                        $reqto = $reqdetails->request_to;
                        $location = LocationType::model()->findByPk($reqto)->location_type;
                        $newmodel->item_status = $location;
                        } */
                  }
                  $this->redirect(array('ToolTransferRequest/vendordetails&id=' . $id));
              }
            }else{

              // bulk

              $ids = $_POST['ids'];
              $paid_amount = $_POST['paid_amount'];
              $rating = $_POST['rating'];
              $remarks = $_POST['remarks'];
              $service_report = $_POST['service_report'];
              //$model->request_status = 11;

              $qryres1 = Yii::app()->db->createCommand()
                      ->select('*')
                      ->from('tms_status')
                      ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Received back to stock'))
                      ->queryRow();
              $model->request_status = $qryres1['sid'];

              if ($model->save()) {

                  foreach ($ids as $i => $idnew) {
                      //$newmodel = ToolTransferItems::model()->findByPk($idnew);
                      $obj = ToolTransferItems::model()->findByPk($idnew);
                      $newmodel = new ToolTransferItems;
                      $newmodel->attributes = $obj->attributes;
  //                        if($paid_amount[$i] !=""){
                      $newmodel->paid_amount = $paid_amount[$i];
                      $newmodel->rating = $rating[$i];
                      $newmodel->remarks = $remarks[$i];
                      $newmodel->service_report = $service_report[$i];
                      $newmodel->req_date = date('Y-m-d');
                      $newmodel->physical_condition = 3;
                      $newmodel->request_vendor_status = 1;
                      $transferid = $_POST['transferid'];
                      $reqdetails = ToolTransferRequest::model()->findByPk($transferid);
                      $reqfrom = $reqdetails->request_from;
                      $location = LocationType::model()->findByPk(1)->location_type;
                      $newmodel->item_status = $location;
                      $newmodel->location = $reqfrom;
                      $tools = Tools::model()->findByPk($obj->tool_id);
                      $tools->active_status = 0;
                      $tools->qty = 0;
                      $tools->save();
                      $tools2 = Tools::model()->findByPk($query_data['parent_id']);
                      $tools2->qty = $tools2->qty+$tools->qty;
                      $itemdata = ToolTransferItems::model()->find(array('condition'=>"tool_transfer_id =".$transferid." AND tool_id=".$obj->tool_id.""));
                      $item_ids = $itemdata->id;
                      $transfer_items = ToolTransferItems::model()->findByPk($itemdata->id);
                      // echo "fdfdf";
                      // exit();

                      //print_r($itemdata->tool_id);
                      $tools_details = Tools::model()->findByPk($itemdata->tool_id);
                      //echo $tools_details->parent_id;
                      $qryres1 = Yii::app()->db->createCommand("SELECT SUM(tms_approve_itemqty.qty) as qty FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$tools_details->parent_id." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2")->queryRow();
                      $qryres2 = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.qty,tms_approve_itemqty.id FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$tools_details->parent_id." AND tms_approve_itemqty.physical_condition = 3")->queryRow();
                      $qryres3 = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.id FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$tools_details->parent_id." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2")->queryAll();
                      $damage =$qryres1['qty'];
                      $approve_ids = array();
                      foreach($qryres3 as $key=>$value){
                          array_push($approve_ids,$value['id']);
                      }
                      $approve_id = implode(",",$approve_ids);
                      $total_qty = $damage+$qryres2['qty'];
                      $tools2->qty = $tools2->qty+$damage;
                      $tools2->save();
                      $newmodel->save();

                      $command1   = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'approve_itemqty set qty = '.$total_qty.' WHERE id = '.$qryres2['id'].'')->execute();
                      Yii::app()->db->createCommand('DELETE FROM ' . Yii::app()->db->tablePrefix . 'approve_itemqty  WHERE id IN ('.$approve_id.')')->execute();
                      // echo $damage;
                      // exit();


                      //Yii::app()->db->createCommand("DELETE FROM tms_tools WHERE id = ".$obj->tool_id)->execute();
                  }
                  $this->redirect(array('ToolTransferRequest/vendordetails&id=' . $id));
              }

            }




        }
        $this->redirect(array('ToolTransferRequest/vendordetails&id=' . $id));
    }

    public function actionGettool() {
        $id = $_POST['id'];
        if ($id == 0) {

            $toolslist = CHtml::listData(Tools::model()->findAll(
                                    array(
                                        'select' => array('id,ref_no'),
                                        'distinct' => true
                            )), "id", "ref_no");
            $tools = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($toolslist as $toolid => $toolcode)
                $tools .= CHtml::tag('option', array('value' => $toolid), CHtml::encode($toolcode), true);
            $unitlist = CHtml::listData(Unit::model()->findAll(
                                    array(
                                        'select' => array('id,unitname'),
                                        'condition' => 'id = 1',
                                        'distinct' => true
                            )), "id", "unitname");
            $unittype = "<option value=''>Please choose </option>";

            // each $row is an array representing a row of data
            foreach ($unitlist as $unitid => $unitname)
                $unittype .= CHtml::tag('option', array('value' => $unitid), CHtml::encode($unitname), true);


            echo CJSON::encode(array('tools' => $tools, 'unittype' => $unittype));
        }
    }

    public function actionGettooldetails() {
        $id = $_POST['id'];
        if (isset($_POST['id'])) {

            $toolslist = Tools::model()->findByPk($id);
            $name = $toolslist->tool_name;
            echo CJSON::encode(array('name' => $name));
        }
    }

    public function actionFinalreject() {
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        //$model->request_status = 9;

        $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Rejected'))
                ->queryRow();
        $model->request_status = $qryres1['sid'];

        if ($model->save()) {
            $userid = ToolTransferRequest::model()->findByPk($id)->ack_user_id;
            $useremail = Users::model()->findByPk($userid);
            $mail = new JPhpMailer;
            $email = $useremail->email;
            $subject = Yii::app()->name;
            $headers = Yii::app()->name;

            $bodyContent = "<p>Hello $useremail->first_name,</p><p>This is a notification to let you know that  Tool Transfer Request has been Rejected.</p>";
            $bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $id), true);


            /*
              $mail->isSMTP();                                   // Set mailer to use SMTP
              $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
              $mail->SMTPAuth = true;                            // Enable SMTP authentication
              $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
              $mail->Password = 'Test2paSS()';                           // SMTP password
              $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
              $mail->Port = 465;

              $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);
             */
            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
            $mail->addAddress($email);   // Add a recipient
            $mail->isHTML(true);

            $mail->Subject = Yii::app()->name;

            $mail->Body = $bodyContent;
            $mail->Send();
        }
    }

    public function actionItems() {
$model = new ToolTransferRequest('requestitems');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolTransferRequest']))
            $model->attributes = $_GET['ToolTransferRequest'];

        $this->render('request_items', array(
            'model' => $model, 'page' => 'site',
        ));
}

public function actionReturnDate() {
$tblpx = Yii::app()->db->tablePrefix;

$data = Yii::app()->db->createCommand("SELECT request.request_no, items.id, request.id as request_id, items.duration_in_days, items.duration_type, request.received_date FROM {$tblpx}tool_transfer_request request INNER JOIN {$tblpx}tool_transfer_items items ON request.id=items.tool_transfer_id WHERE request.request_status =9  AND request.received_date !='' ORDER BY request.id DESC")->queryAll();
foreach($data as $value) {
//echo $value['duration_type'].'-'.$value['id'].'<br/>';

$duration_id = $value['duration_type'];

if($value['duration_type'] != '' && $value['duration_in_days'] !='') {
$duration_type = Yii::app()->db->createCommand("SELECT caption FROM {$tblpx}status WHERE sid='$duration_id'")->queryRow();
if($duration_type['caption'] == 'Days') {
  $return_date = date("Y-m-d",strtotime($value['received_date']."+".$value['duration_in_days']." day"));
}else {
  $return_date = date("Y-m-d",strtotime($value['received_date']."+".$value['duration_in_days']." month"));
}
} else {
$return_date = date("Y-m-d",strtotime($value['received_date']."+".$value['duration_in_days']." day"));
}

  echo $return_date.'<br/>';

  $user=Yii::app()->db->createCommand()->update("{$tblpx}tool_transfer_items", array(
'return_date'=> $return_date,
  ), 'id=:id', array(':id'=>$value['id']));

}

}



     public function actionAssignStatus($id) {
     
        $page='site';
        $id = $_REQUEST['id'];
        $type = (isset($_POST['type']))?$_POST['type']:NULL;
        $transferItem       = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id = '.$id.''));
        $model = $this->loadModel($id);
        $sql="SELECT *,tms_tool_transfer_items.id as tid, tms_tool_transfer_items.qty as quantity FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$id."";
        $requestitems =  Yii::app()->db->createCommand($sql)->queryAll();

        $items1 = new ToolTransferInfo('search');

        if(isset($_POST['items'])){
            $items              = (isset($_POST['items']))?$_POST['items']:NULL;
            $item_serial        = (isset($_POST['item_serial']))?$_POST['item_serial']:NULL;
            $item_nonserial     = (isset($_POST['item_nonserial']))?$_POST['item_nonserial']:NULL;
            $accepted           = (isset($_POST['accepted']))?$_POST['accepted']:NULL;
            $defective          = (isset($_POST['defective']))?$_POST['defective']:NULL;
            $missing            = (isset($_POST['missing']))?$_POST['missing']:NULL;
           
            $not_requested      = (isset($_POST['not_requested']))?$_POST['not_requested']:NULL;
            $serialno_status    = (isset($_POST['serialno_status']))?$_POST['serialno_status']:NULL;
            $request_item       = (isset($_POST['request_item']))?$_POST['request_item']:NULL;
            $i = 0;
            $j = 0;
            foreach($serialno_status as $key => $status) {
                if($status == "Y") {
                    if($type != "") {

                        foreach($_POST['item_serial'][0] as $key2=>$values){

                          $ids        = implode(',', $item_serial[$i]);
                         foreach($item_serial[$i] as $item){
        
                        
                         
                          $tool_id=ToolTransferItems::model()->findByPk( $item);
                         
                          $tools=Tools::model()->findByPk($tool_id->tool_id);
                          
                            // $tools->qty=$tools->qty-1;
                          if($tools->qty<=0)
                          {
                            $tools->qty=0;  
                          }
                          $qty1=1;
                          $tools->location=$tool_id->location;
                          $tools->save();
                          $command1    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set location = ' . $model->request_to . ',updated_date = now(),
                          updated_by= ' . yii::app()->user->id . ',stock_qty=stock_qty-'.$qty1. '
                          WHERE id ='.$tool_id->tool_id);
                        $num1        = $command1->execute();
                         }
                        
                        
                                   
                          $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "' . $type . '",updated_date = now(),
                                          updated_by= ' . yii::app()->user->id . ',qty=qty-'.$qty1. '
                                          WHERE id ='.$values.' and tool_transfer_id ='. $_REQUEST["id"]);
                          $num        = $command->execute();
                          if($type == 'missing'){
                            $items = ToolTransferItems::model()->find(array('condition'=>'id='.$values.''));
                            $tools = Tools::model()->findByPk($items->tool_id);
                            $tools->location = NULL;
                            $tools->tool_condition = 31;
                            $tools->save(false);
                          }
                        }


                    }
                    $i = $i + 1;
                }
                if($status == "N") {
                 
                    foreach($item_nonserial as $key=>$nonserial) {
                        $count = count($nonserial);
                        for($m = 0; $m < $count; $m++) {
                            $requestId                      = $request_item[$key][$m];
                            if($requestId != "") {
                                $newmodel   = RequestitemStatus::model()->findByPk($requestId);
                            } else {
                                $newmodel   = new RequestitemStatus;
                            }
                            $dem=0;
                            $newmodel->item_id              = $item_nonserial[$key][$m];
                            $newmodel->accepted_qty         = ($accepted[$key][$m]!="")?$accepted[$key][$m]:$dem;
                            $newmodel->defective_qty        = ($defective[$key][$m]!="")?$defective[$key][$m]:$dem;
                            $newmodel->missing_qty          = ($missing[$key][$m]!="")?$missing[$key][$m]:$dem;
                            $newmodel->not_requested_qty    = ($not_requested[$key][$m]!="")?$not_requested[$key][$m]:0;
                            $newmodel->save();
                            $command1   = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "added",updated_date = now(),item_status=9,
                                    updated_by= ' . yii::app()->user->id . '
                                    WHERE id = '.$item_nonserial[$key][$m].' and tool_transfer_id ='. $_REQUEST["id"]);
                            $num        = $command1->execute();
                           
                            if(!isset($missing)){
                                $items = ToolTransferItems::model()->find(array('condition'=>'id='.$item_nonserial[$key][$m].''));
                                $tools = Tools::model()->findByPk($items->tool_id);
                               
                                $tools->location =NULL;
                                $tools->qty = $missing[$key][$m];
                                $tools->save();
                               
                            }
                            else{
                                
                                $items = ToolTransferItems::model()->find(array('condition'=>'id='.$item_nonserial[$key][$m].' and tool_transfer_id='.$_REQUEST['id']));
                              
                                $tools = Tools::model()->findByPk($items->tool_id);
                               
                                $tools->location =isset($model->request_to)?$model->request_to:"";
                            //     $tools->qty = $items->qty;
                            //     $updatemodel1= ToolTransferItems::model()->find("tool_id = ". $items->tool_id  ." and item_status=12 and item_current_status='added'");
                            //     $updatemodel1->qty = $updatemodel1->qty-$transferItem->qty;
                            //   $updatemodel1->save();
                            //     $tools->save();  
                       
                            $qty1=($newmodel->accepted_qty)+($newmodel->defective_qty)+$newmodel->not_requested_qty +$newmodel->missing_qty   ;
                            $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "' . $type . '" and  updated_date = now(),
                                updated_by= ' . yii::app()->user->id . ',qty=qty-'.$qty1.'
                                WHERE tool_id ='. $items->tool_id.' and location=1');
                                $num=$command->execute();
                                $command2   = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set updated_date = now(),
                                updated_by= ' . yii::app()->user->id . ',location='.$tools->location.'
                                WHERE tool_transfer_id ='. $_REQUEST['id']);
                                $num2=$command2->execute();

                                $items_qty = ToolTransferItems::model()->find(array('condition'=>' tool_transfer_id ='. $_REQUEST['id'].' and tool_id='.$tools->id));

                                $command1   = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set stock_qty=qty-'.$qty1. ',location='.$tools->location.',qty='.$items_qty->qty.'
                                WHERE id ='.$tools->id);
                                $num1        = $command1->execute();
                                // if(isset($newmodel->accepted_qty) && ($newmodel->accepted_qty!="")){
                                   
                                //     $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "Accepted" ,  updated_date = now(),
                                //     updated_by= ' . yii::app()->user->id .'
                                //     WHERE tool_id ='. $items->tool_id.' and tool_transfer_id='.$_REQUEST['id']);
                                //     $num=$command->execute();
                                // }
                                // if(isset($newmodel->defective_qty) && ($newmodel->defective_qty!="")){
                                //     $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "Defective" ,  updated_date = now(),
                                //     updated_by= ' . yii::app()->user->id .'
                                //     WHERE tool_id ='. $items->tool_id.' and tool_transfer_id='.$_REQUEST['id']);
                                //     $num=$command->execute();
                                // }
                                // if(isset($newmodel->not_requested_qty) && ($newmodel->not_requested_qty!="")){
                                //     $command    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "Not requested" ,  updated_date = now(),
                                //     updated_by= ' . yii::app()->user->id .'
                                //     WHERE tool_id ='. $items->tool_id.' and tool_transfer_id='.$_REQUEST['id']);
                                //     $num=$command->execute();
                                // }
                                
                                
                            }
                           
                        }
                    }
                    $j = $j + 1;
                }
            }
            echo "Success"; die;
            /* if(isset($_POST['type'])) {
                $ids = implode(',', $items);
                $command = Yii::app()->db->createCommand
                        ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_current_status = "' . $type . '",updated_date = now(),
                      updated_by= ' . yii::app()->user->id . '
                      WHERE id in (' . $ids . ') and tool_transfer_id ='. $id);
                $num = $command->execute();
                echo $ids;
            }else{
              // $newmodel = new RequestitemStatus;
              // $newmodel->

              foreach($items as $key=> $value){
                $newmodel = new RequestitemStatus;
                $newmodel->item_id = $value;
                $newmodel->accepted_qty = isset($_POST['accepted'][$key])?$_POST['accepted'][$key]:NULL;
                $newmodel->defective_qty = isset($_POST['defective'][$key])?$_POST['defective'][$key]:NULL;
                $newmodel->missing_qty = isset($_POST['missing'][$key])?$_POST['missing'][$key]:NULL;
                $newmodel->not_requested_qty = isset($_POST['not_requested'][$key])?$_POST['not_requested'][$key]:NULL;
                $newmodel->save();
              }
              echo "success";
          }*/
        }

       $this->render('approvalview', array( 'model' => $model, 'newmodel'=>$requestitems , 'page'=>$page, 'id'=>$id,'items' => $items1
        ));
    }


    public function actionApproval() {
        $id = $_REQUEST['id'];
       
        $request_to = $_REQUEST["request_to"];
        $model = $this->loadModel($id);
        $tblpx = Yii::app()->db->tablePrefix;

        //$checks = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `item_current_status` is NULL and `tool_transfer_id`= ". $id)->queryAll();
        $checks = Yii::app()->db->createCommand("SELECT ti.* FROM tms_tool_transfer_items ti
                                        LEFT JOIN tms_tool_transfer_info tr ON ti.parent_info_id = tr.id
                                        WHERE ti.item_current_status is NULL AND ti.tool_transfer_id = {$id} AND tr.item_status != 8 GROUP BY ti.id")->queryAll();
        if(!empty($checks)){
            echo "1";exit;
        }



           $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Approved'))
                ->queryRow();
        $model->request_status = $qryres1['sid'];



        $model->received_date = date('Y-m-d');

        $toolTItems = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id = '.$id.' AND (item_current_status = "accepted" OR item_current_status = "defective" OR item_current_status = "missing" OR item_current_status = "not_requested")'));
        foreach($toolTItems as $row){
        $tools_no=Tools::model()->findByPk($row->tool_id);
            if($tools_no->serialno_status=='Y'){
                
                $tool_model=new ToolTransferItems;
                $tool_model->attributes=$row->attributes;
                $tool_model->item_status=$qryres1['sid'];
                $tool_model->item_current_status=$row->item_current_status;
              
                $tool_model->updated_date=date('Y-m-d');
                $tool_model->location=$request_to;
                $tool_model->save();
            }
        }  

        if ($model->save()) {
             
            foreach($toolTItems as $item) {
               
                $toolId = $item->tool_id;
                $tooldata = Tools::model()->findByPk($toolId);

                $tooldata->location = ($tooldata->location != NULL)?$request_to:NULL;
                $tooldata->save();
            }
            $toolNSItems = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id = '.$id.' AND item_current_status = "added"'));
            $itemMissed  = array();
            foreach($toolNSItems as $item) {
                $toolId         = $item->tool_id;
                $toolmodel      = Tools::model()->findByPk($toolId);
                $itemStatus     = RequestitemStatus::model()->find(array('condition' => 'item_id = '.$item->id));
                $itemTransfered = $itemStatus["accepted_qty"] + $itemStatus["defective_qty"] + $itemStatus["not_requested_qty"];
                $missingQty     = $itemStatus["missing_qty"];
                if($itemStatus["missing_qty"] > 0) {
                    $itemMissed[$toolId] = $itemStatus["missing_qty"];
                }
                $toolCategory = $toolmodel->tool_category;
                $toolData   = Tools::model()->find(array('condition' => 'tool_category = '.$toolCategory." AND location = ".$request_to));
                if($toolData) {
                   
                    $toolsmodel = Tools::model()->findByPk($toolData->id);
                    $toolsmodel->qty = $toolData->qty + $itemTransfered;
                    $toolsmodel->stock_qty = $toolData->qty + $itemTransfered;
                    $toolsmodel->updated_by = Yii::app()->user->id;;
                    $toolsmodel->updated_date = date("Y-m-d");
                    $toolsmodel->save();
                } else {
                   die;
                    $toolsmodel = new Tools;
                    $toolsmodel->attributes = $toolmodel->attributes;
                   
                    $toolsmodel->location = $request_to;
                    $toolsmodel->qty = $itemTransfered;
                    $toolsmodel->stock_qty = $itemTransfered;
                    $toolsmodel->updated_by = Yii::app()->user->id;;
                    $toolsmodel->updated_date = date("Y-m-d");
                    $toolsmodel->save();

                    $toolcat    = ToolCategory::model()->findByPk($toolCategory);

                    $cat_id     = $toolcat->cat_id;
                    $qryres     = Yii::app()->db->createCommand()
                                    ->select('MAX(ref_value) AS maxval')
                                    ->from('tms_tools u')
                                    ->where('tool_category=:tool_category', array(':tool_category'=>$cat_id))
                                    ->queryRow();
                    if (!empty($qryres['maxval'])) {
                        $pr_id  = sprintf("%03d", $qryres['maxval'] + 1);
                        $maxval = $pr_id;
                    } else {
                        $pr_id  = sprintf("%03d", '001');
                        $maxval = $pr_id;
                    }
                    $tool_id    = $toolsmodel->id;
                    $newmodel   = new ToolTransferRequest;
                    $qryres2    = Yii::app()->db->createCommand()
                                    ->select('MAX(request_no) AS maxval')
                                    ->from('tms_tool_transfer_request u')
                                    ->queryRow();
                    if (empty($qryres2['maxval'])) {
                        $rno = 1;
                    } else {
                        $rno = $qryres2['maxval'] + 1;
                    }

                    $newmodel->request_no   = $rno;
                    $newmodel->request_from = $request_to;
                    $newmodel->request_to   = $request_to;
                    $newmodel->request_date = date('Y-m-d');
                    $newmodel->location     = $request_to;

                    $newmodel->created_by   = Yii::app()->user->id;
                    $newmodel->created_date = date('Y-m-d');
                    $newmodel->updated_by   = Yii::app()->user->id;
                    $newmodel->updated_date = date('Y-m-d');
                    $newmodel->save();
                    $insert_id = $newmodel->id;

                    $newmodel1                      = new ToolTransferItems;
                    $newmodel1->tool_transfer_id    = $insert_id;
                    $newmodel1->tool_id             = $tool_id;
                    $newmodel1->code                = $toolcat['cat_code'] .'-'. $maxval;
                    $qryres3    = Yii::app()->db->createCommand()
                                    ->select('*')
                                    ->from('tms_tools u')
                                    ->where('id=:id', array(':id' => $tool_id))
                                    ->queryRow();

                    $newmodel1->item_name   = $qryres3['tool_name'];
                    $newmodel1->unit        = $qryres3['unit'];
                    $newmodel1->serial_no   = $qryres3['serial_no'];
                    $newmodel1->req_date    = date('Y-m-d');
                    $newmodel1->qty         = $qryres3['qty'];
                    $qryres4 = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('tms_location_type u')
                            ->where('id=:id', array(':id' => $request_to))
                            ->queryRow();

                    $newmodel1->item_status = 9;
                    $newmodel1->location    = Yii::app()->user->id;
                    $newmodel1->save();
                   
                    // $updatemodel1= ToolTransferItems::model()->find("tool_id = ". $item->tool_id  ." and item_status=12 ");
                    //           $updatemodel1->qty = $updatemodel1->qty-$newmodel1->qty;
                    //           $updatemodel1->save();
                }
                $toolmodel->qty = $toolmodel->qty - ($itemTransfered + $missingQty);
                $toolmodel->stock_qty = $toolmodel->qty - ($itemTransfered + $missingQty);
                $toolmodel->save();




                
            }

            $date = date('Y-m-d');
            $tblpx = Yii::app()->db->tablePrefix;
            $row = ToolTransferItems::model()->findAll(array('condition' => 'tool_transfer_id =' . $id));

            if(!empty($row)){
                foreach($row as $value) {

                    if($value['duration_type']!=''){
                        $duration_type = Yii::app()->db->createCommand("SELECT caption FROM tms_status WHERE sid=".$value['duration_type'])->queryRow();
                       echo $duration_type['caption'];
                       if($duration_type['caption'] == 'Days') {
                           $return_date = date("Y-m-d",strtotime($date."+".$value['duration_in_days']." day"));
                       }else {
                           $return_date = date("Y-m-d",strtotime($date."+".$value['duration_in_days']." month"));
                       }
                    }

                    //new

                    $return_date = date('Y-m-d');


                      $user=Yii::app()->db->createCommand()->update("{$tblpx}tool_transfer_items", array(
                            'return_date'=> $return_date,
                        ), 'id=:id', array(':id'=>$value['id']));
                }
            }

             /* mail */

            $checks_missing = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `item_current_status` ='missing' and `tool_transfer_id`= ". $id)->queryAll();

            if(!empty($checks_missing)){


            $mail = new JPhpMailer;

            $tbl = Yii::app()->db->tablePrefix;
            $model =  Yii::app()->db->createCommand("SELECT email FROM {$tbl}mail_settings JOIN {$tbl}users on `tms_users`.`userid`= {$tbl}mail_settings.`user_id` " )->queryAll();

            if(!empty($model)){
              foreach ($model as $value) {
                 $to_mail[] = $value['email'];
              }
             }else{
                $model =  Yii::app()->db->createCommand("SELECT email FROM {$tbl}users where user_type=1" )->queryAll();
                foreach ($model as $value) {
                   $to_mail[] = $value['email'];
                }
            }

            foreach ($to_mail as $email) {
                  $mail->addAddress($email);
            }
            if(count($itemMissed) == 0) {
                $itemMissed = "";
            }

            $subject = Yii::app()->name;
            $headers = Yii::app()->name;

            $bodyContent = "<p>Hello ,</p><p>This is a notification to let you know that Tool Transfer Request Missing Items found.</p>";
            $bodyContent .= $this->renderPartial('_emailformmissing', array('insert_id' => $id, 'item_missed' => $itemMissed), true);

            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
            $mail->isHTML(true);

            $mail->Subject = Yii::app()->name;

            $mail->Body = $bodyContent;
            $mail->Send();

          }


        }


    }


    public function actionMissingItems(){

        // $model = ToolTransferItems::model()->findAll(array('condition'=>'item_current_status IN ("missing","not_requested") '));
        $model = ToolTransferItems::model()->findAll(array('condition'=>'item_current_status IN ("missing") '));

        $missing_items = array();
        foreach($model as $key=>$value){
            $missing_items[$key]['id'] = $value['id'];
            $missing_items[$key]['code'] = $value['code'];
            $missing_items[$key]['item_name'] = $value['item_name'];
            $missing_items[$key]['location'] = $value['location'];
            $missing_items[$key]['unit'] = $value['unit'];
            $missing_items[$key]['duration_in_days'] = $value['duration_in_days'];
            $missing_items[$key]['duration_type'] = $value['duration_type'];
            $missing_items[$key]['qty'] = $value['qty'];
            $missing_items[$key]['item_remarks'] = $value['item_remarks'];
            $missing_items[$key]['item_current_status'] = $value['item_current_status'];
        }

        $qryres1 = Yii::app()->db->createCommand("SELECT tms_tool_transfer_items.*,tms_requestitem_status.missing_qty FROM `tms_tool_transfer_items` INNER JOIN tms_requestitem_status ON tms_tool_transfer_items.id=tms_requestitem_status.item_id WHERE tms_requestitem_status.missing_qty IS NOT NULL")->queryAll();
        if(!empty($qryres1)){
          $a= count($model);
          foreach($qryres1 as $key=>$value){
            $missing_items[$a]['id'] = $value['id'];
            $missing_items[$a]['code'] = $value['code'];
            $missing_items[$a]['item_name'] = $value['item_name'];
            $missing_items[$a]['location'] = $value['location'];
            $missing_items[$a]['unit'] = $value['unit'];
            $missing_items[$a]['duration_in_days'] = $value['duration_in_days'];
            $missing_items[$a]['duration_type'] = $value['duration_type'];
            $missing_items[$a]['qty'] = $value['missing_qty'];
            $missing_items[$a]['item_remarks'] = $value['item_remarks'];
            $missing_items[$a]['item_current_status'] = 'missing';
            $a++;
          }
        }

        $this->render('missingitems', array( 'model' => $missing_items,
        ));
    }


    public function actionMissingItemsView($id){



      $model = ToolTransferItems::model()->find(array('condition'=>'id ='. $id ));

      if(isset($_POST['ToolTransferItems'])){

        $ToolTransfer = ToolTransferItems::model()->findByPk($id);

        $condition = $_POST['condition'];

        $reqid=$ToolTransfer['tool_transfer_id'];
        //echo "<pre>";print_r($reqid);"</pre>";exit;
        $remarks = $_POST['ToolTransferItems']['item_remarks'];
       
        $location = $_POST['item_location'];
        $condition = $_POST['condition'];
        $reqdate = $_POST['ToolTransferItems']['req_date'];
        //echo print_r($reqdate);exit;
        if($location>="1"){
            $item_status="accepted";

            //echo print_r("gg");exit;
       



        $sql='update tms_tools set location ="'.$location.'", tool_condition="'.$condition.'"
        where id in (select tool_id from  tms_tool_transfer_items where id="'.$id.'")';
        Yii::app()->db->createCommand($sql)->execute();

             $sql = "SELECT  MAX(`request_no`) as no FROM `tms_tool_transfer_request`";
             $num =  Yii::app()->db->createCommand($sql)->queryRow();
             $numreq =  $num['no']+1;

             $sql1="INSERT into tms_tool_transfer_info(tool_transfer_id,tool_id,item_name,unit,serial_no,
             duration_in_days,duration_type,qty,physical_condition,item_status,location,req_date,
             paid_amount,rating,remarks,service_report,type)
              select DISTINCT
              tool_transfer_id,tool_id,item_name,unit,serial_no,
             duration_in_days,duration_type,qty,physical_condition,item_status,location,req_date,
             paid_amount,rating,remarks,service_report,type from tms_tool_transfer_info
             where tool_transfer_id=".$reqid;
             Yii::app()->db->createCommand($sql1)->execute();  

             

        $sql="insert into tms_tool_transfer_request(request_no,request_date,request_to,request_from,
        transfer_to,paid_amount,rating,remarks,service_report,request_owner_id,ack_user_id,
        request_status,staff_remarks,location,created_by,created_date,updated_by,updated_date,
        transfer_date,received_date)
         select distinct
        ".$numreq.",request_date,".$location.",request_from,transfer_to,paid_amount,
        rating,remarks,service_report," . yii::app()->user->id . ",ack_user_id,request_status,
        staff_remarks,".$location.",created_by,created_date,updated_by,updated_date,transfer_date,
        received_date from tms_tool_transfer_request where id=".$reqid;
        Yii::app()->db->createCommand($sql)->execute();  

        $tool_transfer_idd = Yii::app()->db->getLastInsertID();

         
        $sql3='UPDATE ' . Yii::app()->db->tablePrefix
        . 'tool_transfer_items set
        parent_info_id=(SELECT  MAX(id) as id FROM tms_tool_transfer_info),
        updated_date = now(),
        updated_by= ' . yii::app()->user->id . ',
        item_status=30,
        item_current_status="reported missing"
        WHERE id = '.$id;
        Yii::app()->db->createCommand($sql3)->execute();  

        $sql="INSERT IGNORE INTO tms_tool_transfer_info(tool_transfer_id,tool_id,item_name,unit,serial_no,
        duration_in_days,duration_type,qty,physical_condition,item_status,location,req_date,
        paid_amount,rating,remarks,service_report,type)
         select DISTINCT
        ".$tool_transfer_idd.",tool_id,item_name,unit,serial_no,
        duration_in_days,duration_type,qty,physical_condition,item_status,location,req_date,
        paid_amount,rating,remarks,service_report,type from tms_tool_transfer_info
        where tool_transfer_id=".$reqid;
        Yii::app()->db->createCommand($sql)->execute();  



        $sql = "SELECT  MAX(`id`) as id FROM `tms_tool_transfer_info`";
             $tool_info =  Yii::app()->db->createCommand($sql)->queryRow();
             $tool_id =  $tool_info['id'];//UPDATE `tms_tool_transfer_items` SET `parent_info_id` =( SELECT  MAX(`id`) as id FROM `tms_tool_transfer_info`) WHERE `tms_tool_transfer_items`.`id` = 548;
           
        $sql4="INSERT IGNORE INTO " . Yii::app()->db->tablePrefix
        . "tool_transfer_items(tool_transfer_id,tool_id,code,item_name,unit,serial_no,
        duration_in_days,duration_type,qty,physical_condition,item_status,location,req_date,
        paid_amount,rating,remarks,service_report,item_current_status,updated_by,updated_date,item_remarks,type,parent_info_id,return_qty,approved_qty,request_vendor_status)
        select DISTINCT
       ".$tool_transfer_idd.",tool_id,code,item_name,unit,serial_no,
        duration_in_days,duration_type,qty,".$condition.",item_status,".$location.",'".$reqdate."',
        paid_amount,rating,remarks,service_report,'".$item_status."',". yii::app()->user->id .",now(),item_remarks,type,'".$tool_id."',return_qty,approved_qty,request_vendor_status from " . Yii::app()->db->tablePrefix
        . "tool_transfer_items where id=".$id;
        Yii::app()->db->createCommand($sql4)->execute();  
         
        }
        else{
            //echo print_r("ff");exit;
            $item_status="missing";
            $sql='UPDATE ' . Yii::app()->db->tablePrefix
            . 'tool_transfer_items set item_remarks = "' . $remarks . '",
            item_current_status="'.$item_status.'", updated_date = now(),
            updated_by= ' . yii::app()->user->id . '
            WHERE id = '.$id;
   
            $command = Yii::app()->db->createCommand($sql);
            $num = $command->execute();
           
        }
         
       
       
           
        $this->redirect(array("MissingItems"));
       
      }
      if(yii::app()->user->role==10)
      {
        $paramsArr = array(
            ':active_status' => 1,
            ':user_id' => yii::app()->user->id
          );    
      $res= Yii::app()->db->createCommand()
      ->select('id, name')
      ->from('tms_location_type')
      ->join('tms_location_assigned', 'id=site_id')
      ->where('active_status=:active_status and user_id=:user_id', $paramsArr)
      ->queryAll();
        }
      else
      {
        $res= Yii::app()->db->createCommand()
      ->select('id, name')
      ->from('tms_location_type')
      ->where('active_status=:active_status', array(':active_status'=>1))
      ->queryAll();
   
        }

        $res1= Yii::app()->db->createCommand()
      ->select('sid, caption')
      ->from('tms_status')
      ->where('status_type=:status_type', array(':status_type'=>'physical_condition'))
      ->queryAll();
       
        $this->render('missingview', array( 'model' => $model,
        'location' => $res,
        'condition' => $res1,
        ));

    }
    public function actiondispatchrequesttoStock1($id){
        $page  ='stock';

        $items = (isset($_POST['items']))?$_POST['items']:NULL;

        $model = $this->loadModel($id);
        $tool_request=ToolTransferRequest::model()->findByPk($id);
        $dispatch=DispatchTool::model()->findByAttributes(array('request_id_fk'=>$tool_request->id));
     
        $this->render('requesttostock', array( 'model' => $model, 'tool_request'=>$tool_request ,'dispatch'=>$dispatch, 'page'=>$page, 'id'=>$id,
        ));
       
    }

    public function actionRequesttoStock($id){

        $page  ='stock';

        $items = (isset($_POST['items']))?$_POST['items']:NULL;

        $model = $this->loadModel($id);
               
        $sql="SELECT * FROM tms_tool_transfer_items
        LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id
        LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  
        WHERE tms_tool_transfer_items.tool_transfer_id=:id";

        $requestitems =  Yii::app()->db->createCommand($sql, array(':id'=>$id))->queryAll();
        $sql = "SELECT MAX(`request_no`) as no FROM `tms_tool_transfer_request`";
        $num =  Yii::app()->db->createCommand($sql)->queryRow();

        $paramsArr = array(
                            ':type' => 'location_type',
                            ':caption' => 'Stock'
                          );
        $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', $paramsArr)
                ->queryRow();

        $qryres2 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_location_type')
                ->where('location_type=:type ', array(':type' =>$qryres1['sid'] ,))
                ->queryRow();


        if(isset($_POST['items'])){

            $ids = implode(',', $items);
            $newmodel = new ToolTransferRequest;

            $newmodel->request_no =  $num['no']+1;
            $newmodel->request_date = date('y-m-d');
            $newmodel->request_from = $model->request_to;

            $newmodel->request_to = $qryres2['id'];

            $newmodel->transfer_to = 'stock';
            $newmodel->request_owner_id = $model->request_owner_id;
            $newmodel->ack_user_id = $model->ack_user_id;
            $newmodel->staff_remarks = $model->staff_remarks;
            $newmodel->location = $model->location;
            $newmodel->created_by =Yii::app()->user->id;
            $newmodel->created_date = date('Y-m-d');

            $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'site_transfer', ':caption' => 'Transfer Requested'))
                        ->queryRow();
            $newmodel->request_status = $qryres1['sid'];


            if($newmodel->save()){

                $insert_id = Yii::app()->db->getLastInsertID();
                foreach ($items as  $tool_id) {


                    $model2 = new ToolTransferItems;

                    $data = ToolTransferItems::model()->find('tool_transfer_id = '.$id.' and tool_id = ' .$tool_id);


                    $model2->tool_transfer_id = $insert_id;
                    $model2->req_date = date('Y-m-d');
                    $model2->unit = $data->unit;
                    $model2->code = $data->code;
                    $model2->tool_id = $tool_id;
                    $model2->item_name = $data->item_name;
                    $model2->serial_no = $data->serial_no;
                    $model2->duration_in_days = NULL;
                    $model2->duration_type = NULL;
                    $model2->physical_condition = NULL;
                    $model2->qty = $data->qty;
                    $model2->item_status = $data->item_status;
                    $model2->location = $data->location;
                    $model2->item_current_status = NULL;
                    $model2->paid_amount = $data->paid_amount;
                    $model2->return_date = NULL;
                    $model2->rating = $data->rating;
                    $model2->remarks = $data->remarks;
                    $model2->service_report = $data->service_report;
                    $model2->updated_by = NULL;
                    $model2->updated_date = NULL;
                    $model2->item_remarks = NULL;
                    $model2->save();


                    /* mail */

                    $tbl = Yii::app()->db->tablePrefix;
                    $getdata = "SELECT * FROM {$tbl}users
                    INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                    $users = Yii::app()->db->createCommand($getdata)->queryAll();

                    if ($users != NULL) {
                        $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                    }
                    if ($users != NULL) {
                        foreach ($users as $user) {

                            $mail = new JPhpMailer;
                            $newemail = $user->email;

                            $email = $newemail;
                            $subject = Yii::app()->name;
                            $headers = Yii::app()->name;

                            $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                            $bodyContent .= $this->renderPartial('_emailform', array('insert_id' => $insert_id), true);

                            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                            $mail->addAddress($email);
                            $mail->isHTML(true);

                            $mail->Subject = Yii::app()->name;
                            $mail->Body = $bodyContent;

                            $mail->Send();

                        }
                    }
                   /* mail end */


            }

         }


     }




      $this->render('requesttostock', array( 'model' => $model, 'newmodel'=>$requestitems , 'page'=>$page, 'id'=>$id,
        ));

    }


    public function actionStockApprove($id){
 
        $toolid=Yii::app()->db->createCommand("SELECT tool_id  FROM tms_tool_transfer_items WHERE tool_transfer_id=". $id)->queryAll();
       
        $id = $_REQUEST['id'];
         
        $type = (isset($_POST['type']))?$_POST['type']:NULL;
        $items = (isset($_POST['items']))?$_POST['items']:NULL;

        if(isset($_POST) && !empty($_POST)){


          $sts_type_all = Status::model()->findAll( array('condition' => ' status_type= "physical_condition" '));

          foreach($sts_type_all as $dat){
             if(isset($_POST[$dat['caption']]) && $_POST[$dat['caption']]!=0){
                  $reqmodel = new tmsApproveItemqty;
                  $reqmodel->tool_transfer_id   = $_POST['req_id'];
                  $reqmodel->transfer_item_id   = $_POST['item_id'];
                  $reqmodel->physical_condition = $dat['sid'];
                  $reqmodel->qty                = $_POST[$dat['caption']];
                  $reqmodel->save();
              }
          }
          $tool_update=Tools::model()->findByPk($toolid[0]['tool_id']);
          if($_POST['Damage']!=0){
       
            $command = Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set tool_condition=1 , location=1 , updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE id = '.$toolid[0]['tool_id']);
            $num    = $command->execute();
            $tool_update->tool_condition=1;
            $tool_update->location=1;
            $tool_update->save();
            $command1= Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set physical_condition=1 ,updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE tool_id = '.$toolid[0]['tool_id'].'  and item_status=29 and tool_transfer_id='.$id);
            $num1    = $command1->execute();
          }
         elseif($_POST['Breakdown']!=0){
            $command = Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set tool_condition=1 , location=1 , updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE id = '.$toolid[0]['tool_id']);
            $num    = $command->execute();
            $tool_update->tool_condition=2;
            $tool_update->location=1;
            $tool_update->save();
           
            $command1= Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set physical_condition=2 , updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE tool_id = '.$toolid[0]['tool_id'].'  and item_status=29 and tool_transfer_id='.$id.' and return_qty IS NOT NULL');
            $num1    = $command1->execute();
            }
         elseif ($_POST['Running']!=0) {
            $command = Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tools set tool_condition=1 , location=1 , updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE id = '.$toolid[0]['tool_id']);
            $num    = $command->execute();
            $tool_update->tool_condition=3;
            $tool_update->location=1;
            $tool_update->save();
            $command1= Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set physical_condition=3 ,  updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE tool_id = '.$toolid[0]['tool_id'].' and item_status=29 and tool_transfer_id='.$id.' and return_qty IS NOT NULL');
            $num1    = $command1->execute();
               }
          $command = Yii::app()->db->createCommand
                  ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set approved_qty='.$_POST['total'].' and item_status=29 , updated_date = now(),updated_by= ' . yii::app()->user->id . ' WHERE id = '.$_POST['item_id'].' and tool_transfer_id ='.$id);
          $num    = $command->execute();
        //   echo "3";exit;

        }


        $requestitems =  Yii::app()->db->createCommand("SELECT tms_tool_transfer_items.id,tms_tools.unit,code,item_name,tms_tool_transfer_items.qty,return_qty  FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$id." and tms_tool_transfer_items.item_status=29 and tms_tool_transfer_items.return_qty IS NOT NULL")->queryAll();

        $model = $this->loadModel($id);
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));

        $this->render('stockapprove', array( 'model' => $model, 'items'=>$requestitems , 'id'=>$id, 'status'=>$status,
       ));

    }

    public function actionStocksubmit($id){
      
        $id = $_REQUEST['id'];
        $tool_id_item=ToolTransferItems::model()->find('tool_transfer_id='.$id);
        $tool_id=Tools::model()->find('id='.$tool_id_item->tool_id);
       if($tool_id->serialno_status=='Y'){
        $checks = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `approved_qty` is NULL and `tool_transfer_id`= ". $id." and return_qty IS NOT NULL")->queryAll();
       }else{
        $checks = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `tool_transfer_id`= ". $id)->queryAll();
       
       }
        if(empty($checks)){
            echo "1";exit;
        }
       
        

       

      

        $qryres1 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'stock_transfer', ':caption' => 'Stock Approved'))
                ->queryRow();

        $qryres2 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_location_type')
                ->where('id=:id ', array(':id' =>$qryres1['sid'] ))
                ->queryRow();
               

            $command = Yii::app()->db->createCommand
                ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_status = "' . $qryres1['sid'] . '", location= '. $qryres2['id'].', updated_date = now(),
              updated_by= ' . yii::app()->user->id . '
              WHERE  tool_transfer_id ='. $id);
               $num = $command->execute();
            
        if($qryres2['id']!=""){
           
            $command = Yii::app()->db->createCommand
            ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set item_status = "' . $qryres1['sid'] . '", location= '. $qryres2['id'].', updated_date = now(),
          updated_by= ' . yii::app()->user->id . '
          WHERE  tool_transfer_id ='. $id);
           $num = $command->execute();
        }

       


      //  $select = Yii::app()->db->createCommand('SELECT tool_id FROM ' . Yii::app()->db->tablePrefix . 'tool_transfer_items WHERE  tool_transfer_id ='. $id)->queryRow();



        $qryres3 = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption =:caption', array(':type' => 'stock_transfer', ':caption' => 'Stock Approved'))
                ->queryRow();
        $command = Yii::app()->db->createCommand
                    ('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_request set request_status = ' . $qryres3['sid'] .', updated_date = now(),
                  updated_by= ' . yii::app()->user->id . '
                  WHERE  id ='. $id);
         $num = $command->execute();
        
         if($tool_id->serialno_status=='Y'){
            $tool_transfer_final=ToolTransferItems::model()->find('tool_transfer_id='.$id.' and item_status=29 and return_qty IS NOT NULL');

            $tool_transfer_model1=new ToolTransferItems;
            $tool_transfer_model1->tool_transfer_id=$id;
            $tool_transfer_model1->tool_id=$tool_transfer_final['tool_id'];
            $tool_transfer_model1->code=$tool_transfer_final['code'];
            $tool_transfer_model1->item_name=$tool_transfer_final['item_name'];
            $tool_transfer_model1->physical_condition=$tool_transfer_final['physical_condition'];
            $tool_transfer_model1->location=1;
            $tool_transfer_model1->return_date=date('Y-m-d');
            $tool_transfer_model1->return_qty=$tool_transfer_final['return_qty'];
            $tool_transfer_model1->approved_qty=$tool_transfer_final['approved_qty'];

            $tool_transfer_model1->item_status=22;
            $tool_transfer_model1->save();
           
        }
         echo "success";
     }

    public function actionchangesupervisor(){

        $this->render('assigntools', array());
    }

    public function actiongetengineer(){

        if(isset($_POST['id'])){

            $id = $_POST['id'];
            $html ='';

            $site_engineers = Yii::app()->db->createCommand("Select user_id,concat_ws(' ',first_name,last_name ) as name from tms_location_assigned as ase inner join tms_users as us on us.userid = ase.user_id where site_id= ".$id)->queryAll();

            $html .='<select id ="siteeng" name="siteeng" class="form-control">';
            $html .= '<option value="">Please choose</option>';
                 foreach($site_engineers as $data) {
            $html .='<option value= '.$data["user_id"].'>'.$data['name'] .'</option>';
                }

            $html .=' </select>';

            echo json_encode($html);exit;
        }




    }


    public function actionAssignsubmit(){
        

        if(isset($_POST)){

            $loc = $_POST['id'];
            $eng = $_POST['eng'];
            $duration = $_POST['duration'];
            $dur_type = $_POST['dur_type'];
            $date = date('Y-m-d');

            if($dur_type == 15) {
                    $return_date = date("Y-m-d",strtotime($date."+". $duration ." day"));
             }else {
                    $return_date = date("Y-m-d",strtotime($date."+". $duration ." month"));
             }



            if(empty($duration)){
                $duration = 1;
                $dur_type = 16;
            }

            $alltools = Yii::app()->db->createCommand("SELECT tl.* FROM `tms_location_type` as lo
                            inner join  `tms_tools` as  tl on tl.location = lo.id  WHERE lo.id= ".$loc)->queryAll();
            $data = array();
            $arr = 0;
            foreach ($alltools as $key => $value){

                $info = Yii::app()->db->createCommand( "SELECT *  FROM `tms_tool_transfer_info` WHERE location = ".$loc." and tool_id = ".$value['id'] )->queryRow();

                $items = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `tool_transfer_id`= ".$value['id']." and `duration_type` is not null ")->queryRow();

                if(empty($info) and empty($items)){
                    $data[] = $value;
                }
            }

            //echo '<pre>';print_r($data);exit;


        if( !empty( $data)){

            /* insert to request */
            $model = new ToolTransferRequest;

            $maxreq = Yii::app()->db->createCommand("SELECT MAX(request_no) FROM tms_tool_transfer_request")->queryScalar();
            $maxreq = $maxreq +1;

                $model->request_no    = $maxreq;
                $model->request_date  = date('Y-m-d');
                $model->request_to    = $loc;
                $model->request_from  = $loc;
                $model->transfer_to   = 'site';
                $model->request_owner_id = $eng;
                $model->request_status = 7; // final approve
                $model->location = $loc;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d');

                if( $model->save() ){

                    $insert_id = Yii::app()->db->getLastInsertID();

                    foreach($data as $key => $value) {

                        $newmodel = new ToolTransferInfo;

                        $newmodel->tool_transfer_id = $insert_id;
                        $newmodel->tool_id = $value['id'];
                        $newmodel->item_name = $value['tool_name'];
                        $newmodel->unit = $value['unit'];
                        $newmodel->duration_in_days = $duration;
                        $newmodel->duration_type = $dur_type;
                        $newmodel->qty = $value['qty'];
                        $newmodel->item_status = 12;
                        $newmodel->location = $loc;
                        $newmodel->req_date = date('Y-m-d');
                        $newmodel->save();

                        $itemmodel = new ToolTransferItems;
                        $itemmodel->tool_transfer_id = $insert_id;
                        $itemmodel->tool_id = $value['id'];
                        $itemmodel->code = $value['ref_no'];
                        $itemmodel->item_name = $value['tool_name'];
                        $itemmodel->unit = $value['unit'];
                        $itemmodel->duration_in_days = $duration;
                        $itemmodel->duration_type = $dur_type;
                        $itemmodel->serial_no = $value['serial_no'];
                        $itemmodel->qty = $value['qty'];
                        $itemmodel->item_status =13;
                        $itemmodel->location = $loc;
                        $itemmodel->req_date = date('Y-m-d');
                        $itemmodel->return_date = $return_date;
                         $command1    = Yii::app()->db->createCommand('UPDATE ' . Yii::app()->db->tablePrefix . 'tool_transfer_items set return_date ='.$return_date.',updated_date = now(),
                           updated_by= ' . yii::app()->user->id .'
                          WHERE id ='.$tool_id['tool_id'].' and item_status=30 and return_qty IS NULL');
                            $num1       = $command1->execute();
                        $itemmodel->save();

                    }

                 echo 1;exit;

                }
            }else{

                 echo 2;exit;
            }


        } //post

     }

      public function actionChangeeng(){

        if(isset($_POST)){



            $loc = $_POST['locid'];
            $engfrm = $_POST['engfrm'];
            $engto = $_POST['engto'];
            $duration = $_POST['duration'];
            $dur_type = $_POST['dur_type'];
            $date = date('Y-m-d');
             if($dur_type == 15) {
                    $return_date = date("Y-m-d",strtotime($date."+". $duration ." day"));
             }else {
                    $return_date = date("Y-m-d",strtotime($date."+". $duration ." month"));
             }

            $alltools = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_request` WHERE `request_owner_id`=".$engfrm." and `location`=".$loc."  and `request_status`= 9
                ")->queryAll();
            $data = array();



        if(!empty($alltools)){

            foreach ($alltools as $key => $value){

               /*$data[] = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` WHERE `tool_transfer_id`= ".$value['id'] )->queryAll();*/

                $model = new ToolTransferRequest;

                $maxreq = Yii::app()->db->createCommand("SELECT MAX(request_no) FROM tms_tool_transfer_request")->queryScalar();
                $maxreq = $maxreq +1;

                    $model->request_no    = $maxreq;
                    $model->request_date  = date('Y-m-d');
                    $model->request_to    = $loc;
                    $model->request_from  = $loc;
                    $model->transfer_to   = 'site';
                    $model->request_owner_id = $engto;
                    $model->request_status = 7; // final approve
                    $model->location = $loc;
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d');

                    if( $model->save() ){

                        $insert_id = Yii::app()->db->getLastInsertID();

                        $itemall = Yii::app()->db->createCommand("SELECT * FROM `tms_tool_transfer_items` as itm left join tms_tools as tol on tol.id= itm.tool_id WHERE `tool_transfer_id`= ".$value['id'] )->queryAll();
                        foreach($itemall as $key => $data) {

                             $newmodel = new ToolTransferInfo;

                            $newmodel->tool_transfer_id = $insert_id;
                            $newmodel->tool_id = $data['id'];
                            $newmodel->item_name = $data['tool_name'];
                            $newmodel->unit = $data['unit'];
                            $newmodel->duration_in_days = 1;
                            $newmodel->duration_type = 15;
                            $newmodel->qty = $data['qty'];
                            $newmodel->item_status = 12;
                            $newmodel->location = $loc;
                            $newmodel->req_date = date('Y-m-d');
                            $newmodel->save();

                            $itemmodel = new ToolTransferItems;
                            $itemmodel->tool_transfer_id = $insert_id;
                            $itemmodel->tool_id = $data['id'];
                            $itemmodel->code = $data['ref_no'];
                            $itemmodel->item_name = $data['tool_name'];
                            $itemmodel->unit = $data['unit'];
                            $itemmodel->duration_in_days = 1;
                            $itemmodel->duration_type = 15;
                            $itemmodel->serial_no = $data['serial_no'];
                            $itemmodel->qty = $data['qty'];
                            $itemmodel->item_status = 13;
                            $itemmodel->location = $loc;
                            $itemmodel->req_date = date('Y-m-d');


                            $itemmodel->return_date = $return_date;

                            $itemmodel->save();

                        }
                    }
                }

                 echo 1;exit;
            }else{

               echo 2;exit;

            }

        } //post



    }

    public function actionGetitems(){
$type = (isset($_REQUEST['type']) ? $_REQUEST['type'] : '');
$item_array = array();
$model = new Tools();
                if($type == 0){
                    //$criteria->addCondition('pending_status ='.$this->getType($type));
                    $parents = Tools::model()->findAll(array('condition' => 'pending_status ='.$model->getType($type),'group' => 'tool_name,tool_category'));
                }else if($type == 1){
                      $parents = Tools::model()->findAll(array('condition' => 'pending_status ='.$model->getType($type),'group' => 'tool_name,tool_category'));
                }
//$model->getType($type);

$datanew=array();
foreach ($parents as $parent) {
array_push($datanew,array("value"=>$parent['id'],"label"=>$parent['tool_name']));
}


echo json_encode($datanew);

}

 public function actionGetitemscode(){

$item_array = array();
    $location = $_POST['location'];
   
    if($_POST['type'] ==''){
       
      $qryres = Yii::app()->db->createCommand()
              ->select('*')
              ->from('tms_status')
              ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
              ->queryRow();
    $parents = Tools::model()->findAll(array('condition' => 'serialno_status="Y" AND pending_status = '.$qryres['sid'].' AND location='.$location.''));
    }else{
        
       $qryres = Yii::app()->db->createCommand()
              ->select('*')
              ->from('tms_status')
              ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
              ->queryRow();
      if($_POST['type'] == 0){
        // echo "d";die;
        // $parents = Tools::model()->findAll(array('condition' => 'serialno_status="Y" AND pending_status = '.$qryres['sid'].' AND location='.$location.''));
         $parents = Tools::model()->findAll(array('condition' => ' pending_status = '.$qryres['sid'].' AND location ='.$location.''));

      }else{
         $qryres = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tms_status')
                ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool Set'))
                ->queryRow();
         $parents = Tools::model()->findAll(array('condition' => 'pending_status = '.$qryres['sid'].' AND location='.$location.''));
      }
    }

$datanew=array();
foreach ($parents as $parent) {
array_push($datanew,array("value"=>$parent['id'],"label"=>$parent['ref_no']));
}


echo json_encode($datanew);

}


/*--------------Mod by rajisha 31-8-2018-----------------------------*/

        public function actionSiteviewallocate($id){
           
            if(isset($_REQUEST['request_id'])){
              
                $requestid = $_REQUEST['request_id'];
                $iteminfoid = isset($_REQUEST['iteminfoid']) ? $_REQUEST['iteminfoid'] : '';
                $selecteditems = isset($_REQUEST['chked_vals']) ? $_REQUEST['chked_vals'] : '';
                $chked_reqqty = isset($_REQUEST['chked_reqqty']) ? $_REQUEST['chked_reqqty'] : '';
                $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
                $duration = isset($_REQUEST['duration']) ? $_REQUEST['duration'] : '';
                $duration_type = isset($_REQUEST['duration_type']) ? $_REQUEST['duration_type'] : '';
                for ($i = 0; $i < count($selecteditems); $i++) {
                        $request_details = ToolTransferRequest::model()->findByPk($id);
                        $tooldata = Tools::model()->findByPk($selecteditems[$i]);
                    
                        $exit_data =  Yii::app()->db->createCommand("SELECT COUNT(*) FROM tms_tool_transfer_items LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id= tms_tools.id LEFT JOIN tms_status ON tms_status.sid=tms_tools.tool_condition  WHERE tms_tool_transfer_items.tool_transfer_id=".$id." AND tool_id = ".$selecteditems[$i])->queryScalar();
                        if($exit_data == 0){
                            
                            $newmodel = new ToolTransferItems;
                            $newmodel->tool_transfer_id = $id;
                            $newmodel->req_date = date('Y-m-d');
                            $newmodel->unit = $tooldata['unit'];
                            $newmodel->code = $tooldata['ref_no'];
                            $newmodel->tool_id = $selecteditems[$i];
                            $newmodel->item_name = $tooldata['tool_name'];
                            $newmodel->location = $tooldata->location;
                           
                            $newmodel->item_status = 29;
                            
                            
                            $newmodel->duration_in_days = $duration;
                            $newmodel->duration_type = $duration_type;
                           //$newmodel->qty = 1;
                            if(isset($_REQUEST['chked_reqqty'])){
                              $newmodel->qty = $chked_reqqty[$i];
                              $updatemodel1= ToolTransferItems::model()->find("tool_id = ".$selecteditems[$i]." and item_status=12");
                            //   $updatemodel1->qty = $updatemodel1->qty-$newmodel->qty;
                              if($updatemodel1->qty<=0)
                              {
                                $updatemodel1->qty=0;  
                              }
                              $updatemodel1->save();
                              
                            }else{

                              $newmodel->qty = 1;
                              $updatemodel1= ToolTransferItems::model()->find("tool_id = ".$selecteditems[$i]." and item_status=12");
                            //   $updatemodel1->qty = $updatemodel1->qty-$newmodel->qty;
                              if($updatemodel1->qty<=0)
                              {
                                $updatemodel1->qty=0;  
                              }
                              $updatemodel1->save();
                            }
                            $newmodel->parent_info_id = $iteminfoid;
                           // echo "<pre>";                            print_r($newmodel->attributes);exit;
                            //echo $site; die;
                            //$tooldata->location = $site;
                            //$tooldata->save();
                            if($newmodel->save())
                            {
                                 "Success";
                            }
                        }else{

                          // new section update tool qty

                          if(isset($_REQUEST['chked_reqqty']) && isset($chked_reqqty[$i])){

                             $updatemodel= ToolTransferItems::model()->find(" tool_transfer_id = ".$id." and tool_id = ".$selecteditems[$i]);
                            //  $updatemodel->qty = $updatemodel->qty+$chked_reqqty[$i];
                           
                             $updatemodel->save();
                             
                             $updatemodel1= ToolTransferItems::model()->find("tool_id = ".$selecteditems[$i]." and item_status=12");
                            //  $updatemodel1->qty = $updatemodel1->qty-$chked_reqqty[$i];
                             $updatemodel1->save();
                          }
                          $msg = "Its already added";
                        }

                }
              
            }
            

        }

        public function actionRejectitem(){
            if(isset($_REQUEST['info'])){
                $infoid = $_REQUEST['info'];
                $model = ToolTransferInfo::model()->findByPk($infoid);
                $model->item_status = 8;
                $model->save();
            }
        }

        public function actionDeleteallocateditem($id = ''){
            if(isset($id)){
                $model = ToolTransferItems::model()->findByPk($id);
                $req = $model->tool_transfer_id;
                $del = ToolTransferItems::model()->deleteByPk($id);
                if($del){
                    Yii::app()->user->setFlash('success', 'Item was delted successfully.');
                    $this->redirect(array('/toolTransferRequest/siteview&id=' . $req));
                }
            }
        }
        public function actionScheduleddate($id) {
        $model = $this->loadModel($id);
        //$model->request_status = 9;


        $model->received_date = $_POST['ToolTransferRequest']['received_date'];



        $model->save();
        $this->redirect('index.php?r=/ToolTransferRequest/view&id='.$id.'&page=stock');
    }

    public function actionGetvendoritemscode(){

$item_array = array();

$tblpx = Yii::app()->db->tablePrefix;
    if($_POST['type'] ==''){
      $qryres = Yii::app()->db->createCommand()
              ->select('*')
              ->from('tms_status')
              ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
              ->queryRow();
   $parents = Yii::app()->db->createCommand("SELECT t.id, t.ref_no, t.tool_name, {$tblpx}status.caption as statuscaption FROM {$tblpx}tools t LEFT JOIN `{$tblpx}status` ON {$tblpx}status.sid = t.tool_condition WHERE t.pending_status = ".$qryres['sid']." AND ({$tblpx}status.caption ='Damage' OR {$tblpx}status.caption ='Breakdown')")->queryAll();
    }else{
          if($_POST['type'] == 0){
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool'))
                    ->queryRow();
         //$parents = Yii::app()->db->createCommand("SELECT t.id, t.ref_no, t.tool_name, {$tblpx}status.caption as statuscaption FROM {$tblpx}tools t LEFT JOIN `{$tblpx}status` ON {$tblpx}status.sid = t.tool_condition WHERE t.pending_status = ".$qryres['sid']." AND ({$tblpx}status.caption ='Damage' OR {$tblpx}status.caption ='Breakdown')")->queryAll();
            $parents = Yii::app()->db->createCommand("SELECT tms_tools.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.pending_status = ".$qryres['sid']."  AND (tms_approve_itemqty.physical_condition =1 OR tms_approve_itemqty.physical_condition =2) AND tms_tools.location =1 AND tms_tool_transfer_items.request_vendor_status='1' GROUP BY tms_tools.id")->queryAll();

            // echo "SELECT tms_tools.* FROM tms_tool_transfer_items INNER JOIN tms_approve_itemqty ON tms_tool_transfer_items.id=tms_approve_itemqty.transfer_item_id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.pending_status = ".$qryres['sid']."  AND tms_approve_itemqty.physical_condition =1 OR tms_approve_itemqty.physical_condition =2";
            // exit();
          }else{
            $qryres = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tms_status')
                    ->where('status_type=:type and caption=:caption', array(':type' => 'pending_status', ':caption' => 'Tool Set'))
                    ->queryRow();
         $parents = Yii::app()->db->createCommand("SELECT t.id, t.ref_no, t.tool_name, {$tblpx}status.caption as statuscaption FROM {$tblpx}tools t LEFT JOIN `{$tblpx}status` ON {$tblpx}status.sid = t.tool_condition WHERE t.pending_status = ".$qryres['sid']." AND ({$tblpx}status.caption ='Damage' OR {$tblpx}status.caption ='Breakdown')")->queryAll();
          }
    }
$datanew=array();
foreach ($parents as $parent) {
array_push($datanew,array("value"=>$parent['id'],"label"=>$parent['ref_no']));
}


echo json_encode($datanew);

}

        public function actionCreatevendor() {
//$this->layout = false;
        $model = new ToolTransferRequest;
        $page = 'vendor';
        $newmodel = new ToolTransferItems;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));

        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));
        if (isset($_POST['ToolTransferRequest'])) {


          //  print_r($_POST);die;

            $req_no=$_POST['ToolTransferRequest']['request_no'];
            $reqto = $_POST['ToolTransferRequest']['request_to'];
            $transaction=$_POST['trans_id'];
            //echo  $transaction;die;
            if($transaction == '' || $transaction == 0){
              $tool_details = Tools::model()->findByPk($_POST['newcode']);
              if($tool_details->serialno_status == 'Y'){

            $model->attributes = $_POST['ToolTransferRequest'];
            $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
            $model->request_date = date('Y-m-d');
            // $model->location = $_POST['ToolTransferRequest']['request_from'];
            $reqto = $_POST['ToolTransferRequest']['request_to'];

            if (Yii::app()->user->role == 1) {
                $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
            } else {
                $model->request_owner_id = Yii::app()->user->id;
            }

                $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                        ->queryRow();
$model->request_status = $qryres1['sid'];
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->transfer_to = 'vendor';
            if ($model->save()) {


                $insert_id = Yii::app()->db->getLastInsertID();
                  //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                       // print_r($_POST);die;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                        $type=$_POST['type'];
                    $newcode = $_POST['newcode'];
                    $item_name = $_POST['item_name'];
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferItems;
                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                        $newmodel->unit = $unit;
                        $newmodel->type = $type;
                        $newmodel->tool_id = $newcode;
                        $newmodel->item_name = $item_name;
                        $newmodel->code = $code;
                    // $newmodel->serial_no = $serial_no[$i];
                        $newmodel->duration_in_days = isset($duration) ? $duration : '';
                        $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                        $newmodel->qty = $quantity;
                        $newmodel->physical_condition = $physical_condition;

                                                    if($newcode != NULL) {
                                                            $tools = Tools::model()->findByPk($newcode);

$location = LocationType::model()->findByPk($tools->location)->location_type;
$newmodel->item_status = $location;
$newmodel->location = $reqto;

// $tools->location = $reqfrom;
              $tools->location = $reqto;
$tools->save();
                    $newmodel->save();
                    //print_r($newmodel->getErrors());
                }
//print_r($newmodel->getErrors());
//echo "<pre>";print_r($newmodel->attributes);exit;

                    // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                    $tbl = Yii::app()->db->tablePrefix;
                    $getdata = "SELECT * FROM {$tbl}users
INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                    $users = Yii::app()->db->createCommand($getdata)->queryAll();

                    if ($users != NULL) {
                        $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                    }
                    if ($users != NULL) {
                        foreach ($users as $user) {
                            // require_once 'PHPMailer/class.phpmailer.php';
                            $mail = new JPhpMailer;
                            $newemail = $user->email;

                            $email = $newemail;
                            $subject = Yii::app()->name;
                            $headers = Yii::app()->name;

                            $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                            $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                            $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                            $mail->addAddress($email);   // Add a recipient
                            $mail->isHTML(true);

                            $mail->Subject = Yii::app()->name;
                            $mail->Body = $bodyContent;
                            $mail->Send();

                        }
                    }


                        $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$model->id));
                    //$this->redirect(array('/toolTransferRequest/site'));
            }

            }else{
                // with out serial no

                  // print_r($tool_details->attributes);


                  // existing tools edit
                  $qryres1 = Yii::app()->db->createCommand("SELECT SUM(tms_approve_itemqty.qty) as qty FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2")->queryRow();
                  $damage =$qryres1['qty'];

                  $qryres2 = Yii::app()->db->createCommand("SELECT SUM(tms_approve_itemqty.qty) as qty FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition=3")->queryRow();
                  $running =$qryres2['qty'];

                  if($running ==0 && $damage !=0){
                    // only location sqlite_changes

                    $model->attributes = $_POST['ToolTransferRequest'];
                    $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
                    $model->request_date = date('Y-m-d');
                    // $model->location = $_POST['ToolTransferRequest']['request_from'];
                    $reqto = $_POST['ToolTransferRequest']['request_to'];

                    if (Yii::app()->user->role == 1) {
                        $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
                    } else {
                        $model->request_owner_id = Yii::app()->user->id;
                    }

                        $qryres1 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_status')
                                ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                                ->queryRow();
        $model->request_status = $qryres1['sid'];
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d');
                    $model->transfer_to = 'vendor';
                    if ($model->save()) {


                        $insert_id = Yii::app()->db->getLastInsertID();
                          //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                               // print_r($_POST);die;
                            $unit = $_POST['unit'];
                            $code = $_POST['code'];
                                $type=$_POST['type'];
                            $newcode = $_POST['newcode'];
                            $item_name = $_POST['item_name'];
                            // $serial_no = $_POST['serial_no'];
                            $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                            $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                            $quantity = $_POST['quantity'];
                            $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                            $newmodel = new ToolTransferItems;
                            $newmodel->tool_transfer_id = $insert_id;
                            $newmodel->req_date = date('Y-m-d');
                                $newmodel->unit = $unit;
                                $newmodel->type = $type;
                                $newmodel->tool_id = $newcode;
                                $newmodel->item_name = $item_name;
                                $newmodel->code = $code;
                            // $newmodel->serial_no = $serial_no[$i];
                                $newmodel->duration_in_days = isset($duration) ? $duration : '';
                                $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                                $newmodel->qty = $quantity;
                                $newmodel->physical_condition = $physical_condition;

                                                            if($newcode != NULL) {
                                                                    $tools = Tools::model()->findByPk($newcode);

        $location = LocationType::model()->findByPk($tools->location)->location_type;
        $newmodel->item_status = $location;
        $newmodel->location = $reqto;
                      //$newmodel->location = $location;
                      //$tools->qty = $running;
                      //$tools->stock_qty = $running;
        // $tools->location = $reqfrom;
                      $tools->location = $reqto;
        $tools->save();
                            $newmodel->save();
                            //print_r($newmodel->getErrors());
                        }
        //print_r($newmodel->getErrors());
        //echo "<pre>";print_r($newmodel->attributes);exit;

                            // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                            $tbl = Yii::app()->db->tablePrefix;
                            $getdata = "SELECT * FROM {$tbl}users
        INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                            $users = Yii::app()->db->createCommand($getdata)->queryAll();

                            if ($users != NULL) {
                                $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                            }
                            if ($users != NULL) {
                                foreach ($users as $user) {
                                    // require_once 'PHPMailer/class.phpmailer.php';
                                    $mail = new JPhpMailer;
                                    $newemail = $user->email;

                                    $email = $newemail;
                                    $subject = Yii::app()->name;
                                    $headers = Yii::app()->name;

                                    $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                                    $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                                    $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                                    $mail->addAddress($email);   // Add a recipient
                                    $mail->isHTML(true);

                                    $mail->Subject = Yii::app()->name;
                                    $mail->Body = $bodyContent;
                                    $mail->Send();

                                }
                            }
                    }



                  }else{

                    //          print_r($_POST);die;


                    $model->attributes = $_POST['ToolTransferRequest'];
                    $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
                    $model->request_date = date('Y-m-d');
                    // $model->location = $_POST['ToolTransferRequest']['request_from'];
                    $reqto = $_POST['ToolTransferRequest']['request_to'];

                    if (Yii::app()->user->role == 1) {
                        $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
                    } else {
                        $model->request_owner_id = Yii::app()->user->id;
                    }

                        $qryres1 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_status')
                                ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                                ->queryRow();
        $model->request_status = $qryres1['sid'];
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d');
                    $model->transfer_to = 'stock';
                    $model->request_status = 22;
                    if ($model->save()) {


                        $insert_id = Yii::app()->db->getLastInsertID();
                          //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                               // print_r($_POST);die;
                            $unit = $_POST['unit'];
                            $code = $_POST['code'];
                                $type=$_POST['type'];
                            $newcode = $_POST['newcode'];
                            $item_name = $_POST['item_name'];
                            // $serial_no = $_POST['serial_no'];
                            $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                            $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                            $quantity = $_POST['quantity'];
                            $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                            $newmodel = new ToolTransferItems;
                            $newmodel->tool_transfer_id = $insert_id;
                            $newmodel->req_date = date('Y-m-d');
                                $newmodel->unit = $unit;
                                $newmodel->type = $type;
                                $newmodel->tool_id = $newcode;
                                $newmodel->item_name = $item_name;
                                $newmodel->code = $code;
                            // $newmodel->serial_no = $serial_no[$i];
                                $newmodel->duration_in_days = isset($duration) ? $duration : '';
                                $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                                $newmodel->qty = $running;
                                $newmodel->physical_condition = $physical_condition;

                                                            if($newcode != NULL) {
                                                                    $tools = Tools::model()->findByPk($newcode);

        $location = LocationType::model()->findByPk($tools->location)->location_type;
        $newmodel->item_status = $location;
        //$newmodel->location = $reqto;
                      $newmodel->location = $location;
                      $tools->qty = $running;
                      $tools->stock_qty = $running;
        // $tools->location = $reqfrom;
                      //$tools->location = $reqto;
        $tools->save();
                            $newmodel->save();
                            //print_r($newmodel->getErrors());
                        }
        //print_r($newmodel->getErrors());
        //echo "<pre>";print_r($newmodel->attributes);exit;

                            // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                            $tbl = Yii::app()->db->tablePrefix;
                            $getdata = "SELECT * FROM {$tbl}users
        INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                            $users = Yii::app()->db->createCommand($getdata)->queryAll();

                            if ($users != NULL) {
                                $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                            }
                            if ($users != NULL) {
                                foreach ($users as $user) {
                                    // require_once 'PHPMailer/class.phpmailer.php';
                                    $mail = new JPhpMailer;
                                    $newemail = $user->email;

                                    $email = $newemail;
                                    $subject = Yii::app()->name;
                                    $headers = Yii::app()->name;

                                    $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                                    $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                                    $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                                    $mail->addAddress($email);   // Add a recipient
                                    $mail->isHTML(true);

                                    $mail->Subject = Yii::app()->name;
                                    $mail->Body = $bodyContent;
                                    $mail->Send();

                                }
                            }
                    }


                    // add new tool
                  //  if($damage !=0){
                    $tmodel = new Tools;
                    $model->attributes = $_POST['ToolTransferRequest'];
                    $tmodel->tool_code = $tool_details->tool_code;
                    $tmodel->tool_name = $tool_details->tool_name;
                    $tmodel->unit = $tool_details->unit;
                    $tmodel->make = $tool_details->make;
                    $tmodel->model_no = $tool_details->model_no;
                    $tmodel->serial_no = $tool_details->serial_no;
                    $tmodel->ref_no = $tool_details->ref_no;
                    $tmodel->batch_no = $tool_details->batch_no;
                    $tmodel->created_by = Yii::app()->user->id;
                    $tmodel->created_date = date('Y-m-d');
                    $tmodel->updated_by = Yii::app()->user->id;
                    $tmodel->updated_date = date('Y-m-d');
                    $tmodel->pending_status = $tool_details->pending_status;
                    $tmodel->location = $reqto;
                    $tmodel->qty = $damage;
                    $tmodel->stock_qty = $damage;
                    $tmodel->tool_condition = $tool_details->tool_condition;
                    $tmodel->active_status = $tool_details->active_status;
                    $tmodel->prev_main = "0";
                    $tmodel->prev_hrs  = NULL;
                    $tmodel->prev_date = NULL;
                    $tmodel->serialno_status = $tool_details->serialno_status;
                    $tmodel->warranty_date = $tool_details->warranty_date;
                    $tmodel->parent_id = $tool_details->id;
                    $tmodel->item_id = $tool_details->item_id;
                    $tmodel->tool_category = $tool_details->tool_category;
                    $toolcat = ToolCategory::model()->findByPk($tool_details->tool_category);
                    $qryres = Yii::app()->db->createCommand()
                            ->select('MAX(ref_value) AS maxval')
                            ->from('tms_tools u')
                            ->where('tool_category=:tool_category', array(':tool_category'=>$tool_details->tool_category))
                            ->queryRow();


                    if (!empty($qryres['maxval'])) {
                        $pr_id = sprintf("%03d", $qryres['maxval'] + 1);
                        $maxval = $pr_id;
                    } else {
                        $pr_id = sprintf("%03d", '001');
                        $maxval = $pr_id;
                    }
                    if ($tmodel->save(false)) {
                        $toolid = Yii::app()->db->getLastInsertID();

                        // $qryres1 = Yii::app()->db->createCommand("SELECT tms_approve_itemqty.* FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2")->queryRow();
                        // $qryres1->execute();

                        $qryres1 = Yii::app()->db->createCommand("UPDATE `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id  SET tms_approve_itemqty.status='0'  WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2");
                         $qryres1->execute();


                        // Tools location
                        // $tool_item = new ToolsLocation;
                        // $tool_item->qty = $damage;
                        // $tool_item->tool_id = $model->id;
                        // $tool_item->location = $reqto;
                        // $tool_item->created_by = Yii::app()->user->id;
                        // $tool_item->created_date = date('Y-m-d');
                        // $tool_item->save(false);

                        // $item_model->remaining_qty = ($item_model->remaining_qty - $_POST['Tools']['qty']);
                        // $item_model->save();
                        // if ($item_model->remaining_qty == 0) {
                        //     $item_model->tool_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                        //     $item_model->save();
                        // }
                        $newmodel = new ToolTransferRequest;
                        //$newmodel->tool_id = $id;
                        $qryres2 = Yii::app()->db->createCommand()
                                ->select('MAX(request_no) AS maxval')
                                ->from('tms_tool_transfer_request u')
                                //->where('id=:id', array(':id'=>$id))
                                ->queryRow();
                        if (empty($qryres2['maxval'])) {
                            $rno = 1;
                        } else {
                            $rno = $qryres2['maxval'] + 1;
                        }

                        $newmodel->request_no = $rno;
                        $newmodel->request_from = 1;
                        $newmodel->request_status = 10;
                        $newmodel->request_to = $reqto;
                        $newmodel->request_date = date('Y-m-d');
                        $newmodel->location = $reqto;
                        $newmodel->transfer_to = 'vendor';
                        $newmodel->created_by = Yii::app()->user->id;
                        $newmodel->created_date = date('Y-m-d');
                        $newmodel->updated_by = Yii::app()->user->id;
                        $newmodel->updated_date = date('Y-m-d');
                        $newmodel->save();
                        $insert_id = Yii::app()->db->getLastInsertID();
                        $request_id = $insert_id;
                        $newmodel1 = new ToolTransferItems;
                        $newmodel1->tool_transfer_id = $insert_id;
                        $newmodel1->tool_id = $toolid;
                        $newmodel1->code = $toolcat['cat_code'] .'-'. $maxval;
                        $qryres3 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_tools u')
                                ->where('id=:id', array(':id' => $toolid))
                                ->queryRow();

                        $newmodel1->item_name = $qryres3['tool_name'];
                        $newmodel1->unit = $qryres3['unit'];
                        $newmodel1->serial_no = $qryres3['serial_no'];
                        $newmodel1->req_date = date('Y-m-d');
                        $newmodel1->request_vendor_status = 0;
                        // $newmodel1->duration_in_days = $qryres3['tool_name;'];
                        $newmodel1->qty = $qryres3['qty'];
                        //$newmodel1->physical_condition = $qryres3['tool_name;'];
                        $lid = $reqto;
                        $qryres4 = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('tms_location_type u')
                                ->where('id=:id', array(':id' => $lid))
                                ->queryRow();

                        $newmodel1->item_status = $qryres4['location_type'];
                        $newmodel1->location = $reqto;
                        $newmodel1->save();
                         $item_id = Yii::app()->db->getLastInsertID();
                        $toolmodel = Tools::model()->findByPk($toolid);
                        $toolmodel->vendor_request_id = $request_id;
                        $toolmodel->save(false);
                        //$this->redirect(array('admin'));
                    }
                  //  }
                  }





                  $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$model->id));
            }
            }

            else{
                $model = $this->loadModel($transaction);


                /* $model->attributes = $_POST['ToolTransferRequest'];
                 $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
                if ($model->save()) {


                    $insert_id = $transaction;
              //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                    //print_r($_POST);die;
                    $unit = $_POST['unit'];
                    $code = $_POST['code'];
                    $type=$_POST['type'];
                    $newcode = $_POST['newcode'];
                    $item_name = $_POST['item_name'];
                    // $serial_no = $_POST['serial_no'];
                    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                    $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                    $quantity = $_POST['quantity'];
                    $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                    $newmodel = new ToolTransferItems;
                    $newmodel->tool_transfer_id = $insert_id;
                    $newmodel->req_date = date('Y-m-d');
                    $newmodel->unit = $unit;
                    $newmodel->type = $type;
                    $newmodel->tool_id = $newcode;
                    $newmodel->item_name = $item_name;
                    $newmodel->code = $code;
                    $newmodel->request_vendor_status = 0;
                    // $newmodel->serial_no = $serial_no[$i];
                    $newmodel->duration_in_days = isset($duration) ? $duration : '';
                    $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                    $newmodel->qty = $quantity;
                    $newmodel->physical_condition = $physical_condition;

if($newcode != NULL) {
$tools = Tools::model()->findByPk($newcode);

$location = LocationType::model()->findByPk($tools->location)->location_type;
$newmodel->item_status = $location;
//$newmodel->location = $tools->location;
              $newmodel->location = $reqto;
              $tools->location = $reqto;
$tools->save();
  // }
                    $newmodel->save();
        }

            }
             $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$model->id));
             */

             $tool_details = Tools::model()->findByPk($_POST['newcode']);
             if($tool_details->serialno_status == 'Y'){

           $model->attributes = $_POST['ToolTransferRequest'];
           $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
           $model->request_date = date('Y-m-d');
           // $model->location = $_POST['ToolTransferRequest']['request_from'];
           $reqto = $_POST['ToolTransferRequest']['request_to'];

           if (Yii::app()->user->role == 1) {
               $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
           } else {
               $model->request_owner_id = Yii::app()->user->id;
           }

               $qryres1 = Yii::app()->db->createCommand()
                       ->select('*')
                       ->from('tms_status')
                       ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                       ->queryRow();
     $model->request_status = $qryres1['sid'];
           $model->created_by = Yii::app()->user->id;
           $model->created_date = date('Y-m-d');
           $model->transfer_to = 'vendor';
           if ($model->save()) {


               $insert_id = Yii::app()->db->getLastInsertID();
                 //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                      // print_r($_POST);die;
                   $unit = $_POST['unit'];
                   $code = $_POST['code'];
                       $type=$_POST['type'];
                   $newcode = $_POST['newcode'];
                   $item_name = $_POST['item_name'];
                   // $serial_no = $_POST['serial_no'];
                   $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                   $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                   $quantity = $_POST['quantity'];
                   $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                   $newmodel = new ToolTransferItems;
                   $newmodel->tool_transfer_id = $insert_id;
                   $newmodel->req_date = date('Y-m-d');
                       $newmodel->unit = $unit;
                       $newmodel->type = $type;
                       $newmodel->tool_id = $newcode;
                       $newmodel->item_name = $item_name;
                       $newmodel->code = $code;
                   // $newmodel->serial_no = $serial_no[$i];
                       $newmodel->duration_in_days = isset($duration) ? $duration : '';
                       $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                       $newmodel->qty = $quantity;
                       $newmodel->physical_condition = $physical_condition;

                                                   if($newcode != NULL) {
                                                           $tools = Tools::model()->findByPk($newcode);

             $location = LocationType::model()->findByPk($tools->location)->location_type;
             $newmodel->item_status = $location;
             $newmodel->location = $reqto;

             // $tools->location = $reqfrom;
             $tools->location = $reqto;
             $tools->save();
                   $newmodel->save();
                   //print_r($newmodel->getErrors());
               }
//print_r($newmodel->getErrors());
//echo "<pre>";print_r($newmodel->attributes);exit;

                   // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                   $tbl = Yii::app()->db->tablePrefix;
                   $getdata = "SELECT * FROM {$tbl}users
         INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                   $users = Yii::app()->db->createCommand($getdata)->queryAll();

                   if ($users != NULL) {
                       $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                   }
                   if ($users != NULL) {
                       foreach ($users as $user) {
                           // require_once 'PHPMailer/class.phpmailer.php';
                           $mail = new JPhpMailer;
                           $newemail = $user->email;

                           $email = $newemail;
                           $subject = Yii::app()->name;
                           $headers = Yii::app()->name;

                           $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                           $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                           $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                           $mail->addAddress($email);   // Add a recipient
                           $mail->isHTML(true);

                           $mail->Subject = Yii::app()->name;
                           $mail->Body = $bodyContent;
                           $mail->Send();

                       }
                   }


                       $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$model->id));
                   //$this->redirect(array('/toolTransferRequest/site'));
           }

           }else{
               // with out serial no

                 // print_r($tool_details->attributes);


                 // existing tools edit
                 $qryres1 = Yii::app()->db->createCommand("SELECT SUM(tms_approve_itemqty.qty) as qty FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition = 1 OR tms_approve_itemqty.physical_condition = 2")->queryRow();
                 $damage =$qryres1['qty'];

                 $qryres2 = Yii::app()->db->createCommand("SELECT SUM(tms_approve_itemqty.qty) as qty FROM `tms_approve_itemqty` LEFT JOIN tms_tool_transfer_items ON tms_approve_itemqty.transfer_item_id=tms_tool_transfer_items.id LEFT JOIN tms_tools ON tms_tool_transfer_items.tool_id=tms_tools.id WHERE tms_tools.id=".$_POST['newcode']." AND tms_approve_itemqty.physical_condition=3")->queryRow();
                 $running =$qryres2['qty'];

                 if($running ==0 && $damage !=0){
                   // only location sqlite_changes

                   $model->attributes = $_POST['ToolTransferRequest'];
                   $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
                   $model->request_date = date('Y-m-d');
                   // $model->location = $_POST['ToolTransferRequest']['request_from'];
                   $reqto = $_POST['ToolTransferRequest']['request_to'];

                   if (Yii::app()->user->role == 1) {
                       $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
                   } else {
                       $model->request_owner_id = Yii::app()->user->id;
                   }

                       $qryres1 = Yii::app()->db->createCommand()
                               ->select('*')
                               ->from('tms_status')
                               ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                               ->queryRow();
             $model->request_status = $qryres1['sid'];
                   $model->created_by = Yii::app()->user->id;
                   $model->created_date = date('Y-m-d');
                   $model->transfer_to = 'vendor';
                   if ($model->save()) {


                       $insert_id = Yii::app()->db->getLastInsertID();
                         //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                              // print_r($_POST);die;
                           $unit = $_POST['unit'];
                           $code = $_POST['code'];
                               $type=$_POST['type'];
                           $newcode = $_POST['newcode'];
                           $item_name = $_POST['item_name'];
                           // $serial_no = $_POST['serial_no'];
                           $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                           $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                           $quantity = $_POST['quantity'];
                           $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                           $newmodel = new ToolTransferItems;
                           $newmodel->tool_transfer_id = $insert_id;
                           $newmodel->req_date = date('Y-m-d');
                               $newmodel->unit = $unit;
                               $newmodel->type = $type;
                               $newmodel->tool_id = $newcode;
                               $newmodel->item_name = $item_name;
                               $newmodel->code = $code;
                           // $newmodel->serial_no = $serial_no[$i];
                               $newmodel->duration_in_days = isset($duration) ? $duration : '';
                               $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                               $newmodel->qty = $quantity;
                               $newmodel->physical_condition = $physical_condition;

                                                           if($newcode != NULL) {
                                                                   $tools = Tools::model()->findByPk($newcode);

                     $location = LocationType::model()->findByPk($tools->location)->location_type;
                     $newmodel->item_status = $location;
                     $newmodel->location = $reqto;
                     //$newmodel->location = $location;
                     //$tools->qty = $running;
                     //$tools->stock_qty = $running;
                     // $tools->location = $reqfrom;
                     $tools->location = $reqto;
                     $tools->save();
                           $newmodel->save();
                           //print_r($newmodel->getErrors());
                       }
       //print_r($newmodel->getErrors());
       //echo "<pre>";print_r($newmodel->attributes);exit;

                           // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                           $tbl = Yii::app()->db->tablePrefix;
                           $getdata = "SELECT * FROM {$tbl}users
                 INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                           $users = Yii::app()->db->createCommand($getdata)->queryAll();

                           if ($users != NULL) {
                               $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                           }
                           if ($users != NULL) {
                               foreach ($users as $user) {
                                   // require_once 'PHPMailer/class.phpmailer.php';
                                   $mail = new JPhpMailer;
                                   $newemail = $user->email;

                                   $email = $newemail;
                                   $subject = Yii::app()->name;
                                   $headers = Yii::app()->name;

                                   $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                                   $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                                   $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                                   $mail->addAddress($email);   // Add a recipient
                                   $mail->isHTML(true);

                                   $mail->Subject = Yii::app()->name;
                                   $mail->Body = $bodyContent;
                                   $mail->Send();

                               }
                           }
                   }



                 }else{

                   //          print_r($_POST);die;


                   $model->attributes = $_POST['ToolTransferRequest'];
                   $model->service_report = isset($_POST['ToolTransferRequest']['service_report']) ? $_POST['ToolTransferRequest']['service_report'] : "";
                   $model->request_date = date('Y-m-d');
                   // $model->location = $_POST['ToolTransferRequest']['request_from'];
                   $reqto = $_POST['ToolTransferRequest']['request_to'];

                   if (Yii::app()->user->role == 1) {
                       $model->request_owner_id = $_POST['ToolTransferRequest']['request_owner_id'];
                   } else {
                       $model->request_owner_id = Yii::app()->user->id;
                   }

                       $qryres1 = Yii::app()->db->createCommand()
                               ->select('*')
                               ->from('tms_status')
                               ->where('status_type=:type and caption =:caption', array(':type' => 'vendor_status', ':caption' => 'Transfer to vendor'))
                               ->queryRow();
             $model->request_status = $qryres1['sid'];
                   $model->created_by = Yii::app()->user->id;
                   $model->created_date = date('Y-m-d');
                   $model->transfer_to = 'stock';
                   $model->request_status = 22;
                   if ($model->save()) {


                       $insert_id = Yii::app()->db->getLastInsertID();
                         //  for ($i = 0; $i < sizeof($sl_No); $i++) {
                              // print_r($_POST);die;
                           $unit = $_POST['unit'];
                           $code = $_POST['code'];
                               $type=$_POST['type'];
                           $newcode = $_POST['newcode'];
                           $item_name = $_POST['item_name'];
                           // $serial_no = $_POST['serial_no'];
                           $duration = isset($_POST['duration']) ? $_POST['duration'] : '';
                           $duration_type = isset($_POST['duration_type']) ? $_POST['duration_type'] : '';
                           $quantity = $_POST['quantity'];
                           $physical_condition = isset($_POST['physical_condition']) ? $_POST['physical_condition'] : NULL;
                           $newmodel = new ToolTransferItems;
                           $newmodel->tool_transfer_id = $insert_id;
                           $newmodel->req_date = date('Y-m-d');
                               $newmodel->unit = $unit;
                               $newmodel->type = $type;
                               $newmodel->tool_id = $newcode;
                               $newmodel->item_name = $item_name;
                               $newmodel->code = $code;
                           // $newmodel->serial_no = $serial_no[$i];
                               $newmodel->duration_in_days = isset($duration) ? $duration : '';
                               $newmodel->duration_type = isset($duration_type) ? $duration_type : '';
                               $newmodel->qty = $running;
                               $newmodel->physical_condition = $physical_condition;

                                                           if($newcode != NULL) {
                                                                   $tools = Tools::model()->findByPk($newcode);

                     $location = LocationType::model()->findByPk($tools->location)->location_type;
                     $newmodel->item_status = $location;
                     //$newmodel->location = $reqto;
                     $newmodel->location = $location;
                     $tools->qty = $running;
                     $tools->stock_qty = $running;
                     // $tools->location = $reqfrom;
                     //$tools->location = $reqto;
                     $tools->save();
                           $newmodel->save();
                           //print_r($newmodel->getErrors());
                       }
       //print_r($newmodel->getErrors());
       //echo "<pre>";print_r($newmodel->attributes);exit;

                           // $users = Users::model()->findAll(array('condition' => 'user_type = 9 OR user_type = 1'));

                           $tbl = Yii::app()->db->tablePrefix;
                           $getdata = "SELECT * FROM {$tbl}users
                 INNER JOIN {$tbl}mail_settings ON {$tbl}mail_settings.user_id = {$tbl}users.userid ";
                           $users = Yii::app()->db->createCommand($getdata)->queryAll();

                           if ($users != NULL) {
                               $users = Users::model()->findAll(array('condition' => 'user_type = 1'));
                           }
                           if ($users != NULL) {
                               foreach ($users as $user) {
                                   // require_once 'PHPMailer/class.phpmailer.php';
                                   $mail = new JPhpMailer;
                                   $newemail = $user->email;

                                   $email = $newemail;
                                   $subject = Yii::app()->name;
                                   $headers = Yii::app()->name;

                                   $bodyContent = "<p>Hello $user->first_name,</p><p>This is a notification to let you know that new Tool Transfer Request has been created.</p>";
                                   $bodyContent .= $this->renderPartial('_emailformsite', array('insert_id' => $insert_id), true);


                                   $mail->setFrom('info@bhiapp.com', Yii::app()->name);
                                   $mail->addAddress($email);   // Add a recipient
                                   $mail->isHTML(true);

                                   $mail->Subject = Yii::app()->name;
                                   $mail->Body = $bodyContent;
                                   $mail->Send();

                               }
                           }
                   }


                   // add new tool
                 //  if($damage !=0){
                   $tmodel = new Tools;
                   $model->attributes = $_POST['ToolTransferRequest'];
                   $tmodel->tool_code = $tool_details->tool_code;
                   $tmodel->tool_name = $tool_details->tool_name;
                   $tmodel->unit = $tool_details->unit;
                   $tmodel->make = $tool_details->make;
                   $tmodel->model_no = $tool_details->model_no;
                   $tmodel->serial_no = $tool_details->serial_no;
                   $tmodel->ref_no = $tool_details->ref_no;
                   $tmodel->batch_no = $tool_details->batch_no;
                   $tmodel->created_by = Yii::app()->user->id;
                   $tmodel->created_date = date('Y-m-d');
                   $tmodel->updated_by = Yii::app()->user->id;
                   $tmodel->updated_date = date('Y-m-d');
                   $tmodel->pending_status = $tool_details->pending_status;
                   $tmodel->location = $reqto;
                   $tmodel->qty = $damage;
                   $tmodel->stock_qty = $damage;
                   $tmodel->tool_condition = $tool_details->tool_condition;
                   $tmodel->active_status = $tool_details->active_status;
                   $tmodel->prev_main = "0";
                   $tmodel->prev_hrs  = NULL;
                   $tmodel->prev_date = NULL;
                   $tmodel->serialno_status = $tool_details->serialno_status;
                   $tmodel->warranty_date = $tool_details->warranty_date;
                   $tmodel->parent_id = $tool_details->id;
                   $tmodel->item_id = $tool_details->item_id;
                   $tmodel->tool_category = $tool_details->tool_category;
                   $toolcat = ToolCategory::model()->findByPk($tool_details->tool_category);
                   $qryres = Yii::app()->db->createCommand()
                           ->select('MAX(ref_value) AS maxval')
                           ->from('tms_tools u')
                           ->where('tool_category=:tool_category', array(':tool_category'=>$tool_details->tool_category))
                           ->queryRow();


                   if (!empty($qryres['maxval'])) {
                       $pr_id = sprintf("%03d", $qryres['maxval'] + 1);
                       $maxval = $pr_id;
                   } else {
                       $pr_id = sprintf("%03d", '001');
                       $maxval = $pr_id;
                   }
                   if ($tmodel->save(false)) {
                       $toolid = Yii::app()->db->getLastInsertID();
                       // Tools location
                       // $tool_item = new ToolsLocation;
                       // $tool_item->qty = $damage;
                       // $tool_item->tool_id = $model->id;
                       // $tool_item->location = $reqto;
                       // $tool_item->created_by = Yii::app()->user->id;
                       // $tool_item->created_date = date('Y-m-d');
                       // $tool_item->save(false);

                       // $item_model->remaining_qty = ($item_model->remaining_qty - $_POST['Tools']['qty']);
                       // $item_model->save();
                       // if ($item_model->remaining_qty == 0) {
                       //     $item_model->tool_status = Status::model()->find(array("condition" => "caption = 'Tool' AND status_type = 'pending_status'"))->sid;
                       //     $item_model->save();
                       // }
                       $newmodel = new ToolTransferRequest;
                       //$newmodel->tool_id = $id;
                       $qryres2 = Yii::app()->db->createCommand()
                               ->select('MAX(request_no) AS maxval')
                               ->from('tms_tool_transfer_request u')
                               //->where('id=:id', array(':id'=>$id))
                               ->queryRow();
                       if (empty($qryres2['maxval'])) {
                           $rno = 1;
                       } else {
                           $rno = $qryres2['maxval'] + 1;
                       }

                       $newmodel->request_no = $rno;
                       $newmodel->request_from = 1;
                       $newmodel->request_status = 10;
                       $newmodel->request_to = $reqto;
                       $newmodel->request_date = date('Y-m-d');
                       $newmodel->location = $reqto;
                       $newmodel->transfer_to = 'vendor';
                       $newmodel->created_by = Yii::app()->user->id;
                       $newmodel->created_date = date('Y-m-d');
                       $newmodel->updated_by = Yii::app()->user->id;
                       $newmodel->updated_date = date('Y-m-d');
                       $newmodel->save();
                       $insert_id = Yii::app()->db->getLastInsertID();
                       $request_id = $insert_id;
                       $newmodel1 = new ToolTransferItems;
                       $newmodel1->tool_transfer_id = $insert_id;
                       $newmodel1->tool_id = $toolid;
                       $newmodel1->code = $toolcat['cat_code'] .'-'. $maxval;
                       $qryres3 = Yii::app()->db->createCommand()
                               ->select('*')
                               ->from('tms_tools u')
                               ->where('id=:id', array(':id' => $toolid))
                               ->queryRow();

                       $newmodel1->item_name = $qryres3['tool_name'];
                       $newmodel1->unit = $qryres3['unit'];
                       $newmodel1->serial_no = $qryres3['serial_no'];
                       $newmodel1->req_date = date('Y-m-d');
                       $newmodel1->request_vendor_status = 0;
                       // $newmodel1->duration_in_days = $qryres3['tool_name;'];
                       $newmodel1->qty = $qryres3['qty'];
                       //$newmodel1->physical_condition = $qryres3['tool_name;'];
                       $lid = $reqto;
                       $qryres4 = Yii::app()->db->createCommand()
                               ->select('*')
                               ->from('tms_location_type u')
                               ->where('id=:id', array(':id' => $lid))
                               ->queryRow();

                       $newmodel1->item_status = $qryres4['location_type'];
                       $newmodel1->location = $reqto;
                       $newmodel1->save();
                        $item_id = Yii::app()->db->getLastInsertID();
                       $toolmodel = Tools::model()->findByPk($toolid);
                       $toolmodel->vendor_request_id = $request_id;
                       $toolmodel->save(false);
                       //$this->redirect(array('admin'));
                   }
                 //  }
                 }





                 $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$model->id));
           }



            }


        }

        $this->render('create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'status' => $status, 'duration_type' => $duration_type,'page' => $page
        ));
}
         public function actionUpdatevendor()
        {
          //$model = new ToolTransferRequest;
        $tran_id=$_GET['t_id'];
$page = 'vendor';
        $infoid=$_GET['id'];
         $model = $this->loadModel($tran_id);
         $newmodel= ToolTransferItems::model()->findBypk($infoid);

           $this->performAjaxValidation($model);
        $units = Unit::model()->findAll(array('condition' => 'active_status = "1"'));
        $duration_type = Status::model()->findAll(
                array(
                    'select' => array('sid,caption'),
                    'condition' => 'status_type = "duration_type"',
                    'distinct' => true
        ));
        $status = TmsStatus::model()->findAll(array('condition' => 'status_type = "physical_condition"'));

        if(isset($_POST['ToolTransferRequest'])){
          //   echo '<pre>';
       //print_r($_POST);

        $model->attributes = $_POST['ToolTransferRequest'];
        $model->request_owner_id=$_POST['ToolTransferRequest']['request_owner_id'];
         //print_r($model->attributes);die;
        $model->save();
        $type=$_POST['type'];
        $item_name=$_POST['item_name'];
        $unit=$_POST['unit'];
        $duration=$_POST['duration'];
        $duration_type=$_POST['duration_type'];
        $qty=$_POST['quantity'];
        $newmodel->type=$type;
        $newmodel->item_name=$item_name;
        $newmodel->unit=$unit;
        $newmodel->duration_in_days=$duration;
        $newmodel->duration_type=$duration_type;
        $newmodel->qty=$qty;
        $newmodel->save();


         $this->redirect(array('/toolTransferRequest/createvendor','tr_id'=>$tran_id));
        }


             $this->render('create', array(
            'model' => $model,'newmodel'=>$newmodel, 'units' => $units, 'status' => $status, 'duration_type' => $duration_type,'page' => $page
        ));

        }
public function actionDeletevendor($id){

            ToolTransferItems::model()->deleteByPk($id);
        echo true;
        }

  public function actiongetsiteById(){
    $id = $_GET['id'];
    $Locations =  Yii::app()->db->createCommand("SELECT id,name FROM `tms_location_assigned`
join tms_location_type on tms_location_type.id = `tms_location_assigned`.`site_id` WHERE active_status = '1' and location_type = 13 AND `user_id`= ". $id)->queryAll();
    $html = "<option value=''>Please choose</option>";
    if(!empty($Locations)){
    foreach($Locations as $key=> $value){
      $html .="<option value=".$value['id'].">".$value['name']."</option>";
      }
    }
      echo json_encode(array('html'=>$html));
  }
/*-----------------------------------*/

  /*public function actionUpdatesitelocation() {
      $tools = Tools::model()->findAll(array('condition' => 'serialno_status = "N"'));
      foreach($tools as $item) {
          $model = new ToolsLocation;
          $model->tool_id = $item->id;
          $model->location = $item->location;
          $model->qty = $item->qty;
          $model->created_by = Yii::app()->user->id;
          $model->created_date = date("Y-m-d");
          $model->save();
      }
      echo "Success";
  }*/



}