<?php

class ToolCategoryController extends Controller {

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
    public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules() {
		return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'categoryview','changestatus','gettooltype','categoryupdate','categoryshow','testtoolcat'),
                'users' => array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
            array('deny', // deny all users
                'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate() {
		$this->layout = false;
        $model = new ToolCategory;

		// Uncomment the following line if AJAX validation is needed
	     //$this->performAjaxValidation($model);

        if (isset($_POST['ToolCategory'])) {
            $model->attributes = $_POST['ToolCategory'];
			$model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->validate() && $model->save()) {
			$data = null;
			print_r(json_encode($data));
            } else {
			$error = $model->getErrors();
			print_r(json_encode($error));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate() {
		$id = $_POST['id'];
                
		
		//echo $value; die;
        $model = $this->loadModel($id);
        if (isset($_POST['value'])) {
                    $value = $_POST['value'];
                    $model->cat_name = $value;
                }
        if (isset($_POST['cat_code'])) {
                    $cat_code = $_POST['cat_code'];
                    $model->cat_code = $cat_code;
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        if ($model->save()) {
			$data = null;
			print_r(json_encode($data));
        } else {
			$error = $model->getErrors(); 
			print_r(json_encode($error));
		}
	}
        
            public function actionCategoryupdate() {
		$id = $_REQUEST['id'];                                                
                $parent = $_REQUEST['parent'];		
		//echo $value; die;               
                if(isset($id) && isset($parent)){
                $res = ToolCategory::model()->findByPk($parent);
                if(!isset($res)){
                $parent = NULL;    
                }
                $model = $this->loadModel($id);
                $model->parent_id = $parent;
                if ($model->save()) {
                echo "1";     
                }else{
                echo "2"    ;
                }
//                echo "<pre>";
//                print_r($model);
//                die;
                }else{
                echo "2";    
                }
            	}
        
           public function actionCategoryshow() {
            $id = $_REQUEST['id'];            	   
            if(!empty($id)){                                                               
            $catTree = ToolCategory::allcategories();           
            $categories_list = self::visualTree($catTree, 0);
            /* getting the all categories */               
                $model = new ToolCategory('search');
		$model->unsetAttributes();  // clear any default values                
                unset($categories_list[$id]);               
                array_push($categories_list, "ROOT CATEGORY");                
//                 echo "<pre>";
//                print_r($categories_list);
//                die;                 
           echo CHtml::activeDropDownList($model,'parent_id',$categories_list, array('empty' => 'Select category','class' => 'form-control','onchange'=>'cTrig(this.value,"'.$id.'");')).'<br><a class=\'cancel btn default btn-xs\'>Cancel</a>';
           }else{
           echo "1";    
           }
		

	}


    public function actionDelete($id) {
		
		$cat1 = Yii::app()->db->createCommand()
                    ->select(['tool_category'])
                    ->from('tms_tools')
                ->where('tool_category =' . $id)
                    ->queryAll();
       // print_r($cat1); die;
        if ($cat1 == NULL) {
			//echo "hi"; die;
			$this->loadModel($id)->delete();
            echo json_encode(array('response' => 'success'));
        } else {
			//echo "hello"; die;
            echo json_encode(array('response' => 'fail'));
 		}
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ToolCategory');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
    public function actionAdmin() {
        /* getting the all categories */
         $catTree = ToolCategory::allcategories();
         $categories_list = self::visualTree($catTree, 0);
         /* getting the all categories */   
            
        $model = new ToolCategory('search');
		$model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolCategory']))
            $model->attributes = $_GET['ToolCategory'];

        $this->render('admin', array(
            'model' => $model,
            'testmodel' => $categories_list,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
    public function loadModel($id) {
        $model = ToolCategory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tool-category-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
    /* added by minu */
        
    public function actionCategoryview() {
        
        $modelnew = new ToolCategory();
        $this->performAjaxValidation($modelnew);
        
        $catTree = ToolCategory::allcategories();
        $categories_list = self::visualTree($catTree, 0);

        if (isset($_POST['ToolCategory'])) {
        //print_r($_POST['ToolCategory']);die;
            
            if(!empty($_POST['ToolCategory']['parent_id'])){
            $pid = $_POST['ToolCategory']['parent_id'];
            $tooltype = Status::model()->find(array('condition'=>'caption ="Tool Set" and status_type ="tool_type"'))->sid;    
            $result = ToolCategory::model()->find(array('condition'=>'cat_id = '.$pid.' and tool_type = '.$tooltype.''));     
            if(!empty($result)){
            $_POST['ToolCategory']['tool_type'] = 1;     
            }
            }
            $modelnew->attributes = $_POST['ToolCategory'];
            if($_POST['ToolCategory']['tool_type'] == 1){
            $modelnew->tool_type = Status::model()->find(array('condition'=>'caption ="Tool Set" and status_type ="tool_type"'))->sid;
            }else{
                $modelnew->tool_type = Status::model()->find(array('condition'=>'caption ="Tool" and status_type ="tool_type"'))->sid;
            }
            $modelnew->created_by = yii::app()->user->id;
            $modelnew->created_date = date('Y-m-d');
        
            $modelnew->updated_by = yii::app()->user->id;
            $modelnew->updated_date = date('Y-m-d');
            if ($modelnew->save()) {
                 $this->redirect(array('categoryview'));
            } 
        }
        array_push($categories_list, "ROOT CATEGORY");
        
        $model = new ToolCategory('search');
		$model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolCategory']))
            $model->attributes = $_GET['ToolCategory'];
        $this->render('categoryview', array(
            'model' => $model, 'modelnew' => $modelnew, 'testmodel' => $categories_list,
		));
        }
        
        public function getChilds($id) {
        $row = '';
        $res = ToolCategory::model()->findAll( array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id = ' . $id,
                              'order'=>'cat_name'
                             ));
        if ($res != NULL) {
            foreach (ToolCategory::model()->findAll( array(
                              'select'=>'cat_id,cat_name',
                              'condition'=>'parent_id = ' . $id,
                              'order'=>'cat_name'
                             )) as $model) {
                            $row.=' <ul class="tree">';
                $row.= '<li class="tree-title" data-id="' . $model->cat_id . '">' . $model->cat_name . '</li>';
                            $row.= $this->getChilds($model->cat_id);
                            $row.=' </ul>';
                    }
                }
                return $row;
        }
        
        private static function visualTree($catTree, $level) {
            $res = array();
            foreach ($catTree as $item) {
                $res[$item['id']] = '' . str_pad('', $level * 2, '-') . ' ' . $item['text'];
                if (isset($item['children'])) {
                    $res_iter = self::visualTree($item['children'], $level + 1);
                    foreach ($res_iter as $key => $val) {
                        $res[$key] = $val;
                    }
                }
            }
            return $res;
        }
        
          public function actionChangestatus($id)
	{	
                ////////////////////////////////////////select the current active status               
                
                $active_status      = '';
                $new_status         = '';
                
                
                if($id != ''){
                $criteria           = new CDbCriteria;     
                $criteria->condition='cat_id=:Id';
                $criteria->params   = array(':Id'=>$id);
                $model              = ToolCategory::model()->find($criteria); 
                $active_status      = $model->active_status;
                if($active_status == 1){
                $new_status = 0;
                }else{
                $new_status = 1;
                }               
                $site = ToolCategory::model()->findByPk($id);
                
                if($site->parent_id != NULL){
                $parentres = ToolCategory::model()->findAll('cat_id = ' .$site->parent_id); 
            
                if($parentres[0]['active_status'] == 1){
                $site->active_status = $new_status; 
                $site->update();    
                
                $res = ToolCategory::model()->findAll('parent_id = ' . $id);
                if ($res != NULL) {
                $result = $this->getResults($id);     
                }else{
                $result = true;
                }
                }else{
                $result = false;    
                }
                }else{
                $site->active_status = $new_status; 
                $site->update();    
                
                $res = ToolCategory::model()->findAll('parent_id = ' . $id);
                if ($res != NULL) {
                $result = $this->getResults($id);     
                }else{
                $result = true;
                }    
                }
                
                
                    
                
                
               
                
                if($result)
		{ 			
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{			
 			echo json_encode(array('response'=> 'fail'));
 		}
                
                }
	
	}
         public function getResults($id) {
        $res = ToolCategory::model()->findAll('parent_id = ' . $id);
        if ($res != NULL) {
            foreach (ToolCategory::model()->findAll('parent_id = ' . $id) as $model) {    
              $active_status      = $model->active_status;
              if($active_status == 1){
                $new_status = 0;
                }else{
                $new_status = 1;
                }   
                $site = ToolCategory::model()->findByPk($model->cat_id);
                $site->active_status = $new_status; 
                $site->update();    
                $this->getResults($model->cat_id);                          
                }
                }
                return true;
        }
        public function actionGettooltype(){
            $catid = $_POST['catid'];
            $tooltype = ToolCategory::model()->findByPk($catid)->tool_type;
            $sid = Status::model()->find(array('condition'=>'caption ="Tool" and status_type ="tool_type"'))->sid;
            if($tooltype == $sid){
                $type = 0;
            } else {
                $type = 1;
            }
            echo $type;
        }
//        public function actionTesttoolcat(){
//
//
//for($i=5;$i<1000;$i++){
//$cname = 'test'.$i;
//$ccode = 'TC'.$i;
//$sql    = "INSERT INTO `tms_tool_category` (`cat_id`, `cat_name`, `cat_code`, `created_by`, `created_date`, `updated_by`, `updated_date`, `parent_id`, `active_status`, `tool_type`) VALUES (NULL, '$cname', '$ccode', '6', '2017-07-12', '6', '2017-07-12', NULL, '1', '18');";
//Yii::app()->db->createCommand($sql)->execute();
//
//}
//
//
//        }
        
 }
