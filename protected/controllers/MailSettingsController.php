<?php

class MailSettingsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getusers','savepermissions'),
				 'users' => array('@'),
                                'expression' => 'yii::app()->user->role<=1',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				 'users' => array('@'),
                                 'expression' => 'yii::app()->user->role<=1',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				 'users' => array('@'),
                                'expression' => 'yii::app()->user->role<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MailSettings;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$tbl = Yii::app()->db->tablePrefix;
		$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
				. "`{$tbl}user_roles`.`role` "
				. "FROM `{$tbl}users` `t` JOIN {$tbl}user_roles ON {$tbl}user_roles.id= t.user_type "
				. "WHERE status=0   ORDER BY user_type,full_name ASC";
				//echo $sql;
		$users = Yii::app()->db->createCommand($sql)->queryAll();
		if(isset($_POST['MailSettings']))
		{
			$model->attributes=$_POST['MailSettings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,'users'=>$users,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MailSettings']))
		{
			$model->attributes=$_POST['MailSettings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MailSettings');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MailSettings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MailSettings']))
			$model->attributes=$_GET['MailSettings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MailSettings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mail-settings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionGetusers() {
        $tbl = Yii::app()->db->tablePrefix;

        if (isset($_REQUEST['type'])) {
            if ($_REQUEST['type'] == "individual") {


				$sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
				. "`{$tbl}user_roles`.`role` "
				. "FROM `{$tbl}users` `t` JOIN {$tbl}user_roles ON {$tbl}user_roles.id= t.user_type "
				. "WHERE status=0  ORDER BY user_type,full_name ASC";
				//echo $sql;
				$users = Yii::app()->db->createCommand($sql)->queryAll();

                echo "<ul class='users-listview' style='list-style:none'>";

                foreach ($users as $user) {
					$userid = $user['userid'];
					$newqry = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = $userid AND status = 1")->queryRow();
					if($newqry != NULL){
                    echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " checked >" . $user['full_name'] . "</li>";
					}else{

					 echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><input type='checkbox' value=" . $user['userid'] . " >" . $user['full_name'] . "</li>";
					}
					}

                echo "</ul>";
                //print_r($users);
            } else if ($_REQUEST['type'] == "profile") {

                $sql = "SELECT * FROM {$tbl}user_roles
                INNER JOIN {$tbl}users ON {$tbl}users.user_type = {$tbl}user_roles.id
                WHERE {$tbl}users.status = 0 GROUP by {$tbl}user_roles.id";

                $profiles = Yii::app()->db->createCommand($sql)->queryAll();

                echo "<ul class='users-listview' style='list-style:none'>";

                foreach ($profiles as $profile) {
					$profileid = $profile['id'];

					$count1 = Yii::app()->db->createCommand("SELECT count(*) FROM {$tbl}mail_settings WHERE  role_id = $profileid
					AND status=1")->queryScalar();

					$count2 = Yii::app()->db->createCommand("SELECT count(*) FROM {$tbl}users WHERE  user_type = $profileid
					AND status=0 ")->queryScalar();

					if($count1 == $count2 && $count1 != 0 && $count2 != 0){
					echo "<li class='profileid' name='userid' data-id=" . $profile['id'] . "><input type='checkbox' value=" . $profile['id'] . " checked>" . $profile['role'] . "</li>";
					}else{
					echo "<li class='profileid' name='userid' data-id=" . $profile['id'] . "><input type='checkbox' value=" . $profile['id'] . " >" . $profile['role'] . "</li>";
					}
				}

                echo "</ul>";
            } else {

            }
        }
    }

     public function actionSavepermissions() {

        $tbl = Yii::app()->db->tablePrefix;

        //with userid
        if (isset($_POST['userids'])){

            $userids = $_POST['userids'];
            if (!empty($userids)) {

				Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();
				foreach($userids as $userid)
				{
					//get existing permissions
					$existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = '$userid'")->queryAll();
					if($existquery == NULL)
					{
						$model=new MailSettings;
						$model->user_id = $userid;
						$sql = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE userid = '$userid'")->queryRow();
						$model->role_id = $sql['user_type'];
						$model->status = 1;
						$model->save();
					}else{
						//die('success');
						$model =  MailSettings::model()->find(array("condition" => "user_id = '$userid'"));
						$model->user_id = $userid;
						$sql = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE userid = '$userid'")->queryRow();
						$model->role_id = $sql['user_type'];
						$model->status = 1;
						$model->save();


					}

				}


        }
        }
		 //with profile id
        if (isset($_POST['profileids'])){

			Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();
            $profileids = $_POST['profileids'];
            if (!empty($profileids)) {

				$getallusers = Yii::app()->db->createCommand("SELECT role_id FROM {$tbl}mail_settings ")->queryAll();
				//print_r($profileids);
				//print_r($getallusers); die;

				foreach($profileids as $profileid)
				{

					$getusers = Yii::app()->db->createCommand("SELECT * FROM {$tbl}users WHERE user_type = $profileid AND status=0 ")->queryAll();
					//print_r($getusers); die;
					foreach($getusers as $user1)
					{
						$newuserid = $user1['userid'];
						$existquery = Yii::app()->db->createCommand("SELECT * FROM {$tbl}mail_settings WHERE user_id = $newuserid AND role_id = $profileid")->queryAll();
						if($existquery == NULL)
						{
							$model=new MailSettings;
							$model->user_id = $newuserid;
							$model->role_id = $profileid;
							$model->status = 1;
							$model->save();
						}else{
							//die('success');
							$model =  MailSettings::model()->find(array("condition" => "user_id = $newuserid AND role_id = $profileid "));
							$model->user_id = $newuserid;
							$model->role_id = $profileid;
							$model->status = 1;
							$model->save();


						}

					}
				}

			}
        }

        if(empty($_POST['userids']) && empty($_POST['profileids']))
        {
			Yii::app()->db->createCommand("UPDATE  {$tbl}mail_settings SET status = 0")->execute();

		}






    }

}
