<?php

class ProjectsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
		'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin','addproject'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
/*
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
  */
 public function actionView($id) {
        $pid = $id;
 
        $sql = 'SELECT * from  pms_payment_report
WHERE  pjctid=' . $pid;
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        $projectmodel = $command->queryAll();
        $this->render('view', array(
            'model' => $this->loadModel($id), 'pid' => $pid, 'getdata' => $projectmodel
        ));
    }  
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Projects;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projects'])) {

            $model->attributes = $_POST['Projects'];

            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projects'])) {
            $model->attributes = $_POST['Projects'];

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save())
                 Yii::app()->user->setFlash('success', 'Successfully updated.');
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Projects('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Projects('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Projects']))
            $model->attributes = $_GET['Projects'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }
    
    public function actionaddproject()
    {
        $model = new Projects;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projects'])) {
            $model->attributes = $_POST['Projects'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
			
            if ($model->save())
			{
				 
            //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("parent.location.reload();window.parent.$('#cru-dialog').dialog('close');window.parent.$('#cru-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                } else {
                    //----- end new code --------------------
                    $this->redirect(array('index', 'id' => $model->pid));
                }
        }
        }

        //----- begin new code --------------------
        if (!empty($_GET['asDialog'])){
         
            $this->layout = '//layouts/iframe';
        }
        //----- end new code --------------------

        $this->render('create', array(
            'model' => $model,
        ));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Projects::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'projects-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
