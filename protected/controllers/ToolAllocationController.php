<?php
class ToolAllocationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin','checkvalidation','deleteitem'),
				'users'=>array('@'),
				//'expression' => 'yii::app()->user->role<=1',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		$this->layout = false;
		$model=new ToolAllocation;
		
		$units= Unit::model()->findAll();
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['ToolAllocation']))
		{ 
		    $code = $_POST['code'];
		    $name = $_POST['name'];
		    $unit = $_POST['unit'];
		    $no   = $_POST['no'];
		    $duration     = $_POST['duration'];
		    $quantity     = $_POST['quantity'];
		    $fee     = $_POST['fee'];
		   
			
			$model->attributes=$_POST['ToolAllocation'];
			$model->created_by 		= Yii::app()->user->id;
			$model->created_date 	= date('Y-m-d');
			$model->updated_by 		= Yii::app()->user->id;
			$model->updated_date 	= date('Y-m-d');
			
			if ($model->save()) {


               $insert_id = Yii::app()->db->getLastInsertID();
               for ($i = 0; $i < sizeof($code); $i++) {
                       
                       $newmodel = new ToolItems;
                       $newmodel->tool_allocated_id = $insert_id;
                       $newmodel->code = $code[$i];
                       $newmodel->item_name = $name[$i];
                       $newmodel->unit  = $unit[$i];
                       $newmodel->serial_no = $no[$i];
                       $newmodel->duration_in_days = $duration[$i];
                       $newmodel->qty = $quantity[$i];
                       $newmodel->late_return_fee = $fee[$i];
                       $newmodel->save();
                       
                       
               }
               $this->redirect(array('admin'));
              
               
           }
			
		}

		$this->render('create',array(
			'model'=>$model,'units'=>$units,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = false;
		$model=$this->loadModel($id);
		$newmodel = ToolItems::model()->findAll(array("condition" => "tool_allocated_id = '$id'"));
		
		
		$units= Unit::model()->findAll();
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['ToolAllocation']))
		{	
			$ids = $_POST['ids'];
			$code = $_POST['code'];
		    $name = $_POST['name'];
		    $unit = $_POST['unit'];
		    $no   = $_POST['no'];
		    $duration     = $_POST['duration'];
		    $quantity     = $_POST['quantity'];
		    $fee     = $_POST['fee'];
		    
			$model->attributes=$_POST['ToolAllocation'];
			$model->updated_by 		= Yii::app()->user->id;
			$model->updated_date 	= date('Y-m-d');
			if ($model->save()) {
				 foreach($ids as $i=>$idnew){
					 
						
                        if($idnew!=NULL)
                        {
							
								//$newmodel = ToolItems::model()->findAll(array("condition" => "tool_allocated_id = '$id'"));
								$newmodel = ToolItems::model()->findByPk($idnew);
								$newmodel->tool_allocated_id = $id;
							    $newmodel->code = $code[$i];
							    $newmodel->item_name = $name[$i];
							    $newmodel->unit  = $unit[$i];
							    $newmodel->serial_no = $no[$i];
							    $newmodel->duration_in_days = $duration[$i];
							    $newmodel->qty = $quantity[$i];
							    $newmodel->late_return_fee = $fee[$i];
							    $newmodel->save();	

						}
						else
						{	
							
							   $newmodel = new ToolItems;
							   $newmodel->tool_allocated_id = $id;
							   $newmodel->code = $code[$i];
							   $newmodel->item_name = $name[$i];
							   $newmodel->unit  = $unit[$i];
							   $newmodel->serial_no = $no[$i];
							   $newmodel->duration_in_days = $duration[$i];
							   $newmodel->qty = $quantity[$i];
							   $newmodel->late_return_fee = $fee[$i];
							   $newmodel->save();
					
							
						}
						
                   
					}

              
				$this->redirect(array('admin'));
              
               
           }
		}

		$this->render('update',array(
			'model'=>$model,'newmodel'=>$newmodel,'units'=>$units,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ToolAllocation');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ToolAllocation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ToolAllocation']))
			$model->attributes=$_GET['ToolAllocation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ToolAllocation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tool-allocation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	 public function actionCheckvalidation() {
		$tblpx = Yii::app()->db->tablePrefix;
        $allocation_no = $_POST['allocation_no'];
        $id = $_POST['val'];
        
        if($id == 0){
			$row = ToolAllocation::model()->findAll(array('condition' => "allocation_no ='".$allocation_no."'"));
			$count = count($row);
			if ($count > 0) {
				echo "false";
			} else {
				echo "true";
			}
		}else{
					
			$row = Yii::app()->db->createCommand("select * from {$tblpx}tool_allocation where `allocation_no`='$allocation_no' AND `id`!='$id' ")->queryAll();
			$count = count($row);
			if ($count > 0) {
				echo "false";
			} else {
				echo "true";
			}
		}
       
    }
    public function actionDeleteItem($id) {
		 ToolItems::model()->deleteByPk($id);
		 echo true;
		 
	}
    
}


