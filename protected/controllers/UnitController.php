<?php

class UnitController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','changestatus'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{       
		$this->layout = false;
		$model=new Unit;
		 
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];
                        
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            
			 if($model->validate() && $model->save())
			{
			$data = null;
			print_r(json_encode($data));
			}
			else
			{
			$error = $model->getErrors();
			print_r(json_encode($error['unitname']));
			}
		}

		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		
		$id = $_POST['id'];
		$value = $_POST['value'];
		//echo $value; die;
		$model=$this->loadModel($id);
		$model->unitname = $value;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->save())
		{
			$data = null;
			print_r(json_encode($data));
		}else
		{
			$error = $model->getErrors(); 
			print_r(json_encode($error['unitname']));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	 
	
	public function actionDelete($id)
	{
		
		$unit1 = Yii::app()->db->createCommand()
                    ->select(['unit'])
                    ->from('tms_tools')
                    ->where('unit ='.$id)
                    ->queryAll();
        $unit2 = Yii::app()->db->createCommand()
                    ->select(['unit'])
                    ->from('tms_purchase_items')
                    ->where('unit ='.$id)
                    ->queryAll();
        $unit3 = Yii::app()->db->createCommand()
                    ->select(['unit'])
                    ->from('tms_tool_items')
                    ->where('unit ='.$id)
                    ->queryAll();
        $unit4 = Yii::app()->db->createCommand()
                    ->select(['unit'])
                    ->from('tms_tool_transfer_items')
                    ->where('unit ='.$id)
                    ->queryAll();
        
		if($unit1 == NULL && $unit2 == NULL && $unit3 == NULL && $unit4 == NULL)
		{  
			//echo "hi"; die;
			$this->loadModel($id)->delete();
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{	
			//echo "hello"; die;
 			echo json_encode(array('response'=> 'fail'));
 		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Unit');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Unit('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Unit']))
			$model->attributes=$_GET['Unit'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Unit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='unit-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionChangestatus($id)
	{	
               //select the current active status               
                
                $active_status      = '';
                $new_status         = '';
                $criteria           = new CDbCriteria;     
                $criteria->condition='id=:Id';
                $criteria->params   = array(':Id'=>$id);
                $model              = Unit::model()->find($criteria); 
                $active_status      = $model->active_status;
                if($active_status == 1)
                {
                    $new_status = 0;
                }
                else
                {
                    $new_status = 1;
                }               
               
                $site = Unit::model()->findByPk($id);
                $site->active_status = $new_status;               		
                
		if($site->update())
		{ 			
			echo json_encode(array('response'=> 'success'));
 		}
		else
		{			
 			echo json_encode(array('response'=> 'fail'));
 		}
		
	}
}
