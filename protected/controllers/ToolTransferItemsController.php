<?php

class ToolTransferItemsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'appovestatus','acknowldgestatus','rejectstatus'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=1',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ToolTransferItems;

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['ToolTransferItems'])) {
            $model->attributes = $_POST['ToolTransferItems'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model);

        if (isset($_POST['ToolTransferItems'])) {
            $model->attributes = $_POST['ToolTransferItems'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ToolTransferItems');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ToolTransferItems('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ToolTransferItems']))
            $model->attributes = $_GET['ToolTransferItems'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ToolTransferItems::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tool-transfer-items-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAppovestatus() {
        $checked_items = isset($_POST['ids']) ? $_POST['ids'] : 0;
        $unchecked_items = isset($_POST['unchecked']) ? $_POST['unchecked'] : 0;
        $transferid = $_POST['transferid'];
        $reqdetails = ToolTransferRequest::model()->findByPk($transferid);
        $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
        $ack_user = $_POST['ack_user'];
        if($checked_items != 0){
        foreach ($checked_items as $items) {
            $model = $this->loadModel($items);           
            $reqto = $reqdetails->request_to;
            $reqdetails->location = $reqto;
            $reqdetails->save();
            $location = LocationType::model()->findByPk($reqto)->location_type;
            $model->item_status = $location;
            $model->location = $reqto;
            $toolid = ToolTransferItems::model()->findByPk($items)->tool_id;
            $tools = Tools::model()->findByPk($toolid);
            $tools->location = $reqto;
            $tools->save();
            $model->save();
        }
        }
        if($unchecked_items != 0){
        foreach ($unchecked_items as $items) {
            $model = $this->loadModel($items);
            $reqfrom = $reqdetails->request_from;
            $location = LocationType::model()->findByPk($reqfrom)->location_type;
            $model->item_status = $location;
            $model->location = $reqfrom;
            $model->save();
        }
        }
        $newmodel = ToolTransferRequest::model()->findByPk($transferid);
        $newmodel->staff_remarks = $remarks;
        $newmodel->ack_user_id = $ack_user;
        //$newmodel->request_status = 7;
        
          $qryres1 = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tms_status')
                        ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Approved'))           
                        ->queryRow();
         $newmodel->request_status = $qryres1['sid'];
         $newmodel->transfer_date = date('Y-m-d');
         
        $newmodel->save();
        $mail = new JPhpMailer;
        $req_owner = ToolTransferRequest::model()->findByPk($transferid);
        //$newemail = Users::model()->find(array('condition'=>'userid='.$ack_user))->email;         
        //$email = $newemail;
        $subject = Yii::app()->name;
        $headers = Yii::app()->name;

       $bodyContent = "<p>Hello,</p><p>This is a notification to let you know that  Tool Transfer Request has been verified and approved.</p>"; 
       $bodyContent .= $this->renderPartial('//toolTransferRequest/_emailform', array('insert_id' => $transferid), true);


        /*
        $mail->isSMTP();                                   // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                            // Enable SMTP authentication
        $mail->Username = 'testing@bluehorizoninfotech.com';                 // SMTP username
        $mail->Password = 'Test2paSS()';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;
        $mail->setFrom('testing@bluehorizoninfotech.com', Yii::app()->name);
        */
       
        $mail->setFrom('info@bhiapp.com', Yii::app()->name);
        if($req_owner->request_owner_id == $req_owner->created_by){
            $newemail = Users::model()->find(array('condition'=>'userid='.$req_owner->request_owner_id))->email;
           $mail->addAddress($newemail);
        }else{
            $newemail1 = Users::model()->find(array('condition'=>'userid='.$req_owner->request_owner_id))->email;
            $newemail2 = Users::model()->find(array('condition'=>'userid='.$req_owner->created_by))->email;
             $mail->addAddress($newemail1);
             $mail->addAddress($newemail2);
        }
        //$mail->addAddress($email);   // Add a recipient
        $mail->isHTML(true);

        $mail->Subject = Yii::app()->name;

        $mail->Body = $bodyContent;
        $mail->Send();
    }
    
    
    public  function actionAcknowldgestatus(){
        if(isset($_POST['transferid']) && isset($_POST['ack_user'])){
			
            $transferid = $_POST['transferid'];
            $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
            $ack_user = $_POST['ack_user'];
            $newmodel = ToolTransferRequest::model()->findByPk($transferid);
            $newmodel->staff_remarks = $remarks;
            $newmodel->ack_user_id = $ack_user;
            //$newmodel->request_status = 7;

              $qryres1 = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('tms_status')
                            ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Acknowledged'))           
                            ->queryRow();
             $newmodel->request_status = $qryres1['sid'];
		
            if($newmodel->save()){
              echo true;   
            }
        }
        
    }
    
    public function actionRejectstatus(){
        if(isset($_POST['transferid']) && isset($_POST['ack_user'])){
            $transferid = $_POST['transferid'];
            $remarks = isset($_POST['remarks']) ? $_POST['remarks'] : "";
            $ack_user = $_POST['ack_user'];
            $newmodel = ToolTransferRequest::model()->findByPk($transferid);
            $newmodel->staff_remarks = $remarks;
            $newmodel->ack_user_id = $ack_user;
            //$newmodel->request_status = 7;

              $qryres1 = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('tms_status')
                            ->where('status_type=:type and caption =:caption', array(':type'=>'site_transfer',':caption'=>'Rejected'))           
                            ->queryRow();
             $newmodel->request_status = $qryres1['sid'];

            if($newmodel->save()){
              echo true;   
            }
        }
    }
    

}
