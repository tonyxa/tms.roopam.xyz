//
var global_downlist = 0;
var list_count_start = 0;
//
var ended_always_cnt = 0;
var last_asdn_positn = 0;
var global_pass_aftc = 0;
var list = new Array();
var liststr=0;
if($('#manage-tags').val()){
    liststr= $('#manage-tags').val();
    list= liststr.split(',');
}
//
list_count_start = parseInt(list_count_start);
ended_always_cnt = parseInt(ended_always_cnt);
last_asdn_positn = parseInt(last_asdn_positn);
global_pass_aftc = parseInt(global_pass_aftc);
//
$('.render_autocomplete').hide();
//
$(document).on('input', '#searchinput', function (e) {
//$('#searchinput').bind('input', function () {
    //
    var whattyped = $(this).val();
    //
    if (whattyped.length > 0) {
        //
        getthelist(whattyped);
        //
    } else {
        $('.render_autocomplete').hide();
    }
    //
});
$(document).on('click', '#searchinput', function (e) {
//$('#searchinput').bind('click', function () {
    //
    whattyped='';
    getthelist(whattyped);
});




//
$(document).on('click', '.close_auto_cmplt', function (e) {
//$('.close_auto_cmplt').click(function () {
    /* body... */
	e.stopPropagation();
    $('.render_autocomplete').hide();
    //
	//updated by arun on 14-2-17
	$('#searchinput').val('');
	//
});
//
$(document).on('click', '.repeater_down', function (e) {
    
   
     $('.errormsg').hide();
    //
    $('.auto_dn_sl_' + last_asdn_positn).removeClass('hover_assign_auto_rpt');
    var clicked = $(this).data('sl');
    text_box_alter(clicked);
    
    var whatclicked = $('.auto_dn_sl_' + clicked).text();
    
    var dataid = $('.auto_dn_sl_' + clicked).attr('data-id');
    
    var json = JSON.stringify(list);
    
    if(jQuery.inArray(parseInt(dataid),list) == -1){
         list.push(parseInt(dataid));
        $('.attach_pdt_here').append('<span class=\"tagbox_small\" data-id="' + dataid + '">' + whatclicked + '<span class=\"glyphicon glyphicon-remove removetag attach_rem\" data-id="' + dataid + '"></span></span>');
        $('#manage-tags').val(list);
    }else{
        
    }

    
    
    
    $('.render_autocomplete').hide();
    document.getElementById('searchinput').value = '';
    //
});

$(document).on('click','.removetag',function(){
    var tagid= $(this).attr('data-id');
    var removeItem = tagid;
    
    //remove closed value
    list = jQuery.grep(list, function(value) {
      return value != removeItem;
    });
    
    $(this).parent().hide();
    $('#manage-tags').val(list);
    $('.render_autocomplete').hide();
    
});



//$(document).on('keypress', '.repeater_down', function(e) {
////$('.repeater_down').keypress(function (e) {
// var key = e.keyCode;
// if(key == 13)  // the enter key code
//  {
//    var clicked = $(this).data('sl');
//    text_box_alter(clicked);  
//  }
//});


//
function getthelist(whattyped) {
    // body...
    $('.render_autocomplete').show();
    $('.wrap_here').append('<div class="spinner_here" style="text-align: center;margin-top: 10px;"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></div>');
    // 
    
    
    $.ajax({
        url: '/admin/design/autocompletetag',
        method: 'POST',
        dataType: 'JSON',
        data: {search: whattyped,list:list},
    }).success(function (data) {
        /* body... */
        //
        appendthedata(data);
        //
    });
    // 
}
//
function appendthedata(data) {
    // body... 
    $('.wrap_here').html('');
    //
    if (data.tags.length > 0) {
        //
        list_count_start = 0;
        ended_always_cnt = 0;
        global_pass_aftc = 0;
        //

        //
        //$('.wrap_here').append('<div class="head_repeat_cls">Tags</div>');
        //
        for (var i = 0; i < data.tags.length; i++) {
            //
            list_count_start++;
            //
            $('.wrap_here').append('<div class="repeater_down auto_dn_sl_' + list_count_start + '" data-id="' + data.tags[i].id + '"  data-sl="' + list_count_start + '">' + data.tags[i].tag_name + '</div>');
            // 	
        }
        //


        ended_always_cnt = list_count_start;
        global_downlist = 1;
        //
    } else {
        //
        $('.render_autocomplete').hide();
        //$('.wrap_here').append('<div class="repeater_down_no_data">No records find!</div>');
        //
        global_downlist = 0;
        //
    }
    //
}
//
function myFunction(event) {
    //
    var keycode = event.keyCode;
    //
	
    if (global_downlist == 1) {
        //
        if (keycode == 40) { //Down key pressed refer key codes
            //
            calculate_the_position(0);
            //	
        } else if (keycode == 38) { //Up key is pressed
            //
            calculate_the_position(1);
            //	
        } else if (keycode == 37) { //Left key is pressed
            //
            calculate_the_position(2);
            //	
        } else if (keycode == 39) { //Right key is pressed
            //
            calculate_the_position(3);
            //	
        }
    }
    //
}
//
function calculate_the_position(calculate) {
    //
	
    if (calculate == 0) {
        //
        global_pass_aftc++;
        //
    } else if (calculate == 1) {
        //
        global_pass_aftc--;
        //
    } else if (calculate == 2) {
        //
        global_pass_aftc = 1;
        //
    } else if (calculate == 3) {
        //
        global_pass_aftc = list_count_start;
        //	
    }
    //
    if (global_pass_aftc <= 0) {
        //
        global_pass_aftc = 1;
        //
    }
    //
    if (global_pass_aftc > ended_always_cnt) {
        //
        global_pass_aftc = ended_always_cnt;
        //
    }
    //
    applay_position(global_pass_aftc);
    //
}
//
function applay_position(position_in) {
    //
    if (last_asdn_positn > 0 && last_asdn_positn <= list_count_start) {
        //
        $('.auto_dn_sl_' + last_asdn_positn).removeClass('hover_assign_auto_rpt');
        //
    }
    //
    $('.auto_dn_sl_' + position_in).addClass('hover_assign_auto_rpt');
    last_asdn_positn = position_in;
    //
    text_box_alter(position_in);
    //
}
//
function text_box_alter(position_in) {
    //
    var whatclicked = $('.auto_dn_sl_' + position_in).text();
    //
    document.getElementById('searchinput').value = whatclicked;
    //
    $('#searchinput').trigger("change");



    //
}


