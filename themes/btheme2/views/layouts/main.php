<?php
$tblpx = yii::app()->db->tablePrefix;
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
   'jquery.js'=>false,
   'jquery.min.js'=>false, 
);
$cs->coreScriptPosition = CClientScript::POS_END;
//Yii::app()->clientScript->coreScriptPosition=CClientScript::POS_END;

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo CHtml::encode(Yii::app()->name); ?> | Tool Management System (TMS)</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout2.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/form.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/screen.css" rel="stylesheet" type="text/css">
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/tmsstyle.css" rel="stylesheet" type="text/css">
        <!-- END THEME STYLES -->
        <link href="<?php echo Yii::app()->theme->baseUrl;?>/assets/admin/layout3/img/favicon.png" rel="shortcut icon" type="image/x-icon"/>
        
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> 
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        
    </head>

    <body>
        <!-- BEGIN HEADER -->
        <div class="page-wrapper">
        <div class="page-header" >
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top"   >
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span> 
                        </button>
                    <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a href="/"><img src="<?php echo Yii::app()->theme->baseUrl;?>/assets/admin/layout3/img/roopam_logo_transparent.png" border="0" height= "48" alt="logo" class="logo-default1"></a>
                        </div>
                    </div>
                     
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
<!--                    <a href="javascript:;" class="menu-toggler"></a>-->
                    <div class=" collapse navbar-collapse" id="myNavbar">
                        <div class="nav navbar-nav navbar-right">
                            <ul class="nav navbar-nav">
                                <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                                        <span class="fa fa-user user-account"></span>
                                        <!-- <img style="height:28px; vertical-align:middle;margin-top:-3px;" alt="" class="img-circle" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/avatar.jpg"> -->
<!--                                        <span class="username username-hide-mobile"><?php // echo ucfirst(Yii::app()->user->name); ?></span> -->
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <div class="user-name"><?php echo ucfirst(Yii::app()->user->name); ?></div>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;My Profile', array('Users/myprofile'), array('class' => "icon-user")); ?>
                                        </li>
                                        <li>
                                            <?php echo CHtml::link('&nbsp;&nbsp;&nbsp;Logout', array('site/logout'), array('class' => "icon-key")); ?>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <?php
                            $this->widget('zii.widgets.CMenu', array(
                                'activeCssClass' => 'active',
                                'encodeLabel' => false,
                                'activateParents' => true,
                                'items' => array(
                                    array('label' => 'Home', 'url' => array('/site/index')),
                                    array('label' => 'Settings<span class="caretiden"></span>', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'),'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1)), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                        'items' => array(
                                            array('label' => 'User Roles', 'url' => array('/UserRoles/index'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Users', 'url' => array('/Users/index'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Work Site', 'url' => array('/workSite/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Unit', 'url' => array('/unit/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Tools', 'url' => array('/tools/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Tool Set', 'url' => array('/tools/toolsetadmin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Pending Tools', 'url' => array('/tools/pending'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Tool Categories', 'url' => array('/toolCategory/categoryview'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Vendor', 'url' => array('/locationType/vendor'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Mail Settings', 'url' => array('/mailSettings/create'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),

                                        )
                                    ),
                                    array('label' => 'Transactions<span class="caretiden"></span>', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'),'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10)), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                        'items' => array(
                                            array('label' => 'Tool Indent', 'url' => array('/toolTransferRequest/site'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10))),
                                            array('label' => 'Tool Return', 'url' => array('/toolTransferRequest&page=stock'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10 ))),
                                            array('label' => 'Tool Transfer Requests (Vendor)', 'url' => array('/toolTransferRequest&page=vendor'), 'visible' => (isset(Yii::app()->user->role) && ((Yii::app()->user->role == 1)||(Yii::app()->user->role == 9)))),

                                            array('label' => 'Change Supervisor', 'url' => array('/toolTransferRequest/changesupervisor'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 ) )),


                                            array('label' => 'Purchase', 'url' => array('/purchase/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                                            array('label' => 'Missing Items', 'url' => array('/toolTransferRequest/MissingItems'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 ||Yii::app()->user->role == 9|| Yii::app()->user->role == 10))),


                                        )
                                    ),
                                        array('label' => 'Reports<span class="caretiden"></span>', 'url' => '#','linkOptions' => array('class' => 'dropdown-toggle caretiden', 'data-toggle' => 'dropdown'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10)), 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                                            'items' => array(
                                                array('label' => 'Location of tools', 'url' => array('/tools/toolreport'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10))),
                                                array('label' => 'Tools Life History', 'url' => array('/tools/toolhistory'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10))),
                                                 array('label' => 'Service Report', 'url' => array('/tools/Servicehistory'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10))),

                                                array('label' => 'Preventive Maintenance Log', 'url' => array('/tools/toolLog'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1 || Yii::app()->user->role == 9 || Yii::app()->user->role == 10))),
                                            )
                                    ),
                                ),
                                'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                                'htmlOptions' => array('class' => 'nav navbar-nav  navbar-right'),
                            ));
?>
                        
                    </div>
                </div>
            </div>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu" style="display:none">
                        <ul class="nav navbar-nav pull-right ">
                     
                            <li class="droddown dropdown-separator">
                                <span class="separator"></span>
                            </li>
                 
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/avatar9.jpg">
                                    <span class="username username-hide-mobile"><?php echo Yii::app()->user->name;?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
            
                                     <li class="divider">  </li>
                                    <li>
                                          <?php echo CHtml::link('My Profile',array('Users/myprofile'),array('class'=>"icon-user")); ?>
                                    </li>
                                    <li>
                                         <?php echo CHtml::link('Logout',array('site/logout'),array('class'=>"icon-key")); ?> 
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            

            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="extra_search.html" method="GET" style="display:none" >
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                            </span>
                        </div>
                    </form>
                   
               </div>
            </div>
 
        <div class="page-container">
            <!-- BEGIN PAGE HEAD -->
            <div class="page-head">
                <div class="container">
                 <?php if(Yii::app()->user->hasFlash('success')):?>
                      <br/>
                    <div class="note note-success note-bordered">
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                    <?php endif; ?>
                    <?php if(Yii::app()->user->hasFlash('error')):?>
                          <br/>
                    <div class="note note-danger note-bordered">
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
                    <?php endif; ?> 
                     </div>
            </div>
            <div class="page-content">
            
                    <div class="container">
						<?php echo $content; ?>
                     </div>
            </div>

             <div class="page-footer">
                    <div class="container">
                        <?php
						$key = 'url encryption';
						$iv = mcrypt_create_iv(
							mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
							MCRYPT_DEV_URANDOM
						);

						$encrypted = base64_encode(
							$iv .
							mcrypt_encrypt(
								MCRYPT_RIJNDAEL_128,
								hash('sha256', $key, true),
								Yii::app()->user->id,
								MCRYPT_MODE_CBC,
								$iv
							)
						);
						$encryptedmain = base64_encode(
							$iv .
							mcrypt_encrypt(
								MCRYPT_RIJNDAEL_128,
								hash('sha256', $key, true),
								Yii::app()->user->mainuser_id,
								MCRYPT_MODE_CBC,
								$iv
							)
						);
					?>

                            <?php
                            // start of switch app
                            
                            
                            
                                ?>
                            <div>
                                 
                                <!--                </div>-->
                            <?php
                        
                        // end of switch app
                        //Change user login from Direct admin user
                        if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                            ?>
                                <!--                <div>-->
                                <div class="pull-right"><span>User : </span>
                                        <?php

                                        $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                                                . "`{$tblpx}user_roles`.`role` "
                                                . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                                                . "WHERE status=0 ORDER BY user_type,full_name ASC";
                                        $result = Yii::app()->db->createCommand($sql)->queryAll();
                                        $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
                                        echo CHtml::dropDownList('shift_userid', '', $listdata, array('options' => array(Yii::app()->user->id => array('selected' => true)),
                                            'ajax' => array(
                                                'type' => 'POST', //request type
                                                'url' => CController::createUrl('/users/usershift'), //url to call.
                                                'success' => 'js:function(data){
                                        location.reload();
                                    }',
                                                'data' => array('shift_userid' => 'js:this.value'),
                                        )));
                                        ?>
                                </div>
                                    <?php //if (Yii::app()->user->mainuser_id != Yii::app()->user->id) { ?>
                                <!--span style="color:red;font-style: italic;text-decoration: blink;margin-left:20px;">(Note: You are working on other user's session)</span-->
                                    <?php //} ?>
                                    <?php
                                }
                                ?>
                        <div class="pull-right"><?php echo date('Y'); ?> &copy; <?php echo Yii::app()->name; ?>. All Rights Reserved.
                        </div>
                                </div>
                    </div>
                </div>
             
             
        <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
         
           <script>
                jQuery(document).ready(function () {
                    // initiate layout and plugins
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    Demo.init(); // init demo features

                    jQuery('.savepdf').on('click', function (e) {
                        e.preventDefault();
                        
                        var linkurl = $(this).attr('href');
                        
                        var filter_items = '';
                        $('.filters input[type=text], .filters select').each(function () {

                            colname = $('thead  tr:eq(0) th:eq(' + $(this).closest("td").index() + ')').text();

                            if ($(this).prop("tagName").toLowerCase() === 'select') {
                                //alert($(this).prop("tagName").toLowerCase());
                                if ($(this).find('option:selected').text() != '') {
                                    filter_items += "<b>"+colname + "</b> : " + $(this).find('option:selected').text() + "<br />";
                                }



                            } else {
                                if ($(this).val() != '') {
                                    filter_items += "<b>"+colname + "</b> : " + $(this).val() + "<br />";
                                }
                            }
                        });
                        
//                        if(filter_items===''){
//                            alert('No Records to generate report');
//                        }
//                        else{
                            
                           // alert(linkurl+"&filter_cr="+filter_items);
                            window.location.href=linkurl+"&filter_cr="+filter_items;
//                        }
                        
                    });
                });
            </script>
    </body>
    <!-- END BODY -->
</html>
