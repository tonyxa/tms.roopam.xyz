<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
   'jquery.js'=>false,
   'jquery.min.js'=>false, 
);
$cs->coreScriptPosition = CClientScript::POS_END;
//Yii::app()->clientScript->coreScriptPosition=CClientScript::POS_END;

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo CHtml::encode(Yii::app()->name); ?> | Tool Management System (TMS)</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/layout2.css" rel="stylesheet" type="text/css"/>
          <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/css/screen.css" rel="stylesheet" type="text/css">

        <!-- END THEME STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/favicon.png" rel="shortcut icon" type="image/x-icon"/>
        <style>
        .left-sec, .right-sec {
            height: 100vh;
            display: table-cell;
            vertical-align: middle;
            position: relative;
        }
        .page-wrapper {
            overflow: hidden;
        }
        .right-sec {
            background: linear-gradient(to bottom right, rgb(185, 31, 78), rgb(118, 50, 96));;
        }
        .form-container {
            padding: 0px;
            display: table;
            width: 100%;
        }
        .log-form {
            width: 300px;
            margin: 0 auto;
            padding: 10px;
            background: #fff;
            border-radius: 6px;
            color: #010101;
            box-shadow: 0 0 16px 4px rgba(0,0,0,0.25);
        }
        .left_logo, .login-sec {
            position: relative;
            z-index: 10;
            margin-left: auto;
            margin-right: auto;
            -webkit-transition: 1s ease;
            -moz-transition: 1s ease;
            -o-transition: 1s ease;
            -ms-transition: 1s ease;
        }
        .login-sec {
            right: -100%;
        }
        .login-sec.trans {
            right: 0;
        }
        .left_logo {
            left: -100%;
        }
        .left_logo.anim {
            left: 0;
        }
        
        .right-sec h1{color: #fff;padding-bottom:30px;font-size:2rem;}
        @media (min-width: 768px){
            .left-sec, .right-sec {
                width: 50%;
            }
        }
        @media (min-width: 992px){
            .log-form {
            width: 450px;
            padding: 30px;
            }
        }


        </style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <!-- <div class="logo" style="color:white;">
           <div id="logo">
                    <div class="logoimg">
                        <img src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/roopam_login_logo.png" border="0"  height="180">
                        
                    </div>
          </div>
        </div>
                    <h1>Tool Management System </h1> -->

        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <!-- <div class="content">
            <?php // echo $content; ?>
        </div>
        <div class="copyright">
        </div> -->



        <div class="page-wrapper form-container">
            <div class="container-fluid form-container">
                <div class="left-sec text-center hidden-xs">
                    <img class="left_logo" src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/roopam_login_logo.png" border="0">
                </div>
                <div class="right-sec">
                    <div class="login-sec">
                        <div class="hidden-sm hidden-md hidden-lg text-center">
                            <img  src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/img/roopam_login_logo.png" border="0"> 
                        </div>
                        <h1>Tool Management System </h1>
                        
                        <div class="log-form">
                        <!-- <h1>Tool Management System </h1> -->
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/respond.min.js"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
                Demo.init();

                $('.left_logo').addClass('anim');
                $('.login-sec').addClass('trans');
            });


           
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>