# TMS - ROOPAM Projects

## Do the below steps to setup project at first time

- create file called **database.php** in **/protected/config/** folder


- Add below data in it.

```php
<?php

define('_DB','DATABASE NAME');
define ('_USER', 'DATABASE USER NAME');
define ('_PASS', 'DATABASE PASSWORD');
define ('_HOST', 'HOST NAME');
```

## OR

Copy the **protected/config/database.php.dist** file to **database.php** and apply the details



# NOTE:
 - Keep your db change of each commit file inside **protected/db_changes** folder as single file. example: protected/db_changes/**db_changes_sep_25_2018.sql** or **db_changes_sep_25_2018.txt**
